/* eslint-disable @next/next/no-img-element */
/* eslint-disable jsx-a11y/alt-text */
import Table from "@mui/material/Table";
import TableBody from "@mui/material/TableBody";
import TableCell from "@mui/material/TableCell";
import TableContainer from "@mui/material/TableContainer";
import TableHead from "@mui/material/TableHead";
import TableRow from "@mui/material/TableRow";
import Paper from "@mui/material/Paper";
import FormControlLabel from "@mui/material/FormControlLabel";
import Switch from "@mui/material/Switch";
import { styled } from "@mui/material/styles";
import Link from "next/link";
import Barcode from "react-barcode";

import UserLayout from "../../../src/hoc/Layout/UserLayout/UserLayout";
import { ArrowRight, UserBonuses, Manat } from "../../../src/components/Icons";
import classes from "./index.module.scss";
import { useState } from "react";
import UserOrdersItems from "../../../src/components/UserOrdersItems/UserOrdersItems";
import UserBonusesTable from "../../../src/components/UserBonusesTable/UserBonusesTable";

const IOSSwitch = styled((props) => (
  <Switch focusVisibleClassName=".Mui-focusVisible" disableRipple {...props} />
))(({ theme }) => ({
  width: 30,
  height: 16,
  padding: 0,
  "& .MuiSwitch-switchBase": {
    padding: 0,
    margin: 3,
    transitionDuration: "300ms",
    "&.Mui-checked": {
      transform: "translateX(14px)",
      color: "#fff",
      "& + .MuiSwitch-track": {
        backgroundColor: "#ea2427",
        opacity: 1,
        border: 0,
      },
      "&.Mui-disabled + .MuiSwitch-track": {
        opacity: 0.5,
      },
    },
    "&.Mui-focusVisible .MuiSwitch-thumb": {
      color: "#33cf4d",
      border: "6px solid #fff",
    },
    "&.Mui-disabled .MuiSwitch-thumb": {
      color: theme.palette.grey[100],
    },
    "&.Mui-disabled + .MuiSwitch-track": {
      opacity: 0.7,
    },
  },
  "& .MuiSwitch-thumb": {
    boxSizing: "border-box",
    width: 10,
    height: 10,
  },
  "& .MuiSwitch-track": {
    borderRadius: 26 / 2,
    backgroundColor: "#7a7a7a",
    opacity: 1,
    transition: theme.transitions.create(["background-color"], {
      duration: 500,
    }),
  },
}));
const UserTable = styled((props) => <Table {...props} />)(({ theme }) => ({
  "& .MuiTableHead-root": {
    borderBottom: "1px solid rgba(224, 224, 224, 1)",
    "& .MuiTableCell-head": {
      color: "var(--text-primary)",
      fontFamily: "Poppins",
      opacity: "0.5",
      fontSize: "10px",
      padding: "10px",
      textAlign: "left",
    },
  },
  "& .MuiTableBody-root": {
    "& .MuiTableRow-root": {
      "&:nth-of-type(odd)": {
        backgroundColor: theme.palette.action.hover,
      },
      "& .MuiTableCell-body": {
        color: "var(--text-primary)",
        fontFamily: "Poppins",
        borderBottom: "0",
        fontSize: "14px",
        padding: "10px",
        "&:first-child": {
          fontWeight: "600",
          fontSize: "14px",
          "& img": {
            height: "40px",
            width: "40px",
            marginRight: "16px",
          },
        },
        "&:last-child": {
          color: "#00AC07",
          fontWeight: "700",
        },
      },
      "&:last-child": {
        borderBottom: "1px solid rgba(224, 224, 224, 1)",
      },
    },
  },
}));

export default function Index() {
  const [ActiveStatus, setActiveStatus] = useState(true);

  return (
    <UserLayout>
      {/* bonuses top */}
      <div className={classes.Bonuses}>
        {/* bonuses top */}
        <div className={classes.BonusesTop}>
          <h1 className={classes.BonusesTopHeader}>Bonuslarım</h1>
          <p>Bonus kartı</p>
          <div className={classes.BonusesTopBtnGroup}>
            {/*first btn */}
            <div className={classes.LostCard}>
              <Link href="/user-profile/orders">Kartımı itirmişəm</Link>
            </div>
            {/*second btn */}
            <div className={classes.ActiveStatus}>
              <FormControlLabel
                sx={{
                  m: 0,
                  display: "flex",
                  alignItems: "center",
                  justifyContent: "center",
                  gap: "10px",
                }}
                label="Aktiv"
                control={
                  <IOSSwitch
                    defaultChecked
                    onClick={() => setActiveStatus(!ActiveStatus)}
                  />
                }
              />
            </div>
          </div>
        </div>
        {/* bonuses card */}
        <div className={classes.BonusesCard}>
          <div className={classes.BonusesCardInfo}>
            <div className={classes.BonusesCardInfoTop}>
              <div className={classes.BonusesCardInfoTopPrice}>
                <span className={classes.PriceNumber}>
                  <span className={classes.PriceNumberWhole}>209</span>
                  <span className={classes.PriceNumberDecimal}>
                    .88 <Manat />
                  </span>
                </span>
                <span className={classes.Bonus}>Bonus</span>
              </div>
              <div className={classes.BonusesCardInfoTopIcon}>
                <UserBonuses />
              </div>
            </div>
            <div className={classes.BonusesCardInfoLink}>
              <span className={classes.BonusesCardInfoLinkText}>
                Necə xərcləyə bilərəm
              </span>
              <span className={classes.BonusesCardInfoLinkIcon}>
                <Link href="/user-profile/orders">
                  <a>
                    <ArrowRight />
                  </a>
                </Link>
              </span>
            </div>
          </div>
          <div className={classes.BonusesCardImg}>
            <img src="/img/user-bonus-card.png" alt="" />
            <div className={classes.BonusesCardImgContent}>
              {/* card top text */}
              <div className={classes.BonusesCardImgContentTop}>
                <span>Bonus kartım</span>
                <span>PİN kod: 2908</span>
              </div>
              {/* barcode wrapper*/}
              <div className={classes.BonusesCardImgContentBarcode}>
                {/* qr code component */}
                <Barcode
                  value="8896 8896 5669 4663"
                  width={2}
                  height={50}
                  fontSize={40}
                  lineColor="black"
                />
              </div>
            </div>
          </div>
        </div>
        {/* bonuses data table */}
        <div className={classes.BonusesTable}>
          <UserBonusesTable />
        </div>

        <div className={classes.BonusesItems}>
          <UserOrdersItems />
        </div>
      </div>
    </UserLayout>
  );
}
