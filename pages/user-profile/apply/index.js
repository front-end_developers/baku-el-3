import styled from "@emotion/styled";
import { Button, MenuItem, Select, TextField } from "@mui/material";
import { useFormik } from "formik";
import { useState } from "react";
import * as Yup from "yup";
import { AngleDown } from "../../../src/components/Icons";
import MobileBottomNavbar from "../../../src/components/MobileBottomNavbar/MobileBottomNavbar";
import UserLayout from "../../../src/hoc/Layout/UserLayout/UserLayout";
import classes from "./index.module.scss";

function Index() {
  const [age, setAge] = useState("10");

  const handleChange = (event) => {
    setAge(event.target.value);
  };

  const initialValues = {
    type: "",
  };
  const CreateSchema = Yup.object().shape({
    type: Yup.string(),
  });
  const resetForm = () => {
    // onHide();
    formik.resetForm();
  };
  const formik = useFormik({
    initialValues,
    validationSchema: CreateSchema,
    onSubmit: (values, { resetForm, setSubmitting }) => {
      // onPostGroup(values, resetForm, setSubmitting);
    },
  });

  const StyledTextField = styled((props) => <TextField {...props} />)({
    "& .MuiOutlinedInput-input": {
      padding: "10px 24px",
      color: "red",
      fontFamily: "Poppins",
      color: "var(--text-primary)",
      fontSize: "14px",
    },

    ".MuiOutlinedInput-notchedOutline": {
      top: 0,
    },

    "& fieldset": {
      width: "100%",
      border: "none",
      outline: "1px solid #b3b3b3",
      borderRadius: "16px",
    },
    "& label": {
      fontFamily: "Poppins",
      color: "var(--text-primary)",
      backgroundColor: "var(--user-sidebar-main)",
      paddingBlock: "2px",
      paddingInline: "10px",
      marginTop: "-8px",
      fontSize: "14px",
      color: "var(--text-primary) !important",
    },
  });

  const StyledApplySubmitBtn = styled((props) => <Button {...props} />)({
    borderRadius: "12px",
    border: 0,
    padding: " 10px 20px",
    backgroundColor: " var(--text-primary)",
    color: "var(--f-to-0)",
    textTransform: "inherit",
    fontFamily: "Poppins",

    "&:hover": {
      backgroundColor: "var(--text-primary)",
    },
  });

  const StyledResetBtn = styled((props) => <Button {...props} />)({
    borderRadius: "12px",
    border: 0,
    padding: " 10px 20px",
    color: "var(--text-primary)",
    background: "var(--navbar-bg-main)",
    textTransform: "inherit",
    fontFamily: "Poppins",

    "&:hover": {
      background: "var(--navbar-bg-main)",
    },
  });

  const getInputClasses = (filedName) => {
    if (formik.touched[filedName] && formik.errors[filedName]) {
      return true;
    }
    if (formik.touched[filedName] && !formik.errors[filedName]) {
      return false;
    }
    return false;
  };

  const StyledSelect = styled((props) => <Select {...props} />)(
    ({ theme }) => ({
      width: "100%",
      border: "1px solid #b3b3b3",
      borderRadius: "16px",
      paddingInline: "24px",

      "& .MuiSelect-select": {
        fontWeight: "500",
        fontSize: "14px",
        padding: "10px 0",
        fontFamily: "Poppins",
        color: "var(--text-primary)",
      },

      "& svg": {
        width: "10px",
        color: "var(--red-main)",
        transition: "0.2s",
      },
      "& [aria-expanded='true']~svg": {
        transform: "rotate(-180deg)",
      },
      "& fieldset": {
        display: "none",
      },

      "& .MuiInputAdornment-root": {
        "& svg": {
          marginTop: "3px",
          color: "yellow",
          height: "18px",
          width: "18px",
        },
      },
    })
  );

  return (
    <UserLayout>
      <div className={`${classes.Apply} Apply`}>
        <div className={classes.ApplyTop}>
          <h1 className={classes.ApplyTopHeader}>Müraciət</h1>
        </div>
        <div className={classes.ApplyForm}>
          <div className={`row ${classes.ApplyFormBody}`}>
            <div
              className={`col-12 col-sm-6 col-md-6 col-lg-6  ${classes.ApplyFormBodyGroup}`}
            >
              <StyledSelect
                value={age}
                onChange={handleChange}
                IconComponent={() => <AngleDown />}
                label=""
                // placeholder="SELECT"
                // labelId="demo-simple-select-label-ASLAN"
                id="demo-simple-select"
              >
                <MenuItem value={10}>Müraciət növü</MenuItem>
                <MenuItem value={20}>Twenty</MenuItem>
                <MenuItem value={30}>Thirty</MenuItem>
              </StyledSelect>

              {formik.touched.type && formik.errors.type ? (
                <div className="fv-plugins-message-container">
                  <div className="fv-help-block">{formik.errors.type}</div>
                </div>
              ) : null}
            </div>
            <div
              className={`col-12 col-sm-6 col-md-6 col-lg-6  ${classes.ApplyFormBodyGroup}`}
            >
              <StyledTextField
                variant="outlined"
                id="Type"
                // name="Type"
                label="Ad, soyad"
                placeholder="Ad, soyad"
                fullWidth
                // error={getInputClasses("type")}
                {...formik.getFieldProps("type")}
                autoComplete="off"
              />
              {formik.touched.type && formik.errors.type ? (
                <div className="fv-plugins-message-container">
                  <div className="fv-help-block">{formik.errors.type}</div>
                </div>
              ) : null}
            </div>
            <div
              className={`col-12 col-sm-6 col-md-6 col-lg-6  ${classes.ApplyFormBodyGroup}`}
            >
              <StyledTextField
                variant="outlined"
                id="Type"
                // name="Type"
                label="Telefon nömrəsi"
                placeholder="Telefon nömrəsi"
                fullWidth
                autoComplete="off"
                // error={getInputClasses("type")}
                {...formik.getFieldProps("type")}
              />
              {formik.touched.type && formik.errors.type ? (
                <div className="fv-plugins-message-container">
                  <div className="fv-help-block">{formik.errors.type}</div>
                </div>
              ) : null}
            </div>
            <div
              className={`col-12 col-sm-6 col-md-6 col-lg-6  ${classes.ApplyFormBodyGroup}`}
            >
              <StyledTextField
                variant="outlined"
                id="Type"
                // name="Type"
                label="E-poçt"
                placeholder="E-poçt"
                fullWidth
                autoComplete="off"
                // error={getInputClasses("type")}
                {...formik.getFieldProps("type")}
              />
              {formik.touched.type && formik.errors.type ? (
                <div className="fv-plugins-message-container">
                  <div className="fv-help-block">{formik.errors.type}</div>
                </div>
              ) : null}
            </div>
            <div
              className={`col-12 col-md-12 col-lg-12  ${classes.ApplyFormBodyGroup}`}
            >
              <StyledTextField
                variant="outlined"
                id="Type"
                // name="Type"
                label="Müraciətiniz"
                placeholder="Müraciətiniz..."
                fullWidth
                autoComplete="off"
                // error={getInputClasses("type")}
                {...formik.getFieldProps("type")}
                multiline
                sx={{
                  height: "100px",

                  "& .MuiOutlinedInput-root": {
                    height: "100%",
                    alignItems: "flex-start",
                  },
                }}
              />
              {formik.touched.type && formik.errors.type ? (
                <div className="fv-plugins-message-container">
                  <div className="fv-help-block">{formik.errors.type}</div>
                </div>
              ) : null}
            </div>
          </div>
          <div className={classes.ApplyFormSubmit}>
            <StyledApplySubmitBtn
              type="submit"
              className={classes.ApplyFormSubmitBtn}
            >
              Göndər
            </StyledApplySubmitBtn>
            <StyledResetBtn className={classes.ApplyFormSubmitResetBtn}>
              Ləğv et
            </StyledResetBtn>
          </div>
        </div>
      </div>
      {/* <MobileBottomNavbar/> */}
    </UserLayout>
  );
}

export default Index;
