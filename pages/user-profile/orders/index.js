/* eslint-disable @next/next/no-img-element */
import { connect } from "react-redux";
import UserLayout from "../../../src/hoc/Layout/UserLayout/UserLayout";
import { styled } from "@mui/material/styles";
import Box from "@mui/material/Box";
import Tab, { tabClasses } from "@mui/material/Tab";
import Tabs, { tabsClasses } from "@mui/material/Tabs";
import TabPanel, { tabPanelClasses } from "@mui/lab/TabPanel";
import UserOrdersTable from "../../../src/components/UserOrdersTable/UserOrdersTable";
import UserOrdersItems from "../../../src/components/UserOrdersItems/UserOrdersItems";
import Typography from "@mui/material/Typography";
// import { User } from "../../../src/components/Icons";
import classes from "./index.module.scss";
import { useState } from "react";
import CustomPagination from "../../../src/components/Pagination/Pagination";
import { Pagination, PaginationItem, Stack } from "@mui/material";
import { ArrowLeft, ArrowRight } from "../../../src/components/Icons";

const StyledTab = styled(Tab)(({ theme }) => ({
  // [`&.${tabClasses.root}`]: {
  display: "flex",
  flexDirection: "row-reverse",
  gap: "15px",
  minHeight: "unset",
  minWidth: "unset",
  fontFamily: "unset",
  padding: " 3px 3px 3px 18px",
  borderRadius: "35px",
  backgroundColor: "var(--user-tab-btn-bg)",
  fontWeight: "600",
  fontSize: "12px",
  textTransform: "unset",
  color: "var(--text-primary)",
  // border: "1px solid var(--text-primary)",

  "&.Mui-selected": {
    backgroundColor: "var(--user-tab-btn-bg-selected)",
    color: "#ffffff",
    border: "1px solid gray",
  },
}));

function TabCircle(props) {
  const { num } = props;
  return (
    <div className={`${classes.TabCircle} TabCircle`}>
      <span className={classes.TabCircleContent}>{num}</span>
    </div>
  );
}

const StyledPagination = styled((props) => <Pagination {...props} />)({
  position: "absolute",
  width: "100%",
  bottom: "0",
  // "& .MuiPagination-root": {
  "& .MuiPagination-ul": {
    "& li": {
      "& .MuiButtonBase-root": {
        background: "var(--pagination-bg)",
        margin: "unset",
        padding: "unset",
        fontWeight: "700",
        fontSize: "12px",
        lineHeight: "13px",
        borderRadius: "unset",
        color: "var(--text-primary)",

        "& svg": {
          width: "6px",
          height: "9px",
          color: "var(--text-primary)",
        },

        "&:hover": {
          background: "rgba(234, 36, 39)",
          color: "#fff",

          "& svg": {
            color: "#FFF",
          },
        },

        "& .Mui-selected": {
          color: "#ea2427",

          "&:hover": {
            color: "#fff",
          },
        },

        "&:first-child": {
          "& button": {
            margin: "unset",
            marginRight: "10px",
            padding: "unset",
            borderRadius: "12px",
          },
        },

        "&:last-child": {
          "& button": {
            margin: "unset",
            marginLeft: "10px",
            padding: "unset",
            borderRadius: "12px",
          },
        },

        "&:nth-child(2)": {
          "& button": {
            borderRadius: "12px 0 0 12px",
          },
        },

        "&:nth-last-child(2)": {
          "& button": {
            borderRadius: "0 12px 12px 0",
          },
        },
      },
    },
  },
  // },
});

export default function Index(props) {
  const [value, setValue] = useState(0);

  const handleChange = (event, newValue) => {
    setValue(newValue);
  };

  function TabPanel(props) {
    const { children, value, index, ...other } = props;

    return (
      <div
        role="tabpanel"
        hidden={value !== index}
        id={`simple-tabpanel-${index}`}
        aria-labelledby={`simple-tab-${index}`}
        {...other}
      >
        {value === index && <Box sx={{ p: 3 }}>{children}</Box>}
      </div>
    );
  }

  function a11yProps(index) {
    return {
      id: `simple-tab-${index}`,
      "aria-controls": `simple-tabpanel-${index}`,
    };
  }

  return (
    <UserLayout>
      <div className={`${classes.Orders} Orders`}>
        <div className={classes.OrdersContent}>
          <h1 className={classes.OrdersHeader}>Sifarislerim</h1>
          <Tabs
            value={value}
            onChange={handleChange}
            aria-label="basic tabs example"
          >
            <StyledTab
              label="Hamısı"
              icon={<TabCircle num={20} iconPosition="end" />}
              {...a11yProps(0)}
            />
            <StyledTab
              label="Baxılır"
              icon={<TabCircle num={0} iconPosition="end" />}
              {...a11yProps(1)}
            />
            <StyledTab
              label="Təsdiqlənib"
              icon={<TabCircle num={3} iconPosition="end" />}
              {...a11yProps(2)}
            />
            <StyledTab
              label="İmtina"
              icon={<TabCircle num={1} iconPosition="end" />}
              {...a11yProps(2)}
            />
            <StyledTab
              label="Məhsul gözlənilir"
              icon={<TabCircle num={2} iconPosition="end" />}
              {...a11yProps(2)}
            />
            <StyledTab
              label="Müştəri fikirləşir"
              icon={<TabCircle num={0} iconPosition="end" />}
              {...a11yProps(2)}
            />
          </Tabs>
          <TabPanel value={value} index={0}>
            <UserOrdersTable />
          </TabPanel>
          <TabPanel value={value} index={1}>
            Item Two
            <UserOrdersTable />
          </TabPanel>
          <TabPanel value={value} index={2}>
            Item Three
            <UserOrdersTable />
          </TabPanel>
          <TabPanel value={value} index={3}>
            Item Four
            <UserOrdersTable />
          </TabPanel>
          <TabPanel value={value} index={4}>
            Item Five
            <UserOrdersTable />
          </TabPanel>
          <TabPanel value={value} index={5}>
            Item Six
            <UserOrdersTable />
          </TabPanel>
          <CustomPagination />
          {/* <Stack spacing={2}> */}
          {/* <StyledPagination
            count={4}
            renderItem={(item) => (
              <PaginationItem
                components={{ previous: ArrowLeft, next: ArrowRight }}
                {...item}
              />
            )}
          /> */}
          {/* </Stack> */}
        </div>
      </div>
      <div className={`${classes.OrdersMobile} OrdersMobile`}>
        <div className={classes.OrdersMobileContent}>
          <h1 className={classes.OrdersMobileContentHeader}>Sifarislerim</h1>
          <p>1200 Məhsul</p>
          <Tabs
            value={value}
            onChange={handleChange}
            aria-label="basic tabs example"
          >
            <StyledTab
              label="Hamısı"
              icon={<TabCircle num={20} iconPosition="end" />}
              {...a11yProps(0)}
            />
            <StyledTab
              label="Baxılır"
              icon={<TabCircle num={0} iconPosition="end" />}
              {...a11yProps(1)}
            />
            <StyledTab
              label="Təsdiqlənib"
              icon={<TabCircle num={3} iconPosition="end" />}
              {...a11yProps(2)}
            />
            <StyledTab
              label="İmtina"
              icon={<TabCircle num={1} iconPosition="end" />}
              {...a11yProps(2)}
            />
            <StyledTab
              label="Məhsul gözlənilir"
              icon={<TabCircle num={2} iconPosition="end" />}
              {...a11yProps(2)}
            />
            <StyledTab
              label="Müştəri fikirləşir"
              icon={<TabCircle num={0} iconPosition="end" />}
              {...a11yProps(2)}
            />
          </Tabs>
          <TabPanel value={value} index={0}>
            <UserOrdersItems />
          </TabPanel>
          <TabPanel value={value} index={1}>
            Item Two
            <UserOrdersItems />
          </TabPanel>
          <TabPanel value={value} index={2}>
            Item Three
            <UserOrdersItems />
          </TabPanel>
          <TabPanel value={value} index={3}>
            Item Four
            <UserOrdersItems />
          </TabPanel>
          <TabPanel value={value} index={4}>
            Item Five
            <UserOrdersItems />
          </TabPanel>
          <TabPanel value={value} index={5}>
            Item Six
            <UserOrdersItems />
          </TabPanel>
          <CustomPagination />
          {/* <Stack spacing={2}> */}
          {/* <StyledPagination
            count={4}
            renderItem={(item) => (
              <PaginationItem
                components={{ previous: ArrowLeft, next: ArrowRight }}
                {...item}
              />
            )}
          /> */}
          {/* </Stack> */}
        </div>
      </div>
    </UserLayout>
  );
}
