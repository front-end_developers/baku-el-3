import React, { useState } from "react";
import UserLayout from "../../../src/hoc/Layout/UserLayout/UserLayout";
import { connect } from "react-redux";
import CustomPagination from "../../../src/components/Pagination/Pagination";
import classes from "./index.module.scss";
import UserCard from "../../../src/components/UserCard/UserCard";
import ProductCard from "../../../src/components/ProductCard/ProductCard";
import ActiveLink from "../../../src/components/Link/Link";
import { Button, Drawer } from "@mui/material";
import UserProfileSidebar from "../../../src/components/UserProfileDrawer/UserProfileDrawer";
import { CloseIcon } from "../../../src/components/Icons";
import { makeStyles } from "@mui/styles";

function Latest(props) {
  const { theme } = props;
  return (
    <div data-theme={theme}>
      <UserLayout>
        <div className="Latest">
          <div className={classes.LatestContent}>
            <h1 className={classes.LatestHeader}> Ən son baxdıqlarım</h1>
            <div className={classes.LatestBody}>
              <div className={classes.LatestBodyBox}>
                <UserCard />
              </div>
              <div className={classes.LatestBodyBox}>
                <UserCard />
              </div>
              <div className={classes.LatestBodyBoxMobile}>
                <div className={classes.LatestBodyBoxMobileTitle}>
                  <div className={classes.TitleLeft}>
                    <p>Ən son baxdıqlarım</p>
                    <span>1200 Məhsul</span>
                  </div>
                </div>
                <div className={classes.LatestBodyBoxMobileProducts}>
                  <div className={classes.LatestBodyBoxMobileProductsContainer}>
                    <div
                      className={
                        classes.LatestBodyBoxMobileProductsContainerItem
                      }
                    >
                      <ActiveLink href="product-details/1">
                        <a>
                          <ProductCard />
                        </a>
                      </ActiveLink>
                    </div>
                    <div
                      className={
                        classes.LatestBodyBoxMobileProductsContainerItem
                      }
                    >
                      <ActiveLink href="product-details/1">
                        <a>
                          <ProductCard />
                        </a>
                      </ActiveLink>
                    </div>
                    <div
                      className={
                        classes.LatestBodyBoxMobileProductsContainerItem
                      }
                    >
                      <ActiveLink href="product-details/1">
                        <a>
                          <ProductCard />
                        </a>
                      </ActiveLink>
                    </div>
                    <div
                      className={
                        classes.LatestBodyBoxMobileProductsContainerItem
                      }
                    >
                      <ActiveLink href="product-details/1">
                        <a>
                          <ProductCard />
                        </a>
                      </ActiveLink>
                    </div>
                    <div
                      className={
                        classes.LatestBodyBoxMobileProductsContainerItem
                      }
                    >
                      <ActiveLink href="product-details/1">
                        <a>
                          <ProductCard />
                        </a>
                      </ActiveLink>
                    </div>
                    <div
                      className={
                        classes.LatestBodyBoxMobileProductsContainerItem
                      }
                    >
                      <ActiveLink href="product-details/1">
                        <a>
                          <ProductCard />
                        </a>
                      </ActiveLink>
                    </div>
                    <div
                      className={
                        classes.LatestBodyBoxMobileProductsContainerItem
                      }
                    >
                      <ActiveLink href="product-details/1">
                        <a>
                          <ProductCard />
                        </a>
                      </ActiveLink>
                    </div>
                    <div
                      className={
                        classes.LatestBodyBoxMobileProductsContainerItem
                      }
                    >
                      <ActiveLink href="product-details/1">
                        <a>
                          <ProductCard />
                        </a>
                      </ActiveLink>
                    </div>
                    <div
                      className={
                        classes.LatestBodyBoxMobileProductsContainerItem
                      }
                    >
                      <ActiveLink href="product-details/1">
                        <a>
                          <ProductCard />
                        </a>
                      </ActiveLink>
                    </div>
                    <div
                      className={
                        classes.LatestBodyBoxMobileProductsContainerItem
                      }
                    >
                      <ActiveLink href="product-details/1">
                        <a>
                          <ProductCard />
                        </a>
                      </ActiveLink>
                    </div>
                    <div
                      className={
                        classes.LatestBodyBoxMobileProductsContainerItem
                      }
                    >
                      <ActiveLink href="product-details/1">
                        <a>
                          <ProductCard />
                        </a>
                      </ActiveLink>
                    </div>
                    <div
                      className={
                        classes.LatestBodyBoxMobileProductsContainerItem
                      }
                    >
                      <ActiveLink href="product-details/1">
                        <a>
                          <ProductCard />
                        </a>
                      </ActiveLink>
                    </div>
                  </div>
                </div>
              </div>
            </div>

            <CustomPagination />
          </div>
        </div>
      </UserLayout>
    </div>
  );
}
const mapStateToProps = (state) => {
  return { theme: state.layout.theme };
};

// const mapDispatchToProps = {};

export default connect(mapStateToProps, null)(Latest);
