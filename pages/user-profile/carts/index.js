import UserLayout from "../../../src/hoc/Layout/UserLayout/UserLayout";
import ActiveLink from "../../../src/components/Link/Link";
import { Edit, Delete } from "../../../src/components/Icons";
import CustomPagination from "../../../src/components/Pagination/Pagination";
import Button from "@mui/material/Button";
import { connect } from "react-redux";
import classes from "./index.module.scss";
import { styled } from "@mui/material/styles";
// import Barcode from "react-barcode";

const StyledSubmitBtn = styled((props) => <Button {...props} />)({
  borderRadius: "12px",
  border: 0,
  padding: " 10px 20px",
  backgroundColor: " var(--text-primary)",
  color: "var(--f-to-0)",
  textTransform: "inherit",
  fontFamily: "Poppins",

  "&:hover": {
    backgroundColor: "var(--text-primary)",
  },
});

export default function carts(props) {
  return (
    <UserLayout>
      <div className="Carts">
        {/* <Barcode value="14545687568" /> */}
        <div className={classes.CartsContent}>
          <div className={classes.CartsContentTop}>
            <h1 className={classes.CartsContentTopHeader}>Kartlarım</h1>
            <ActiveLink href="/user-profile/carts/add-cart">
              <StyledSubmitBtn
                variant="contained"
                disableElevation
                className={classes.CartsContentTopAdd}
              >
                + Əlavə et
              </StyledSubmitBtn>
            </ActiveLink>
          </div>
          <div className={classes.CartsContentBody}>
            <div className={classes.CartsContentBodyItem}>
              <div className={classes.CartsContentBodyItemImgGroup}>
                <div>
                  <img src="/img/mastercard-2.png" />
                </div>
                <span>**** 8249</span>
              </div>
              <div className={classes.CartsContentBodyItemButtons}>
                <div>
                  <span>Bitmə vaxtı: </span>
                  <span> 10/2024</span>
                </div>
                <div>
                  <button>
                    <Edit />
                  </button>
                  <button>
                    <Delete />
                  </button>
                </div>
              </div>
            </div>
            <div className={classes.CartsContentBodyItem}>
              <div className={classes.CartsContentBodyItemImgGroup}>
                <div>
                  <img src="/img/visa-2.png" />
                </div>
                <span>**** 8249</span>
              </div>
              <div className={classes.CartsContentBodyItemButtons}>
                <div>
                  <span>Bitmə vaxtı: </span>
                  <span> 10/2024</span>
                </div>
                <div>
                  <button>
                    <Edit />
                  </button>
                  <button>
                    <Delete />
                  </button>
                </div>
              </div>
            </div>
          </div>
          <CustomPagination />
        </div>
      </div>
    </UserLayout>
    // </div>
  );
}
