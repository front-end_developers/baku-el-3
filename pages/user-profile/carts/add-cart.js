/* eslint-disable @next/next/no-img-element */
import UserLayout from "../../../src/hoc/Layout/UserLayout/UserLayout";
import ActiveLink from "../../../src/components/Link/Link";
import { ArrowBack } from "@mui/icons-material";
import Button from "@mui/material/Button";
import TextField from "@mui/material/TextField";
import { useFormik } from "formik";
import * as Yup from "yup";
import classes from "./add-cart.module.scss";
import { useState } from "react";
import { styled } from "@mui/material/styles";

export default function AddCart() {
  const [showBack, setShowBack] = useState();

  const initialValues = {
    type: "",
  };
  const CreateSchema = Yup.object().shape({
    type: Yup.string(),
  });
  const resetForm = () => {
    // onHide();
    formik.resetForm();
  };
  const formik = useFormik({
    initialValues,
    validationSchema: CreateSchema,
    onSubmit: (values, { resetForm, setSubmitting }) => {
      // onPostGroup(values, resetForm, setSubmitting);
    },
  });

  const textFieldStyles = {
    "& .MuiOutlinedInput-root": {
      "& .MuiOutlinedInput-input": {
        padding: "10px 24px",
        color: "red",
        fontFamily: "Poppins",
        color: "var(--text-primary)",
        fontSize: "14px",
        "&:hover": {
          "& + fieldset": {
            borderColor: "var(--black-to-white)",
          },
        },
      },
      "& fieldset.MuiOutlinedInput-notchedOutline": {
        width: "100%",
        // top: "35px",
        borderRadius: "4px",
        // border: "1px solid red"
        // border: "none",
        // outline: "1px solid #b3b3b3",
        borderColor: "var(--black-to-white)",
        borderRadius: "16px",
      },
      "&.Mui-error": {
        "& .MuiOutlinedInput-input": {
          "&:hover": {
            "& + fieldset": {
              borderColor: "var(--red-main)!important",
            },
          },
        },
        "& fieldset.MuiOutlinedInput-notchedOutline": {
          borderColor: "var(--red-main)!important",
        },
      },
      "&.Mui-focused:not(.Mui-error)": {
        "& fieldset.MuiOutlinedInput-notchedOutline": {
          borderColor: "var(--black-to-white)!important",
        },
      },
    },
    "& label": {
      fontFamily: "Poppins",
      color: "var(--text-primary)",
      backgroundColor: "var(--user-sidebar-main)",
      paddingBlock: "2px",
      paddingInline: "10px",
      marginTop: "-8px",
      fontSize: "14px",
      color: "var(--text-primary) !important",
    },
  };

  const StyledTextField = styled((props) => <TextField {...props} />)(
    textFieldStyles
  );

  const StyledSubmitBtn = styled((props) => <Button {...props} />)({
    borderRadius: "12px",
    border: 0,
    padding: " 10px 20px",
    backgroundColor: " var(--text-primary)",
    color: "var(--f-to-0)",
    textTransform: "inherit",
    fontFamily: "Poppins",

    "&:hover": {
      backgroundColor: "var(--text-primary)",
    },
  });

  const StyledResetBtn = styled((props) => <Button {...props} />)({
    borderRadius: "12px",
    border: 0,
    padding: " 10px 20px",
    color: "var(--text-primary)",
    background: "var(--navbar-bg-main)",
    textTransform: "inherit",
    fontFamily: "Poppins",

    "&:hover": {
      background: "var(--navbar-bg-main)",
    },
  });

  const getInputClasses = (filedName) => {
    if (formik.touched[filedName] && formik.errors[filedName]) {
      return true;
    }
    if (formik.touched[filedName] && !formik.errors[filedName]) {
      return false;
    }
    return false;
  };
  // formik.getFieldProps("type").onBlur = () => {console.log('not focused')}
  // console.log(formik.getFieldProps("type"));

  return (
    <UserLayout>
      <div className={`${classes.AddCart} AddCart`}>
        <div className={classes.AddCartTop}>
          <ActiveLink href="/user-profile/carts">
            <a>
              <ArrowBack />
            </a>
          </ActiveLink>
          <h1 className={classes.AddCartHeader}>Kart Əlavə Et</h1>
        </div>
        <form className={classes.AddCartForm} onSubmit={formik.handleSubmit}>
          <div className={`row ${classes.AddCartFormBody}`}>
            <div
              className={`col-12 col-md-12 col-lg-12  ${classes.AddCartFormBodyGroup}`}
            >
              <StyledTextField
                variant="outlined"
                id="Type"
                // name="Type"
                label="Kart nömrəsi"
                fullWidth
                placeholder="Kart nömrəsi"
                autoComplete="off"
                // error={getInputClasses("type")}
                // {...formik.getFieldProps("type")}
              />
              {formik.touched.type && formik.errors.type ? (
                <div className="fv-plugins-message-container">
                  <div className="fv-help-block">{formik.errors.type}</div>
                </div>
              ) : null}
            </div>
            <div
              className={`col-12 col-md-12 col-lg-12  ${classes.AddCartFormBodyGroup}`}
            >
              <StyledTextField
                key="hey"
                variant="outlined"
                id="Type"
                // name="Type"
                label="Kartın üzərindəki ad və soyad"
                fullWidth
                placeholder="Kartın üzərindəki ad və soyad"
                autoComplete="off"
                // className="w-100 mg-top-3"
                // error={getInputClasses("type")}
                // {...formik.getFieldProps("type")}
              />
              {formik.touched.type && formik.errors.type ? (
                <div className="fv-plugins-message-container">
                  <div className="fv-help-block">{formik.errors.type}</div>
                </div>
              ) : null}
            </div>
            <div
              className={`col-6 col-sm-6 col-md-6 col-lg-6 ${classes.AddCartFormBodyGroup}`}
            >
              <StyledTextField
                key="hey"
                variant="outlined"
                id="Type"
                // name="Type"
                label="MM/YY"
                fullWidth
                placeholder="MM/YY"
                autoComplete="off"
                // className="w-100 mg-top-3"
                // error={getInputClasses("type")}
                // {...formik.getFieldProps("type")}
              />
              {formik.touched.type && formik.errors.type ? (
                <div className="fv-plugins-message-container">
                  <div className="fv-help-block">{formik.errors.type}</div>
                </div>
              ) : null}
            </div>
            <div
              className={`col-6 col-sm-6 col-md-6 col-lg-6 ${classes.AddCartFormBodyGroup}`}
            >
              <TextField
                key="hey"
                // variant="outlined"
                id="Type"
                // name="Type"
                label="CVV/CVC"
                fullWidth
                autoComplete="off"
                placeholder="CVV/CVC"
                error={true}
                // {...formik.getFieldProps("type")}
                onFocus={(e) => {
                  // formik.handleBlur(e);
                  setShowBack(true);
                }}
                onBlur={(e) => {
                  formik.handleBlur(e);
                  setShowBack(false);
                }}
                sx={textFieldStyles}
                // className="w-100 mg-top-3"
                // error={getInputClasses("type")}
                // error={true}
              />
              {formik.touched.type && formik.errors.type ? (
                <div className="fv-plugins-message-container">
                  <div className="fv-help-block">
                    {/* error */}
                    {formik.errors.type}
                  </div>
                </div>
              ) : null}
            </div>

            {/* {isError && (
              <div className="fv-plugins-message-container">
                <div className="fv-help-block MuiFormLabel-root Mui-error">
                  {isError}
                </div>
              </div>
            )} */}
          </div>
          <div className={classes.AddCartFormImg}>
            <div
              className={`${classes.FlipCard} ${
                showBack ? classes.ShowBack : ""
                // true ? classes.ShowBack : ""
              }`}
            >
              <div className={classes.FlipCardInner}>
                <div className={classes.FlipCardFront}>
                  <img
                    className={classes.FlipCardFrontImg}
                    src="/img/user-card.png"
                    alt=""
                  />
                  <div className={classes.FlipCardFrontBody}>
                    {/* card number && name && validity */}
                    <div>
                      {/* card number */}
                      <div className={classes.FlipCardFrontBodyNumber}>
                        0000 0000 0000 0000
                      </div>

                      {/* name && validity*/}
                      <div className={classes.FlipCardFrontBodyNameValidity}>
                        {/* name */}
                        <span>Ad və Soyad</span>
                        {/* validity */}
                        <span>04/24</span>
                      </div>
                    </div>
                    {/* card type logo */}
                    <div>Visa</div>
                  </div>
                </div>
                <div className={classes.FlipCardBack}>
                  <img
                    className={classes.FlipCardBackImg}
                    src="/img/user-card-back.png"
                    alt=""
                  />
                  <div className={classes.FlipCardBackBody}>
                    <div className={classes.FlipCardBackBodyNameField}>
                      Ad Soyad
                    </div>
                    <div className={classes.FlipCardBackBodyCVV}>355</div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div className={classes.AddCartFormSubmit}>
            <StyledSubmitBtn
              className={classes.AddCartFormSubmitBtn}
              type="submit"
            >
              Yadda Saxla
            </StyledSubmitBtn>
            <StyledResetBtn
              className={classes.AddCartFormSubmitResetBtn}
              variant="light"
              onClick={() => {
                // onHide(false);
                formik.resetForm();
              }}
            >
              Ləğv et
            </StyledResetBtn>
          </div>
        </form>
      </div>
    </UserLayout>
  );
}
