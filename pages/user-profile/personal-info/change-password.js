import { useState } from "react";
import UserLayout from "../../../src/hoc/Layout/UserLayout/UserLayout";
import classes from "./change-password.module.scss";
// import Button from "@mui/material/Button";
import { MdOutlineVisibility } from "react-icons/md";
import { MdOutlineVisibilityOff } from "react-icons/md";
import { Button, TextField, InputAdornment, IconButton } from "@mui/material";
import Link from "next/link";
import { styled } from "@mui/material/styles";

function ChangePassword() {
  const [showPassword, setShowPassword] = useState(false);

  const StyledTextField = styled((props) => <TextField {...props} />)({
    "& .MuiOutlinedInput-input": {
      padding: "10px 24px",
      color: "red",
      fontFamily: "Poppins",
      color: "var(--text-primary)",
      fontSize: "14px",
    },

    ".MuiOutlinedInput-notchedOutline": {
      top: 0,
    },

    "& fieldset": {
      width: "100%",
      border: "none",
      outline: "1px solid #b3b3b3",
      borderRadius: "16px",
    },
    "& label": {
      fontFamily: "Poppins",
      color: "var(--text-primary)",
      backgroundColor: "var(--user-sidebar-main)",
      paddingBlock: "2px",
      paddingInline: "10px",
      marginTop: "-8px",
      fontSize: "14px",
      color: "var(--text-primary) !important",
    },

    "& .MuiInputAdornment-root": {
      "& svg": {
        marginTop: "3px",
        color: "var(--red-main)",
        height: "18px",
        width: "18px",
      },
    },
  });

  const StyledSubmitBtn = styled((props) => <Button {...props} />)({
    borderRadius: "12px",
    border: 0,
    padding: " 10px 20px",
    backgroundColor: " var(--text-primary)",
    color: "var(--f-to-0)",
    textTransform: "inherit",
    fontFamily: "Poppins",

    "&:hover": {
      backgroundColor: "var(--text-primary)",
    },
  });

  const StyledResetBtn = styled((props) => <Button {...props} />)({
    borderRadius: "12px",
    border: 0,
    padding: " 10px 20px",
    color: "var(--text-primary)",
    background: "var(--navbar-bg-main)",
    textTransform: "inherit",
    fontFamily: "Poppins",

    "&:hover": {
      background: "var(--navbar-bg-main)",
    },
  });

  return (
    <UserLayout>
      <div className={`${classes.ChangePassword} ChangePassword`}>
        <div className={classes.ChangePasswordTop}>
          <h1 className={classes.ChangePasswordTopHeader}>Şifrəni dəyişdir</h1>
        </div>
        <div className={classes.ChangePasswordForm}>
          <div className={classes.ChangePasswordFormBody}>
            <div className={classes.ChangePasswordFormBodyItem}>
              <StyledTextField
                id="outlined-basic"
                label="Köhnə şifrə"
                placeholder="Köhnə şifrə"
                variant="outlined"
                fullWidth
                autoComplete="off"
                type={showPassword ? "text" : "password"}
                // value={values.password}
                // onChange={handleChange("password")}
                InputProps={{
                  endAdornment: (
                    <InputAdornment position="end">
                      <IconButton
                        aria-label="toggle password visibility"
                        onClick={() => setShowPassword(!showPassword)}
                        // onMouseDown={handleMouseDownPassword}
                        edge="end"
                      >
                        {showPassword ? (
                          <MdOutlineVisibilityOff />
                        ) : (
                          <MdOutlineVisibility />
                        )}
                      </IconButton>
                    </InputAdornment>
                  ),
                }}
              />
            </div>
            <div className={classes.ChangePasswordFormBodyItem}>
              <StyledTextField
                id="outlined-basic"
                label="Yeni şifrə"
                placeholder="Yeni şifrə"
                variant="outlined"
                fullWidth
                autoComplete="off"
                type={showPassword ? "text" : "password"}
                // value={values.password}
                // onChange={handleChange("password")}
                InputProps={{
                  endAdornment: (
                    <InputAdornment position="end">
                      <IconButton
                        aria-label="toggle password visibility"
                        onClick={() => setShowPassword(!showPassword)}
                        // onMouseDown={handleMouseDownPassword}
                        edge="end"
                      >
                        {showPassword ? (
                          <MdOutlineVisibilityOff />
                        ) : (
                          <MdOutlineVisibility />
                        )}
                      </IconButton>
                    </InputAdornment>
                  ),
                }}
              />
            </div>
            <div className={classes.ChangePasswordFormBodyItem}>
              <StyledTextField
                id="outlined-basic"
                label="Şifrəni təkrar et"
                placeholder="Şifrəni təkrar et"
                variant="outlined"
                fullWidth
                autoComplete="off"
                type={showPassword ? "text" : "password"}
                // value={values.password}
                // onChange={handleChange("password")}
                InputProps={{
                  endAdornment: (
                    <InputAdornment position="end">
                      <IconButton
                        aria-label="toggle password visibility"
                        onClick={() => setShowPassword(!showPassword)}
                        // onMouseDown={handleMouseDownPassword}
                        edge="end"
                      >
                        {showPassword ? (
                          <MdOutlineVisibilityOff />
                        ) : (
                          <MdOutlineVisibility />
                        )}
                      </IconButton>
                    </InputAdornment>
                  ),
                }}
              />
            </div>
            <div>
              <Link href="/">
                <a className={classes.ToForgotPassword}>Şifrəmi unutmuşam</a>
              </Link>
            </div>
          </div>
          <div className={classes.ChangePasswordFormSubmit}>
            <StyledSubmitBtn
              className={classes.ChangePasswordFormSubmitBtn}
              type="submit"
            >
              Yadda Saxla
            </StyledSubmitBtn>
            <StyledResetBtn
              className={classes.ChangePasswordFormSubmitResetBtn}
              variant="light"
              onClick={() => {
                // formik.resetForm();
              }}
            >
              Ləğv et
            </StyledResetBtn>
          </div>
        </div>
      </div>
    </UserLayout>
  );
}

export default ChangePassword;
