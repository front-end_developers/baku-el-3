import * as React from "react";
import classes from "./index.module.scss";
import UserLayout from "../../../src/hoc/Layout/UserLayout/UserLayout";
import Link from "next/link";
import Button from "@mui/material/Button";
import TextField from "@mui/material/TextField";
import { DatePicker } from "@mui/x-date-pickers/DatePicker";
import { MobileDatePicker } from "@mui/x-date-pickers/MobileDatePicker";
import FormControlLabel from "@mui/material/FormControlLabel";
import Switch from "@mui/material/Switch";
import { styled } from "@mui/material/styles";

import InputAdornment from "@mui/material/InputAdornment";
import {
  User,
  Call,
  Message,
  Calendar,
  GoogleLogo,
  FbLogo,
} from "../../../src/components/Icons";
import { AdapterDateFns } from "@mui/x-date-pickers/AdapterDateFns";
import { LocalizationProvider } from "@mui/x-date-pickers/LocalizationProvider";
import { useFormik } from "formik";
import * as Yup from "yup";

export default function Index() {
  const [value, setValue] = React.useState(null);
  const initialValues = {
    type: "",
  };
  const CreateSchema = Yup.object().shape({
    type: Yup.string(),
  });
  const resetForm = () => {
    // onHide();
    formik.resetForm();
  };
  const formik = useFormik({
    initialValues,
    validationSchema: CreateSchema,
    onSubmit: (values, { resetForm, setSubmitting }) => {
      // onPostGroup(values, resetForm, setSubmitting);
    },
  });

  const StyledTextField = styled((props) => <TextField {...props} />)({
    "& .MuiOutlinedInput-input": {
      padding: "10px 24px",
      color: "red",
      fontFamily: "Poppins",
      color: "var(--text-primary)",
      fontSize: "14px",
    },

    ".MuiOutlinedInput-notchedOutline": {
      top: 0,
    },

    "& fieldset": {
      width: "100%",
      border: "none",
      outline: "1px solid #b3b3b3",
      borderRadius: "16px",
    },
    "& label": {
      fontFamily: "Poppins",
      color: "var(--text-primary)",
      backgroundColor: "var(--user-sidebar-main)",
      paddingBlock: "2px",
      paddingInline: "10px",
      marginTop: "-8px",
      fontSize: "14px",
      color: "var(--text-primary) !important",
    },

    "& .MuiInputAdornment-root": {
      "& svg": {
        marginTop: "3px",
        color: "var(--red-main)",
        height: "18px",
        width: "18px",
      },
    },
  });

  const StyledDatePicker = styled((props) => <DatePicker {...props} />)({
    background: "yellow",

    "& .MuiFormControl-root": {
      width: "100%",
    },
    "& .MuiOutlinedInput-input": {
      fontFamily: "Poppins",
      color: "var(--text-primary)",
      fontSize: "14px",
    },
  });

  const StyledSubmitBtn = styled((props) => <Button {...props} />)({
    borderRadius: "12px",
    border: 0,
    padding: " 10px 20px",
    backgroundColor: " var(--text-primary)",
    color: "var(--f-to-0)",
    textTransform: "inherit",
    fontFamily: "Poppins",

    "&:hover": {
      backgroundColor: "var(--text-primary)",
    },
  });

  const StyledChangePasswordBtn = styled((props) => <Button {...props} />)({
    borderRadius: "12px",
    border: 0,
    padding: " 10px 20px",
    color: "var(--text-primary)",
    background: "var(--navbar-bg-main)",
    textTransform: "inherit",
    fontFamily: "Poppins",

    "&:hover": {
      background: "var(--navbar-bg-main)",
    },

    // padding: 12px 24px;
  });

  const getInputClasses = (filedName) => {
    if (formik.touched[filedName] && formik.errors[filedName]) {
      return true;
    }
    if (formik.touched[filedName] && !formik.errors[filedName]) {
      return false;
    }
    return false;
  };

  const IOSSwitch = styled((props) => (
    <Switch
      focusVisibleClassName=".Mui-focusVisible"
      disableRipple
      {...props}
    />
  ))(({ theme }) => ({
    width: 30,
    height: 16,
    padding: 0,
    "& .MuiSwitch-switchBase": {
      padding: 0,
      margin: 3,
      transitionDuration: "300ms",
      "&.Mui-checked": {
        transform: "translateX(14px)",
        color: "#fff",
        "& + .MuiSwitch-track": {
          backgroundColor: "#00AC07",
          opacity: 1,
          border: 0,
        },
        "&.Mui-disabled + .MuiSwitch-track": {
          opacity: 0.5,
        },
      },
      "&.Mui-focusVisible .MuiSwitch-thumb": {
        color: "#33cf4d",
        border: "6px solid #fff",
      },
      "&.Mui-disabled .MuiSwitch-thumb": {
        color: theme.palette.grey[100],
      },
      "&.Mui-disabled + .MuiSwitch-track": {
        opacity: 0.7,
      },
    },
    "& .MuiSwitch-thumb": {
      boxSizing: "border-box",
      width: 10,
      height: 10,
    },
    "& .MuiSwitch-track": {
      borderRadius: 26 / 2,
      backgroundColor: "#7a7a7a",
      opacity: 1,
      transition: theme.transitions.create(["background-color"], {
        duration: 500,
      }),
    },
  }));
  
  return (
    <UserLayout>
      <div className={`${classes.Personal} Personal`}>
        <div className={classes.PersonalTop}>
          <h1 className={classes.PersonalTopHeader}>Şəxsi məlumatlarım</h1>
          <p>Redaktə</p>
        </div>

        <div className={classes.PersonalFormWrapper}>
          <form className={classes.PersonalForm} onSubmit={formik.handleSubmit}>
            {/* left side form inputs */}

            <div className={classes.PersonalFormLeft}>
              <div
                className={`col-12 col-md-12 col-lg-12  ${classes.PersonalFormLeftGroup}`}
              >
                <StyledTextField
                  variant="outlined"
                  id="Type"
                  // name="Type"
                  label="Ad və soyad"
                  fullWidth
                  autoComplete="off"
                  placeholder="Ad və soyad"
                  InputProps={{
                    endAdornment: (
                      <InputAdornment
                        position="start"
                        sx={{
                          height: "16px",
                          width: "16px",
                        }}
                      >
                        <User />
                      </InputAdornment>
                    ),
                  }}
                  // error={getInputClasses("type")}
                  // {...formik.getFieldProps("type")}
                />
                {formik.touched.type && formik.errors.type ? (
                  <div className="fv-plugins-message-container">
                    <div className="fv-help-block">{formik.errors.type}</div>
                  </div>
                ) : null}
              </div>
              <div
                className={`col-12 col-md-12 col-lg-12  ${classes.PersonalFormLeftGroup}`}
              >
                <StyledTextField
                  variant="outlined"
                  id="Type"
                  // name="Type"
                  label="Telefon"
                  fullWidth
                  autoComplete="off"
                  placeholder="Telefon"
                  // error={getInputClasses("type")}
                  // {...formik.getFieldProps("type")}
                  InputProps={{
                    endAdornment: (
                      <InputAdornment position="start">
                        <Call />
                      </InputAdornment>
                    ),
                  }}
                />
                {formik.touched.type && formik.errors.type ? (
                  <div className="fv-plugins-message-container">
                    <div className="fv-help-block">{formik.errors.type}</div>
                  </div>
                ) : null}
              </div>
              <div
                className={`col-12 col-md-12 col-lg-12  ${classes.PersonalFormLeftGroup}`}
              >
                <StyledTextField
                  variant="outlined"
                  id="Type"
                  // name="Type"
                  label="E-poçt"
                  fullWidth
                  autoComplete="off"
                  placeholder="E-poçt"
                  // error={getInputClasses("type")}
                  // {...formik.getFieldProps("type")}
                  InputProps={{
                    endAdornment: (
                      <InputAdornment position="start">
                        <Message />
                      </InputAdornment>
                    ),
                  }}
                />
                {formik.touched.type && formik.errors.type ? (
                  <div className="fv-plugins-message-container">
                    <div className="fv-help-block">{formik.errors.type}</div>
                  </div>
                ) : null}
              </div>
              <div
                className={`col-12 col-md-12 col-lg-12 mb-5  ${classes.PersonalFormLeftGroup}`}
              >
                <LocalizationProvider dateAdapter={AdapterDateFns}>
                  <DatePicker
                    // openTo="year"
                    inputFormat="dd MMM yyyy"
                    views={["day", "month", "year"]}
                    label="Doğum tarixi"
                    // placeholder="Doğum tarixi"
                    // fullWidth
                    minDate={new Date("1900-01-01")}
                    maxDate={new Date("2023-06-01")}
                    value={value}
                    onChange={(newValue) => {
                      setValue(newValue);
                    }}
                    renderInput={(params) => (
                      <TextField
                        {...params}
                        helperText={null}
                        sx={{
                          width: "100%",
                          "& .MuiOutlinedInput-input": {
                            padding: "10px 24px",
                            color: "red",
                            fontFamily: "Poppins",
                            color: "var(--text-primary)",
                            fontSize: "14px",
                          },

                          ".MuiOutlinedInput-notchedOutline": {
                            top: 0,
                          },

                          "& fieldset": {
                            width: "100%",
                            border: "none",
                            outline: "1px solid #b3b3b3",
                            borderRadius: "16px",
                          },
                          "& label": {
                            fontFamily: "Poppins",
                            color: "var(--text-primary)",
                            backgroundColor: "var(--user-sidebar-main)",
                            paddingBlock: "2px",
                            paddingInline: "10px",
                            marginTop: "-8px",
                            fontSize: "14px",
                            color: "var(--text-primary) !important",
                          },

                          "& .MuiInputAdornment-root": {
                            "& svg": {
                              marginTop: "3px",
                              color: "var(--red-main)",
                              height: "18px",
                              width: "18px",
                              marginTop: "-4px",
                            },
                          },

                          "& .MuiIconButton-root": {
                            marginRight: 0,
                          },
                        }}
                      />
                    )}
                    components={{
                      OpenPickerIcon: Calendar,
                    }}
                    // inputFormat="'gün-ay-il'"
                    // inputFormat="'Week of' MMM d"
                  />

                  {/* <MobileDatePicker
                  inputFormat="dd/MM/yyyy"
                  views={["day", "month", "year"]}
                  label="Doğum tarixi"
                  // placeholder="Doğum tarixi"
                  // fullWidth
                  minDate={new Date("1900-01-01")}
                  maxDate={new Date("2023-06-01")}
                  value={value}
                  onChange={(newValue) => {
                    setValue(newValue);
                  }}
                  renderInput={(params) => (
                    <TextField
                      {...params}
                      helperText={null}
                      InputProps={{
                        endAdornment: (
                          <InputAdornment position="start">
                            <Calendar />
                          </InputAdornment>
                        ),
                      }}
                    />
                  )}
                /> */}
                </LocalizationProvider>

                {formik.touched.type && formik.errors.type ? (
                  <div className="fv-plugins-message-container">
                    <div className="fv-help-block">{formik.errors.type}</div>
                  </div>
                ) : null}
              </div>
            </div>
            {/* right side connect accounts */}
            <div className={classes.PersonalFormRight}>
              {/* social media links */}
              <div className={classes.SocialMedia}>
                <h2 className={classes.SocialMediaHeader}>
                  Sosial şəbəkə profilləri
                </h2>
                <div className={classes.SocialMediaGoogle}>
                  <div>
                    {/* google logo */}
                    <GoogleLogo />
                    {/* profile name */}
                    <span>Ulviyya İmamova</span>
                  </div>
                  <div>
                    {/* deyisdir btn */}
                    <button className={classes.SocialMediaBtn}>Dəyişdir</button>
                  </div>
                </div>
                <div className={classes.SocialMediaFacebook}>
                  <div>
                    {/* facebook logo */}
                    <FbLogo />
                    {/* profile name */}
                    <span>Ulviyya İmamova</span>
                  </div>
                  <div>
                    {/* deyisdir btn */}
                    <button className={classes.SocialMediaBtn}>Dəyişdir</button>
                  </div>
                </div>
              </div>
              {/* news subscription */}
              <div className={classes.NewsSubscription}>
                <h2 className={classes.NewsSubscriptionHeader}>
                  Abunə olan xəbərlərin idarə edilməsi
                </h2>
                <div className={classes.NewsSubscriptionCheck}>
                  <span>ulviyyeimamova@gmail.com</span>
                  <FormControlLabel
                    sx={{
                      m: 0,
                      display: "flex",
                      alignItems: "center",
                      justifyContent: "center",
                      gap: "10px",
                    }}
                    label=""
                    control={
                      <IOSSwitch
                        defaultChecked
                        onClick={() => setActiveStatus(!ActiveStatus)}
                      />
                    }
                  />
                </div>
              </div>
            </div>
          </form>

          <div className={classes.PersonalFormWrapperSubmit}>
            <StyledSubmitBtn
              className={classes.PersonalFormWrapperSubmitBtn}
              type="submit"
            >
              Redaktə et
            </StyledSubmitBtn>
            <Link href="/user-profile/personal-info/change-password">
              <StyledChangePasswordBtn className={classes.ToChangePassword}>
                Şifrəni dəyişdir
              </StyledChangePasswordBtn>
            </Link>
          </div>
        </div>
      </div>
    </UserLayout>
  );
}
