/* eslint-disable @next/next/no-img-element */

import UserLayout from "../../../src/hoc/Layout/UserLayout/UserLayout";
import ActiveLink from "../../../src/components/Link/Link";
import Button from "@mui/material/Button";
import { connect } from "react-redux";
import CustomPagination from "../../../src/components/Pagination/Pagination";
import { Edit, Delete } from "../../../src/components/Icons";
import classes from "./index.module.scss";

function location(props) {
  const { theme } = props;
  return (
    <div data-theme={theme}>
      <UserLayout>
        <div className="Location">
          <div className={classes.LocationContent}>
            <div className={classes.LocationContentTop}>
              <h1 className={classes.LocationHeader}>Ünvanlarım</h1>
              <ActiveLink href="/user-profile/location/add-location">
                <Button
                  variant="contained"
                  disableElevation
                  className={classes.LocationAdd}
                >
                  + Əlavə et
                </Button>
              </ActiveLink>
            </div>

            <p>3 Ünvan</p>

            <div className={classes.LocationContentBody}>
              <div className={classes.LocationContentBodyItem}>
                <div className={classes.LocationContentBodyItemAddress}>
                  <span>Nigar Rafibayli küçəsi, 23</span>
                </div>
                <div className={classes.LocationContentBodyItemButtons}>
                  <div>
                    <span>Azərbaycan,</span>
                    <span>Bakl şəhər,</span>
                    <span>Səbail rayonu</span>
                  </div>
                  <div>
                    <button>
                      <Edit />
                    </button>
                    <button>
                      <Delete />
                    </button>
                  </div>
                </div>
              </div>
              <div className={classes.LocationContentBodyItem}>
                <div className={classes.LocationContentBodyItemAddress}>
                  <span>Nigar Rafibayli küçəsi, 23</span>
                </div>
                <div className={classes.LocationContentBodyItemButtons}>
                  <div>
                    <span>Azərbaycan</span>
                    <span>Bakl şəhər</span>
                    <span>Səbail rayonu </span>
                  </div>
                  <div>
                    <button>
                      <Edit />
                    </button>
                    <button>
                      <Delete />
                    </button>
                  </div>
                </div>
              </div>
              <div className={classes.LocationContentBodyItem}>
                <div className={classes.LocationContentBodyItemAddress}>
                  <span>Nigar Rafibayli küçəsi, 23</span>
                </div>
                <div className={classes.LocationContentBodyItemButtons}>
                  <div>
                    <span>Azərbaycan</span>
                    <span>Bakl şəhər</span>
                    <span>Səbail rayonu </span>
                  </div>
                  <div>
                    <button>
                      <Edit />
                    </button>
                    <button>
                      <Delete />
                    </button>
                  </div>
                </div>
              </div>
            </div>
            <CustomPagination />
          </div>
        </div>
      </UserLayout>
    </div>
  );
}
const mapStateToProps = (state) => {
  return { theme: state.layout.theme };
};

const mapDispatchToProps = {};

export default connect(mapStateToProps, mapDispatchToProps)(location);
