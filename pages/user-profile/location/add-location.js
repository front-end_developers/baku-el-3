import { useState } from "react";
import UserLayout from "../../../src/hoc/Layout/UserLayout/UserLayout";
import Link from "next/link";
import { ArrowBack } from "@mui/icons-material";
import Button from "@mui/material/Button";
import classes from "./addLocation.module.scss";
import ReactSelect from "../../../src/components/ReactSelect/ReactSelect";
import { AngleDown } from "../../../src/components/Icons";
import TextField from "@mui/material/TextField";
import MenuItem from "@mui/material/MenuItem";
import Select from "@mui/material/Select";
import { styled } from "@mui/material/styles";
import { useFormik } from "formik";
import * as Yup from "yup";

export default function AddLocation() {
  const [age, setAge] = useState("10");

  const handleChange = (event) => {
    setAge(event.target.value);
  };

  const initialValues = {
    type: "",
  };
  const CreateSchema = Yup.object().shape({
    type: Yup.string(),
  });
  const resetForm = () => {
    // onHide();
    formik.resetForm();
  };
  const formik = useFormik({
    initialValues,
    validationSchema: CreateSchema,
    onSubmit: (values, { resetForm, setSubmitting }) => {
      // onPostGroup(values, resetForm, setSubmitting);
    },
  });
  const getInputClasses = (filedName) => {
    if (formik.touched[filedName] && formik.errors[filedName]) {
      return true;
    }
    if (formik.touched[filedName] && !formik.errors[filedName]) {
      return false;
    }
    return false;
  };

  const StyledTextField = styled((props) => <TextField {...props} />)({
    "& .MuiOutlinedInput-input": {
      padding: "10px 24px",
      color: "red",
      fontFamily: "Poppins",
      color: "var(--text-primary)",
      fontSize: "14px",
    },

    ".MuiOutlinedInput-notchedOutline": {
      top: 0,
    },

    "& fieldset": {
      width: "100%",
      border: "none",
      outline: "1px solid #b3b3b3",
      borderRadius: "16px",
    },
    "& label": {
      fontFamily: "Poppins",
      color: "var(--text-primary)",
      backgroundColor: "var(--user-sidebar-main)",
      paddingBlock: "2px",
      paddingInline: "10px",
      marginTop: "-8px",
      fontSize: "14px",
      color: "var(--text-primary) !important",
    },
  });

  const StyledSelect = styled((props) => <Select {...props} />)(
    ({ theme }) => ({
      width: "100%",
      border: "1px solid #b3b3b3",
      borderRadius: "16px",
      paddingInline: "24px",

      "& .MuiSelect-select": {
        fontWeight: "500",
        fontSize: "14px",
        padding: "10px 0",
        fontFamily: "Poppins",
        color: "var(--text-primary)",
        fontWeight: "500",
      },

      "& svg": {
        width: "10px",
        color: "var(--red-main)",
        transition: "0.2s",
      },
      "& [aria-expanded='true']~svg": {
        transform: "rotate(-180deg)",
      },
      "& fieldset": {
        display: "none",
      },
    })
  );

  const StyledSubmitBtn = styled((props) => <Button {...props} />)({
    borderRadius: "12px",
    border: 0,
    padding: " 10px 20px",
    backgroundColor: " var(--text-primary)",
    color: "var(--f-to-0)",
    textTransform: "inherit",
    fontFamily: "Poppins",

    "&:hover": {
      backgroundColor: "var(--text-primary)",
    },
  });

  const StyledResetBtn = styled((props) => <Button {...props} />)({
    borderRadius: "12px",
    border: 0,
    padding: " 10px 20px",
    color: "var(--text-primary)",
    background: "var(--navbar-bg-main)",
    textTransform: "inherit",
    fontFamily: "Poppins",

    "&:hover": {
      background: "var(--navbar-bg-main)",
    },
  });

  return (
    <UserLayout>
      <div className={`${classes.AddLocation} AddLocation`}>
        <div className={classes.AddLocationTop}>
          <Link href="/user-profile/location">
            <a>
              <ArrowBack />
            </a>
          </Link>
          <h1 className={classes.AddLocationHeader}>Ünvan əlavə et</h1>
        </div>
        <form
          className={classes.AddLocationForm}
          onSubmit={formik.handleSubmit}
        >
          <div className={`row ${classes.AddLocationFormBody}`}>
            <div
              className={`col-12 col-sm-6 col-md-6 col-lg-6  ${classes.AddLocationFormBodyGroup}`}
            >
              <StyledTextField
                variant="outlined"
                id="Type"
                // name="Type"
                label="Təhvil veriləcək şəxsin adı"
                fullWidth
                autoComplete="off"
                placeholder="Təhvil veriləcək şəxsin adı"
                // error={getInputClasses("type")}
                {...formik.getFieldProps("type")}
              />
              {formik.touched.type && formik.errors.type ? (
                <div className="fv-plugins-message-container">
                  <div className="fv-help-block">{formik.errors.type}</div>
                </div>
              ) : null}
            </div>
            <div
              className={`col-12 col-sm-6 col-md-6 col-lg-6  ${classes.AddLocationFormBodyGroup}`}
            >
              <StyledTextField
                variant="outlined"
                id="Type"
                // name="Type"
                label="Soyadı"
                fullWidth
                placeholder="Soyadı"
                autoComplete="off"
                // error={getInputClasses("type")}
                {...formik.getFieldProps("type")}
              />
              {formik.touched.type && formik.errors.type ? (
                <div className="fv-plugins-message-container">
                  <div className="fv-help-block">{formik.errors.type}</div>
                </div>
              ) : null}
            </div>

            <div
              className={`col-12 col-sm-6 col-md-6 col-lg-6  ${classes.AddLocationFormBodyGroup}`}
            >
              <StyledTextField
                variant="outlined"
                id="Type"
                // name="Type"
                label="Telefon nömrəsi"
                fullWidth
                autoComplete="off"
                placeholder="Telefon nömrəsi"
                // error={getInputClasses("type")}
                {...formik.getFieldProps("type")}
              />
              {formik.touched.type && formik.errors.type ? (
                <div className="fv-plugins-message-container">
                  <div className="fv-help-block">{formik.errors.type}</div>
                </div>
              ) : null}
            </div>
            <div
              className={`col-12 col-sm-6 col-md-6 col-lg-6  ${classes.AddLocationFormBodyGroup}`}
            >
              <StyledTextField
                variant="outlined"
                id="Type"
                // name="Type"
                label="E-poçt"
                fullWidth
                autoComplete="off"
                placeholder="E-poçt"
                // error={getInputClasses("type")}
                {...formik.getFieldProps("type")}
              />
              {formik.touched.type && formik.errors.type ? (
                <div className="fv-plugins-message-container">
                  <div className="fv-help-block">{formik.errors.type}</div>
                </div>
              ) : null}
            </div>

            {/* <div
              className={`col-6 col-md-6 col-lg-6  ${classes.AddLocationFormBodyGroup}`}
            >
              <ReactSelect />

              {formik.touched.type && formik.errors.type ? (
                <div className="fv-plugins-message-container">
                  <div className="fv-help-block">{formik.errors.type}</div>
                </div>
              ) : null}
            </div> */}
            <div
              className={`col-12 col-sm-6 col-md-6 col-lg-6  ${classes.AddLocationFormBodyGroup}`}
            >
              <StyledSelect
                value={age}
                onChange={handleChange}
                IconComponent={() => <AngleDown />}
                label=""
                // placeholder="SELECT"
                // labelId="demo-simple-select-label-ASLAN"
                id="demo-simple-select"
              >
                <MenuItem value={10}>Ten</MenuItem>
                <MenuItem value={20}>Twenty</MenuItem>
                <MenuItem value={30}>Thirty</MenuItem>
              </StyledSelect>

              {formik.touched.type && formik.errors.type ? (
                <div className="fv-plugins-message-container">
                  <div className="fv-help-block">{formik.errors.type}</div>
                </div>
              ) : null}
            </div>
            <div
              className={`col-12 col-sm-6 col-md-6 col-lg-6  ${classes.AddLocationFormBodyGroup}`}
            >
              <StyledSelect
                value={age}
                onChange={handleChange}
                IconComponent={() => <AngleDown />}
                label=""
                // placeholder="SELECT"
                // labelId="demo-simple-select-label-ASLAN"
                id="demo-simple-select"
              >
                <MenuItem value={10}>Ten</MenuItem>
                <MenuItem value={20}>Twenty</MenuItem>
                <MenuItem value={30}>Thirty</MenuItem>
              </StyledSelect>

              {formik.touched.type && formik.errors.type ? (
                <div className="fv-plugins-message-container">
                  <div className="fv-help-block">{formik.errors.type}</div>
                </div>
              ) : null}
            </div>

            {/* 
            <div className={classes.FormGroup}>
              </div> */}

            <div
              className={`col-8 col-sm-10 col-md-10 col-lg-10  ${classes.AddLocationFormBodyGroup}`}
            >
              <StyledTextField
                variant="outlined"
                id="Type"
                // name="Type"
                label="Dəqiq ünvan"
                fullWidth
                autoComplete="off"
                placeholder="Dəqiq ünvan"
                // error={getInputClasses("type")}
                {...formik.getFieldProps("type")}
              />
              {formik.touched.type && formik.errors.type ? (
                <div className="fv-plugins-message-container">
                  <div className="fv-help-block">{formik.errors.type}</div>
                </div>
              ) : null}
            </div>
            <div
              className={`col-4 col-sm-2 col-md-2 col-lg-2  ${classes.AddLocationFormBodyGroup}`}
            >
              <StyledTextField
                variant="outlined"
                id="Type"
                // name="Type"
                label="ZİP kod"
                fullWidth
                autoComplete="off"
                placeholder="ZİP kod"
                // error={getInputClasses("type")}
                {...formik.getFieldProps("type")}
              />
              {formik.touched.type && formik.errors.type ? (
                <div className="fv-plugins-message-container">
                  <div className="fv-help-block">{formik.errors.type}</div>
                </div>
              ) : null}
            </div>

            {/* {isError && (
              <div className="fv-plugins-message-container">
                <div className="fv-help-block MuiFormLabel-root Mui-error">
                  {isError}
                </div>
              </div>
            )} */}
          </div>
          <div className={classes.AddLocationFormSubmit}>
            <StyledSubmitBtn
              className={classes.AddLocationFormSubmitBtn}
              type="submit"
            >
              Yadda Saxla
            </StyledSubmitBtn>
            <StyledResetBtn
              className={classes.AddLocationFormSubmitResetBtn}
              variant="light"
              onClick={() => {
                // onHide(false);
                formik.resetForm();
              }}
            >
              Ləğv et
            </StyledResetBtn>
          </div>
        </form>
      </div>
    </UserLayout>
  );
}

{
  /* <form className={classes.Form}>
            <div className={classes.FormTop}>
              <div className={classes.FormGroup}>
                <input placeholder="Təhvil veriləcək şəxsin adı" type="text" />
              </div>
              <div className={classes.FormGroup}>
                <input placeholder="Soyadı" type="text" />
              </div>
              <div className={classes.FormGroup}>
                <input placeholder="Telefon nömrəsi" type="number" />
              </div>
              <div className={classes.FormGroup}>
                <input placeholder="E-poçt" type="email" />
              </div>
              <div className={classes.FormGroup}>
                <ReactSelect />
              </div>
              <div className={classes.FormGroup}>
                <ReactSelect />
              </div>
              <div
                className={`${classes.FormGroup} ${classes.FormGroupAddress}`}
              >
                <input placeholder="Dəqiq ünvan" type="text" />
              </div>
              <div className={`${classes.FormGroup} ${classes.FormGroupZip}`}>
                <input placeholder="Zip kod" type="text" />
              </div>
            </div>
            <div className={classes.FormBottom}>
              <Button className={classes.FormApprove}>Yadda Saxla</Button>
              <Button className={classes.FormCancel}>Ləğv et</Button>
            </div>
          </form> */
}
