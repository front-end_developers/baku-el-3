import "../styles/globals.scss";
import Aux from "../src/hoc/Auxilary/Auxilary";
import "slick-carousel/slick/slick.scss";
import "slick-carousel/slick/slick-theme.scss";
import Head from "next/head";
import { Provider } from "react-redux";
import store, { persistor } from "../redux/store";
import { PersistGate } from "redux-persist/integration/react";
import setupAxios from "../redux/axios";
import axios from "axios";
// import { wrapper } from "../redux/store";
// import "bootstrap/dist/css/bootstrap.min.css";
// import "bootstrap/dist/js/bootstrap.min.js";

function MyApp({ Component, pageProps }) {
  // setupAxios(axios, store);
  return (
    <Provider store={store}>
      <PersistGate
        persistor={persistor}
        loading={
          <Aux>
            <Head>
              <meta
                name="viewport"
                content="initial-scale=1.0, width=device-width"
              />
            </Head>
            <Component {...pageProps} />
          </Aux>
        }
      >
        <Aux>
          <Head>
            <meta
              name="viewport"
              content="initial-scale=1.0, width=device-width"
            />
          </Head>
          <Component {...pageProps} />
        </Aux>
      </PersistGate>
    </Provider>
  );
}
export default MyApp;
// export default wrapper.withRedux(MyApp);

/*
      <Aux>
            <Head>
              <meta
                name="viewport"
                content="initial-scale=1.0, width=device-width"
              />
            </Head>
            <Component {...pageProps} />
          </Aux>
*/
