import { useState } from "react";
import { useRouter } from "next/router";
import Layout from "../../src/hoc/Layout/Layout/Layout";
import Breadcrumb from "../../src/components/Breadcrumb/Breadcrumb";
import classes from "./empty.module.scss";
import Slider from "../../src/components/LightGallerySlider/Slider";
import StarRating from "../../src/components/StarRating/StarRating";
import Chat from "../../src/components/Icons/Chat";
import ActiveLink from "/src/components/Link/Link";
import {
  Linfo,
  Comments,
  Gift,
  // Info,
  Promocode,
  AddToShoppingCar,
  Scales,
  Heart,
  OneClick,
  PaymentDiscount1,
  WeSuggest,
  Paper,
  Star,
  CallToAsk,
  Bell,
} from "/src/components/Icons";
import ProductTab from "/src/components/ProductCreditTab/ProductTab";
import {
  Button,
  Collapse,
  Avatar,
  Rating,
  SwipeableDrawer,
} from "@mui/material";
import ProductCard from "/src/components/ProductCard/ProductCard";
import Tabs from "@mui/material/Tabs";
import Tab, { tabClasses } from "@mui/material/Tab";
import Typography from "@mui/material/Typography";
import Box from "@mui/material/Box";
import { styled } from "@mui/styles";
import { createTheme, ThemeProvider } from "@mui/material/styles";
import SwipeableEdgeDrawerDrawer from "/src/components/SwipeableDrawer/SwipeableDrawer";
import ProductEvent from "../../src/components/ProductEvent/ProductEvent";

const TabTheme = createTheme({
  palette: {
    primary: {
      main: "#EA2427",
    },
  },
});

const CustomTab = styled(Tab)(({ theme }) => ({
  [`&.${tabClasses.root}`]: {
    textTransform: "unset",
    // color: "var(--text-primary)",
    fontFamily: "Poppins",
  },
}));

export default function Index(props) {
  let route = useRouter();
  const color = { blue: "#02379D", gold: "#B6A179", black: "#727272" };
  const [productColor, setProductColor] = useState(`${color.blue}`);
  const [oneClickToggle, setOneClickToggle] = useState(false);
  const [creditToggle, setCreditToggle] = useState(false);
  const [showAddition, setShowAddition] = useState(false);
  const [showAbout, setShowAbout] = useState(false);
  const [showFeatures, setShowFeatures] = useState(false);
  const [showComments, setShowComments] = useState(false);
  const [value, setValue] = useState(0);
  const [rating, setRating] = useState(0);
  const [other, setOther] = useState([
    {
      name: "airpods",
      title: "Apple Airpods Pro",
      count: 0,
    },
    {
      name: "applewatch",
      title: "Apple Watch Series 6",
      count: 0,
    },
    {
      name: "applemagsafe",
      title: "Apple Magsafe",
      count: 0,
    },
  ]);

  const handleChange = (event, newValue) => {
    setValue(newValue);
  };

  const HandleAddition = () => {
    setShowAddition(!showAddition);
  };

  const HandleAbout = () => {
    setShowAbout(!showAbout);
  };

  const HandleFeatures = () => {
    setShowFeatures(!showFeatures);
  };

  const HandleComments = () => {
    setShowComments(!showComments);
  };

  function TabPanel(props) {
    const { children, value, index, ...other } = props;

    return (
      <div
        role="tabpanel"
        hidden={value !== index}
        id={`simple-tabpanel-${index}`}
        aria-labelledby={`simple-tab-${index}`}
        {...other}
      >
        {value === index && (
          <Box sx={{ p: 3 }}>
            <Typography>{children}</Typography>
          </Box>
        )}
      </div>
    );
  }

  function a11yProps(index) {
    return {
      id: `simple-tab-${index}`,
      "aria-controls": `simple-tabpanel-${index}`,
    };
  }

  return (
    <Layout>
      {/* <Breadcrumb
        steps={[
          "Ana səhifə",
          "Smartfonlar",
          "Apple Iphone 12 Pro Max 128 GB Blue",
        ]}
      /> */}

      <div className={classes.ProductDetails}>
        <div className={classes.ProductDetailsContent}>
          <div className={classes.ProductDetailsContentLeft}>
            <div className={classes.ProductDetailsContentLeftSlider}>
              <Slider />
            </div>
          </div>

          <div className={classes.ProductDetailsContentRight}>
            <div className={classes.Title}>
              <div className={classes.TitleStats}>
                <a href="#" className={classes.TitleStatsComment}>
                  <Chat /> 6 rəy
                </a>
                <StarRating />
              </div>
              <div className={classes.TitleText}>
                <h1>Smartfon Apple Iphone 12 Pro Max 128GB Blue</h1>
              </div>
              <div className={classes.TitleProductInfo}>
                <p>
                  Məhsul kodu: <span>166232</span>
                </p>
                <p>
                  Məhsulun fiziki statusu: <span>Vitrin</span>
                </p>
              </div>
              <div className={classes.TitleProductOptions}>
                <div className={classes.TitleProductOptionsColor}>
                  <p className={classes.TitleProductOptionsColorHeading}>
                    Rəng:
                  </p>
                  <div className={classes.ColorContainer}>
                    <div className={classes.ColorButton}>
                      <ActiveLink
                        activeClassName={classes.ColorButtonActive}
                        href="/product-details/Iphone13ProMaxBlue"
                      >
                        <a style={{ background: `${color.blue}` }}></a>
                      </ActiveLink>
                    </div>
                    <div className={classes.ColorButton}>
                      <ActiveLink
                        activeClassName={classes.ColorButtonActive}
                        href="/product-details/Iphone13ProMaxGold"
                      >
                        <a style={{ background: `${color.gold}` }}></a>
                      </ActiveLink>
                    </div>
                    <div className={classes.ColorButton}>
                      <ActiveLink
                        activeClassName={classes.ColorButtonActive}
                        href="/product-details/Iphone13ProMaxBlack"
                      >
                        <a style={{ background: `${color.black}` }}></a>
                      </ActiveLink>
                    </div>
                  </div>
                </div>
                <div className={classes.TitleProductOptionsMemory}>
                  <p className={classes.TitleProductOptionsMemoryHeading}>
                    Daxili yaddaş:
                  </p>
                  <div className={classes.MemoryContainer}>
                    <div className={classes.MemoryButton}>
                      <ActiveLink
                        activeClassName={classes.MemoryButtonActive}
                        href="/product-details/128gb"
                      >
                        <a>128GB</a>
                      </ActiveLink>
                    </div>
                    <div className={classes.MemoryButton}>
                      <ActiveLink
                        activeClassName={classes.MemoryButtonActive}
                        href="/product-details/256gb"
                      >
                        <a>256GB</a>
                      </ActiveLink>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div className={classes.IsEmpty}>
              <p className={classes.IsEmptyHeader}>
                Məhsul hal-hazırda stokda mövcud deyil
              </p>
              <div className={classes.IsEmptyButtons}>
                <button className={classes.IsEmptyAddToFavorites}>
                  <Heart />
                </button>
                <button className={classes.IsEmptyCreateNotification}>
                  <Bell />
                  <span>Gələndə xəbər et</span>
                </button>
              </div>
            </div>
          </div>
        </div>
      </div>

      <ProductEvent />

      <SwipeableEdgeDrawerDrawer />
    </Layout>
  );
}
