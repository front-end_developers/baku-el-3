import { useState } from "react";
import { useRouter } from "next/router";
import Layout from "../../src/hoc/Layout/Layout/Layout";
import Breadcrumb from "../../src/components/Breadcrumb/Breadcrumb";
import classes from "./productDetails.module.scss";
import Slider from "../../src/components/LightGallerySlider/Slider";
import StarRating from "../../src/components/StarRating/StarRating";
import Chat from "../../src/components/Icons/Chat";
import ActiveLink from "/src/components/Link/Link";
import {
  Gift,
  // Info,
  Promocode,
  AddToShoppingCar,
  Scales,
  Heart,
  OneClick,
  PaymentDiscount1,
  WeSuggest,
  Paper,
  Star,
  CallToAsk,
  Manat,
  Linfo,
  Comments,
} from "/src/components/Icons";
import ProductTab from "/src/components/ProductCreditTab/ProductTab";
import {
  Button,
  Collapse,
  Avatar,
  Rating,
  SwipeableDrawer,
} from "@mui/material";
import ProductCard from "/src/components/ProductCard/ProductCard";
import Tabs from "@mui/material/Tabs";
import Tab, { tabClasses } from "@mui/material/Tab";
import Typography from "@mui/material/Typography";
import Box from "@mui/material/Box";
import { styled } from "@mui/styles";
import { createTheme, ThemeProvider } from "@mui/material/styles";
import SwipeableEdgeDrawerDrawer from "/src/components/SwipeableDrawer/SwipeableDrawer";
import ProductEvent from "../../src/components/ProductEvent/ProductEvent";

const TabTheme = createTheme({
  palette: {
    primary: {
      main: "#EA2427",
    },
  },
});

const CustomTab = styled(Tab)(({ theme }) => ({
  [`&.${tabClasses.root}`]: {
    textTransform: "unset",
    // color: "var(--text-primary)",
    fontFamily: "Poppins",
  },
}));

export default function Index(props) {
  let route = useRouter();
  const color = { blue: "#02379D", gold: "#B6A179", black: "#727272" };
  const [productColor, setProductColor] = useState(`${color.blue}`);
  const [oneClickToggle, setOneClickToggle] = useState(false);
  const [creditToggle, setCreditToggle] = useState(false);
  const [oneClickToggleBottom, setOneClickToggleBottom] = useState(false);
  const [creditToggleBottom, setCreditToggleBottom] = useState(false);
  const [showAddition, setShowAddition] = useState(false);
  const [showAbout, setShowAbout] = useState(false);
  const [showFeatures, setShowFeatures] = useState(false);
  const [showComments, setShowComments] = useState(false);
  const [value, setValue] = useState(0);
  const [rating, setRating] = useState(0);
  const [other, setOther] = useState([
    {
      name: "airpods",
      title: "Apple Airpods Pro",
      count: 0,
    },
    {
      name: "applewatch",
      title: "Apple Watch Series 6",
      count: 0,
    },
    {
      name: "applemagsafe",
      title: "Apple Magsafe",
      count: 0,
    },
  ]);

  const handleChange = (event, newValue) => {
    setValue(newValue);
  };

  const HandleAddition = () => {
    setShowAddition(!showAddition);
  };

  const HandleAbout = () => {
    setShowAbout(!showAbout);
  };

  const HandleFeatures = () => {
    setShowFeatures(!showFeatures);
  };

  const HandleComments = () => {
    setShowComments(!showComments);
  };

  function TabPanel(props) {
    const { children, value, index, ...other } = props;

    return (
      <div
        role="tabpanel"
        hidden={value !== index}
        id={`simple-tabpanel-${index}`}
        aria-labelledby={`simple-tab-${index}`}
        {...other}
      >
        {value === index && (
          <Box sx={{ p: 3 }}>
            <Typography>{children}</Typography>
          </Box>
        )}
      </div>
    );
  }

  function a11yProps(index) {
    return {
      id: `simple-tab-${index}`,
      "aria-controls": `simple-tabpanel-${index}`,
    };
  }

  return (
    <Layout>
      <div className={classes.ProductDetails}>
        <div className={classes.ProductDetailsContent}>
          <div className={classes.ProductDetailsContentLeft}>
            <div className={classes.ProductDetailsContentLeftSlider}>
              <Slider />
            </div>
          </div>

          <div className={classes.ProductDetailsContentRight}>
            <div className={classes.Title}>
              <div className={classes.TitleStats}>
                <a href="#" className={classes.TitleStatsComment}>
                  <Chat /> 6 rəy
                </a>
                <StarRating />
              </div>
              <div className={classes.TitleText}>
                <h1>Smartfon Apple Iphone 12 Pro Max 128GB Blue</h1>
              </div>
              <div className={classes.TitleProductInfo}>
                <p>
                  Məhsul kodu: <span>166232</span>
                </p>
                <p>
                  Məhsulun fiziki statusu: <span>Vitrin</span>
                </p>
              </div>
              <div className={classes.TitleProductOptions}>
                <div className={classes.TitleProductOptionsColor}>
                  <p className={classes.TitleProductOptionsColorHeading}>
                    Rəng:
                  </p>
                  <div className={classes.ColorContainer}>
                    <div className={classes.ColorButton}>
                      <ActiveLink
                        activeClassName={classes.ColorButtonActive}
                        href="/product-details/Iphone13ProMaxBlue"
                      >
                        <a style={{ background: `${color.blue}` }}></a>
                      </ActiveLink>
                    </div>
                    <div className={classes.ColorButton}>
                      <ActiveLink
                        activeClassName={classes.ColorButtonActive}
                        href="/product-details/Iphone13ProMaxGold"
                      >
                        <a style={{ background: `${color.gold}` }}></a>
                      </ActiveLink>
                    </div>
                    <div className={classes.ColorButton}>
                      <ActiveLink
                        activeClassName={classes.ColorButtonActive}
                        href="/product-details/Iphone13ProMaxBlack"
                      >
                        <a style={{ background: `${color.black}` }}></a>
                      </ActiveLink>
                    </div>
                  </div>
                </div>
                <div className={classes.TitleProductOptionsMemory}>
                  <p className={classes.TitleProductOptionsMemoryHeading}>
                    Daxili yaddaş:
                  </p>
                  <div className={classes.MemoryContainer}>
                    <div className={classes.MemoryButton}>
                      <ActiveLink
                        activeClassName={classes.MemoryButtonActive}
                        href="/product-details/128gb"
                      >
                        <a>128GB</a>
                      </ActiveLink>
                    </div>
                    <div className={classes.MemoryButton}>
                      <ActiveLink
                        activeClassName={classes.MemoryButtonActive}
                        href="/product-details/256gb"
                      >
                        <a>256GB</a>
                      </ActiveLink>
                    </div>
                  </div>
                </div>
              </div>
              <div className={classes.TitleProductPrice}>
                <div className="row">
                  <div className="col-6">
                    <div className={classes.PriceLeft}>
                      <p className={classes.PriceLeftLastPrice}>
                        3200  <Manat/> <span></span>
                      </p>
                      <p className={classes.PriceLeftCurrentPrice}>2969  <Manat/></p>
                    </div>
                  </div>
                  <div className="col-6">
                    <div className={classes.PriceRight}>
                      <div className={classes.Bonus}>
                        <div className={classes.BonusText}>
                          <span>
                            <Gift /> 259  <Manat/>
                          </span>
                          <span>Bonus</span>
                        </div>
                        <div className={classes.BonusIcon}>
                          {/* <Info /> */}
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div className={classes.PurchaseContainer}>
              {/* 1 */}
              <div className={classes.PurchaseContainerSectionPromo}>
                <div className={classes.PromoCodeInput}>
                  <span>
                    <Promocode />
                  </span>
                  <input placeholder="Promokod daxil edin..." type="text" />
                </div>
                <div className={classes.ButtonContainer}>
                  <div className={classes.AddToCartBtn}>
                    <Button>
                      <span>
                        <AddToShoppingCar />
                      </span>
                      <span>Səbətə at</span>
                    </Button>
                  </div>
                  <div className={classes.CompareBtn}>
                    <Button>
                      <Scales />
                    </Button>
                  </div>
                  <div className={classes.FavouriteBtn}>
                    <Button>
                      <Heart />
                    </Button>
                  </div>
                </div>
              </div>
              {/* 2 */}
              <div className={classes.PurchaseContainerSectionPayementOptions}>
                <div className="row">
                  <div className="col-6">
                    <Button
                      className={classes.BuyRightNow}
                      onClick={() => {
                        setOneClickToggle(!oneClickToggle);
                        setCreditToggle(false);
                      }}
                    >
                      <p
                        className={classes.OneClick}
                        style={{
                          background: `${
                            oneClickToggle ? "#ea2427" : "#E9E9E9"
                          }`,
                        }}
                      >
                        <OneClick
                          style={{
                            color: `${oneClickToggle ? "#ffffff" : "#000000"}`,
                          }}
                        />
                      </p>
                      <div className={classes.Text}>
                        <p>Bir kliklə al</p>
                        <p>Sürətli sifariş</p>
                      </div>
                      {oneClickToggle && (
                        <span className={classes.Arrow}></span>
                      )}
                    </Button>
                  </div>
                  <div className="col-6">
                    <Button
                      className={classes.BuyWithCredit}
                      onClick={() => {
                        setCreditToggle(!creditToggle);
                        setOneClickToggle(false);
                      }}
                    >
                      <p
                        className={classes.Credit}
                        style={{
                          background: `${creditToggle ? "#ea2427" : "#E9E9E9"}`,
                        }}
                      >
                        <PaymentDiscount1
                          style={{
                            color: `${creditToggle ? "#ffffff" : "#000000"}`,
                          }}
                        />
                      </p>
                      <div className={classes.Text}>
                        <p>Hissə-hissə ödə</p>
                        <p>18 ay 269  <Manat/></p>
                      </div>
                      {creditToggle && <span className={classes.Arrow}></span>}
                    </Button>
                  </div>
                </div>
              </div>
              {/* 3 */}
              <div className={classes.PurchaseContainerSectionDropdown}>
                <Collapse in={oneClickToggle}>
                  <div className={classes.PurchaseItemForm}>
                    <div className={classes.PurchaseItemFormTitle}>
                      <span>Telefon nömrəni yaz, sənə zəng edək!</span>
                    </div>
                    <div>
                      <form action="">
                        <input
                          placeholder="Sənə necə səslənək?"
                          name="name"
                          type="text"
                        />
                        <input
                          placeholder="Telefon"
                          name="number"
                          type="text"
                        />
                        <button type="submit">Göndər</button>
                      </form>
                    </div>
                  </div>
                </Collapse>
                <Collapse in={creditToggle}>
                  <div className={classes.PurchaseItemCredit}>
                    <div className="credit-tab">
                      <ProductTab />
                    </div>
                  </div>
                </Collapse>
              </div>
              {/* 4 */}
              <div className={classes.PurchaseContainerSectionGift}>
                <div className={classes.PurchaseContainerSectionGiftTop}>
                  <span>Hədiyyə</span>
                </div>
                <div className={classes.PurchaseContainerSectionGiftBottom}>
                  <img src="/img/prod-details-gift-item.png" alt="" />
                  <div className={classes.GiftBody}>
                    <span className={classes.GiftBodyTitle}>
                      İphone Adapter
                    </span>
                    <span className={classes.GiftBodyMore}>
                      Məhsul haqqında daha çox
                    </span>
                  </div>
                </div>
              </div>
              {/* 5 */}
              <div className={classes.PurchaseContainerSectionExtraToProduct}>
                <div className={classes.Addition}>
                  <div className={classes.AdditionTitle}>
                    <span>Məhsula əlavə</span>
                  </div>
                  <div className={classes.AdditionBox}>
                    <div className={classes.AdditionBoxItem}>
                      <label>
                        <div className={classes.AdditionBoxItemLeft}>
                          <div className={classes.AdditionBoxItemLeftInput}>
                            <label>
                              <input type="checkbox" />
                              <span className={classes.Marker}></span>
                            </label>
                          </div>
                          <div className={classes.AdditionBoxItemLeftLabel}>
                            <span>Qızıl zəmanət</span>
                          </div>
                        </div>
                        <div className={classes.AdditionBoxItemRight}>
                          <span>+ 298  <Manat/></span>
                        </div>
                      </label>
                    </div>
                    <div className={classes.AdditionBoxItem}>
                      <label>
                        <div className={classes.AdditionBoxItemLeft}>
                          <div className={classes.AdditionBoxItemLeftInput}>
                            <label>
                              <input type="checkbox" />
                              <span className={classes.Marker}></span>
                            </label>
                          </div>
                          <div className={classes.AdditionBoxItemLeftLabel}>
                            <span>Qızıl zəmanət</span>
                          </div>
                        </div>
                        <div className={classes.AdditionBoxItemRight}>
                          <span>+ 298  <Manat/></span>
                        </div>
                      </label>
                    </div>
                    <div className={classes.AdditionBoxItem}>
                      <label>
                        <div className={classes.AdditionBoxItemLeft}>
                          <div className={classes.AdditionBoxItemLeftInput}>
                            <label>
                              <input type="checkbox" />
                              <span className={classes.Marker}></span>
                            </label>
                          </div>
                          <div className={classes.AdditionBoxItemLeftLabel}>
                            <span>Qızıl zəmanət</span>
                          </div>
                        </div>
                        <div className={classes.AdditionBoxItemRight}>
                          <span>+ 298  <Manat/></span>
                        </div>
                      </label>
                    </div>

                    <Collapse in={showAddition}>

                    <div className={classes.AdditionBoxItem}>
                      <label>
                        <div className={classes.AdditionBoxItemLeft}>
                          <div className={classes.AdditionBoxItemLeftInput}>
                            <label>
                              <input type="checkbox" />
                              <span className={classes.Marker}></span>
                            </label>
                          </div>
                          <div className={classes.AdditionBoxItemLeftLabel}>
                            <span>Qızıl zəmanət</span>
                          </div>
                        </div>
                        <div className={classes.AdditionBoxItemRight}>
                          <span>+ 298  <Manat/></span>
                        </div>
                      </label>
                    </div>                  <div className={classes.AdditionBoxItem}>
                      <label>
                        <div className={classes.AdditionBoxItemLeft}>
                          <div className={classes.AdditionBoxItemLeftInput}>
                            <label>
                              <input type="checkbox" />
                              <span className={classes.Marker}></span>
                            </label>
                          </div>
                          <div className={classes.AdditionBoxItemLeftLabel}>
                            <span>Qızıl zəmanət</span>
                          </div>
                        </div>
                        <div className={classes.AdditionBoxItemRight}>
                          <span>+ 298  <Manat/></span>
                        </div>
                      </label>
                    </div>

                    </Collapse>
                    <button
                      className={classes.AdditionBoxBtn}
                      onClick={HandleAddition}
                    >
                      {showAddition ? (
                        <span>
                          Bağla{" "}
                          <i
                            className={`${classes.AdditionBoxBtnArrow} ${classes.AdditionBoxBtnArrowDown}`}
                          ></i>
                        </span>
                      ) : (
                        <span>
                          Daha 5 xidməti göstər{" "}
                          <i className={classes.AdditionBoxBtnArrow}></i>
                        </span>
                      )}
                    </button>
                  </div>
                </div>
              </div>
              {/* 6 */}
              <div className={classes.PurchaseContainerSectionBuyExtra}>
                <div className={classes.ProductBesides}>
                  <div className={classes.ProductBesidesTitle}>
                    <span>Məhsulun yanında al</span>
                  </div>
                  {other.map((item) => (
                    <div key={item.name} className={classes.ProductBesidesBox}>
                      <div className={classes.ProductBesidesBoxLeft}>
                        <div className={classes.ProductBesidesBoxLeftImg}></div>
                        <div className={classes.ProductBesidesBoxLeftText}>
                          <div
                            className={classes.ProductBesidesBoxLeftTextTitle}
                          >
                            <span>{item.title}</span>
                          </div>
                          <div
                            className={classes.ProductBesidesBoxLeftTextInner}
                          >
                            <span
                              className={
                                classes.ProductBesidesBoxLeftTextInnerLeft
                              }
                            >
                              {/* <span className={classes.OldPrice}>699 <Manat/></span> */}
                              <span className={classes.CurrentPrice}>
                                499 <Manat/>
                              </span>
                            </span>
                            <span
                              className={
                                classes.ProductBesidesBoxLeftTextInnerRight
                              }
                            >
                              90 <Manat/> <span>/ 15 ay</span>
                            </span>
                          </div>
                        </div>
                      </div>
                      <div className={classes.ProductBesidesBoxRight}>
                        <div className={classes.ProductBesidesBoxRightCount}>
                          <button>-</button>
                          <input name={item.name} value={item.count} />
                          <button>+</button>
                        </div>
                        <div className={classes.ProductBesidesBoxRightBtn}>
                          <Button>
                            <AddToShoppingCar />
                          </Button>
                        </div>
                      </div>
                    </div>
                  ))}
                </div>
              </div>
              {/* 7 */}
              <div className={classes.PurchaseContainerSectionOurOffers}>
                <div className={classes.OfferInfo}>
                  <div className={classes.OfferInfoTitle}>
                    <span>Biz təklif edirik</span>
                  </div>
                  <div className={classes.OfferInfoInner}>
                    <div className={classes.OfferInfoInnerBox}>
                      <div className={classes.OfferInfoInnerBoxTitle}>
                        <span>Ödəniş:</span>
                      </div>
                      <div className={classes.OfferInfoInnerBoxItem}>
                        <WeSuggest />
                        <p>Nağd</p>
                      </div>
                      <div className={classes.OfferInfoInnerBoxItem}>
                        <WeSuggest />
                        <p>Visa/Mastercard</p>
                      </div>
                      <div className={classes.OfferInfoInnerBoxItem}>
                        <WeSuggest />
                        <p>Taksit kartlar</p>
                      </div>
                      <div className={classes.OfferInfoInnerBoxItem}>
                        <WeSuggest />
                        <p>Nisyə alış</p>
                      </div>
                    </div>
                    <div className={classes.OfferInfoInnerBox}>
                      <div className={classes.OfferInfoInnerBoxTitle}>
                        <span>Xidmət:</span>
                      </div>
                      <div className={classes.OfferInfoInnerBoxItem}>
                        <WeSuggest /> <p>Rəsmi zamanət</p>
                      </div>
                      <div className={classes.OfferInfoInnerBoxItem}>
                        <WeSuggest />{" "}
                        <p>
                          4 gün ərzində məhsulun dəyişdirilməsi və ya
                          qaytarılması.
                        </p>
                      </div>
                      <div className={classes.OfferInfoInnerBoxItem}>
                        <WeSuggest />
                        <p>Quraşdırma</p>
                      </div>
                    </div>
                    <div className={classes.OfferInfoInnerBox}>
                      <div className={classes.OfferInfoInnerBoxTitle}>
                        <span>Ödəniş:</span>
                      </div>
                      <div className={classes.OfferInfoInnerBoxItem}>
                        <WeSuggest />
                        <p>Mağazadan götür</p>
                      </div>
                      <div className={classes.OfferInfoInnerBoxItem}>
                        <WeSuggest />
                        <p>Kuryerlə çatdırılma</p>
                      </div>
                      <div className={classes.OfferInfoInnerBoxItem}>
                        <WeSuggest />
                        <p>Sürətli çatdırılma</p>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              {/* 8 */}
              <div className={classes.PurchaseContainerSectionOthers}>
                <div className={classes.Other}>
                  <div className={classes.OtherTitle}>
                    <span>Digər</span>
                  </div>
                  <div className={classes.OtherInner}>
                    <div className={classes.OtherInnerItem}>
                      <div className={classes.OtherInnerItemBtn}>
                        <Button onClick={HandleAbout}>
                          <div className={classes.OtherInnerItemLeft}>
                            <div className={classes.OtherInnerItemLeftIcon}>
                              <Linfo />
                            </div>
                          </div>
                          <div className={classes.OtherInnerItemRight}>
                            <div className={classes.OtherInnerItemRightTitle}>
                              <div
                                className={classes.OtherInnerItemRightTitleText}
                              >
                                <h2>Məhsul haqqında</h2>
                                <span>Baxmaq üçün kliklə</span>
                              </div>
                              <div
                                className={classes.OtherInnerItemRightTitleBtn}
                              >
                                <span>+</span>
                              </div>
                            </div>
                          </div>
                        </Button>
                      </div>
                      <Collapse in={showAbout}>
                        <div className={classes.OtherInnerItemRightText}>
                          <p className={classes.OtherInnerItemRightTextAbout}>
                            Fringilla laoreet suspendisse pellentesque
                            scelerisque diam eu massa. Purus pharetra sagittis
                            id cum habitasse mattis tristique. Proin commodo
                            diam odio et tempus neque, malesuada faucibus. Vitae
                            donec id interdum faucibus aliquam. Ut diam cras
                            eros sed pharetra, odio fames pellentesque aliquam.
                            Bibendum non, dui lorem vitae. Risus id praesent
                            pellentesque egestas volutpat. Integer habitant
                            lectus consectetur ipsum et. Amet felis est mauris
                            elementum vel tincidunt pharetra, pretium. Faucibus
                            nulla quam dictum aliquet.
                          </p>
                        </div>
                      </Collapse>
                    </div>
                    <div className={classes.OtherInnerItem}>
                      <div className={classes.OtherInnerItemBtn}>
                        <Button onClick={HandleFeatures}>
                          <div className={classes.OtherInnerItemLeft}>
                            <div className={classes.OtherInnerItemLeftIcon}>
                              <Paper />
                            </div>
                          </div>
                          <div className={classes.OtherInnerItemRight}>
                            <div className={classes.OtherInnerItemRightTitle}>
                              <div
                                className={classes.OtherInnerItemRightTitleText}
                              >
                                <h2>Xüsusiyyətlər</h2>
                                <span>Baxmaq üçün kliklə</span>
                              </div>
                              <div
                                className={classes.OtherInnerItemRightTitleBtn}
                              >
                                <span>+</span>
                              </div>
                            </div>
                          </div>
                        </Button>
                      </div>
                      <Collapse in={showFeatures}>
                        <div className={classes.OtherInnerItemRightText}>
                          <div
                            className={classes.OtherInnerItemRightTextFeatures}
                          >
                            <div className={classes.FeaturesInner}>
                              <div className={classes.FeaturesInnerTitle}>
                                <span>Şəbəkə</span>
                              </div>
                              <div className={classes.FeaturesInnerText}>
                                <ul>
                                  <li>LTE: Var</li>
                                  <li>5G: Var</li>
                                  <li>SIM-kart növü: Nano-SIM</li>
                                  <li>SIM-kart sayı: 1</li>
                                </ul>
                              </div>
                            </div>
                            <div className={classes.FeaturesInner}>
                              <div className={classes.FeaturesInnerTitle}>
                                <span>Akkumlyator</span>
                              </div>
                              <div className={classes.FeaturesInnerText}>
                                <ul>
                                  <li>Batareya tutumu (mAh): 3687</li>
                                  <li>
                                    Batareyanın növü: Non-removable Li-Ion
                                  </li>
                                  <li>Sürətli şarj 20W, 30 dəqiqəyə 50%</li>
                                  <li>Naqilsiz sürətli şarj 15W</li>
                                </ul>
                              </div>
                            </div>
                            <div className={classes.FeaturesInner}>
                              <div className={classes.FeaturesInnerTitle}>
                                <span>Ekran</span>
                              </div>
                              <div className={classes.FeaturesInnerText}>
                                <ul>
                                  <li>
                                    Ekran tipi: Super Retina XDR OLED, HDR10,
                                    800 nits (typ), 1200 nits (peak)
                                  </li>
                                  <li>
                                    Ekran ölçüsü: 6.7 inches, 109.8 cm2 (~87.4%
                                    screen-to-body ratio)
                                  </li>
                                  <li>
                                    Ekran imkanları: 1284 x 2778 pixels, 19.5:9
                                    ratio (~458 ppi density)
                                  </li>
                                </ul>
                              </div>
                            </div>
                            <div className={classes.FeaturesInner}>
                              <div className={classes.FeaturesInnerTitle}>
                                <span>Yaddaş</span>
                              </div>
                              <div className={classes.FeaturesInnerText}>
                                <ul>
                                  <li>LTE: Var</li>
                                  <li>5G: Var</li>
                                  <li>SIM-kart növü: Nano-SIM</li>
                                  <li>SIM-kart sayı: 1</li>
                                </ul>
                              </div>
                            </div>
                            <div className={classes.FeaturesInner}>
                              <div className={classes.FeaturesInnerTitle}>
                                <span>Korpus</span>
                              </div>
                              <div className={classes.FeaturesInnerText}>
                                <ul>
                                  <li>Ölçülər: 160.8 x 78.1 x 7.4 mm</li>
                                  <li>Çəki: 228 g</li>
                                  <li>
                                    Ön / arxa şüşə, paslanmayan polad çərçivə
                                  </li>
                                </ul>
                              </div>
                            </div>
                            <div className={classes.FeaturesInner}>
                              <div className={classes.FeaturesInnerTitle}>
                                <span>Prossesor</span>
                              </div>
                              <div className={classes.FeaturesInnerText}>
                                <ul>
                                  <li>
                                    Əməliyyat sistemi: iOS 14.1, upgradable to
                                    iOS 14.2
                                  </li>
                                  <li>
                                    Prosessor tipi (Chipset): Apple A14 Bionic
                                    (5 nm)
                                  </li>
                                  <li>
                                    Prosessor tezliyi (CPU): Hexa-core (2x3.1
                                    GHz Fm)
                                  </li>
                                  <li>Qrafik prosessor (GPU): Apple GPU</li>
                                  <li>Nüvə sayı: 6</li>
                                </ul>
                              </div>
                            </div>
                          </div>
                        </div>
                      </Collapse>
                    </div>
                    <div className={classes.OtherInnerItem}>
                      <div className={classes.OtherInnerItemBtn}>
                        <Button onClick={HandleComments}>
                          <div className={classes.OtherInnerItemLeft}>
                            <div className={classes.OtherInnerItemLeftIcon}>
                              <Comments />
                            </div>
                          </div>
                          <div className={classes.OtherInnerItemRight}>
                            <div className={classes.OtherInnerItemRightTitle}>
                              <div
                                className={classes.OtherInnerItemRightTitleText}
                              >
                                <h2>Rəylər</h2>
                                <span>12 baxılacaq rəy</span>
                              </div>
                              <div
                                className={classes.OtherInnerItemRightTitleBtn}
                              >
                                <span>+</span>
                              </div>
                            </div>
                          </div>
                        </Button>
                      </div>
                      <Collapse in={showComments}>
                        <div className={classes.OtherInnerItemRightText}>
                          <div
                            className={classes.OtherInnerItemRightTextComments}
                          >
                            <div className={classes.Comment}>
                              <div className={classes.CommentText}>
                                <p>
                                  Fringilla laoreet suspendisse pellentesque
                                  scelerisque diam eu massa. Purus pharetra
                                  sagittis id cum habitasse mattis tristique.
                                  Proin commodo diam odio et tempus neque,
                                  malesuada faucibus.
                                </p>
                              </div>
                              <div className={classes.CommentFooter}>
                                <div className={classes.CommentFooterUser}>
                                  <div
                                    className={classes.CommentFooterUserAvatar}
                                  >
                                    <Avatar alt="Ulviyya İmamova" />
                                  </div>
                                  <div
                                    className={classes.CommentFooterUserInfo}
                                  >
                                    <div className={classes.UserName}>
                                      <span>Ulviyya İmamova</span>
                                    </div>
                                    <div className={classes.Rate}>
                                      <Star /> <span>(5)</span>
                                    </div>
                                  </div>
                                </div>
                                <div className={classes.CommentFooterDate}>
                                  <span>29 Avq 2021</span>
                                </div>
                              </div>
                            </div>
                            <div className={classes.Comment}>
                              <div className={classes.CommentText}>
                                <p>
                                  Fringilla laoreet suspendisse pellentesque
                                  scelerisque diam eu massa. Purus pharetra
                                  sagittis id cum habitasse mattis tristique.
                                  Proin commodo diam odio et tempus neque,
                                  malesuada faucibus.
                                </p>
                              </div>
                              <div className={classes.CommentFooter}>
                                <div className={classes.CommentFooterUser}>
                                  <div
                                    className={classes.CommentFooterUserAvatar}
                                  >
                                    <Avatar alt="Rufat Babakishiyev" />
                                  </div>
                                  <div
                                    className={classes.CommentFooterUserInfo}
                                  >
                                    <div className={classes.UserName}>
                                      <span>Rufat Babakishiyev</span>
                                    </div>
                                    <div className={classes.Rate}>
                                      <Star /> <span>(5)</span>
                                    </div>
                                  </div>
                                </div>
                                <div className={classes.CommentFooterDate}>
                                  <span>29 Avq 2021</span>
                                </div>
                              </div>
                            </div>
                            <div className={classes.Comment}>
                              <div className={classes.CommentText}>
                                <p>
                                  Fringilla laoreet suspendisse pellentesque
                                  scelerisque diam eu massa. Purus pharetra
                                  sagittis id cum habitasse mattis tristique.
                                  Proin commodo diam odio et tempus neque,
                                  malesuada faucibus.
                                </p>
                              </div>
                              <div className={classes.CommentFooter}>
                                <div className={classes.CommentFooterUser}>
                                  <div
                                    className={classes.CommentFooterUserAvatar}
                                  >
                                    <Avatar alt="Ulviyya İmamova" />
                                  </div>
                                  <div
                                    className={classes.CommentFooterUserInfo}
                                  >
                                    <div className={classes.UserName}>
                                      <span>Ulviyya İmamova</span>
                                    </div>
                                    <div className={classes.Rate}>
                                      <Star /> <span>(5)</span>
                                    </div>
                                  </div>
                                </div>
                                <div className={classes.CommentFooterDate}>
                                  <span>29 Avq 2021</span>
                                </div>
                              </div>
                            </div>
                          </div>
                          <div className={classes.OtherInnerItemRightTextForm}>
                            <div className={classes.FormTitle}>
                              <span>Rəy yaz</span>
                            </div>
                            <div className={classes.FormText}>
                              <form>
                                <textarea
                                  placeholder="Fikirləriniz..."
                                  name="comment"
                                  cols="30"
                                  rows="10"
                                ></textarea>
                              </form>
                            </div>
                          </div>
                        </div>
                      </Collapse>
                    </div>
                    <div className={classes.OtherInnerItem}>
                      <div className={classes.OtherInnerItemBtn}>
                        <Button>
                          <div className={classes.OtherInnerItemLeft}>
                            <div className={classes.OtherInnerItemLeftIcon}>
                              <CallToAsk />
                            </div>
                          </div>
                          <div className={classes.OtherInnerItemRight}>
                            <div className={classes.OtherInnerItemRightTitle}>
                              <div
                                className={classes.OtherInnerItemRightTitleText}
                              >
                                <h2>Sualınız var?</h2>
                                <span>Zəng edin soruşun</span>
                              </div>
                              <div
                                className={
                                  classes.OtherInnerItemRightTitleCallBtn
                                }
                              >
                                <span>Zəng et</span>
                              </div>
                            </div>
                          </div>
                        </Button>
                      </div>
                    </div>
                  </div>
                </div>
                <div className={`${classes.OtherMobile} other-mobile`}>
                  <Box sx={{ width: "100%" }}>
                    <Box
                      sx={{
                        borderBottom: 1,
                        borderColor: "divider",
                        textTransform: "unset",
                      }}
                    >
                      <ThemeProvider theme={TabTheme}>
                        <Tabs
                          indicatorColor="primary"
                          scrollButtons="auto"
                          variant="scrollable"
                          value={value}
                          onChange={handleChange}
                        >
                          <CustomTab
                            label="Məhsul haqqında"
                            {...a11yProps(0)}
                            sx={{
                              fontFamily: "Poppins",
                              color: "var(--text-primary)",
                            }}
                          />
                          <CustomTab
                            label="Xüsusiyyətlər"
                            {...a11yProps(1)}
                            sx={{
                              fontFamily: "Poppins",
                              color: "var(--text-primary)",
                            }}
                          />
                          <CustomTab
                            label="Biz təklif edirik"
                            {...a11yProps(2)}
                            sx={{
                              fontFamily: "Poppins",
                              color: "var(--text-primary)",
                            }}
                          />
                        </Tabs>
                      </ThemeProvider>
                    </Box>
                    <TabPanel
                      value={value}
                      index={0}
                      style={{
                        color: "var(--text-primary)",
                        fontFamily: "Poppins",
                      }}
                    >
                      Fringilla laoreet suspendisse pellentesque scelerisque
                      diam eu massa. Purus pharetra sagittis id cum habitasse
                      mattis tristique. Proin commodo diam odio et tempus neque,
                      malesuada faucibus. Vitae donec id interdum faucibus
                      aliquam. Ut diam cras eros sed pharetra, odio fames
                      pellentesque aliquam. Bibendum non, dui lorem vitae. Risus
                      id praesent pellentesque egestas volutpat. Integer
                      habitant lectus consectetur ipsum et. Amet felis est
                      mauris elementum vel tincidunt pharetra, pretium. Faucibus
                      nulla quam dictum aliquet.
                    </TabPanel>
                    <TabPanel
                      value={value}
                      index={1}
                      style={{
                        color: "var(--text-primary)",
                      }}
                    >
                      Item Two
                    </TabPanel>
                    <TabPanel
                      value={value}
                      index={2}
                      style={{
                        color: "var(--text-primary)",
                      }}
                    >
                      Item Three
                    </TabPanel>
                  </Box>
                </div>
              </div>
              {/* 9 */}
              <div className={classes.PurchaseContainerSectionSecondCheckout}>
                <span className={classes.PurchaseContainerSectionSecondCheckoutTitle}>
                  CƏMI:
                </span>
                <br />
                <div className={classes.SecondCheckoutTop}>
                  <div className={classes.SecondCheckoutTopLeft}>
                    <span className={classes.SecondCheckoutTopLeftBig}>
                      3969 <Manat />
                    </span>
                  </div>
                  <div className={classes.SecondCheckoutTopRight}>
                    <Button className={classes.SecondCheckoutTopRightBasket}>
                      <AddToShoppingCar /> <span>Səbətə əlavə et</span>
                    </Button>
                    <Button className={classes.SecondCheckoutTopRightFavorites}>
                      <Heart />
                    </Button>
                  </div>
                </div>
                <div className={classes.SecondCheckoutBottom}>
                  <div className={classes.SecondCheckoutBottomButtons}>
                    <Button
                      className={classes.BuyRightNow}
                      onClick={() => {
                        setOneClickToggleBottom(!oneClickToggleBottom);
                        setCreditToggleBottom(false);
                      }}
                    >
                      <p
                        className={classes.OneClick}
                        style={{
                          background: `${
                            oneClickToggleBottom ? "#ea2427" : "#E9E9E9"
                          }`,
                        }}
                      >
                        <Linfo
                          style={{
                            color: `${
                              oneClickToggleBottom ? "#ffffff" : "#000000"
                            }`,
                          }}
                        />
                      </p>
                      <div className={classes.Text}>
                        <p>Bir kliklə al</p>
                        <p>Sürətli sifariş</p>
                      </div>
                      {oneClickToggleBottom && (
                        <span className={classes.Arrow}></span>
                      )}
                    </Button>
                    <Button
                      className={classes.BuyWithCredit}
                      onClick={() => {
                        setCreditToggleBottom(!creditToggleBottom);
                        setOneClickToggleBottom(false);
                      }}
                    >
                      <p
                        className={classes.Credit}
                        style={{
                          background: `${
                            creditToggleBottom ? "#ea2427" : "#E9E9E9"
                          }`,
                        }}
                      >
                        <Linfo
                          style={{
                            color: `${
                              creditToggleBottom ? "#ffffff" : "#000000"
                            }`,
                          }}
                        />
                      </p>
                      <div className={classes.Text}>
                        <p>Hissə-hissə ödə</p>
                        <p>18 ay 269 <Manat/></p>
                      </div>
                      {creditToggleBottom && (
                        <span className={classes.Arrow}></span>
                      )}
                    </Button>
                  </div>
                  <div className={classes.SecondCheckoutBottomCollapse}>
                    <Collapse in={oneClickToggleBottom}>
                      <div className={classes.PurchaseItemForm}>
                        <div className={classes.PurchaseItemFormTitle}>
                          <span>Telefon nömrəni yaz, sənə zəng edək!</span>
                        </div>
                        <div>
                          <form action="">
                            <input
                              placeholder="Sənə necə səslənək?"
                              name="name"
                              type="text"
                            />
                            <input
                              placeholder="Telefon"
                              name="number"
                              type="text"
                            />
                            <button type="submit">Göndər</button>
                          </form>
                        </div>
                      </div>
                    </Collapse>
                    <Collapse in={creditToggleBottom}>
                      <div className={classes.PurchaseItemCredit}>
                        <div className="credit-tab">
                          <ProductTab />
                        </div>
                      </div>
                    </Collapse>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>

      <ProductEvent />

      <SwipeableEdgeDrawerDrawer />
    </Layout>
  );
}
