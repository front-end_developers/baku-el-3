import Layout from "../../src/hoc/Layout/Layout/Layout";
import classes from "./index.module.scss";
import ReactSelect from "../../src/components/ReactSelect/ReactSelect";
import { Container, Row, Col } from "react-bootstrap";
import Button from "@mui/material/Button";
import { Location2, MapPin } from "../../src/components/Icons";
import { ButtonBase } from "@mui/material";
import Link from "next/link";
import MobileBottomNavbar from "../../src/components/MobileBottomNavbar/MobileBottomNavbar";


export default function Index() {
  return (
    <Layout>
      <div className={`${classes.ShopPage} ShopPage`}>
        <div className={classes.ShopPageTitle}>
          <h5>Mağazalar</h5>
          <div className={classes.FormContent}>
            <form action="#">
              <div className={classes.FormGroup}>
                <ReactSelect />
              </div>
            </form>

            <ButtonBase sx={{ borderRadius: "12px" }}>
              <Link href="/shops/map">
                <a className={classes.Map}>
                  <Location2 />
                  Xəritədə bax
                </a>
              </Link>
            </ButtonBase>
          </div>
        </div>

        <Row className={`mt-4 mb-5 ${classes.ShopPageBody}`} style={{rowGap: "20px"}}>
          <Col sm={12} md={6} lg={4} xl={4}>
            <div className={classes.LocationBox}>
              <MapPin />
              <h1>28 may filialı</h1>
              <p>Ünvan:</p>
              <span>Bakı ş., Nəsimi rayonu, Füzuli küç. 73</span>
              <p>Telefon:</p>
              <span>143</span>
              <p>İş saatı:</p>
              <span>Hər gün 09:00-dan 20:30-dək</span>
            </div>
          </Col>
          <Col sm={12} md={6} lg={4} xl={4}>
            <div className={classes.LocationBox}>
              <MapPin />
              <h1>28 may filialı</h1>
              <p>Ünvan:</p>
              <span>Bakı ş., Nəsimi rayonu, Füzuli küç. 73</span>
              <p>Telefon:</p>
              <span>143</span>
              <p>İş saatı:</p>
              <span>Hər gün 09:00-dan 20:30-dək</span>
            </div>
          </Col>
          <Col sm={12} md={6} lg={4} xl={4}>
            <div className={classes.LocationBox}>
              <MapPin />
              <h1>28 may filialı</h1>
              <p>Ünvan:</p>
              <span>Bakı ş., Nəsimi rayonu, Füzuli küç. 73</span>
              <p>Telefon:</p>
              <span>143</span>
              <p>İş saatı:</p>
              <span>Hər gün 09:00-dan 20:30-dək</span>
            </div>
          </Col>
          <Col sm={12} md={6} lg={4} xl={4}>
            <div className={classes.LocationBox}>
              <MapPin />
              <h1>28 may filialı</h1>
              <p>Ünvan:</p>
              <span>Bakı ş., Nəsimi rayonu, Füzuli küç. 73</span>
              <p>Telefon:</p>
              <span>143</span>
              <p>İş saatı:</p>
              <span>Hər gün 09:00-dan 20:30-dək</span>
            </div>
          </Col>
          <Col sm={12} md={6} lg={4} xl={4}>
            <div className={classes.LocationBox}>
              <MapPin />
              <h1>28 may filialı</h1>
              <p>Ünvan:</p>
              <span>Bakı ş., Nəsimi rayonu, Füzuli küç. 73</span>
              <p>Telefon:</p>
              <span>143</span>
              <p>İş saatı:</p>
              <span>Hər gün 09:00-dan 20:30-dək</span>
            </div>
          </Col>
          <Col sm={12} md={6} lg={4} xl={4}>
            <div className={classes.LocationBox}>
              <MapPin />
              <h1>28 may filialı</h1>
              <p>Ünvan:</p>
              <span>Bakı ş., Nəsimi rayonu, Füzuli küç. 73</span>
              <p>Telefon:</p>
              <span>143</span>
              <p>İş saatı:</p>
              <span>Hər gün 09:00-dan 20:30-dək</span>
            </div>
          </Col>
        </Row>
      </div>
      <MobileBottomNavbar/>
    </Layout>
  );
}
