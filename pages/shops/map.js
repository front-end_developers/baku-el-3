import * as React from "react";
import { useState } from "react";
import Layout from "../../src/hoc/Layout/Layout/Layout";
import classes from "./map.module.scss";
// import { GoogleMap, useJsApiLoader } from '@react-google-maps/api';
import ReactSelect from "../../src/components/ReactSelect/ReactSelect";
import Button from "@mui/material/Button";
import { Paper, MapPin } from "../../src/components/Icons";

import { YMaps, Map, Placemark, TypeSelector } from "react-yandex-maps";
import { ButtonBase } from "@mui/material";
import Link from "next/link";
import MobileBottomNavbar from "../../src/components/MobileBottomNavbar/MobileBottomNavbar";

// console.log(YMaps)
const center = [40.373228, 49.833469];

export default function Index({ clickOnMap }) {
  // console.log(clickOnMap);
  // const [isActive, setIsActive] = useState(true);

  var points = [
    {
      id: 1,
      title: "28 may filialı",
      descr: "Some description",
      coords: [40.401846, 49.838598],
    },
    {
      id: 2,
      title: "28 may filialı",
      address: "Bakı ş., Nəsimi rayonu, Füzuli küç. 73",
      phone: "143",
      time: "Hər gün 09:00-dan 20:30-dək",
      descr: "Some description",
      coords: [40.404534, 49.870043],
    },
    {
      id: 3,
      title: "İnşaatçılar",
      address: "Bakı ş., Nəsimi rayonu, Füzuli küç. 73",
      phone: "143",
      time: "Hər gün 09:00-dan 20:30-dək",
      descr: "Some description",
      coords: [40.38983, 49.802651],
    },
    {
      id: 4,
      title: "Azadlıq filialı",
      address: "Bakı ş., Nəsimi rayonu, Füzuli küç. 73",
      phone: "143",
      time: "Hər gün 09:00-dan 20:30-dək",
      descr: "Some description",
      coords: [40.4212186, 49.8198976],
    },
    {
      id: 5,
      title: "Zabrat filialı",
      address: "Bakı ş., Nəsimi rayonu, Füzuli küç. 73",
      phone: "143",
      time: "Hər gün 09:00-dan 20:30-dək",
      descr: "Some description",
      coords: [40.466845, 49.945076],
    },
    {
      id: 6,
      title: "Xırdalan filialı",
      address: "Bakı ş., Nəsimi rayonu, Füzuli küç. 73",
      phone: "143",
      time: "Hər gün 09:00-dan 20:30-dək",
      descr: "Some description",
      coords: [40.45696, 49.756464],
    },
  ];

  return (
    <Layout>
      <div className={`${classes.MapPage} MapPage`}>
        <div className={classes.MyPageWrapper}>
          <div className={classes.MapPageTitle}>
            <h1>Mağazalar</h1>
            <div className={classes.FormContent}>
              <form action="#">
                <div className={classes.FormGroup}>
                  <ReactSelect />
                </div>
              </form>

              <ButtonBase sx={{ borderRadius: "12px" }}>
                <Link href="/shops">
                  <a className={classes.Map}>
                    <Paper />
                    Listə bax
                  </a>
                </Link>
              </ButtonBase>
            </div>
          </div>
          <YMaps
            query={{
              lang: "en-AZ",
            }}
          >
            <Map
              defaultState={{
                center: center,
                zoom: 9,
                controls: [
                  "zoomControl",
                  "fullscreenControl",
                  "geolocationControl",
                ],
                type: "yandex#map",
              }}
              modules={[
                "control.ZoomControl",

                "control.FullscreenControl",
                "control.GeolocationControl",
              ]}
              width="100%"
              height="400px"
            >
              <TypeSelector
                options={{
                  float: "left",
                }}
              />
              {points.map((point) => {
                return (
                  <Placemark
                    key={point.id}
                    geometry={point.coords}
                    defaultOptions={{
                      iconLayout: "default#image",
                      iconImageHref: "/img/mapPinSvg.svg",
                      iconImageSize: [32, 32],
                    }}
                    defaultProperties={{
                      balloonContentBody: `<div className={classes.MapContent}>
              <img style="width:32px" src="/img/LocationPin.png"/>
              <h1>${point.title}</h1>
              <p style="font-size:14px;font-weight:600 ;margin-top:5px">Ünvan:</p>
              <span style="opacity:80%">${point.address}</span>
              <p style="font-size:14px;font-weight:600;margin-top:15px">Telefon:</p>
              <span style="opacity:80%">${point.phone}</span>
              <p style="font-size:14px;font-weight:600;margin-top:15px">İş saatı:</p>
              <span style="opacity:80%">${point.time}</span>
              </div>`,
                    }}
                    modules={[
                      "geoObject.addon.balloon",
                      "geoObject.addon.hint",
                    ]}
                  />
                );
              })}

              {/* {points &&
            points.map(
              (m) => (
                console.log(m),
                (
                  <Placemark
                    key={m.id}
                    defaultGeometry={m.coords}
                    defaultOptions={{
                      iconLayout: "default#image",
                      iconImageHref: "/img/AppleImage.png",
                      iconImageSize: [32, 32],
                    }}
                    defaultProperties={{
                      balloonContentBody: `<strong>${m.title}</strong>`,
                      hintContent: `${m.descr}`,
                    }}
                    modules={[
                      "geoObject.addon.balloon",
                      "geoObject.addon.hint",
                    ]}
                  />
                )
              )
            )} */}
            </Map>
          </YMaps>
        </div>
      </div>

      <div className={classes.MapEnd}></div>
      <MobileBottomNavbar />
    </Layout>
  );
}
