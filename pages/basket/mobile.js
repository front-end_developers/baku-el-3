import { Button, styled } from "@mui/material";
import Link from "next/link";
import React from "react";
import BasketItemMobile from "../../src/components/BasketItemMobile/BasketItemMobile";
import Layout from "../../src/hoc/Layout/Layout/Layout";
import classes from "./mobile.module.scss";
import MobileBottomNavbar from '../../src/components/MobileBottomNavbar/MobileBottomNavbar'


function BasketMobile() {
  const StyledSubmitBtn = styled((props) => <a {...props} />)({
    borderRadius: "12px",
    border: 0,
    padding: " 10px 20px",
    backgroundColor: " var(--text-primary)",
    color: "var(--f-to-0)",
    textTransform: "inherit",
    fontFamily: "Poppins",
    display: "inline-block",
    marginTop: "40px",

    "&:hover": {
      cursor: "pointer",
      color: "var(--f-to-0)",
    },
  });
  return (
    <Layout>
      <div className={classes.BasketMobile}>
        <div className={classes.Top}>
          <h1 className={classes.Header}>Mənim səbətim</h1>
          <span className={classes.Count}>3 Məhsul</span>
        </div>
        <form>
          <div className={classes.BasketMobileContent}>
            <div className={classes.BasketMobileContentItem}>
              <BasketItemMobile />
            </div>
            <div className={classes.BasketMobileContentItem}>
              <BasketItemMobile />
            </div>
            <div className={classes.BasketMobileContentItem}>
              <BasketItemMobile />
            </div>
          </div>

          <Link href="/basket">
            <StyledSubmitBtn>Sifarişi rəsmiləşdir</StyledSubmitBtn>
          </Link>
        </form>
      </div>
      <MobileBottomNavbar/>
    </Layout>
  );
}

export default BasketMobile;
