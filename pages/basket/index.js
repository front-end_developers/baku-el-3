/* eslint-disable @next/next/no-img-element */
/* eslint-disable jsx-a11y/alt-text */
import Layout from "../../src/hoc/Layout/Layout/Layout";
import MobileBottomNavbar from "../../src/components/MobileBottomNavbar/MobileBottomNavbar";
import classes from "./index.module.scss";
import {
  Promocode,
  FilledBag,
  Box,
  CreditCard,
  Delete,
  AngleDown,
  Archer,
  Check,
  BasketCard,
  BasketCash,
  BasketTaksit,
  BasketNisye,
  Manat,
} from "../../src/components/Icons";
import {
  Stepper,
  Step,
  StepLabel,
  StepConnector,
  styled,
  Button,
  TextField,
  Select,
  MenuItem,
  Tabs,
  Tab,
  ButtonBase,
  InputAdornment,
} from "@mui/material";
import { useEffect, useRef, useState } from "react";
import { useFormik } from "formik";
import * as Yup from "yup";
import Breadcrumb from "../../src/components/Breadcrumb/Breadcrumb";
import { TextFields } from "@mui/icons-material";
import Barcode from "react-barcode";

const StyledStep = styled((props) => <Step {...props} />)({
  padding: 0,
  "& svg": {
    color: "red",
    height: "24px",
    width: "24px",
  },
  "& .Mui-disabled": {
    "& svg": {
      color: "gray",
    },
  },
  "& .MuiStepLabel-iconContainer": {
    padding: 0,
  },
});
const StyledConnector = styled((props) => <StepConnector {...props} />)({
  "& span": {
    borderColor: "gray",
    // border: " 1px solid gray",
  },
  "&.Mui-active, &.Mui-completed": {
    "& span": {
      borderColor: "red",
    },
  },
});

const StyledTextField = styled((props) => <TextField {...props} />)({
  paddingRight: "1px",
  ["@media(max-width: 640px)"]: {
    "& label": {
      fontSize: "13px!important",
    },
  },
  "& .MuiOutlinedInput-input": {
    padding: "10px 24px",
    color: "red",
    fontFamily: "Poppins",
    color: "var(--text-primary)",
    fontSize: "14px",
  },

  ".MuiOutlinedInput-notchedOutline": {
    top: 0,
  },

  "& fieldset": {
    width: "100%",
    border: "none",
    outline: "1px solid #b3b3b3",
    borderRadius: "16px",
  },
  "& label": {
    fontFamily: "Poppins",
    color: "var(--text-primary)",
    backgroundColor: "var(--user-sidebar-main)",
    paddingBlock: "2px",
    paddingInline: "10px",
    marginTop: "-8px",
    fontSize: "14px",
    color: "var(--text-primary) !important",
  },
});

const StyledDeleteButton = styled((props) => <Button {...props} />)({
  fontFamily: "unset",
  width: "96px",
  height: "44px",
  background: "#ffffff",
  borderRadius: "12px",
  svg: { color: "red" },

  "&:hover": {
    background: "#ffffff",

    "& svg": {
      transform: "scale(1.1)",
    },
  },

  ["@media (max-width: 1200px)"]: {
    width: "75px",
  },
});

const StyledOrderButton = styled((props) => <Button {...props} />)({
  fontFamily: "unset",
  borderRadius: "12px",
  background: "var(--text-primary)",
  padding: "12px 24px",
  color: "var(--f-to-0);",
  fontWeight: "500",
  fontSize: "14px",
  textTransform: "unset",

  "&:hover": {
    background: "var(--text-primary)",
  },
});

const StyledSelect = styled((props) => <Select {...props} />)({
  width: "100%",
  border: "1px solid #b3b3b3",
  borderRadius: "16px",
  paddingInline: "24px",

  "& .MuiSelect-select": {
    fontWeight: "500",
    fontSize: "14px",
    padding: "10px 0",
    fontFamily: "Poppins",
    color: "var(--text-primary)",
    fontWeight: "500",
  },

  "& svg": {
    width: "10px",
    color: "var(--red-main)",
    transition: "0.2s",
  },
  "& [aria-expanded='true']~svg": {
    transform: "rotate(-180deg)",
  },
  "& fieldset": {
    display: "none",
  },
});

const StyledTabs = styled((props) => <Tabs {...props} />)({
  minHeight: "unset",
  margin: "6px 0 38px 0",
  // background: "red",
  // border: "1px solid red"
  "& .MuiTabs-indicator": {
    background: "var(--red-main)",
  },
  "& .MuiTabs-flexContainer": {
    gap: "24px",
    "& .MuiButtonBase-root": {
      padding: "8px 0",
      minHeight: "unset",
      minWidth: "unset",
      color: "var(--text-primary)",
      opacity: "0.5",
      fontWeight: "500",
      fontSize: "14px",
      textTransform: "unset",
      "&.Mui-selected": { opacity: "1", fontWeight: "600" },
    },
  },
});

const StyledTaksitTabs = styled((props) => <Tabs {...props} />)({
  // background: "red",
  "& .MuiTabs-indicator": {
    display: "none",
  },
  "& .MuiTabs-flexContainer": {
    gap: "16px",
    "& .MuiButtonBase-root": {
      display: "flex",
      flexDirection: "row-reverse",
      gap: "15px",
      minHeight: "unset",
      minWidth: "unset",
      fontFamily: "unset",
      padding: " 3px 3px 3px 18px",
      borderRadius: "35px",
      backgroundColor: "var(--user-tab-btn-bg)",
      fontWeight: "600",
      fontSize: "12px",
      textTransform: "unset",
      color: "var(--text-primary)",

      "& .TabCircle": {
        backgroundColor: "white",
        color: "#333",
        borderRadius: "50px",
        height: "30px",
        width: "30px",
        display: "flex",
        alignItems: "center",
        justifyContent: "center",

        "& img": {
          height: "100%",
          width: "100%",
          objectFit: "contain",
        },
      },
      "&.Mui-selected": {
        backgroundColor: "var(--user-tab-btn-bg-selected)",
        color: "#ffffff",
        border: "1px solid gray",
      },
    },
  },
});

function TabCircle(props) {
  const { imgUrl } = props;
  return (
    <div className={`${classes.TabCircle} TabCircle`}>
      {/* <span className={classes.TabCircleContent}>{num}</span> */}
      <img src={imgUrl} alt="" />
    </div>
  );
}

function TabPanel(props) {
  const { children, value, index, ...other } = props;

  return (
    <div
      role="tabpanel"
      hidden={value !== index}
      id={`simple-tabpanel-${index}`}
      aria-labelledby={`simple-tab-${index}`}
      {...other}
    >
      {value === index && <div>{children}</div>}
    </div>
  );
}

function Index() {
  // useEffect(() => {console.log(myRef)}, []);
  const [showBack, setShowBack] = useState();

  const DeliveryRef = useRef(null);
  const PayementRef = useRef(null);
  const [age, setAge] = useState("10");
  const [value, setValue] = useState(0);
  const [tabValue, setTabValue] = useState(0);

  const handleChangeTab = (event, newValue) => {
    setValue(newValue);
  };
  const handleChangeCardTab = (event, newValue) => {
    setTabValue(newValue);
  };
  const handleChange = (event) => {
    setAge(event.target.value);
  };

  function a11yProps(index) {
    return {
      id: `simple-tab-${index}`,
      "aria-controls": `simple-tabpanel-${index}`,
    };
  }

  const executeScroll = (ref) => {
    // this function will scroll the given element to the top of the page
    let elemPosition = ref.current.getBoundingClientRect().top;
    let pageScrollY = window.scrollY;

    window.scrollTo({
      top: elemPosition + pageScrollY - 100,
      behavior: "smooth",
    });
  };

  const initialValues = {
    bonus: false,
    card: false,
    cash: false,
    taksit: false,
    nisye: false,
  };
  const CreateSchema = Yup.object().shape({
    bonus: Yup.string(),
    card: Yup.string(),
    cash: Yup.string(),
    taksit: Yup.string(),
    nisye: Yup.string(),
  });
  const resetForm = () => {
    // onHide();
    formik.resetForm();
  };
  const formik = useFormik({
    initialValues,
    validationSchema: CreateSchema,
    onSubmit: (values, { resetForm, setSubmitting }) => {
      // onPostGroup(values, resetForm, setSubmitting);
    },
  });
  // console.log(formik.values.bonus);
  // console.log(formik);

  const getInputClasses = (filedName) => {
    if (formik.touched[filedName] && formik.errors[filedName]) {
      return true;
    }
    if (formik.touched[filedName] && !formik.errors[filedName]) {
      return false;
    }
    return false;
  };
  return (
    <Layout>
      <div className={`${classes.Basket} Basket`}>
        <div className={classes.BasketWrapper}>
          {/* basket left form */}
          <form
            className={classes.BasketForm}
            onSubmit={(e) => e.preventDefault()}
          >
            <div className={classes.BasketFormProducts}>
              <div className={classes.BasketFormProductsTop}>
                <h1 className={classes.BasketFormProductsTopHeader}>Səbət</h1>
                <div className={classes.BasketFormProductsTopStepper}>
                  <Stepper activeStep={0} connector={<StyledConnector />}>
                    <StyledStep>
                      <StepLabel StepIconComponent={FilledBag}></StepLabel>
                    </StyledStep>
                    <StyledStep>
                      <StepLabel StepIconComponent={Box}></StepLabel>
                    </StyledStep>
                    <StyledStep>
                      <StepLabel StepIconComponent={CreditCard}></StepLabel>
                    </StyledStep>
                  </Stepper>
                </div>
              </div>
              <div className={classes.BasketFormProductsItem}>
                <div className={classes.BasketFormProductsItemLeft}>
                  <div className={classes.ItemImg}>
                    <img src="/img/iphone-13.png" alt="" />
                  </div>
                  <div className={classes.ItemBody}>
                    <span className={classes.ItemBodyTitle}>
                      Iphone 13 Pro Max 128GB Blue
                    </span>
                    <div className={classes.ItemBodyPrice}>
                      <span>
                        3699 <Manat />
                      </span>
                      <br />
                      <span>
                        3499 <Manat />
                      </span>
                    </div>
                  </div>
                </div>
                <div className={classes.BasketFormProductsItemRight}>
                  <div className={classes.ItemAmount}>
                    <button>-</button>
                    <input type="number" value="1" />
                    <button>+</button>
                  </div>
                  <div className={classes.ItemRemove}>
                    <StyledDeleteButton>
                      <Delete />
                    </StyledDeleteButton>
                  </div>
                </div>
              </div>
              <div className={classes.BasketFormProductsItem}>
                <div className={classes.BasketFormProductsItemLeft}>
                  <div className={classes.ItemImg}>
                    <img src="/img/iphone-13.png" alt="" />
                  </div>
                  <div className={classes.ItemBody}>
                    <span className={classes.ItemBodyTitle}>
                      Iphone 13 Pro Max 128GB Blue
                    </span>
                    <div className={classes.ItemBodyPrice}>
                      <span>
                        3699 <Manat />
                      </span>
                      <br />
                      <span>
                        3499 <Manat />
                      </span>
                    </div>
                  </div>
                </div>
                <div className={classes.BasketFormProductsItemRight}>
                  <div className={classes.ItemAmount}>
                    <button>-</button>
                    <input type="number" value="1" />
                    <button>+</button>
                  </div>
                  <div className={classes.ItemRemove}>
                    <StyledDeleteButton>
                      <Delete />
                    </StyledDeleteButton>
                  </div>
                </div>
              </div>
              <div className={classes.BasketFormProductsItem}>
                <div className={classes.BasketFormProductsItemLeft}>
                  <div className={classes.ItemImg}>
                    <img src="/img/iphone-13.png" alt="" />
                  </div>
                  <div className={classes.ItemBody}>
                    <span className={classes.ItemBodyTitle}>
                      Iphone 13 Pro Max 128GB Blue
                    </span>
                    <div className={classes.ItemBodyPrice}>
                      <span>
                        3699 <Manat />
                      </span>
                      <br />
                      <span>
                        3499 <Manat />
                      </span>
                    </div>
                  </div>
                </div>
                <div className={classes.BasketFormProductsItemRight}>
                  <div className={classes.ItemAmount}>
                    <button>-</button>
                    <input type="number" value="1" />
                    <button>+</button>
                  </div>
                  <div className={classes.ItemRemove}>
                    <StyledDeleteButton>
                      <Delete />
                    </StyledDeleteButton>
                  </div>
                </div>
              </div>
              <div className={classes.BasketFormProductsOrderItems}>
                <StyledOrderButton onClick={() => executeScroll(DeliveryRef)}>
                  Sifariş et
                </StyledOrderButton>
              </div>
            </div>
            <div ref={DeliveryRef} className={classes.BasketFormDelivery}>
              <div className={classes.BasketFormDeliveryTop}>
                <h2 className={classes.BasketFormDeliveryTopHeader}>
                  Çatdırılma detalları
                </h2>
                <div className={classes.BasketFormDeliveryTopStepper}>
                  <Stepper activeStep={1} connector={<StyledConnector />}>
                    <StyledStep>
                      <StepLabel StepIconComponent={FilledBag}></StepLabel>
                    </StyledStep>
                    <StyledStep>
                      <StepLabel StepIconComponent={Box}></StepLabel>
                    </StyledStep>
                    <StyledStep>
                      <StepLabel StepIconComponent={CreditCard}></StepLabel>
                    </StyledStep>
                  </Stepper>
                </div>
              </div>
              <StyledTabs
                value={value}
                onChange={handleChangeTab}
                aria-label="basic tabs example"
                variant="scrollable"
              >
                <Tab label="Ünvana çatdırılma" {...a11yProps(0)} />
                <Tab label="Mağazadan götürəcəm" {...a11yProps(1)} />
              </StyledTabs>

              <TabPanel value={value} index={0}>
                <div className={`row ${classes.BasketFormDeliveryInputs}`}>
                  <div
                    className={`col-12 col-sm-6 col-md-6 col-lg-6  ${classes.InputGroup}`}
                  >
                    <StyledTextField
                      variant="outlined"
                      id="Type"
                      // name="Type"
                      label="Ad"
                      fullWidth
                      placeholder="Ad"
                      autoComplete="off"
                      // error={getInputClasses("type")}
                      // {...formik.getFieldProps("type")}
                    />
                    {formik.touched.type && formik.errors.type ? (
                      <div className="fv-plugins-message-container">
                        <div className="fv-help-block">
                          {formik.errors.type}
                        </div>
                      </div>
                    ) : null}
                  </div>
                  <div
                    className={`col-12 col-sm-6 col-md-6 col-lg-6  ${classes.InputGroup}`}
                  >
                    <StyledTextField
                      variant="outlined"
                      id="Type"
                      // name="Type"
                      label="Soyad"
                      fullWidth
                      placeholder="Soyad"
                      autoComplete="off"
                      // error={getInputClasses("type")}
                      // {...formik.getFieldProps("type")}
                    />
                    {formik.touched.type && formik.errors.type ? (
                      <div className="fv-plugins-message-container">
                        <div className="fv-help-block">
                          {formik.errors.type}
                        </div>
                      </div>
                    ) : null}
                  </div>
                  <div
                    className={`col-12 col-sm-6 col-md-6 col-lg-6  ${classes.InputGroup}`}
                  >
                    <StyledTextField
                      variant="outlined"
                      id="Type"
                      // name="Type"
                      label="Telefon nömrəsi"
                      fullWidth
                      placeholder="Telefon nömrəsi"
                      autoComplete="off"
                      // error={getInputClasses("type")}
                      // {...formik.getFieldProps("type")}
                    />
                    {formik.touched.type && formik.errors.type ? (
                      <div className="fv-plugins-message-container">
                        <div className="fv-help-block">
                          {formik.errors.type}
                        </div>
                      </div>
                    ) : null}
                  </div>
                  <div
                    className={`col-12 col-sm-6 col-md-6 col-lg-6  ${classes.InputGroup}`}
                  >
                    <StyledTextField
                      variant="outlined"
                      id="Type"
                      // name="Type"
                      label="E-poçt"
                      fullWidth
                      placeholder="E-poçt"
                      autoComplete="off"
                      // error={getInputClasses("type")}
                      // {...formik.getFieldProps("type")}
                    />
                    {formik.touched.type && formik.errors.type ? (
                      <div className="fv-plugins-message-container">
                        <div className="fv-help-block">
                          {formik.errors.type}
                        </div>
                      </div>
                    ) : null}
                  </div>
                  <div className={`col-12 ${classes.InputGroup}`}>
                    <div className="col-12 col-sm-6 col-md-6 col-lg-6 pe-lg-3 pe-md-3 pe-sm-0">
                      <StyledSelect
                        value={age}
                        onChange={handleChange}
                        IconComponent={() => <AngleDown />}
                        label=""
                        // placeholder="SELECT"
                        // labelId="demo-simple-select-label-ASLAN"
                        id="demo-simple-select"
                      >
                        <MenuItem value={10}>Şəhər </MenuItem>
                        <MenuItem value={20}>Şəhər</MenuItem>
                        <MenuItem value={30}>Şəhər</MenuItem>
                      </StyledSelect>
                      {formik.touched.type && formik.errors.type ? (
                        <div className="fv-plugins-message-container">
                          <div className="fv-help-block">
                            {formik.errors.type}
                          </div>
                        </div>
                      ) : null}
                    </div>
                  </div>
                  <div
                    className={`col-12 col-sm-6 col-md-6 col-lg-6  ${classes.InputGroup}`}
                  >
                    <StyledTextField
                      variant="outlined"
                      id="Type"
                      // name="Type"
                      label="Dəqiq ünvan"
                      fullWidth
                      placeholder="Dəqiq ünvan"
                      autoComplete="off"
                      // error={getInputClasses("type")}
                      // {...formik.getFieldProps("type")}
                    />
                    {formik.touched.type && formik.errors.type ? (
                      <div className="fv-plugins-message-container">
                        <div className="fv-help-block">
                          {formik.errors.type}
                        </div>
                      </div>
                    ) : null}
                  </div>
                  <div
                    className={`col-6 col-sm-3 col-md-2 col-lg-2  ${classes.InputGroup}`}
                  >
                    <StyledTextField
                      variant="outlined"
                      id="Type"
                      // name="Type"
                      label="ZİP kod"
                      fullWidth
                      placeholder="ZİP kod"
                      autoComplete="off"
                      // error={getInputClasses("type")}
                      // {...formik.getFieldProps("type")}
                    />
                    {formik.touched.type && formik.errors.type ? (
                      <div className="fv-plugins-message-container">
                        <div className="fv-help-block">
                          {formik.errors.type}
                        </div>
                      </div>
                    ) : null}
                  </div>
                </div>
                <div className={classes.BasketFormDeliveryButton}>
                  <StyledOrderButton onClick={() => executeScroll(PayementRef)}>
                    Ödənişə keç
                  </StyledOrderButton>
                </div>
              </TabPanel>
              <TabPanel value={value} index={1}>
                <div className={`row ${classes.BasketFormDeliveryInputs}`}>
                  <div
                    className={`col-12 col-sm-6 col-md-6 col-lg-6  ${classes.InputGroup}`}
                  >
                    <StyledTextField
                      variant="outlined"
                      id="Type"
                      // name="Type"
                      label="Ad"
                      fullWidth
                      placeholder="Ad"
                      autoComplete="off"
                      // error={getInputClasses("type")}
                      // {...formik.getFieldProps("type")}
                    />
                    {formik.touched.type && formik.errors.type ? (
                      <div className="fv-plugins-message-container">
                        <div className="fv-help-block">
                          {formik.errors.type}
                        </div>
                      </div>
                    ) : null}
                  </div>
                  <div
                    className={`col-12 col-sm-6 col-md-6 col-lg-6  ${classes.InputGroup}`}
                  >
                    <StyledTextField
                      variant="outlined"
                      id="Type"
                      // name="Type"
                      label="Soyad"
                      fullWidth
                      placeholder="Soyad"
                      autoComplete="off"
                      // error={getInputClasses("type")}
                      // {...formik.getFieldProps("type")}
                    />
                    {formik.touched.type && formik.errors.type ? (
                      <div className="fv-plugins-message-container">
                        <div className="fv-help-block">
                          {formik.errors.type}
                        </div>
                      </div>
                    ) : null}
                  </div>
                  <div
                    className={`col-12 col-sm-6 col-md-6 col-lg-6  ${classes.InputGroup}`}
                  >
                    <StyledTextField
                      variant="outlined"
                      id="Type"
                      // name="Type"
                      label="Telefon nömrəsi"
                      fullWidth
                      placeholder="Telefon nömrəsi"
                      autoComplete="off"
                      // error={getInputClasses("type")}
                      // {...formik.getFieldProps("type")}
                    />
                    {formik.touched.type && formik.errors.type ? (
                      <div className="fv-plugins-message-container">
                        <div className="fv-help-block">
                          {formik.errors.type}
                        </div>
                      </div>
                    ) : null}
                  </div>
                  <div
                    className={`col-12 col-sm-6 col-md-6 col-lg-6  ${classes.InputGroup}`}
                  >
                    <StyledTextField
                      variant="outlined"
                      id="Type"
                      // name="Type"
                      label="E-poçt"
                      fullWidth
                      placeholder="E-poçt"
                      autoComplete="off"
                      // error={getInputClasses("type")}
                      // {...formik.getFieldProps("type")}
                    />
                    {formik.touched.type && formik.errors.type ? (
                      <div className="fv-plugins-message-container">
                        <div className="fv-help-block">
                          {formik.errors.type}
                        </div>
                      </div>
                    ) : null}
                  </div>
                  <div className={`col-12 ${classes.InputGroup}`}>
                    <div className="col-12 col-sm-6 col-md-6 col-lg-6 pe-lg-3 pe-md-3 pe-sm-0">
                      <StyledSelect
                        value={age}
                        onChange={handleChange}
                        IconComponent={() => <AngleDown />}
                        label=""
                        // placeholder="SELECT"
                        // labelId="demo-simple-select-label-ASLAN"
                        id="demo-simple-select"
                      >
                        <MenuItem value={10}>Şəhər </MenuItem>
                        <MenuItem value={20}>Şəhər</MenuItem>
                        <MenuItem value={30}>Şəhər</MenuItem>
                      </StyledSelect>
                      {formik.touched.type && formik.errors.type ? (
                        <div className="fv-plugins-message-container">
                          <div className="fv-help-block">
                            {formik.errors.type}
                          </div>
                        </div>
                      ) : null}
                    </div>
                  </div>
                </div>
                <div className={classes.BasketFormDeliveryButton}>
                  <StyledOrderButton onClick={() => executeScroll(PayementRef)}>
                    Ödənişə keç
                  </StyledOrderButton>
                </div>
              </TabPanel>
            </div>
            <div ref={PayementRef} className={classes.BasketFormPayement}>
              <div className={classes.BasketFormPayementTop}>
                <h1 className={classes.BasketFormPayementTopHeader}>
                  Ödəniş növünü seçin
                </h1>
                <div className={classes.BasketFormPayementTopStepper}>
                  <Stepper activeStep={2} connector={<StyledConnector />}>
                    <StyledStep>
                      <StepLabel StepIconComponent={FilledBag}></StepLabel>
                    </StyledStep>
                    <StyledStep>
                      <StepLabel StepIconComponent={Box}></StepLabel>
                    </StyledStep>
                    <StyledStep>
                      <StepLabel StepIconComponent={CreditCard}></StepLabel>
                    </StyledStep>
                  </Stepper>
                </div>
              </div>
              <div className={classes.BasketFormPayementNotice}>
                <p>
                  Qeyd: Bir neçə ödəmə növündən istifadə etmək isteyirsinizsə,
                  uyğun ödəmə növlərinin seçili olduğundan əmin olun.
                </p>
              </div>
              <div className={classes.BasketFormPayementOptionGroup}>
                <div className={classes.BasketFormPayementOption}>
                  <ButtonBase className={classes.BasketFormPayementOptionItem}>
                    <label className={classes.Label}>
                      <span
                        className={`${classes.LabelTop} ${
                          formik.values.bonus ? classes.checked : ""
                        }`}
                      >
                        <span className={classes.LabelTopIcon}>
                          <Archer />
                        </span>
                        <span className={classes.LabelTopCheckSign}>
                          <Check />
                        </span>
                      </span>
                      <span className={classes.LabelBottom}>
                        <span>Bonusla ödə</span>
                        <span>Seçmək üçün kliklə</span>
                      </span>
                      <input
                        type="checkbox"
                        // value={bonusChecked}
                        // onChange={(e) => handleInputClick(e, setBonusChecked)}
                        {...formik.getFieldProps("bonus")}
                      />
                    </label>
                  </ButtonBase>
                </div>
                <div className={classes.BasketFormPayementOption}>
                  <ButtonBase className={classes.BasketFormPayementOptionItem}>
                    <label className={classes.Label}>
                      <span
                        className={`${classes.LabelTop} ${
                          formik.values.card ? classes.checked : ""
                        }`}
                      >
                        <span className={classes.LabelTopIcon}>
                          <BasketCard />
                        </span>
                        <span className={classes.LabelTopCheckSign}>
                          <Check />
                        </span>
                      </span>
                      <span className={classes.LabelBottom}>
                        <span>Kartla ödə</span>
                        <span>Seçmək üçün kliklə</span>
                      </span>
                      <input
                        type="checkbox"
                        // value={bonusChecked}
                        // onChange={(e) => handleInputClick(e, setBonusChecked)}
                        {...formik.getFieldProps("card")}
                      />
                    </label>
                  </ButtonBase>
                </div>
                <div className={classes.BasketFormPayementOption}>
                  <ButtonBase className={classes.BasketFormPayementOptionItem}>
                    <label className={classes.Label}>
                      <span
                        className={`${classes.LabelTop} ${
                          formik.values.cash ? classes.checked : ""
                        }`}
                      >
                        <span className={classes.LabelTopIcon}>
                          <BasketCash />
                        </span>
                        <span className={classes.LabelTopCheckSign}>
                          <Check />
                        </span>
                      </span>
                      <span className={classes.LabelBottom}>
                        <span>Nağd alıram</span>
                        <span>Seçmək üçün kliklə</span>
                      </span>
                      <input
                        type="checkbox"
                        // value={bonusChecked}
                        // onChange={(e) => handleInputClick(e, setBonusChecked)}
                        {...formik.getFieldProps("cash")}
                      />
                    </label>
                  </ButtonBase>
                </div>
                <div className={classes.BasketFormPayementOption}>
                  <ButtonBase className={classes.BasketFormPayementOptionItem}>
                    <label className={classes.Label}>
                      <span
                        className={`${classes.LabelTop} ${
                          formik.values.taksit ? classes.checked : ""
                        }`}
                      >
                        <span className={classes.LabelTopIcon}>
                          <BasketTaksit />
                        </span>
                        <span className={classes.LabelTopCheckSign}>
                          <Check />
                        </span>
                      </span>
                      <span className={classes.LabelBottom}>
                        <span>Taksitlə alıram</span>
                        <span>Seçmək üçün kliklə</span>
                      </span>
                      <input
                        type="checkbox"
                        // value={bonusChecked}
                        // onChange={(e) => handleInputClick(e, setBonusChecked)}
                        {...formik.getFieldProps("taksit")}
                      />
                    </label>
                  </ButtonBase>
                </div>
                <div className={classes.BasketFormPayementOption}>
                  <ButtonBase className={classes.BasketFormPayementOptionItem}>
                    <label className={classes.Label}>
                      <span
                        className={`${classes.LabelTop} ${
                          formik.values.nisye ? classes.checked : ""
                        }`}
                      >
                        <span className={classes.LabelTopIcon}>
                          <BasketNisye />
                        </span>
                        <span className={classes.LabelTopCheckSign}>
                          <Check />
                        </span>
                      </span>
                      <span className={classes.LabelBottom}>
                        <span>Nisyə alıram</span>
                        <span>Seçmək üçün kliklə</span>
                      </span>
                      <input
                        type="checkbox"
                        // value={bonusChecked}
                        // onChange={(e) => handleInputClick(e, setBonusChecked)}
                        {...formik.getFieldProps("nisye")}
                      />
                    </label>
                  </ButtonBase>
                </div>
              </div>
              <div className={classes.BasketFormPayementWith}>
                {/* pay with bonus card */}
                <div
                  className={`${
                    formik.values.bonus ? classes.expand : classes.collapse
                  } ${classes.BasketFormPayementWithBonus}`}
                >
                  <div className={classes.PayWithBonusTop}>
                    <h2 className={classes.PayWithBonusTopHeader}>
                      Bonus kart məlumatları
                    </h2>
                  </div>
                  <div className={`row ${classes.PayWithBonusBottom}`}>
                    <div className={`col-6 ${classes.PayWithBonusBottomLeft}`}>
                      <div className={classes.PayWithBonusBottomLeftItem}>
                        <StyledTextField
                          fullWidth
                          label="Bonus kartın nömrəsi"
                          placeholder="Bonus kartın nömrəsi"
                          autoComplete="off"
                        />
                      </div>
                      <div className={classes.PayWithBonusBottomLeftItem}>
                        <StyledTextField
                          fullWidth
                          label="PİN"
                          placeholder="PIN"
                          autoComplete="off"
                        />
                      </div>
                      <div className={classes.PayWithBonusBottomLeftItem}>
                        <StyledTextField
                          // type="number"
                          sx={{
                            "& svg": { color: "red" },
                            "& .MuiInputLabel-root": {
                              top: "3px",
                            },
                          }}
                          fullWidth
                          label="Bonus Karta ödəniləcək məbləğ"
                          placeholder="Bonus Karta ödəniləcək məbləğ"
                          autoComplete="off"
                          InputProps={{
                            endAdornment: (
                              <InputAdornment position="end">
                                <Manat />
                              </InputAdornment>
                            ),
                          }}
                        />
                      </div>
                    </div>
                    <div className={`col-6 ${classes.PayWithBonusBottomRight}`}>
                      <div className={classes.PayWithBonusBottomRightImg}>
                        <img
                          src="/img/user-card-back.png"
                          alt=""
                          // style={ActiveStatus ? null : { opacity: "0.3" }}
                        />
                        <div
                          className={classes.PayWithBonusBottomRightImgContent}
                        >
                          {/* card top text */}
                          <div
                            className={
                              classes.PayWithBonusBottomRightImgContentTop
                            }
                          >
                            <span>Balans: -</span>
                            <span>Ad və Soyad</span>
                          </div>
                          {/* barcode wrapper*/}
                          <div
                            className={
                              classes.PayWithBonusBottomRightImgContentBarcode
                            }
                          >
                            {/* qr code component */}
                            <Barcode
                              value="8896 8896 5669 4663"
                              width={2}
                              height={50}
                              fontSize={40}
                              lineColor="black"
                            />
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                {/* pay with card */}
                <div
                  className={`${
                    formik.values.card ? classes.expand : classes.collapse
                  } ${classes.BasketFormPayementWithCard}`}
                >
                  <div className={classes.PayWithCardTop}>
                    <h2 className={classes.PayWithCardTopHeader}>
                      Kart məlumatları
                    </h2>
                  </div>
                  <div className={classes.PayWithCardBottom}>
                    <div className={classes.PayWithCardBottomLeft}>
                      <div className={classes.PayWithCardBottomLeftItem}>
                        <StyledTextField
                          fullWidth
                          label="Kart nömrəsi"
                          placeholder="Kart nömrəsi"
                          autoComplete="off"
                        />
                      </div>
                      <div className={classes.PayWithCardBottomLeftItem}>
                        <StyledTextField
                          fullWidth
                          label="Kartın üzərindəki ad və soyad"
                          placeholder="Kartın üzərindəki ad və soyad"
                          autoComplete="off"
                        />
                      </div>
                      <div
                        className={classes.PayWithCardBottomLeftItem}
                        style={{ width: "50%", paddingRight: "15px" }}
                      >
                        <StyledTextField
                          fullWidth
                          label="MM/YY"
                          placeholder="MM/YY"
                          autoComplete="off"
                        />
                      </div>
                      <div
                        className={classes.PayWithCardBottomLeftItem}
                        style={{ width: "50%", paddingLeft: "15px" }}
                      >
                        <StyledTextField
                          fullWidth
                          label="CVV/CVC"
                          placeholder="CVV/CVC"
                          autoComplete="off"
                          onFocus={(e) => {
                            // formik.handleBlur(e);
                            setShowBack(true);
                          }}
                          onBlur={(e) => {
                            formik.handleBlur(e);
                            setShowBack(false);
                          }}
                        />
                      </div>
                      <div className={classes.PayWithCardBottomLeftItem}>
                        <StyledTextField
                          // type="number"
                          sx={{
                            "& svg": { color: "red" },
                            "& .MuiInputLabel-root": {
                              top: "3px",
                            },
                          }}
                          fullWidth
                          label="Bonus Karta ödəniləcək məbləğ"
                          placeholder="Bonus Karta ödəniləcək məbləğ"
                          autoComplete="off"
                          InputProps={{
                            endAdornment: (
                              <InputAdornment position="end">
                                <Manat />
                              </InputAdornment>
                            ),
                          }}
                        />
                      </div>
                    </div>
                    <div className={`col-6 ${classes.PayWithCardBottomRight}`}>
                      <div className={classes.AddCartFormImg}>
                        <div
                          className={`${classes.FlipCard} ${
                            showBack ? classes.ShowBack : ""
                            // true ? classes.ShowBack : ""
                          }`}
                        >
                          <div className={classes.FlipCardInner}>
                            <div className={classes.FlipCardFront}>
                              <img
                                className={classes.FlipCardFrontImg}
                                src="/img/user-card.png"
                                alt=""
                              />
                              <div className={classes.FlipCardFrontBody}>
                                {/* card number && name && validity */}
                                <div>
                                  {/* card number */}
                                  <div
                                    className={classes.FlipCardFrontBodyNumber}
                                  >
                                    0000 0000 0000 0000
                                  </div>

                                  {/* name && validity*/}
                                  <div
                                    className={
                                      classes.FlipCardFrontBodyNameValidity
                                    }
                                  >
                                    {/* name */}
                                    <span>Ad və Soyad</span>
                                    {/* validity */}
                                    <span>04/24</span>
                                  </div>
                                </div>
                                {/* card type logo */}
                                <div>Visa</div>
                              </div>
                            </div>
                            <div className={classes.FlipCardBack}>
                              <img
                                className={classes.FlipCardBackImg}
                                src="/img/user-card-back.png"
                                alt=""
                              />
                              <div className={classes.FlipCardBackBody}>
                                <div
                                  className={classes.FlipCardBackBodyNameField}
                                >
                                  Ad Soyad
                                </div>
                                <div className={classes.FlipCardBackBodyCVV}>
                                  355
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                {/* pay with cash */}
                <div
                  className={`${
                    formik.values.cash ? classes.expand : classes.collapse
                  } ${classes.BasketFormPayementWithCash}`}
                >
                  <p>
                    <b>Qeyd: </b>
                    <span>
                      Nağd alıram seçdiyiniz təqdirdə ödənişin tamamını
                      seçiminizdən asılı olaraq qapıda və ya mağazada nağd
                      şəkildə ödəməyiniz tələb olunacaqdır.
                    </span>
                  </p>
                </div>
                {/* pay taksit */}
                <div
                  className={`${
                    formik.values.taksit ? classes.expand : classes.collapse
                  } ${classes.BasketFormPayementWithTaksit}`}
                >
                  <div className={classes.BasketFormPayementWithTaksitTop}>
                    <h2
                      className={classes.BasketFormPayementWithTaksitTopHeader}
                    >
                      Taksit kart məlumatları
                    </h2>
                  </div>
                  <div className={classes.BasketFormPayementWithTaksitTabs}>
                    <StyledTaksitTabs
                      value={tabValue}
                      onChange={handleChangeCardTab}
                      aria-label="basic tabs example"
                    >
                      <Tab
                        label="Birkart"
                        icon={
                          <TabCircle
                            imgUrl={"/img/birkart.png"}
                            iconPosition="end"
                          />
                        }
                        {...a11yProps(0)}
                      />
                      <Tab
                        label="TamKart"
                        icon={
                          <TabCircle
                            imgUrl={"/img/tamkart.png"}
                            iconPosition="end"
                          />
                        }
                        {...a11yProps(1)}
                      />
                    </StyledTaksitTabs>
                    <TabPanel
                      value={tabValue}
                      index={0}
                      style={{ marginTop: "10px" }}
                    >
                      <div className={classes.PayementDurationBtnGroup}>
                        <ButtonBase className={classes.PayementDurationBtn}>
                          <label className={classes.PayementDurationCheck}>
                            <input
                              type="checkbox"
                              className={classes.PayementDurationCheckInput}
                            />
                            {/* checked icon holder */}
                            <span
                              className={classes.PayementDurationCheckStatus}
                            >
                              <Check />
                            </span>
                            {/* input body holder */}
                            <span className={classes.PayementDurationCheckBody}>
                              <span>6 ay</span>
                              <span>
                                Aylıq 289 <Manat />
                              </span>
                            </span>
                          </label>
                        </ButtonBase>
                        <ButtonBase className={classes.PayementDurationBtn}>
                          <label className={classes.PayementDurationCheck}>
                            <input
                              type="checkbox"
                              className={classes.PayementDurationCheckInput}
                            />
                            {/* checked icon holder */}
                            <span
                              className={classes.PayementDurationCheckStatus}
                            >
                              <Check />
                            </span>
                            {/* input body holder */}
                            <span className={classes.PayementDurationCheckBody}>
                              <span>6 ay</span>
                              <span>
                                Aylıq 289 <Manat />
                              </span>
                            </span>
                          </label>
                        </ButtonBase>
                        <ButtonBase className={classes.PayementDurationBtn}>
                          <label className={classes.PayementDurationCheck}>
                            <input
                              type="checkbox"
                              className={classes.PayementDurationCheckInput}
                            />
                            {/* checked icon holder */}
                            <span
                              className={classes.PayementDurationCheckStatus}
                            >
                              <Check />
                            </span>
                            {/* input body holder */}
                            <span className={classes.PayementDurationCheckBody}>
                              <span>6 ay</span>
                              <span>
                                Aylıq 289 <Manat />
                              </span>
                            </span>
                          </label>
                        </ButtonBase>
                      </div>
                    </TabPanel>
                    <TabPanel
                      value={tabValue}
                      index={1}
                      style={{ marginTop: "10px" }}
                    >
                      <div className={classes.PayementDurationBtnGroup}>
                        <ButtonBase className={classes.PayementDurationBtn}>
                          <label className={classes.PayementDurationCheck}>
                            <input
                              type="checkbox"
                              className={classes.PayementDurationCheckInput}
                            />
                            {/* checked icon holder */}
                            <span
                              className={classes.PayementDurationCheckStatus}
                            >
                              <Check />
                            </span>
                            {/* input body holder */}
                            <span className={classes.PayementDurationCheckBody}>
                              <span>6 ay</span>
                              <span>
                                Aylıq 289 <Manat />
                              </span>
                            </span>
                          </label>
                        </ButtonBase>{" "}
                        <ButtonBase className={classes.PayementDurationBtn}>
                          <label className={classes.PayementDurationCheck}>
                            <input
                              type="checkbox"
                              className={classes.PayementDurationCheckInput}
                            />
                            {/* checked icon holder */}
                            <span
                              className={classes.PayementDurationCheckStatus}
                            >
                              <Check />
                            </span>
                            {/* input body holder */}
                            <span className={classes.PayementDurationCheckBody}>
                              <span>6 ay</span>
                              <span>
                                Aylıq 289 <Manat />
                              </span>
                            </span>
                          </label>
                        </ButtonBase>
                      </div>
                    </TabPanel>
                  </div>
                  <div className={classes.BasketFormPayementWithTaksitBottom}>
                    <div
                      className={classes.BasketFormPayementWithTaksitBottomLeft}
                    >
                      <div
                        className={
                          classes.BasketFormPayementWithTaksitBottomLeftItem
                        }
                      >
                        <StyledTextField
                          fullWidth
                          label="Kart nömrəsi"
                          placeholder="Kart nömrəsi"
                          autoComplete="off"
                        />
                      </div>
                      <div
                        className={
                          classes.BasketFormPayementWithTaksitBottomLeftItem
                        }
                      >
                        <StyledTextField
                          fullWidth
                          label="Kartın üzərindəki ad və soyad"
                          placeholder="Kartın üzərindəki ad və soyad"
                          autoComplete="off"
                        />
                      </div>
                      <div
                        className={
                          classes.BasketFormPayementWithTaksitBottomLeftItem
                        }
                        style={{ width: "50%", paddingRight: "15px" }}
                      >
                        <StyledTextField
                          fullWidth
                          label="MM/YY"
                          placeholder="MM/YY"
                          autoComplete="off"
                        />
                      </div>
                      <div
                        className={
                          classes.BasketFormPayementWithTaksitBottomLeftItem
                        }
                        style={{ width: "50%", paddingLeft: "15px" }}
                      >
                        <StyledTextField
                          fullWidth
                          label="CVV/CVC"
                          placeholder="CVV/CVC"
                          autoComplete="off"
                          onFocus={(e) => {
                            // formik.handleBlur(e);
                            setShowBack(true);
                          }}
                          onBlur={(e) => {
                            formik.handleBlur(e);
                            setShowBack(false);
                          }}
                        />
                      </div>
                    </div>
                    <div
                      className={`col-6 ${classes.BasketFormPayementWithTaksitBottomRight}`}
                    >
                      <div className={classes.AddCartFormImg}>
                        <div
                          className={`${classes.FlipCard} ${
                            showBack ? classes.ShowBack : ""
                            // true ? classes.ShowBack : ""
                          }`}
                        >
                          <div className={classes.FlipCardInner}>
                            <div className={classes.FlipCardFront}>
                              <img
                                className={classes.FlipCardFrontImg}
                                src="/img/user-card.png"
                                alt=""
                              />
                              <div className={classes.FlipCardFrontBody}>
                                {/* card number && name && validity */}
                                <div>
                                  {/* card number */}
                                  <div
                                    className={classes.FlipCardFrontBodyNumber}
                                  >
                                    0000 0000 0000 0000
                                  </div>

                                  {/* name && validity*/}
                                  <div
                                    className={
                                      classes.FlipCardFrontBodyNameValidity
                                    }
                                  >
                                    {/* name */}
                                    <span>Ad və Soyad</span>
                                    {/* validity */}
                                    <span>04/24</span>
                                  </div>
                                </div>
                                {/* card type logo */}
                                <div>Visa</div>
                              </div>
                            </div>
                            <div className={classes.FlipCardBack}>
                              <img
                                className={classes.FlipCardBackImg}
                                src="/img/user-card-back.png"
                                alt=""
                              />
                              <div className={classes.FlipCardBackBody}>
                                <div
                                  className={classes.FlipCardBackBodyNameField}
                                >
                                  Ad Soyad
                                </div>
                                <div className={classes.FlipCardBackBodyCVV}>
                                  355
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <StyledOrderButton>Sifarişi tamamla</StyledOrderButton>
              </div>
            </div>
          </form>
          {/* basket right */}
          <div className={classes.BasketRight}>
            {/* basket right top */}
            <div className={classes.BasketRightTop}>
              {/* top header */}
              <h2 className={classes.BasketRightTopHeader}>Ümumi baxış</h2>
            </div>
            {/* basket right body */}
            <div className={classes.BasketRightBody}>
              <div className={classes.BasketRightBodyItem}>
                <div className={classes.ItemNamePrice}>
                  <span className={classes.ItemName}>
                    İphone 13 Pro Max 128GB Blue
                  </span>
                  <span className={classes.ItemPrice}>
                    3499 <Manat />
                  </span>
                </div>
                <div className={classes.ItemCount}>1 ədəd</div>
              </div>
              <div className={classes.BasketRightBodyItem}>
                <div className={classes.ItemNamePrice}>
                  <span className={classes.ItemName}>
                    İphone 13 Pro Max 128GB Blue
                  </span>
                  <span className={classes.ItemPrice}>
                    3499 <Manat />
                  </span>
                </div>
                <div className={classes.ItemCount}>1 ədəd</div>
              </div>
            </div>
            {/* basket right total */}
            <div className={classes.BasketRightTotal}>
              <div className={classes.TotalTop}>
                <span className={classes.TotalTopTitle}>Cəmi:</span>
                <br />
                <span className={classes.TotalTopPrice}>
                  4198 <Manat />
                </span>
              </div>
              <div className={classes.TotalBottom}>
                <div className={classes.TotalBottomText}>
                  <div>
                    <span>Ümumi məhsul sayı:</span>
                    <span>2 ədəd</span>
                  </div>
                  <div>
                    <span>Ümumi endirim:</span>
                    <span>
                      -230 <Manat />
                    </span>
                  </div>
                </div>
                <div className={classes.TotalBottomPromocode}>
                  <Promocode />
                  <input placeholder="Promokod..." />
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <MobileBottomNavbar />
    </Layout>
  );
}

export default Index;
