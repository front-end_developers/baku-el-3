import * as React from "react";
import Box from "@mui/material/Box";
import Tab from "@mui/material/Tab";
import TabContext from "@mui/lab/TabContext";
import TabList from "@mui/lab/TabList";
import TabPanel from "@mui/lab/TabPanel";
import Layout from "../../src/hoc/Layout/Layout/Layout";
import classes from "./index.module.scss";
import Button from "@mui/material/Button";
import { Cashback, Cash, Like } from "../../src/components/Icons";
import { Container, Row, Col } from "react-bootstrap";
import Link from "next/link";
import MobileBottomNavbar from "../../src/components/MobileBottomNavbar/MobileBottomNavbar";

import { ButtonBase } from "@mui/material";

export default function Index() {
  const [value, setValue] = React.useState("1");

  const handleChange = (event, newValue) => {
    setValue(newValue);
  };
  const tabsStyles = {
    ["@media (max-width:1096px)"]: {
      fontSize: "14px",
      padding: "12px",
    },

    ["@media (max-width:768px)"]: {
      padding: "10px",
      fontSize: "13px",
    },

    backgroundColor: "none",
    color: "var(--text-primary)",
    textTransform: "none",
    fontFamily: "Poppins",
    fontWeight: "500",
    opacity: "50%",
    fontSize: "16px",

    "&.Mui-selected": {
      fontWeight: 600,
      color: " var(--text-primary)",
      opacity: "100%",
    },
  };

  const tabsWrapperStyle = {
    "span.MuiTabs-indicator": {
      backgroundColor: "red",
    },

    "& .MuiTabs-scroller": {
      width: "100%",
      overflow: "auto !important",

      "&::-webkit-scrollbar": { background: "transparent" },
    },
  };
  const panelStyle = {
    "&.MuiTabPanel-root": {
      padding: "0px ",
    },
  };

  const boxStyle = {
    display: "flex",
    alignItems: "center",
    justifyContent: "space-between",
    flexWrap: "wrap-reverse",
    rowGap: "14px",
  };

  return (
    <Layout>
      <div className={classes.BonusUserCard}>
        <div className={classes.BonusUserCardWrapper}>
          <h3>Bonus - Müştəri kartı</h3>
          <TabContext value={value}>
            <Box sx={boxStyle}>
              <TabList
                onChange={handleChange}
                sx={tabsWrapperStyle}
                aria-label="lab API tabs example"
              >
                <Tab sx={tabsStyles} label="Terminlər" value="1" />
                <Tab sx={tabsStyles} label="Ümumi şərtlər" value="2" />
                <Tab
                  sx={tabsStyles}
                  label="Bonusların toplanma qaydası"
                  value="3"
                />
                <Tab
                  sx={tabsStyles}
                  label="Bonusların istifadə qaydası"
                  value="4"
                />
              </TabList>

              <ButtonBase sx={{ borderRadius: "12px" }}>
                <Link
                  // variant="contained"
                  // disableElevation
                  href="/bonus-user-card/add"
                >
                  <a className={classes.cardRegister}>Kart qeydiyyatı</a>
                </Link>
              </ButtonBase>
            </Box>
            <TabPanel value="1" sx={panelStyle}>
              <Row className="p-0 mt-4">
                <Col sm={12} md={12} lg={4}>
                  <div className={classes.BonusUserCardContent}>
                    <Cashback />
                    <p> Hər alışın 1% məbləği bonus karta geri qayldır</p>
                  </div>
                </Col>
                <Col sm={12} md={12} lg={4}>
                  <div className={classes.BonusUserCardContent}>
                    <Cash />
                    <p>Ödənişdə istifadə et, ya da topla, istədiyini al.</p>
                  </div>
                </Col>
                <Col sm={12} md={12} lg={4}>
                  <div className={classes.BonusUserCardContent}>
                    <Like />
                    <p> Ödənişdə istifadə et, ya da topla, istədiyini al.</p>
                  </div>
                </Col>
                <Row className="p-0 m-auto mb-5">
                  <Col>
                    <div className={classes.BonusUserCardMainContent}>
                      <h3 className="m-0">Terminlər</h3>
                      <ul>
                        <li>
                          Ödəniş formasından (taksit kartlar xaric) və yerindən
                          asılı olmayaraq müştəri etdiyi bütün alışların
                          məbləğinin 1%-ini bonus olaraq qazanır.
                        </li>
                        <li>
                          Bonuslar avtomatik olaraq kartın balansına daxil
                          edilir.
                        </li>
                        <li>Qazanılan bonuslar 14 gün sonra aktiv edilir.</li>
                        <li>
                          Bonuslar hədiyyə edilmir və bir müştərinin bonusu
                          digər müştərinin kartına keçirilə bilməz.
                        </li>
                        <li>Bonusları pula dəyişmək mümkün deyil.</li>
                        <li>
                          Aksiya şərtlərinə müvafiq olaraq qazanılan bonusların
                          həcmi fərqli ola bilə.
                        </li>
                      </ul>
                    </div>
                  </Col>
                </Row>
              </Row>
            </TabPanel>
            <TabPanel value="2">Item Two</TabPanel>
            <TabPanel value="3">Item Three</TabPanel>
            <TabPanel value="4">Item Four</TabPanel>
          </TabContext>
        </div>
      </div>
      <MobileBottomNavbar />
    </Layout>
  );
}
