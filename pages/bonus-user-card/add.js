import * as React from "react";
import Layout from "../../src/hoc/Layout/Layout/Layout";
import classes from "./add.module.scss";
import { Container, Row, Col } from "react-bootstrap";
import TextField from "@mui/material/TextField";
import ReactSelect from "../../src/components/ReactSelect/ReactSelect";
// import { TextField } from "@material-ui/core";
import Select from "@mui/material/Select";
import { styled } from "@mui/material/styles";
import Button from "@mui/material/Button";
import * as Yup from "yup";
import MenuItem from "@mui/material/MenuItem";
import { useState } from "react";
import { AngleDown } from "../../src/components/Icons";
import ToggleButton from "@mui/material/ToggleButton";
import ToggleButtonGroup from "@mui/material/ToggleButtonGroup";
import MobileBottomNavbar from "../../src/components/MobileBottomNavbar/MobileBottomNavbar";

export default function Index() {
  const [age, setAge] = useState("10");

  const handleChange = (event) => {
    setAge(event.target.value);
  };

  const [showBack, setShowBack] = useState();
  const initialValues = {
    type: "",
  };
  const CreateSchema = Yup.object().shape({
    type: Yup.string(),
  });

  const StyledSelect = styled((props) => <Select {...props} />)(
    ({ theme }) => ({
      // backgroundColor: "blue",

      width: "100%",
      border: "1px solid #b3b3b3",
      fontFamily: "Poppins",
      borderRadius: "16px",
      paddingInline: "24px",
      "& #demo-simple-select": {
        color: "var(--text-primary)",
      },
      // "&:hover": {
      //   borderColor: '#333',
      // },

      "& .MuiSelect-select": {
        fontWeight: "500",
        fontSize: "14px",
        color: "rgba(51, 51, 51, 0.5)",
        padding: "9px 0",
      },
      "& label": {
        color: "var(--text-primary)",
      },

      "& svg": {
        width: "10px",
        color: "var(--red-main)",
        transition: "0.2s",
      },
      "& [aria-expanded='true']~svg": {
        transform: "rotate(-180deg)",
      },
      "& fieldset": {
        display: "none",
      },
    })
  );

  const StyledTextField = styled((props) => <TextField {...props} />)({
    "& .MuiOutlinedInput-input": {
      padding: "10px 24px",
      color: "red",
      fontFamily: "Poppins",
      color: "var(--text-primary)",
      fontSize: "14px",
    },

    ".MuiOutlinedInput-notchedOutline": {
      top: 0,
    },

    "& fieldset": {
      width: "100%",
      border: "none",
      outline: "1px solid #b3b3b3",
      borderRadius: "16px",
    },
    "& label": {
      fontFamily: "Poppins",
      color: "var(--text-primary)",
      backgroundColor: "background: var(--product-details-bg);",
      paddingBlock: "2px",
      paddingInline: "10px",
      marginTop: "-8px",
      fontSize: "14px",
      color: "var(--text-primary) !important",
    },
  });
  const [alignment, setAlignment] = React.useState("web");

  const handleChangeTab = (event, newAlignment) => {
    setAlignment(newAlignment);
  };

  const StyledManGenderButton = styled((props) => <Button {...props} />)({
    borderRadius: "12px",
    border: 0,
    paddingBlock: "7px",
    color: "var(--text-primary)",
    background: "var(--gender-man-btn-bg)",
    textTransform: "inherit",
    fontFamily: "Poppins",
    fontSize: "12px",
    fontWeight: 500,

    "&:hover": {
      background: "var(--gender-man-btn-bg)",
    },
  });

  const StyledWomanGenderButton = styled((props) => <Button {...props} />)({
    borderRadius: "12px",
    border: 0,
    paddingBlock: "7px",
    backgroundColor: "var(--gender-woman-btn-bg)",
    color: "var(--f-to-0)",
    textTransform: "inherit",
    fontFamily: "Poppins",
    fontSize: "12px",
    fontWeight: 500,

    "&:hover": {
      backgroundColor: "var(--gender-woman-btn-bg)",
    },
  });

  return (
    <Layout>
      <div className={`${classes.AddBonusCard} AddBonusCard`}>
        <div className={classes.AddBonusCardWrapper}>
          <h1>Kart qeydiyyatı</h1>
          <form action="#">
            <Row className="mt-5 mb-5">
              <Col
                sm={12}
                md={12}
                lg={6}
                xl={6}
                className={classes.AddBonusCardLeft}
              >
                <h3>Əlaqə məlumatları</h3>
                <Row>
                  <Col sm={12} md={6} lg={6} xl={6} className="mt-3 mb-3">
                    <StyledTextField
                      key="hey"
                      variant="outlined"
                      id="Type"
                      // name="Type"
                      label="Kartın üzərindəki ad və soyad"
                      fullWidth

                      // className="w-100 mg-top-3"
                      // error={getInputClasses("type")}
                      // {...formik.getFieldProps("type")}
                    />
                  </Col>
                  <Col sm={12} md={6} lg={6} xl={6} className="mt-3 mb-3">
                    <StyledTextField
                      key="hey"
                      id="Type"
                      // name="Type"
                      label="Kartın üzərindəki ad və soyad"
                      fullWidth

                      // className="w-100 mg-top-3"
                      // error={getInputClasses("type")}
                      // {...formik.getFieldProps("type")}
                    />
                  </Col>
                </Row>
                <Row className={`${classes.BonusAddForm} BonusAddForm`}>
                  <h3>Yaşadığınız ünvan</h3>
                  <Col
                    className={`${classes.BonusAddFormColLeft} mt-3 mb-3`}
                    sm={12}
                    md={6}
                    lg={6}
                    xl={6}
                  >
                    <StyledSelect
                      value={age}
                      onChange={handleChange}
                      IconComponent={() => <AngleDown />}
                      label=""
                      // placeholder="SELECT"
                      // labelId="demo-simple-select-label-ASLAN"
                      id="demo-simple-select"
                    >
                      <MenuItem value={10}>Şəhər </MenuItem>
                      <MenuItem value={20}>Şəhər</MenuItem>
                      <MenuItem value={30}>Şəhər</MenuItem>
                    </StyledSelect>
                  </Col>
                  <Col
                    className={`${classes.BonusAddFormColRight} mt-3 mb-3`}
                    sm={12}
                    md={6}
                    lg={6}
                    xl={6}
                  >
                    <StyledSelect
                      value={age}
                      onChange={handleChange}
                      IconComponent={() => <AngleDown />}
                      label=""
                      // placeholder="SELECT"
                      // labelId="demo-simple-select-label-ASLAN"
                      id="demo-simple-select"
                    >
                      <MenuItem value={10}>Rayon</MenuItem>
                      <MenuItem value={20}>Rayon</MenuItem>
                      <MenuItem value={30}>Rayon</MenuItem>
                    </StyledSelect>
                  </Col>
                </Row>
                <Row>
                  <Col sm={12} md={12} lg={12} xl={12} className="mt-3 mb-3">
                    <StyledTextField
                      key="hey"
                      variant="outlined"
                      id="Type"
                      // name="Type"
                      label="Kartın üzərindəki ad və soyad"
                      fullWidth

                      // className="w-100 mg-top-3"
                      // error={getInputClasses("type")}
                      // {...formik.getFieldProps("type")}
                    />
                  </Col>
                </Row>
              </Col>

              <Col
                sm={12}
                md={12}
                lg={6}
                xl={6}
                className={classes.AddBonusCardRight}
              >
                <h3>Şəxsi məlumatlar</h3>
                <Row>
                  <Col
                    sm={12}
                    md={12}
                    lg={12}
                    xl={12}
                    className={`${classes.BonusAddFormRight} mt-3 mb-3`}
                  >
                    <StyledTextField
                      variant="outlined"
                      id="Type"
                      // name="Type"
                      label="Ad"
                      fullWidth

                      // error={getInputClasses("type")}
                      // {...formik.getFieldProps("type")}
                    />
                  </Col>

                  <Col sm={12} md={4} lg={4} xl={4} className="mt-3 mb-3">
                    <StyledTextField
                      variant="outlined"
                      id="Type"
                      // name="Type"
                      label="Ad"
                      fullWidth

                      // error={getInputClasses("type")}
                      // {...formik.getFieldProps("type")}
                    />
                  </Col>
                  <Col sm={12} md={4} lg={4} xl={4} className="mt-3 mb-3">
                    <StyledTextField
                      variant="outlined"
                      id="Type"
                      // name="Type"
                      label="Soyad"
                      fullWidth

                      // error={getInputClasses("type")}
                      // {...formik.getFieldProps("type")}
                    />
                  </Col>
                  <Col sm={12} md={4} lg={4} xl={4} className="mt-3 mb-3">
                    <StyledTextField
                      variant="outlined"
                      id="Type"
                      // name="Type"
                      label="Ata adı"
                      fullWidth

                      // error={getInputClasses("type")}
                      // {...formik.getFieldProps("type")}
                    />
                  </Col>
                </Row>
                <Row className={classes.BonusAddFormRightInner}>
                  <Col sm={12} md={6} lg={6} xl={6} className="mt-3 mb-3">
                    <StyledTextField
                      variant="outlined"
                      id="Type"
                      // name="Type"
                      label="Doğum tarixi"
                      fullWidth

                      // error={getInputClasses("type")}
                      // {...formik.getFieldProps("type")}
                    />
                  </Col>
                  <Col sm={12} md={6} lg={6} xl={6} className="mt-3 mb-3">
                    <StyledTextField
                      variant="outlined"
                      id="Type"
                      // name="Type"
                      label="Fin kod"
                      fullWidth

                      // error={getInputClasses("type")}
                      // {...formik.getFieldProps("type")}
                    />
                  </Col>
                </Row>
                <div className={classes.BonusAddGender}>
                  <span>Cinsi :</span>

                  <StyledManGenderButton className={classes.AddCardBtn1}>
                    Kişi
                  </StyledManGenderButton>

                  <StyledWomanGenderButton className={classes.AddCardBtn2}>
                    Qadın
                  </StyledWomanGenderButton>
                </div>
              </Col>
              <Button
                variant="contained"
                disableElevation
                className={classes.cardRegister}
              >
                Kartı aktiv et
              </Button>
            </Row>
          </form>
        </div>
      </div>
      <MobileBottomNavbar />
    </Layout>
  );
}
