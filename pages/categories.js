import Layout from "../src/hoc/Layout/Layout/Layout";
import Categories from "../src/modules/Categories/Categories";
// const CategoriesComponent = require("../src/modules/Categories/Categories")
import MobileBottomNavbar from "../src/components/MobileBottomNavbar/MobileBottomNavbar";

function CategoriesPage(props) {
  const { categories } = props;

  return (
    <Layout>
      <Categories categories={categories} />
      <MobileBottomNavbar />
    </Layout>
  );
}

export default CategoriesPage;

export async function getStaticProps(context) {
  // Call an external API endpoint to get posts.
  // You can use any data fetching library

  const { locale } = context;

  let langStrQuery =
    locale === "AZ"
      ? "Az"
      : locale === "EN"
      ? "En"
      : locale === "RU"
      ? "Ru"
      : "Az";


  let categories="";
  
  try {
    const res = await fetch(
      `${process.env.base_url}${langStrQuery}/categories`
    );
    categories = await res.json();
  } catch (err) {
    console.log("err", err);
  }

  return {
    props: {
      categories,
    },
  };
}
