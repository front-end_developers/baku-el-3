import classes from "./index.module.scss";
import Link from "next/link";
import { Button, Card, styled, TextField, Typography } from "@mui/material";
import { useState } from "react";

import IconButton from "@mui/material/IconButton";
import OutlinedInput from "@mui/material/OutlinedInput";
import InputLabel from "@mui/material/InputLabel";
import InputAdornment from "@mui/material/InputAdornment";
import FormControl from "@mui/material/FormControl";
import { MdOutlineVisibility } from "react-icons/md";
import { MdOutlineVisibilityOff } from "react-icons/md";
import { FaRegEnvelope } from "react-icons/fa";
import FullHeader from "../../../src/components/FullHeader/FullHeader";
import AuthLayout from "../../../src/hoc/Layout/AuthLayout/AuthLayout";
import MobileBottomNavbar from "../../../src/components/MobileBottomNavbar/MobileBottomNavbar";

const Index = () => {
  const [values, setValues] = useState({
    amount: "",
    password: "",
    weight: "",
    weightRange: "",
    showPassword: false,
  });

  const handleChange = (prop) => (event) => {
    setValues({ ...values, [prop]: event.target.value });
  };

  const handleClickShowPassword = () => {
    setValues({
      ...values,
      showPassword: !values.showPassword,
    });
  };

  const handleMouseDownPassword = (event) => {
    event.preventDefault();
  };
  const textFieldStyles = {
    "& .MuiOutlinedInput-root": {
      "& .MuiOutlinedInput-input": {
        padding: "14px 24px",
        color: "red",
        fontFamily: "Poppins",
        color: "var(--text-primary)",
        fontSize: "14px",
        "&:hover": {
          "& + fieldset": {
            borderColor: "var(--black-to-white)",
          },
        },
      },
      "& fieldset.MuiOutlinedInput-notchedOutline": {
        width: "100%",
        borderRadius: "4px",
        borderColor: "var(--black-to-white)",
        borderRadius: "16px",
      },
      "&.Mui-error": {
        "& .MuiOutlinedInput-input": {
          "&:hover": {
            "& + fieldset": {
              borderColor: "var(--red-main)!important",
            },
          },
        },
        "& fieldset.MuiOutlinedInput-notchedOutline": {
          borderColor: "var(--red-main)!important",
        },
      },
      "&.Mui-focused:not(.Mui-error)": {
        "& fieldset.MuiOutlinedInput-notchedOutline": {
          borderColor: "var(--black-to-white)!important",
        },
      },
    },
    "& label": {
      color: "var(--text-primary)",
      backgroundColor: "var(--user-sidebar-main)",
      paddingTop: "8px",
      paddingInline: "10px",
      marginTop: "-8px",
      fontSize: "14px",
      color: "var(--text-primary) !important",
    },
  };
  return (
    <AuthLayout>
      <div className={`${classes.NewPasswordContent} LoginContent`}>
        <div className={classes.NewPasswordContentLeft}>
          <img
            src="/img/Rectangle 1154.png"
            alt=""
            className={classes.NewPasswordContentLeftBgImg}
          />
          <span className={classes.NewPasswordContentLeftOpacity}></span>
          <div className={classes.NewPasswordContentLeftBody}>
            <div>
              <div className={classes.BakuElLogo}>
                <img src="/img/BakuElectronics.png" alt="" />
              </div>
            </div>
            <div>
              <div className={classes.text}>
                <span>
                  Scooterlarda <b>Black Friday</b> endirimləri?
                </span>
              </div>
              <div className={classes.btn}>
                <Button>Ətraflı bax</Button>
              </div>
            </div>
          </div>
        </div>
        <div className={classes.NewPasswordContentRight}>
          <div className={classes.NewPasswordContentRightWrapper}>
            <nav className={classes.Navbar}>
              <Link href="/">Kömək</Link>
              <Link href="/">Ana səhifə</Link>
              <Link href="/">Gizlilik</Link>
              <Link href="/">Şərtlər və qaydalar</Link>
            </nav>
            <div>
              <Card className={classes.Card}>
                <div className={classes.CardTitle}>
                  <div className={classes.CardTitleIcon}>
                    <img
                      src="img/Arrow - Left.svg"
                      alt=""
                      className={classes.LeftArrow}
                    />
                  </div>
                  <div className={classes.CardTitleTxt}>Yeni şifrə</div>
                </div>

                <TextField
                  id="outlined-basic"
                  label="Şifrə"
                  variant="outlined"
                  fullWidth
                  placeholder="Şifrə"
                  type={values.showPassword ? "text" : "password"}
                  value={values.password}
                  onChange={handleChange("password")}
                  InputProps={{
                    endAdornment: (
                      <InputAdornment position="end">
                        <IconButton
                          aria-label="toggle password visibility"
                          onClick={handleClickShowPassword}
                          onMouseDown={handleMouseDownPassword}
                          edge="end"
                          style={{
                            marginRight: "2px",
                            marginTop: "6px",
                          }}
                        >
                          {values.showPassword ? (
                            <MdOutlineVisibilityOff
                              style={{
                                color: "#EA2427",
                                width: "18px",
                              }}
                            />
                          ) : (
                            <MdOutlineVisibility
                              style={{
                                color: "#EA2427",
                                width: "18px",
                              }}
                            />
                          )}
                        </IconButton>
                      </InputAdornment>
                    ),
                  }}
                  sx={textFieldStyles}
                />
                <div
                  style={{
                    height: "36px",
                    color: "red",
                    fontSize: "13px",
                    lineHeight: "2",
                  }}
                >
                  {/* {formik.touched.password && formik.errors.password ? (
                    <div className="">
                      <div className="">{formik.errors.password}</div>
                    </div>
                  ) : null} */}
                </div>

                <TextField
                  id="outlined-basic"
                  label="Şifrəni təkrar et"
                  variant="outlined"
                  fullWidth
                  placeholder="Şifrəni təkrar et"
                  type={values.showPassword ? "text" : "password"}
                  value={values.password}
                  onChange={handleChange("password")}
                  InputProps={{
                    endAdornment: (
                      <InputAdornment position="end">
                        <IconButton
                          aria-label="toggle password visibility"
                          onClick={handleClickShowPassword}
                          onMouseDown={handleMouseDownPassword}
                          edge="end"
                          style={{
                            marginRight: "2px",
                            marginTop: "6px",
                          }}
                        >
                          {values.showPassword ? (
                            <MdOutlineVisibilityOff
                              style={{
                                color: "#EA2427",
                                width: "18px",
                              }}
                            />
                          ) : (
                            <MdOutlineVisibility
                              style={{
                                color: "#EA2427",
                                width: "18px",
                              }}
                            />
                          )}
                        </IconButton>
                      </InputAdornment>
                    ),
                  }}
                  sx={textFieldStyles}
                />
                <div
                  style={{
                    height: "36px",
                    color: "red",
                    fontSize: "13px",
                    lineHeight: "2",
                  }}
                >
                  {/* {formik.touched.password && formik.errors.password ? (
                    <div className="">
                      <div className="">{formik.errors.password}</div>
                    </div>
                  ) : null} */}
                </div>

                <Button className={classes.NewPasswordBtn}>
                  Şifrəni dəyiş
                </Button>
                <Button className={classes.CancelBtn}>Ləğv et</Button>
              </Card>
            </div>
          </div>
        </div>
      </div>
    </AuthLayout>
  );
};

export default Index;
