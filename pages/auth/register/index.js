import classes from "./index.module.scss";
import Link from "next/link";
// import { FacebookLogo } from "../Icons";
import { Button, Card, styled, TextField, Typography } from "@mui/material";
import { useEffect, useState } from "react";
import { FbLogo, GoogleLogo, User } from "../../../src/components/Icons";
import IconButton from "@mui/material/IconButton";
// import OutlinedInput from "@mui/material/OutlinedInput";
// import InputLabel from "@mui/material/InputLabel";
import InputAdornment from "@mui/material/InputAdornment";
// import FormControl from "@mui/material/FormControl";
import { MdOutlineVisibility } from "react-icons/md";
import { MdOutlineVisibilityOff } from "react-icons/md";
import { FaRegEnvelope } from "react-icons/fa";
// import FullHeader from "../../../src/components/FullHeader/FullHeader";
// import MobileBottomNavbar from "../../../src/components/MobileBottomNavbar/MobileBottomNavbar";
import AuthLayout from "../../../src/hoc/Layout/AuthLayout/AuthLayout";
import axios from "axios";
import { useFormik } from "formik";
import * as Yup from "yup";
import Router from "next/router";

import { setUser, setRegisterEmail } from "../../../redux/actions/main";
import { connect } from "react-redux";

const Index = (props) => {
  const { setRegisterEmail } = props;
  const [showPassword, setShowPassword] = useState(false);
  const [isError, setIsError] = useState(false);

  const initialValues = {
    fullname: "",
    email: "",
    password: "",
  };

  const CreateSchema = Yup.object().shape({
    fullname: Yup.string().required("Ad Soyad tələb olunur."),
    email: Yup.string().email().required("Email dəyər tələb olunur."),
    password: Yup.string().required("Şifrə tələb olunur."),
  });

  const formik = useFormik({
    initialValues,
    validationSchema: CreateSchema,
    onSubmit: (values, { resetForm }) => {
      onRegisterUser(values, resetForm);
    },
  });

  const getInputClasses = (filedName) => {
    if (formik.touched[filedName] && formik.errors[filedName]) {
      return true;
    }
    if (formik.touched[filedName] && !formik.errors[filedName]) {
      return false;
    }

    return false;
  };

  const onRegisterUser = (values, resetForm) => {
    const group = {
      fullname: values.fullname,
      email: values.email,
      password: values.password,
      verify: true,
      lang: "AZ",
    };
    axios({
      method: "post",
      url: `${process.env.base_url}account/register`,
      data: group,
    })
      .then((res) => {
        setRegisterEmail(res.data.email);
        Router.push("/auth/register-confirmation-code");
        resetForm();
        setIsError(false);
      })
      .catch((err) => {
        console.log("err", err.response);
        resetForm();
        setIsError(err.response.data.detail);

        //   let message =
        //     typeof err.response !== "undefined"
        //       ? err.response.data.message
        //       : err.message;
        //   setIsError(message);
      });
  };

  function resetForm() {
    formik.resetForm();
  }

  const textFieldStyles = {
    "& .MuiOutlinedInput-root": {
      "& .MuiOutlinedInput-input": {
        padding: "14px 24px",
        color: "red",
        fontFamily: "Poppins",
        color: "var(--text-primary)",
        fontSize: "14px",
        "&:hover": {
          "& + fieldset": {
            borderColor: "var(--black-to-white)",
          },
        },
      },
      "& fieldset.MuiOutlinedInput-notchedOutline": {
        width: "100%",
        borderRadius: "4px",
        borderColor: "var(--black-to-white)",
        borderRadius: "16px",
      },
      "&.Mui-error": {
        "& .MuiOutlinedInput-input": {
          "&:hover": {
            "& + fieldset": {
              borderColor: "var(--red-main)!important",
            },
          },
        },
        "& fieldset.MuiOutlinedInput-notchedOutline": {
          borderColor: "var(--red-main)!important",
        },
      },
      "&.Mui-focused:not(.Mui-error)": {
        "& fieldset.MuiOutlinedInput-notchedOutline": {
          borderColor: "var(--black-to-white)!important",
        },
      },
    },
    "& label": {
      color: "var(--text-primary)",
      backgroundColor: "var(--user-sidebar-main)",
      paddingTop: "8px",
      paddingInline: "10px",
      marginTop: "-8px",
      fontSize: "14px",
      color: "var(--text-primary) !important",
    },
  };

  return (
    <AuthLayout>
      <div className={`${classes.RegisterContent} RegisterContent`}>
        <div className={classes.RegisterContentLeft}>
          <img
            src="/img/Rectangle 1154.png"
            alt=""
            className={classes.RegisterContentLeftBgImg}
          />
          <span className={classes.RegisterContentLeftOpacity}></span>
          <div className={classes.RegisterContentLeftBody}>
            <div>
              <div className={classes.BakuElLogo}>
                <img src="/img/BakuElectronics.png" alt="" />
              </div>
            </div>
            <div>
              <div className={classes.text}>
                <span>
                  Scooterlarda <b>Black Friday</b> endirimləri?
                </span>
              </div>
              <div className={classes.btn}>
                <Button>Ətraflı bax</Button>
              </div>
            </div>
          </div>
        </div>
        <div className={classes.RegisterContentRight}>
          <div className={classes.RegisterContentRightWrapper}>
            <nav className={classes.Navbar}>
              <Link href="/">Kömək</Link>
              <Link href="/">Ana səhifə</Link>
              <Link href="/">Gizlilik</Link>
              <Link href="/">Şərtlər və qaydalar</Link>
            </nav>
            <div>
              <form className={classes.Card} onSubmit={formik.handleSubmit}>
                <div className={classes.CardTitle}>
                  {/* <div className={classes.CardTitleIcon}>
                    <img
                      src="img/Arrow-Left.svg"
                      alt=""
                      className={classes.LeftArrow}
                    />
                  </div> */}
                  <div className={classes.CardTitleTxt}>Qeydiyyat</div>
                </div>

                <TextField
                  sx={textFieldStyles}
                  variant="outlined"
                  label="Ad və soyad"
                  fullWidth
                  placeholder="Ad və soyad"
                  autoComplete="off"
                  error={getInputClasses("fullname")}
                  {...formik.getFieldProps("fullname")}
                  InputProps={{
                    endAdornment: (
                      <User
                        style={{
                          color: "#EA2427",
                          width: "18px",
                          marginRight: "10px",
                        }}
                      />
                    ),
                  }}
                />
                <div
                  style={{
                    height: "36px",
                    color: "red",
                    fontSize: "13px",
                    lineHeight: "2",
                  }}
                >
                  {formik.touched.fullname && formik.errors.fullname ? (
                    <div>
                      <div className="">{formik.errors.fullname}</div>
                    </div>
                  ) : null}
                </div>
                <TextField
                  sx={textFieldStyles}
                  variant="outlined"
                  label="E-poçt"
                  fullWidth
                  placeholder="E-poçt"
                  autoComplete="off"
                  error={getInputClasses("email")}
                  {...formik.getFieldProps("email")}
                  InputProps={{
                    endAdornment: (
                      <FaRegEnvelope
                        style={{
                          color: "#EA2427",
                          width: "18px",
                          marginRight: "10px",
                        }}
                      />
                    ),
                  }}
                />
                <div
                  style={{
                    height: "36px",
                    color: "red",
                    fontSize: "13px",
                    lineHeight: "2",
                  }}
                >
                  {formik.touched.email && formik.errors.email ? (
                    <div className="">
                      <div className="">{formik.errors.email}</div>
                    </div>
                  ) : null}
                </div>
                <TextField
                  sx={textFieldStyles}
                  variant="outlined"
                  label="Şifrə"
                  fullWidth
                  placeholder="Şifrə"
                  autoComplete="off"
                  error={getInputClasses("password")}
                  type={showPassword ? "text" : "password"}
                  {...formik.getFieldProps("password")}
                  InputProps={{
                    endAdornment: (
                      <InputAdornment position="end">
                        <IconButton
                          aria-label="toggle password visibility"
                          onClick={() => setShowPassword(!showPassword)}
                          // onMouseDown={handleMouseDownPassword}
                          edge="end"
                          style={{
                            marginRight: "2px",
                            marginTop: "6px",
                          }}
                        >
                          {showPassword ? (
                            <MdOutlineVisibilityOff
                              style={{
                                color: "#EA2427",
                                width: "18px",
                              }}
                            />
                          ) : (
                            <MdOutlineVisibility
                              style={{
                                color: "#EA2427",
                                width: "18px",
                              }}
                            />
                          )}
                        </IconButton>
                      </InputAdornment>
                    ),
                  }}
                />
                <div
                  style={{
                    minHeight: "36px",
                    color: "red",
                    fontSize: "13px",
                    lineHeight: "2",
                  }}
                >
                  {formik.touched.password && formik.errors.password ? (
                    <div className="">
                      <div className="">{formik.errors.password}</div>
                    </div>
                  ) : null}
                  {isError ? (
                    <div className="">
                      <div className="">{isError}</div>
                    </div>
                  ) : null}
                </div>
                <Button className={classes.RegisterBtn} type="submit">
                  Qeydiyyatdan keç
                </Button>
                <Typography
                  sx={{
                    textAlign: "center",
                    fontSize: "14px",
                    fontWeight: 500,
                    fontFamily: "Poppins",
                    color: "var(--text-primary)",

                    "@media(max-width: 475px)": {
                      fontSize: "12px",
                    },
                  }}
                >
                  Hesabınız var?
                  <Link href="/auth/login">
                    <b
                      style={{
                        marginLeft: "5px",
                        textDecoration: "underline",
                        cursor: "pointer",
                      }}
                    >
                      Daxil olun
                    </b>
                  </Link>
                </Typography>
                <div className={classes.SocialButtons}>
                  <Button
                    className={classes.SocialBtn}
                    sx={{ marginRight: "16px" }}
                  >
                    <GoogleLogo />
                  </Button>
                  <Button className={classes.SocialBtn}>
                    <FbLogo />
                  </Button>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    </AuthLayout>
  );
};

// export default Index;

let getCodeData = {
  email: "ledelep627@agrolivana.com",
  user: null,
  token: null,
  refreshToken: null,
  csrfToken: null,
  errors: [],
  second: 900,
};

// const mapStateToProps = (state) => {
//   return {
//     sidebarIsOpen: state.layout.sidebar.sidebarIsOpen,
//     sidebar: state.layout.sidebar,
//   };
// };

const mapDispatchToProps = {
  setRegisterEmail,
};

export default connect(null, mapDispatchToProps)(Index);
