import classes from "./index.module.scss";
import Link from "next/link";
// import { FacebookLogo } from "../Icons";
import { Button, Card, styled, TextField, Typography } from "@mui/material";
import { useState } from "react";
import Router from "next/router";

import IconButton from "@mui/material/IconButton";
import OutlinedInput from "@mui/material/OutlinedInput";
import InputLabel from "@mui/material/InputLabel";
import InputAdornment from "@mui/material/InputAdornment";
import FormControl from "@mui/material/FormControl";
import { MdOutlineVisibility } from "react-icons/md";
import { MdOutlineVisibilityOff } from "react-icons/md";
import { FaRegEnvelope } from "react-icons/fa";
import FullHeader from "../../../src/components/FullHeader/FullHeader";
import MobileBottomNavbar from "../../../src/components/MobileBottomNavbar/MobileBottomNavbar";
import axios from "axios";
import { useFormik } from "formik";
import * as Yup from "yup";
import { connect } from "react-redux";
import { setUser } from "../../../redux/actions/main";
import AuthLayout from "../../../src/hoc/Layout/AuthLayout/AuthLayout";


const Index = (props) => {
  const { email, setUser } = props;
  const [showPassword, setShowPassword] = useState(false);
  const [isError, setIsError] = useState("");

  const initialValues = {
    code: "",
  };

  const CreateSchema = Yup.object().shape({
    code: Yup.string().required("Kodu daxil edin."),
  });

  const formik = useFormik({
    initialValues,
    validationSchema: CreateSchema,
    onSubmit: (values, { resetForm }) => {
      onSendVerifyCode(values, resetForm);
    },
  });

  const getInputClasses = (filedName) => {
    if (formik.touched[filedName] && formik.errors[filedName]) {
      return true;
    }
    if (formik.touched[filedName] && !formik.errors[filedName]) {
      return false;
    }

    return false;
  };

  const onSendVerifyCode = (values, resetForm) => {
    const group = {
      email: email,
      code: values.code,
    };
    axios({
      method: "post",
      url: `${process.env.base_url}account/verify-code`,
      data: group,
      // headers: {
      //   Authorization: `bearer ${token}`,
      // },
    })
      .then((res) => {
        // console.log("res verify code", res.data);
        setUser(res.data);
        Router.push("/");
        resetForm();
      })
      .catch((err) => {
        console.log("err", err);
        setIsError(err.response.data.detail[0]);
        resetForm();
        //   let message =
        //     typeof err.response !== "undefined"
        //       ? err.response.data.message
        //       : err.message;
        //   setIsError(message);
      });
  };

  function resetForm() {
    formik.resetForm();
  }

  const handleMouseDownPassword = (event) => {
    event.preventDefault();
  };


  const textFieldStyles = {
    "& .MuiOutlinedInput-root": {
      "& .MuiOutlinedInput-input": {
        padding: "14px 24px",
        color: "red",
        fontFamily: "Poppins",
        color: "var(--text-primary)",
        fontSize: "14px",
        "&:hover": {
          "& + fieldset": {
            borderColor: "var(--black-to-white)",
          },
        },
      },
      "& fieldset.MuiOutlinedInput-notchedOutline": {
        width: "100%",
        borderRadius: "4px",
        borderColor: "var(--black-to-white)",
        borderRadius: "16px",
      },
      "&.Mui-error": {
        "& .MuiOutlinedInput-input": {
          "&:hover": {
            "& + fieldset": {
              borderColor: "var(--red-main)!important",
            },
          },
        },
        "& fieldset.MuiOutlinedInput-notchedOutline": {
          borderColor: "var(--red-main)!important",
        },
      },
      "&.Mui-focused:not(.Mui-error)": {
        "& fieldset.MuiOutlinedInput-notchedOutline": {
          borderColor: "var(--black-to-white)!important",
        },
      },
    },
    "& label": {
      color: "var(--text-primary)",
      backgroundColor: "var(--user-sidebar-main)",
      paddingTop: "8px",
      paddingInline: "10px",
      marginTop: "-8px",
      fontSize: "14px",
      color: "var(--text-primary) !important",
    },
  };


  return (
    <AuthLayout>
      {/* <div className={classes.Header}>
        <FullHeader />
      </div> */}
      <div className={`${classes.ConfirmationContent} LoginContent`}>
        <div className={classes.ConfirmationContentLeft}>
          <img
            src="/img/Rectangle 1154.png"
            alt=""
            className={classes.ConfirmationContentLeftBgImg}
          />
          <span className={classes.ConfirmationContentLeftOpacity}></span>
          <div className={classes.ConfirmationContentLeftBody}>
            <div>
              <div className={classes.BakuElLogo}>
                <img src="/img/BakuElectronics.png" alt="" />
              </div>
            </div>
            <div>
              <div className={classes.text}>
                <span>
                  Scooterlarda <b>Black Friday</b> endirimləri?
                </span>
              </div>
              <div className={classes.btn}>
                <Button>Ətraflı bax</Button>
              </div>
            </div>
          </div>
        </div>
        <div className={classes.ConfirmationContentRight}>
          <div className={classes.ConfirmationContentRightWrapper}>
            <nav className={classes.Navbar}>
              <Link href="/">Kömək</Link>
              <Link href="/">Ana səhifə</Link>
              <Link href="/">Gizlilik</Link>
              <Link href="/">Şərtlər və qaydalar</Link>
            </nav>
            <div>
              <form className={classes.Card} onSubmit={formik.handleSubmit}>
                <div className={classes.CardTitle}>
                  <div className={classes.CardTitleIcon}>
                    <img
                      src="img/Arrow - Left.svg"
                      alt=""
                      className={classes.LeftArrow}
                    />
                  </div>
                  <div className={classes.CardTitleTxt}>Təsdiq kodu</div>
                </div>

                <TextField
                  id="outlined-basic"
                  label="Təsdiq kodu"
                  variant="outlined"
                  fullWidth
                  placeholder="Təsdiq kodu"
                  error={getInputClasses("code") || isError}
                  {...formik.getFieldProps("code")}
                  type={showPassword ? "text" : "password"}
                  // value={values.password}
                  // onChange={handleChange("password")}
                  {...formik.getFieldProps("code")}
                  InputProps={{
                    endAdornment: (
                      <InputAdornment position="end">
                        <IconButton
                          aria-label="toggle password visibility"
                          onClick={() => setShowPassword(!showPassword)}
                          // onMouseDown={handleMouseDownPassword}
                          edge="end"
                          style={{
                            marginRight: "2px",
                            marginTop: "6px",
                          }}
                        >
                          {showPassword ? (
                            <MdOutlineVisibilityOff
                              style={{
                                color: "#EA2427",
                                width: "18px",
                              }}
                            />
                          ) : (
                            <MdOutlineVisibility
                              style={{
                                color: "#EA2427",
                                width: "18px",
                              }}
                            />
                          )}
                        </IconButton>
                      </InputAdornment>
                    ),
                  }}
                  sx={textFieldStyles}
                />
                <div
                  style={{
                    height: "36px",
                    color: "red",
                    fontSize: "13px",
                    lineHeight: "2",
                  }}
                >
                  {(formik.touched.code && formik.errors.code) || isError ? (
                    <div>
                      <div className="">{formik.errors.code}</div>
                      <div>{isError}</div>
                    </div>
                  ) : null}
                  {/* <div>{isError && isError}</div> */}
                </div>
                <Button className={classes.ConfirmationBtn} type="submit">
                  Hesabı yarat
                </Button>
                <Typography
                  sx={{
                    textAlign: "center",
                    fontSize: "14px",
                    fontWeight: 500,
                    fontFamily: "Poppins",
                    padding: "13px 40px",
                    color: "var(--text-primary)"
                  }}
                >
                  <b
                    style={{
                      marginRight: "5px",
                      cursor: "pointer",
                      color: "var(--text-primary)"
                    }}
                  >
                    2:59
                  </b>
                  saniyədən sonra yenidən göndər
                </Typography>
              </form>
            </div>
          </div>
        </div>
      </div>
      {/* <MobileBottomNavbar /> */}
    </AuthLayout>
  );
};

// export default Index;

const mapStateToProps = (state) => {
  return {
    email: state.auth.verifyData.email,
  };
};

const mapDispatchToProps = {
  setUser,
};

export default connect(mapStateToProps, mapDispatchToProps)(Index);
