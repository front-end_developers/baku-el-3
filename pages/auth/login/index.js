/* eslint-disable @next/next/no-img-element */
import classes from "./index.module.scss";
import Link from "next/link";
// import { FacebookLogo } from "../Icons";
import Router from "next/router";
import { Button, Card, styled, TextField, Typography } from "@mui/material";
import { useState } from "react";
import IconButton from "@mui/material/IconButton";
import OutlinedInput from "@mui/material/OutlinedInput";
import InputLabel from "@mui/material/InputLabel";
import InputAdornment from "@mui/material/InputAdornment";
import FormControl from "@mui/material/FormControl";
import { MdOutlineVisibility } from "react-icons/md";
import { MdOutlineVisibilityOff } from "react-icons/md";
import { FaRegEnvelope } from "react-icons/fa";
import { GoogleLogo, FbLogo } from "../../../src/components/Icons";
import FullHeader from "../../../src/components/FullHeader/FullHeader";
import MobileBottomNavbar from "../../../src/components/MobileBottomNavbar/MobileBottomNavbar";
import AuthLayout from "../../../src/hoc/Layout/AuthLayout/AuthLayout";
import axios from "axios";
import { useFormik } from "formik";
import * as Yup from "yup";
import { setUser } from "../../../redux/actions/main";
import { connect } from "react-redux";

const Index = (props) => {
  const { setUser } = props;
  const [showPassword, setShowPassword] = useState(false);
  const [isError, setIsError] = useState("");

  const initialValues = {
    email: "",
    password: "",
  };

  const CreateSchema = Yup.object().shape({
    email: Yup.string().email().required("Email daxil edin."),
    password: Yup.string().required("Şifrəni daxil edin."),
  });

  const formik = useFormik({
    initialValues,
    validationSchema: CreateSchema,
    onSubmit: (values, { resetForm }) => {
      onLogin(values, resetForm);
    },
  });

  const getInputClasses = (filedName) => {
    if (formik.touched[filedName] && formik.errors[filedName]) {
      return true;
    }
    if (formik.touched[filedName] && !formik.errors[filedName]) {
      return false;
    }

    return false;
  };

  function resetForm() {
    formik.resetForm();
  }
  const onLogin = (values, resetForm) => {
    const group = {
      email: values.email,
      password: values.password,
      lang: "AZ",
    };
    axios({
      method: "post",
      url: `${process.env.base_url}account/login`,
      data: group,
      // headers: {
      //   Authorization: `bearer ${token}`,
      // },
    })
      .then((res) => {
        // console.log("res verify code", res.data);
        setUser(res.data);
        Router.push("/");
        resetForm();
      })
      .catch((err) => {
        // console.log("err", err.response);
        setIsError(err.response.data.detail[0]);
        resetForm();

        //   let message =
        //     typeof err.response !== "undefined"
        //       ? err.response.data.message
        //       : err.message;
        //   setIsError(message);
      });
  };

  // const handleChange = (prop) => (event) => {
  //   setValues({ ...values, [prop]: event.target.value });
  // };

  // const handleClickShowPassword = () => {
  //   setValues({
  //     ...values,
  //     showPassword: !values.showPassword,
  //   });
  // };

  // const handleMouseDownPassword = (event) => {
  //   event.preventDefault();
  // };

  const textFieldStyles = {
    "& .MuiOutlinedInput-root": {
      "& .MuiOutlinedInput-input": {
        padding: "14px 24px",
        color: "red",
        fontFamily: "Poppins",
        color: "var(--text-primary)",
        fontSize: "14px",
        "&:hover": {
          "& + fieldset": {
            borderColor: "var(--black-to-white)",
          },
        },
      },
      "& fieldset.MuiOutlinedInput-notchedOutline": {
        width: "100%",
        borderRadius: "4px",
        borderColor: "var(--black-to-white)",
        borderRadius: "16px",
      },
      "&.Mui-error": {
        "& .MuiOutlinedInput-input": {
          "&:hover": {
            "& + fieldset": {
              borderColor: "var(--red-main)!important",
            },
          },
        },
        "& fieldset.MuiOutlinedInput-notchedOutline": {
          borderColor: "var(--red-main)!important",
        },
      },
      "&.Mui-focused:not(.Mui-error)": {
        "& fieldset.MuiOutlinedInput-notchedOutline": {
          borderColor: "var(--black-to-white)!important",
        },
      },
    },
    "& label": {
      color: "var(--text-primary)",
      backgroundColor: "var(--user-sidebar-main)",
      paddingTop: "8px",
      paddingInline: "10px",
      marginTop: "-8px",
      fontSize: "14px",
      color: "var(--text-primary) !important",
    },
  };

  return (
    <AuthLayout>
      <div className={`${classes.LoginContent} LoginContent`}>
        <div className={classes.LoginContentLeft}>
          <img
            src="/img/Rectangle 1154.png"
            alt=""
            className={classes.LoginContentLeftBgImg}
          />
          <span className={classes.LoginContentLeftOpacity}></span>
          <div className={classes.LoginContentLeftBody}>
            <div>
              <div className={classes.BakuElLogo}>
                <img src="/img/BakuElectronics.png" alt="" />
              </div>
            </div>
            <div>
              <div className={classes.text}>
                <span>
                  Scooterlarda <b>Black Friday</b> endirimləri?
                </span>
              </div>
              <div className={classes.btn}>
                <Button>Ətraflı bax</Button>
              </div>
            </div>
          </div>
        </div>
        <div className={classes.LoginContentRight}>
          <div className={classes.LoginContentRightWrapper}>
            <nav className={classes.Navbar}>
              <Link href="/">Kömək</Link>
              <Link href="/">Ana səhifə</Link>
              <Link href="/">Gizlilik</Link>
              <Link href="/">Şərtlər və qaydalar</Link>
            </nav>
            <div>
              <form className={classes.Card} onSubmit={formik.handleSubmit}>
                <div className={classes.CardTitle}>
                  <div className={classes.CardTitleIcon}>
                    <img
                      src="img/Arrow - Left.svg"
                      alt=""
                      className={classes.LeftArrow}
                    />
                  </div>
                  <div className={classes.CardTitleTxt}>Daxil olun</div>
                </div>

                <TextField
                  variant="outlined"
                  id="E-poçt"
                  label="E-poçt"
                  fullWidth
                  placeholder="E-poçt"
                  autoComplete="off"
                  error={getInputClasses("email")}
                  {...formik.getFieldProps("email")}
                  InputProps={{
                    endAdornment: (
                      <FaRegEnvelope
                        style={{
                          color: "#EA2427",
                          width: "18px",
                          marginRight: "10px",
                        }}
                      />
                    ),
                  }}
                  sx={textFieldStyles}
                />
                <div
                  style={{
                    height: "36px",
                    color: "red",
                    fontSize: "13px",
                    lineHeight: "2",
                  }}
                >
                  {formik.touched.email && formik.errors.email ? (
                    <div>
                      <div className="">{formik.errors.email}</div>
                    </div>
                  ) : null}
                </div>

                <TextField
                  variant="outlined"
                  id="Şifrə"
                  label="Şifrə"
                  fullWidth
                  placeholder="Şifrə"
                  autoComplete="off"
                  error={getInputClasses("password")}
                  type={showPassword ? "text" : "password"}

                  {...formik.getFieldProps("password")}
                  InputProps={{
                    endAdornment: (
                      <InputAdornment position="end">
                        <IconButton
                          aria-label="toggle password visibility"
                          onClick={() => setShowPassword(!showPassword)}
                          // onMouseDown={handleMouseDownPassword}
                          edge="end"
                          style={{
                            marginRight: "2px",
                            marginTop: "6px",
                          }}
                        >
                          {showPassword ? (
                            <MdOutlineVisibilityOff
                              style={{
                                color: "#EA2427",
                                width: "18px",
                              }}
                            />
                          ) : (
                            <MdOutlineVisibility
                              style={{
                                color: "#EA2427",
                                width: "18px",
                              }}
                            />
                          )}
                        </IconButton>
                      </InputAdornment>
                    ),
                  }}
                  sx={textFieldStyles}
                />
                <div
                  style={{
                    height: "36px",
                    color: "red",
                    fontSize: "13px",
                    lineHeight: "2",
                  }}
                >
                  {(formik.touched.password && formik.errors.password) ||
                  isError ? (
                    <div>
                      <div className="">{formik.errors.password}</div>
                      <div>{isError}</div>
                    </div>
                  ) : null}
                </div>

                <Typography
                  sx={{
                    marginTop: "16px",
                    float: "right",
                    textDecoration: "underline",
                    fontSize: "12px",
                    cursor: "pointer",
                    fontWeight: 500,
                    fontFamily: "Poppins",
                    color: "var(--text-primary)",

                    "@media(max-width: 475px)": {
                      fontSize: "10px",
                    },
                  }}
                >
                  <Link href="/auth/new-password">Şifrəmi unutmuşam</Link>
                </Typography>

                <Button className={classes.LoginBtn} type="submit">
                  Daxil ol
                </Button>
                <Typography
                  sx={{
                    textAlign: "center",
                    fontSize: "14px",
                    fontWeight: 500,
                    fontFamily: "Poppins",
                    color: "var(--text-primary)",

                    "@media(max-width: 475px)": {
                      fontSize: "12px",
                    },
                  }}
                >
                  Hesabınız yoxdur?
                  <Link href="/auth/register">
                    <b
                      style={{
                        marginLeft: "5px",
                        textDecoration: "underline",
                        cursor: "pointer",
                      }}
                    >
                      Qeydiyyatdan keçin
                    </b>
                  </Link>
                </Typography>
                <div className={classes.SocialButtons}>
                  <Button
                    className={classes.SocialBtn}
                    sx={{ marginRight: "16px" }}
                  >
                    <GoogleLogo />
                  </Button>
                  <Button className={classes.SocialBtn}>
                    <FbLogo />
                  </Button>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    </AuthLayout>
  );
};

// export default Index;
// const mapStateToProps = (state) => {
//   return {
//     email: state.auth.verifyData.email,
//   };
// };

const mapDispatchToProps = {
  setUser,
};

export default connect(null, mapDispatchToProps)(Index);
