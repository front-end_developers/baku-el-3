import { Fragment, useState } from "react";
import Breadcrumb from "../../src/components/Breadcrumb/Breadcrumb";
import Layout from "../../src/hoc/Layout/Layout/Layout";
import CampaignsNews from "../../src/components/CampaignsNews/CampaignsNews";
import ProductCard from "../../src/components/ProductCard/ProductCard";
import MobileBottomNavbar from "../../src/components/MobileBottomNavbar/MobileBottomNavbar";
import Pagination from "../../src/components/Pagination/Pagination";
import classes from "./inner-page.module.scss";
import Link from "next/link";
import {
  ArrowLeft,
  ArrowRight,
  FilterSecondary,
  MenuStyleGrid,
  MenuStyleList,
  Search,
} from "../../src/components/Icons";
import { Button } from "@mui/material";
import ActiveLink from "../../src/components/Link/Link";
import RangeSlider from "../../src/components/RangeSlider/RangeSlider";
import Checkbox from "../../src/components/Checkbox/Checkbox";
import SearchBar from "../../src/components/ProductsSidebar/SearchBar/SearchBar";
import Drawer from "@mui/material/Drawer";
import { MdKeyboardArrowDown, MdKeyboardArrowUp } from "react-icons/md";
import ProductsSidebar from "../../src/components/ProductsSidebar/ProductsSidebar";

const InnerPage = () => {
  const [listStyle, setListStyle] = useState("grid");
  const [showDetails, setShowDetails] = useState();
  const [state, setState] = useState({
    left: false,
  });

  const toggleDrawer = (anchor, open) => (event) => {
    if (
      event.type === "keydown" &&
      (event.key === "Tab" || event.key === "Shift")
    ) {
      return;
    }

    setState({ ...state, [anchor]: open });
  };

  const list = (anchor) => <ProductsSidebar />;

  return (
    <Layout>
      {/* <Breadcrumb steps={["Telefonlar və qadcetlər", "Smartfonlar"]} /> */}

      <div className={classes.CampaignsContainer}>
        <div className={classes.CampaignsContainerHeader}>
          <div className={classes.CampaignsContainerHeaderLeft}>
            <div className={classes.CampaignsNews}>
              <img src="/img/carousel-1.png" className={classes.CampaignsImg} />
              <div className={classes.CampaignsNewsButton}>
                <button>Endirim</button>
              </div>
            </div>
            <div className={classes.CampaignsInformation}>
              <div className={classes.CampaignsInformationLastDate}>
                29 Avq - 29 Sentyabr
              </div>
              <div className={classes.CampaignsInformationTitle}>
                Qışa tam hazır təklif, 18 ayadək fazisiz, ilkin ödənişsiz
              </div>
              <div className={classes.CampaignsInformationBody}>
                Ante amet, risus amet turpis sapien, commodo. Fusce pulvinar
                amet posuere a, diam viverra in. Elit habitant urna, enim, id
                suscipit lectus amet tortor. Mi libero malesuada nisl congue.
                Leo purus non in ultrices ut cum hendrerit eros leo. Lectus
                tempor mi eu tellus. Ut ipsum senectus in porttitor ut
                scelerisque sollicitudin. Habitant adipiscing risus, integer
                tempus aliquam id tortor, suspendisse nisl. Quis etiam mauris et
                vestibulum. Adipiscing sit fusce diam suspendisse lorem lacus.
                Sem quis nam ac facilisi nunc nec eget amet. Ornare dapibus
                dictum quisque ac senectus penatibus. Ipsum eu neque semper
                fermentum consectetur vitae. Consequat, dignissim facilisis
                vitae iaculis amet. Venenatis erat malesuada rhoncus nec ut.
                Egestas nulla est praesent nunc risus. Tempus risus, vulputate
                tempor nibh. Venenatis parturient massa mauris mauris. Lorem
                faucibus ut eu, ac, pharetra. Turpis etiam imperdiet cursus
                velit.
              </div>
              <div className={classes.CampaignsInformationLastTime}>
                <div className={classes.CampaignsInformationLastTimeDay}>
                  <span>04</span>
                  <span>Gün</span>
                </div>
                <div className={classes.CampaignsInformationLastTimeHour}>
                  <span>22</span>
                  <span>Saat</span>
                </div>
                <div className={classes.CampaignsInformationLastTimeMinutes}>
                  <span>18</span>
                  <span>Dəqiqə</span>
                </div>
              </div>
            </div>
          </div>
          <div className={classes.CampaignsContainerHeaderRight}>
            <CampaignsNews />
            <CampaignsNews />
          </div>
        </div>

        <div className={classes.CampaignsContainerTitle}>
          <div className={classes.CampaignsContainerTitleTop}>Məhsullar</div>
          <div className={classes.CampaignsContainerTitleBottom}>
            Kampaniyada iştirak edən məhsullar
          </div>
        </div>

        <div className={classes.CampaignsContainerBody}>
          <div className={classes.CampaignsContainerBodyWrapper}>
            <div className={classes.CampaignsContainerBodySidebar}>
              <div className={classes.CampaignsContainerBodySidebarForm}>
                <div className={classes.PriceFilter}>
                  <div className={classes.PriceFilterTitle}>Qiymət</div>
                  <div className={classes.PriceFilterContent}>
                    <div className={classes.PriceFilterContentInput}>
                      <RangeSlider />
                    </div>
                  </div>
                </div>

                {/* <div className={classes.ColorFilter}>
                <div className={classes.ColorFilterTitle}>Rəng</div>
                <div className={classes.ColorFilterContent}>Colors.....</div>
              </div> */}

                <div className={classes.ProductsSidebarBrends}>
                  <div className={classes.ProductsSidebarBrendsTitle}>
                    Brend
                  </div>
                  <div className={classes.Search}>
                    <SearchBar />
                  </div>
                  <div className={classes.ProductsSidebarBrendsFirstGroup}>
                    <div className={classes.ProductsSidebarBrendsCheckbox}>
                      <Checkbox />
                      <span>60 mehsul</span>
                    </div>
                    <div className={classes.ProductsSidebarBrendsCheckbox}>
                      <Checkbox />
                      <span>60 mehsul</span>
                    </div>
                    <div className={classes.ProductsSidebarBrendsCheckbox}>
                      <Checkbox />
                      <span>60 mehsul</span>
                    </div>
                  </div>
                  <div
                    className={`${
                      showDetails
                        ? classes.ProductsSidebarBrendsSecondGroupShow
                        : classes.ProductsSidebarBrendsSecondGroup
                    }`}
                  >
                    <div className={classes.ProductsSidebarBrendsCheckbox}>
                      <Checkbox />
                      <span>7 mehsul</span>
                    </div>
                    <div className={classes.ProductsSidebarBrendsCheckbox}>
                      <Checkbox />
                      <span>6 mehsul</span>
                    </div>
                    <div className={classes.ProductsSidebarBrendsCheckbox}>
                      <Checkbox />
                      <span>12 mehsul</span>
                    </div>
                  </div>

                  <button
                    style={{ marginBottom: "15px", fontSize: "12px" }}
                    type="button"
                    onClick={() => setShowDetails(!showDetails)}
                  >
                    {showDetails ? (
                      <div>
                        Daha az
                        <MdKeyboardArrowUp style={{ fontSize: "20px" }} />
                      </div>
                    ) : (
                      <div>
                        Daha çox{" "}
                        <MdKeyboardArrowDown style={{ fontSize: "20px" }} />
                      </div>
                    )}
                  </button>
                </div>
              </div>
            </div>
            <div className={classes.CampaignsContainerBodyProducts}>
              <div className={classes.ProductsInner}>
                {/* <div className="container"> */}
                <div className={classes.ProductsInnerHeader}>
                  <div className={classes.ProductsInnerHeaderTitle}>
                    <div className={classes.ProductsInnerHeaderTitleMobile}>
                      <div className={classes.BackButton}>
                        <Link href="/">
                          <a>
                            <ArrowLeft />
                          </a>
                        </Link>
                      </div>
                      <div className={classes.Text}>
                        <p>Telefonlar və qadcetlər</p>
                        <p>1200 Məhsul</p>
                      </div>
                    </div>
                  </div>
                  <div className={classes.ProductsInnerHeaderMobileFilter}>
                    <ActiveLink
                      activeClassName={classes.ActiveFilterBtn}
                      href="/products"
                    >
                      <a>Hamısı</a>
                    </ActiveLink>
                    <ActiveLink
                      activeClassName={classes.ActiveFilterBtn}
                      href="/products/smartphones"
                    >
                      <a>Smartfonlar</a>
                    </ActiveLink>
                    <ActiveLink
                      activeClassName={classes.ActiveFilterBtn}
                      href="/products/smartwatches"
                    >
                      <a>Smart saatlar</a>
                    </ActiveLink>
                    <ActiveLink
                      activeClassName={classes.ActiveFilterBtn}
                      href="/products/ebooks"
                    >
                      <a>Elektron kitablar</a>
                    </ActiveLink>
                  </div>
                  <div className={classes.ProductsInnerHeaderFilter}>
                    <div className={classes.FilterLeft}>
                      <div className={classes.FilterLeftMobile}>
                        <Fragment key={"left"}>
                          <Button
                            className={classes.FilterButton}
                            onClick={toggleDrawer("left", true)}
                          >
                            <span>
                              <FilterSecondary />
                            </span>
                            <span>Filter</span>
                          </Button>
                          <Drawer
                            anchor={"left"}
                            open={state["left"]}
                            onClose={toggleDrawer("left", false)}
                          >
                            {list("left")}
                          </Drawer>
                        </Fragment>
                      </div>
                    </div>
                  </div>
                </div>
                {/* </div> */}
                {/* <div className="container"> */}
                <div className={classes.ProductsInnerBody}>
                  <div
                    className={
                      listStyle === "grid"
                        ? `${classes.ProductsInnerBodyGrid} ${classes.GridActive}`
                        : classes.ProductsInnerBodyGrid
                    }
                  >
                    <ProductCard />
                    <ProductCard />
                    <ProductCard />
                    <ProductCard />
                    <ProductCard />
                    <ProductCard />
                  </div>
                </div>
                {/* </div> */}
                {/* <div className="container"> */}
                <div className="pagination products-pagination">
                  <Pagination />
                </div>
                {/* </div> */}
              </div>
              {/* <MobileBottomNavbar /> */}
            </div>
          </div>
        </div>
        <MobileBottomNavbar />
      </div>
    </Layout>
  );
};

export default InnerPage;
