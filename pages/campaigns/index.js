import React from "react";
import Breadcrumb from "../../src/components/Breadcrumb/Breadcrumb";
import Layout from "../../src/hoc/Layout/Layout/Layout";
import FullSectionFive from "../../src/components/FullSectionFive/FullSectionFive";
import CampaignsNews from "../../src/components/CampaignsNews/CampaignsNews";
import MobileBottomNavbar from "../../src/components/MobileBottomNavbar/MobileBottomNavbar";

import { Row, Col } from "react-bootstrap";
import classes from "./index.module.scss";
import Link from "next/link";
import { TopRatedBadge } from "../../src/components/Icons";
import { Button } from "@mui/material";
import FullInfoCardList from "../../src/components/FullInfoCardList/FullInfoCardList";

const Index = () => {
  return (
    <Layout>
      {/* <Breadcrumb steps={["Telefonlar və qadcetlər", "Smartfonlar"]} /> */}

      <div className={classes.CampaignsContainer}>
        <div className={classes.CampaignsContainerInnerHeader}>
          <div className={classes.CampaignsContainerInnerHeaderTitle}>
            Kampaniyalar
          </div>

          <div className={classes.CampaignsContainerInnerHeaderFilter}>
            <ul>
              <li>
                <button className={classes.ActiveLink}>Hamısı</button>
              </li>
              <li>
                <Link href="/">
                  <button>Endirim</button>
                </Link>
              </li>

              <li>
                <Link href="/">
                  <button>Hədiyyə</button>
                </Link>
              </li>
              <li>
                <Link href="/">
                  <button>Nisyə alış</button>
                </Link>
              </li>
            </ul>
          </div>
        </div>

        <div className={classes.CampaignsContainerHeader}>
          <div className={classes.Campaigns}>
            <div className={classes.CampaignsItems}>
              <div className={classes.CampaignsItemsItem}>
                <img
                  src="/img/carousel-1.png"
                  className={classes.CampaignsImg}
                />

                <span className={classes.CampaignsItemsItemBackground}></span>

                <div className={classes.CampaignsItemsItemButton}>
                  <button>Endirim</button>
                </div>
                <div className={classes.CampaignsCaptions}>
                  <div className={classes.CampaignsCaptionsContent}>
                    <div className={classes.CampaignsCaptionsContentTitle}>
                      <div className={classes.CampaignsCaptionsContentTitleDay}>
                        <span>04</span>
                        <span>Gün</span>
                      </div>
                      <div
                        className={classes.CampaignsCaptionsContentTitleHour}
                      >
                        <span>22</span>
                        <span>Saat</span>
                      </div>
                      <div
                        className={classes.CampaignsCaptionsContentTitleMinutes}
                      >
                        <span>18</span>
                        <span>Dəqiqə</span>
                      </div>
                    </div>
                    <div className={classes.CampaignsCaptionsContentBody}>
                      <span>
                        Qışa tam hazır təklif,18 ayadək faizsiz, ilkin
                        odənişsiz!
                      </span>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>

          <CampaignsNews />
          <CampaignsNews />
          <CampaignsNews />
          <CampaignsNews />
          <CampaignsNews />
          <CampaignsNews />
          <CampaignsNews />
          <CampaignsNews />
          <CampaignsNews />
        </div>
      </div>

      <div className={classes.SectionFour}>
        <FullInfoCardList />
      </div>

      <FullSectionFive />
      <MobileBottomNavbar/>
    </Layout>
  );
};

export default Index;
