import { useState } from "react";
import classes from "./index.module.scss";
import SlickSlider from "../../src/components/SlickSlider/SlickSlider";
import NewsCards from "../../src/components/NewsCards/NewsCards";
import Layout from "../../src/hoc/Layout/Layout/Layout";
import { Container, Row, Col } from "react-bootstrap";
import { MdOutlineVisibility } from "react-icons/md";
import { Button } from "@mui/material";
import FullVideoPlayer from "../../src/components/FullVideoPlayer/FullVideoPlayer";
import NewsVideoCard from "../../src/components/NewsVideoCard/NewsVideoCard";
import MobileBottomNavbar from "../../src/components/MobileBottomNavbar/MobileBottomNavbar";
import FullSectionFive from "../../src/components/FullSectionFive/FullSectionFive";

const Index = () => {
  return (
    <Layout>
      <div className={`${classes.NewsPage} NewsPage`}>
        <div className={classes.NewsPageSlider}>
          <SlickSlider />
        </div>

        <NewsCards />

        <FullVideoPlayer />

        <NewsVideoCard />

        <FullSectionFive />
      </div>
      <MobileBottomNavbar />
    </Layout>
  );
};

export default Index;
