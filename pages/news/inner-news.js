import { useState } from "react";
import classes from "./inner-news.module.scss";
import Layout from "../../src/hoc/Layout/Layout/Layout";
import Breadcrumb from "../../src/components/Breadcrumb/Breadcrumb";
import NewsCards from "../../src/components/NewsCards/NewsCards";
import { Container, Row, Col } from "react-bootstrap";
import { MdOutlineVisibility } from "react-icons/md";
import { Button } from "@mui/material";
import FullSectionFive from "../../src/components/FullSectionFive/FullSectionFive";
import VideoPlayer from "../../src/components/VideoPlayer/VideoPlayer";
import MobileBottomNavbar from "../../src/components/MobileBottomNavbar/MobileBottomNavbar";


const InnerNews = () => {
  return (
    <Layout>
      {/* <Breadcrumb steps={["Telefonlar və qadcetlər", "Smartfonlar"]} /> */}

      <div className={classes.InnerNewsPage}>
        <div className={classes.InnerNewsPageTitle}>
          <div className={classes.InnerNewsPageTitleDate}>29 Avq 2021</div>
          <div className={classes.InnerNewsPageTitleText}>
            Mebel, qab-qacaq, tekstil bir sözlə eviniz üçün hər şey artıq bir
            ünvanda
          </div>
        </div>
        <div className={classes.InnerNewsPageContainer}>
          <div className={classes.InnerNewsPageContainerLeft}>
            <div className={classes.InnerNewsPageContainerLeftHeader}>
              <div className={classes.InnerNewsPageContainerLeftHeaderImg}>
                <img src="/img/NewsMain.png" alt="" />
              </div>
            </div>

            <div className={classes.InnerNewsPageContainerLeftBody}>
              <div
                className={classes.InnerNewsPageContainerLeftBodyInformation}
              >
                <div
                  className={
                    classes.InnerNewsPageContainerLeftBodyInformationTitle
                  }
                >
                  Baku Electronicsin yeni filialına özəl olaraq
                </div>
                <div
                  className={
                    classes.InnerNewsPageContainerLeftBodyInformationContent
                  }
                >
                  Ante amet, risus amet turpis sapien, commodo. Fusce pulvinar
                  amet posuere a, diam viverra in. Elit habitant urna, enim, id
                  suscipit lectus amet tortor. Mi libero malesuada nisl congue.
                  Leo purus non in ultrices ut cum hendrerit eros leo. Lectus
                  tempor mi eu tellus. Ut ipsum senectus in porttitor ut
                  scelerisque sollicitudin. Habitant adipiscing risus, integer
                  tempus aliquam id tortor, suspendisse nisl. Quis etiam mauris
                  et vestibulum. Adipiscing sit fusce diam suspendisse lorem
                  lacus. Sem quis nam ac facilisi nunc nec eget amet. Ornare
                  dapibus dictum quisque ac senectus penatibus. Ipsum eu neque
                  semper fermentum consectetur vitae. Consequat, dignissim
                  facilisis vitae iaculis amet. Venenatis erat malesuada rhoncus
                  nec ut. Egestas nulla est praesent nunc risus. Tempus risus,
                  vulputate tempor nibh. Venenatis parturient massa mauris
                  mauris. Lorem faucibus ut eu, ac, pharetra. Turpis etiam
                  imperdiet cursus velit.
                </div>
                <div
                  className={
                    classes.InnerNewsPageContainerLeftBodyInformationTitle
                  }
                >
                  Eviniz üçün hər şey
                </div>
                <div
                  className={
                    classes.InnerNewsPageContainerLeftBodyInformationContent
                  }
                >
                  Ante amet, risus amet turpis sapien, commodo. Fusce pulvinar
                  amet posuere a, diam viverra in. Elit habitant urna, enim, id
                  suscipit lectus amet tortor. Mi libero malesuada nisl congue.
                  Leo purus non in ultrices ut cum hendrerit eros leo. Lectus
                  tempor mi eu tellus. Ut ipsum senectus in porttitor ut
                  scelerisque sollicitudin. Habitant adipiscing risus, integer
                  tempus aliquam id tortor, suspendisse nisl. Quis etiam mauris
                  et vestibulum. Adipiscing sit fusce diam suspendisse lorem
                  lacus. Sem quis nam ac facilisi nunc nec eget amet. Ornare
                  dapibus dictum quisque ac senectus penatibus. Ipsum eu neque
                  semper fermentum consectetur vitae. Consequat, dignissim
                  facilisis vitae iaculis amet. Venenatis erat malesuada rhoncus
                  nec ut. Egestas nulla est praesent nunc risus. Tempus risus,
                  vulputate tempor nibh. Venenatis parturient massa mauris
                  mauris. Lorem faucibus ut eu, ac, pharetra. Turpis etiam
                  imperdiet cursus velit. Ante amet, risus amet turpis sapien,
                  commodo. Fusce pulvinar amet posuere a, diam viverra in. Elit
                  habitant urna, enim, id suscipit lectus amet tortor. Mi libero
                  malesuada nisl congue. Leo purus non in ultrices ut cum
                  hendrerit eros leo. Lectus tempor mi eu tellus. Ut ipsum
                  senectus in porttitor ut scelerisque sollicitudin. Habitant
                  adipiscing risus, integer tempus aliquam id tortor,
                  suspendisse nisl. Quis etiam mauris et vestibulum. Adipiscing
                  sit fusce diam suspendisse lorem lacus. Sem quis nam ac
                  facilisi nunc nec eget amet. Ornare dapibus dictum quisque ac
                  senectus penatibus. Ipsum eu neque semper fermentum
                  consectetur vitae. Consequat, dignissim facilisis vitae
                  iaculis amet. Venenatis erat malesuada rhoncus nec ut. Egestas
                  nulla est praesent nunc risus. Tempus risus, vulputate tempor
                  nibh. Venenatis parturient massa mauris mauris. Lorem faucibus
                  ut eu, ac, pharetra. Turpis etiam imperdiet cursus velit.
                </div>
              </div>

              <VideoPlayer />
            </div>
          </div>
          <div className={classes.InnerNewsPageContainerRight}>
            <div className={classes.Sidebar}>
              <div className={classes.SidebarTitle}>Ən çox baxılanlar</div>
              <div className={classes.SidebarItems}>
                <div className={classes.SidebarItemsItem}>
                  <div className={classes.SidebarItemsItemLeft}>
                    <div className={classes.SidebarItemsItemLeftImg}>
                      <img src="/img/sidebar-small-image.png" alt="" />
                    </div>
                  </div>
                  <div className={classes.SidebarItemsItemRight}>
                    <div className={classes.SidebarItemsItemRightTop}>
                      Iphone 13 Pro MAX Təqdimatı
                    </div>
                    <div className={classes.SidebarItemsItemRightBottom}>
                      <div className={classes.SidebarItemsItemRightBottomView}>
                        <MdOutlineVisibility />
                        1200
                      </div>
                      <div className={classes.SidebarItemsItemRightBottomDate}>
                        29 Avq 2021
                      </div>
                    </div>
                  </div>
                </div>

                <div className={classes.SidebarItemsItem}>
                  <div className={classes.SidebarItemsItemLeft}>
                    <div className={classes.SidebarItemsItemLeftImg}>
                      <img src="/img/sidebar-small-image.png" alt="" />
                    </div>
                  </div>
                  <div className={classes.SidebarItemsItemRight}>
                    <div className={classes.SidebarItemsItemRightTop}>
                      Iphone 13 Pro MAX Təqdimatı
                    </div>
                    <div className={classes.SidebarItemsItemRightBottom}>
                      <div className={classes.SidebarItemsItemRightBottomView}>
                        <MdOutlineVisibility />
                        1200
                      </div>
                      <div className={classes.SidebarItemsItemRightBottomDate}>
                        29 Avq 2021
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>

            <div className={classes.SubscribeSidebar}>
              <div className={classes.SubscribeSidebarTitle}>
                Heç bir yeniliyi qaçırmamaq üçün abunə olun
              </div>

              <div className={classes.SubscribeSidebarContent}>
                <div className={classes.EmailInput}>
                  <input type="email" placeholder="E-poçt" />
                </div>
                <Button>Abunə ol</Button>
              </div>
            </div>
          </div>
        </div>

        <NewsCards />

        <FullSectionFive />
      </div>
      <MobileBottomNavbar/>

    </Layout>
  );
};

export default InnerNews;
