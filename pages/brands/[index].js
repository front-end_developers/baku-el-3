import React, { useRef, useState } from "react";
import classes from "./index.module.scss";
import Layout from "../../src/hoc/Layout/Layout/Layout";
import { DoubleArrow, PlayIcon } from "../../src/components/Icons";
import Link from "next/link";
import { Row, Col } from "react-bootstrap";
import { Button, ButtonBase } from "@mui/material";
import ReactPlayer from "react-player";
import { BsPauseCircle } from "react-icons/bs";
import ActiveLink from "../../src/components/Link/Link";
import BrandTopHeader from "../../src/components/BrandTopHeader/BrandTopHeader";
import Breadcrumb from "../../src/components/Breadcrumb/Breadcrumb";
import ProductEventSlider from "../../src/components/ProductEventSlider/ProductEventSlider";
import SectionFive from "../../src/components/SectionFive/SectionFive";
import FullVideoPlayer from "../../src/components/FullVideoPlayer/FullVideoPlayer";
import MobileBottomNavbar from "../../src/components/MobileBottomNavbar/MobileBottomNavbar";


const Index = () => {
  const [cardToggle, setCardToggle] = useState(false);
  const [play, setPlay] = useState(false);
  const [fullScreen, setFullScreen] = useState(false);

  const handleToggle = () => {
    setCardToggle(!cardToggle);
  };

  const playHandler = () => {
    setPlay(!play);
  };
  const fullScreenHandler = () => {
    setFullScreen(!fullScreen);
  };

  return (
    <div>
      <Layout>
        {/* <Breadcrumb steps={["Telefonlar və qadcetlər", "Smartfonlar"]} /> */}

        <BrandTopHeader />

        <div
          className={`${classes.BrandsMainContainer} BrandsMainContainer container-fluid m-0 p-0`}
        >
          <div className={classes.BrandsMainContainerWrapper}>
            <ProductEventSlider />

            <SectionFive />

            <FullVideoPlayer />

            <ProductEventSlider />
          </div>
        </div>
        <MobileBottomNavbar/>
      </Layout>
    </div>
  );
};

export default Index;
