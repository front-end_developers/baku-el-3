import { Fragment, useState } from "react";
import Layout from "../../src/hoc/Layout/Layout/Layout";
import Breadcrumb from "../../src/components/Breadcrumb/Breadcrumb";
import BrandTopHeader from "../../src/components/BrandTopHeader/BrandTopHeader";
import ProductsSidebar from "../../src/components/ProductsSidebar/ProductsSidebar";
import ProductPageCard from "../../src/components/ProductsPageCard/ProductsPageCard";
import ListStyleCard from "../../src/components/ProductsPageCard/ListStyleCard";
import ProductCard from "../../src/components/ProductCard/ProductCard";
import Pagination from "../../src/components/Pagination/Pagination";
import classes from "./all-products.module.scss";
import Link from "next/link";
import {
  ArrowLeft,
  ArrowRight,
  FilterSecondary,
  MenuStyleGrid,
  MenuStyleList,
  Search,
} from "../../src/components/Icons";
import { Button, Drawer } from "@mui/material";
import InfoCardList from "../../src/components/InfoCardList/InfoCardList";
import MobileBottomNavbar from "../../src/components/MobileBottomNavbar/MobileBottomNavbar";
import ActiveLink from "../../src/components/Link/Link";
import RangeSlider from "../../src/components/RangeSlider/RangeSlider";
import Checkbox from "../../src/components/Checkbox/Checkbox";
import SearchBar from "../../src/components/ProductsSidebar/SearchBar/SearchBar";
import { MdKeyboardArrowDown, MdKeyboardArrowUp } from "react-icons/md";

const AllProducts = () => {
  const [listStyle, setListStyle] = useState("grid");
  const [showDetails, setShowDetails] = useState();

  const [state, setState] = useState({
    left: false,
  });

  const toggleDrawer = (anchor, open) => (event) => {
    if (
      event.type === "keydown" &&
      (event.key === "Tab" || event.key === "Shift")
    ) {
      return;
    }

    setState({ ...state, [anchor]: open });
  };

  const list = (anchor) => <ProductsSidebar />;

  return (
    <Layout>
      {/* <Breadcrumb steps={["Telefonlar və qadcetlər", "Smartfonlar"]} /> */}
      <BrandTopHeader />

      <div className={classes.BrandsAllProductsContainer}>
        <div className={classes.BrandsAllProductsContainerBody}>
          <div className={classes.BrandsAllProductsContainerBodySidebar}>
            <div className={classes.BrandsAllProductsContainerBodySidebarForm}>
              <div className={classes.PriceFilter}>
                <div className={classes.PriceFilterTitle}>Qiymət</div>
                <div className={classes.PriceFilterContent}>
                  <div className={classes.PriceFilterContentInput}>
                    <RangeSlider />
                  </div>
                </div>
              </div>

              {/* <div className={classes.ColorFilter}>
                <div className={classes.ColorFilterTitle}>Rəng</div>
                <div className={classes.ColorFilterContent}>Colors.....</div>
              </div> */}

              <div className={classes.ProductsSidebarBrends}>
                <div className={classes.ProductsSidebarBrendsTitle}>Brend</div>
                <div className={classes.Search}>
                  <SearchBar />
                </div>
                <div className={classes.ProductsSidebarBrendsFirstGroup}>
                  <div className={classes.ProductsSidebarBrendsCheckbox}>
                    <Checkbox />
                    <span>60 mehsul</span>
                  </div>
                  <div className={classes.ProductsSidebarBrendsCheckbox}>
                    <Checkbox />
                    <span>60 mehsul</span>
                  </div>
                  <div className={classes.ProductsSidebarBrendsCheckbox}>
                    <Checkbox />
                    <span>60 mehsul</span>
                  </div>
                </div>
                <div
                  className={`${
                    showDetails
                      ? classes.ProductsSidebarBrendsSecondGroupShow
                      : classes.ProductsSidebarBrendsSecondGroup
                  }`}
                >
                  <div className={classes.ProductsSidebarBrendsCheckbox}>
                    <Checkbox />
                    <span>7 mehsul</span>
                  </div>
                  <div className={classes.ProductsSidebarBrendsCheckbox}>
                    <Checkbox />
                    <span>6 mehsul</span>
                  </div>
                  <div className={classes.ProductsSidebarBrendsCheckbox}>
                    <Checkbox />
                    <span>12 mehsul</span>
                  </div>
                </div>

                <button
                  style={{ marginBottom: "15px", fontSize: "12px" }}
                  type="button"
                  onClick={() => setShowDetails(!showDetails)}
                >
                  {showDetails ? (
                    <div>
                      Daha az
                      <MdKeyboardArrowUp style={{ fontSize: "20px" }} />
                    </div>
                  ) : (
                    <div>
                      Daha çox{" "}
                      <MdKeyboardArrowDown style={{ fontSize: "20px" }} />
                    </div>
                  )}
                </button>
              </div>
            </div>
          </div>
          <div className={classes.BrandsAllProductsContainerBodyProducts}>
            <div className={classes.ProductsInner}>
              <div className={classes.ProductsInnerHeader}>
                <div className={classes.ProductsInnerHeaderTitle}>
                  <h2 className={classes.ProductsInnerHeaderTitleDesktop}>
                    Smartfonlar
                  </h2>
                  <div className={classes.ProductsInnerHeaderTitleMobile}>
                    <div className={classes.BackButton}>
                      <Link href="/">
                        <a>
                          <ArrowLeft />
                        </a>
                      </Link>
                    </div>
                    <div className={classes.Text}>
                      <p>Telefonlar və qadcetlər</p>
                      <p>1200 Məhsul</p>
                    </div>
                  </div>
                </div>
                <div className={classes.ProductsInnerHeaderMobileFilter}>
                  <ActiveLink
                    activeClassName={classes.ActiveFilterBtn}
                    href="/products"
                  >
                    <a>Hamısı</a>
                  </ActiveLink>
                  <ActiveLink
                    activeClassName={classes.ActiveFilterBtn}
                    href="/products/smartphones"
                  >
                    <a>Smartfonlar</a>
                  </ActiveLink>
                  <ActiveLink
                    activeClassName={classes.ActiveFilterBtn}
                    href="/products/smartwatches"
                  >
                    <a>Smart saatlar</a>
                  </ActiveLink>
                  <ActiveLink
                    activeClassName={classes.ActiveFilterBtn}
                    href="/products/ebooks"
                  >
                    <a>Elektron kitablar</a>
                  </ActiveLink>
                </div>
                <div className={classes.ProductsInnerHeaderFilter}>
                  <div className={classes.FilterLeft}>
                    <ul className={classes.FilterLeftDesktop}>
                      <li>
                        <button className={classes.ActiveLink}>Popular</button>
                      </li>
                      <li>
                        <button>Endirim</button>
                      </li>

                      <li>
                        <button>Ucuzdan bahaya</button>
                      </li>
                      <li>
                        <button>Bahadan ucuza</button>
                      </li>
                    </ul>
                    <div className={classes.FilterLeftMobile}>
                      <Button
                        className={classes.FilterButton}
                        onClick={toggleDrawer("left", true)}
                      >
                        <span>
                          <FilterSecondary />
                        </span>
                        <span>Filter</span>
                      </Button>
                      <Drawer
                        anchor={"left"}
                        open={state["left"]}
                        onClose={toggleDrawer("left", false)}
                        sx={{
                          "& .MuiPaper-root": {
                            background: "var(--product-card-bg)",
                          },
                        }}
                      >
                        {list("left")}
                      </Drawer>
                    </div>
                  </div>
                  <div className={classes.FilterRight}>
                    <div className={classes.FilterRightButtons}>
                      <Button
                        className={listStyle === "grid" && classes.ActiveButton}
                        onClick={() => setListStyle("grid")}
                      >
                        <MenuStyleGrid />
                      </Button>
                      <Button
                        className={listStyle === "list" && classes.ActiveButton}
                        onClick={() => setListStyle("list")}
                      >
                        <MenuStyleList />
                      </Button>
                    </div>
                  </div>
                </div>
              </div>
              <div className={classes.ProductsInnerBody}>
                <div
                  className={
                    listStyle === "grid"
                      ? `${classes.ProductsInnerBodyGrid} ${classes.GridActive}`
                      : classes.ProductsInnerBodyGrid
                  }
                >
                  {/* <ProductCard />
                  <ProductCard />
                  <ProductCard />
                  <ProductCard />
                  <ProductCard />
                  <ProductCard /> */}
                  <div className={classes.ProductsInnerBodyGridItem}>
                    <ActiveLink href="product-details/1">
                      <a>
                        <ProductCard />
                      </a>
                    </ActiveLink>
                  </div>

                  <div className={classes.ProductsInnerBodyGridItem}>
                    <ActiveLink href="product-details/1">
                      <a>
                        <ProductCard />
                      </a>
                    </ActiveLink>
                  </div>

                  <div className={classes.ProductsInnerBodyGridItem}>
                    <ActiveLink href="product-details/1">
                      <a>
                        <ProductCard />
                      </a>
                    </ActiveLink>
                  </div>

                  <div className={classes.ProductsInnerBodyGridItem}>
                    <ActiveLink href="product-details/1">
                      <a>
                        <ProductCard />
                      </a>
                    </ActiveLink>
                  </div>

                  <div className={classes.ProductsInnerBodyGridItem}>
                    <ActiveLink href="product-details/1">
                      <a>
                        <ProductCard />
                      </a>
                    </ActiveLink>
                  </div>

                  <div className={classes.ProductsInnerBodyGridItem}>
                    <ActiveLink href="product-details/1">
                      <a>
                        <ProductCard />
                      </a>
                    </ActiveLink>
                  </div>
                </div>
                <div
                  className={
                    listStyle === "list"
                      ? `${classes.ProductsInnerBodyList} ${classes.ListActive}`
                      : classes.ProductsInnerBodyList
                  }
                >
                  <ListStyleCard />
                  <ListStyleCard />
                  <ListStyleCard />
                  <ListStyleCard />
                  <ListStyleCard />
                  <ListStyleCard />
                  <ListStyleCard />
                  <ListStyleCard />
                </div>
              </div>
              <div className="pagination products-pagination">
                <Pagination />
              </div>
            </div>
          </div>
        </div>

        <MobileBottomNavbar />
      </div>
    </Layout>
  );
};

export default AllProducts;
