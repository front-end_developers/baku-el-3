import React from "react";
// import classes from "./index.module.scss";
import Layout from "../../src/hoc/Layout/Layout/Layout";
import ProductFavorites from "../../src/components/ProductFavorites/ProductFavorites";
import MobileBottomNavbar from '../../src/components/MobileBottomNavbar/MobileBottomNavbar'

const Index = () => {
  return (
    <Layout>
      <ProductFavorites />
      <MobileBottomNavbar/>
    </Layout>
  );
};

export default Index;
