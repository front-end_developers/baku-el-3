import Layout from "../../src/hoc/Layout/Layout/Layout";
import CompareComponent from "../../src/modules/Products/Compare";

export default function Compare(props) {
  return (
      <Layout>
        <CompareComponent />
      </Layout>
  );
}
