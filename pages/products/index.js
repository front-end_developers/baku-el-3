import ProductLayout from "../../src/hoc/Layout/ProductLayout/ProductLayout";
import Products from "../../src/modules/Products/Products";

export default function Index(props) {
  return (
    <ProductLayout>
      <Products />
    </ProductLayout>
  );
}
