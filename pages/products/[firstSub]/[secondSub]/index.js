import ProductLayout from "../../../../src/hoc/Layout/ProductLayout/ProductLayout";
import { connect } from "react-redux";
import Products from "../../../../src/modules/Products/Products";

export default function Subcategories() {
  return (
    <div>
      <ProductLayout>
        <Products />
      </ProductLayout>
    </div>
  );
}
