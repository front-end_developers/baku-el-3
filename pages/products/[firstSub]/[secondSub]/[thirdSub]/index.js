import ProductLayout from "../../../../../src/hoc/Layout/ProductLayout/ProductLayout";
import Products from "../../../../../src/modules/Products/Products";

export default function Subcategories() {
  return (
    <div>
      <ProductLayout>
        <Products />
      </ProductLayout>
    </div>
  );
}
