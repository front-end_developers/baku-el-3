import Search from "../src/components/Search/Search";
import HomeLayout from "../src/hoc/Layout/HomeLayout/HomeLayout";
import Home from "../src/modules/Home/Home";
import { connect } from "react-redux";
import classes from "./index.module.scss";
import UserLayout from "../src/hoc/Layout/UserLayout/UserLayout";

export default function index(props) {
  const { categories } = props;
  // console.log("categories", categories)
  return (
    <HomeLayout categories={categories}>
      <Home />
    </HomeLayout>
  );
}
export async function getServerSideProps(context) {
  // Call an external API endpoint to get posts.
  // You can use any data fetching library

  const { locale } = context;

  let langStrQuery =
    locale === "AZ"
      ? "Az"
      : locale === "EN"
      ? "En"
      : locale === "RU"
      ? "Ru"
      : "Az";

  let categories = "";

  try {
    const res = await fetch(
      `${process.env.base_url}${langStrQuery}/categories`
    );
    categories = await res.json();
  } catch (err) {
    console.log("err", err);
  }

  return {
    props: {
      categories,
    },
    // revalidate: 6000,
  };
}

let data ={
  "brands": [
    {
      "id": 32,
      "name": "HUAWEI"
    },
    {
      "id": 33,
      "name": "1More"
    },
    {
      "id": 34,
      "name": "2E"
    },
    {
      "id": 35,
      "name": "A4 Tech"
    },
    {
      "id": 36,
      "name": "ACCURA"
    },
    {
      "id": 37,
      "name": "Acer"
    },
    {
      "id": 38,
      "name": "ACHILL"
    },
    {
      "id": 39,
      "name": "Addison"
    },
    {
      "id": 40,
      "name": "Aigo"
    },
    {
      "id": 41,
      "name": "AIKO"
    },
    {
      "id": 42,
      "name": "AILEEN"
    },
    {
      "id": 43,
      "name": "AirCool"
    },
    {
      "id": 44,
      "name": "Airfel"
    },
    {
      "id": 45,
      "name": "AIST"
    },
    {
      "id": 46,
      "name": "AIWA"
    },
    {
      "id": 47,
      "name": "AKAI"
    },
    {
      "id": 48,
      "name": "Akcam"
    },
    {
      "id": 49,
      "name": "AKG"
    },
    {
      "id": 50,
      "name": "Akvafor"
    },
    {
      "id": 51,
      "name": "ALCATEL"
    },
    {
      "id": 52,
      "name": "ALCTRON"
    },
    {
      "id": 53,
      "name": "Alfi"
    },
    {
      "id": 54,
      "name": "AMATI"
    },
    {
      "id": 55,
      "name": "Amazon"
    },
    {
      "id": 56,
      "name": "AMICA"
    },
    {
      "id": 58,
      "name": "ANKER"
    },
    {
      "id": 59,
      "name": "ANMIER"
    },
    {
      "id": 60,
      "name": "Aopen"
    },
    {
      "id": 61,
      "name": "Apacer"
    },
    {
      "id": 62,
      "name": "Apollo"
    },
    {
      "id": 63,
      "name": "Apple"
    },
    {
      "id": 64,
      "name": "APRILIA"
    },
    {
      "id": 65,
      "name": "Aqara"
    },
    {
      "id": 66,
      "name": "ARCAHAN"
    },
    {
      "id": 67,
      "name": "Arçelik"
    },
    {
      "id": 68,
      "name": "ARCHOS"
    },
    {
      "id": 69,
      "name": "Ardesto"
    },
    {
      "id": 70,
      "name": "ARIEL"
    },
    {
      "id": 71,
      "name": "ARIETE"
    },
    {
      "id": 72,
      "name": "Ariston"
    },
    {
      "id": 73,
      "name": "ARMATA"
    },
    {
      "id": 74,
      "name": "Arti-M"
    },
    {
      "id": 75,
      "name": "ARZUM"
    },
    {
      "id": 76,
      "name": "ASPOR"
    },
    {
      "id": 77,
      "name": "ASUS"
    },
    {
      "id": 78,
      "name": "ATO"
    },
    {
      "id": 79,
      "name": "ATV"
    },
    {
      "id": 80,
      "name": "AUPU"
    },
    {
      "id": 81,
      "name": "AUX"
    },
    {
      "id": 82,
      "name": "Avirtel"
    },
    {
      "id": 83,
      "name": "AZDOME"
    },
    {
      "id": 84,
      "name": "Azzurra"
    },
    {
      "id": 85,
      "name": "Babyliss"
    },
    {
      "id": 86,
      "name": "BAIJIA"
    },
    {
      "id": 87,
      "name": "BAJAJ"
    },
    {
      "id": 88,
      "name": "Bakcell"
    },
    {
      "id": 89,
      "name": "BALLARINI"
    },
    {
      "id": 90,
      "name": "BANG & OLUFSEN"
    },
    {
      "id": 91,
      "name": "Barrier"
    },
    {
      "id": 92,
      "name": "BASEUS"
    },
    {
      "id": 93,
      "name": "BASSEUS"
    },
    {
      "id": 94,
      "name": "BATIS"
    },
    {
      "id": 95,
      "name": "Baykent"
    },
    {
      "id": 96,
      "name": "Beats"
    },
    {
      "id": 97,
      "name": "BEHRINGER"
    },
    {
      "id": 98,
      "name": "BEIRUT"
    },
    {
      "id": 99,
      "name": "BEKO"
    },
    {
      "id": 100,
      "name": "BELL"
    },
    {
      "id": 101,
      "name": "BELORAD"
    },
    {
      "id": 102,
      "name": "Beltextil"
    },
    {
      "id": 103,
      "name": "Besta"
    },
    {
      "id": 104,
      "name": "BESTWAY"
    },
    {
      "id": 105,
      "name": "Beyoglu"
    },
    {
      "id": 106,
      "name": "Bianca Luna"
    },
    {
      "id": 107,
      "name": "biev"
    },
    {
      "id": 108,
      "name": "BlACKVIEW"
    },
    {
      "id": 109,
      "name": "BLING"
    },
    {
      "id": 110,
      "name": "BOE"
    },
    {
      "id": 111,
      "name": "Bomb cosmetics"
    },
    {
      "id": 112,
      "name": "BONSON"
    },
    {
      "id": 113,
      "name": "BOOIL SAFES"
    },
    {
      "id": 114,
      "name": "BOROFONE"
    },
    {
      "id": 115,
      "name": "Bosch"
    },
    {
      "id": 116,
      "name": "BRAUN"
    },
    {
      "id": 117,
      "name": "Brita"
    },
    {
      "id": 118,
      "name": "BUGABOO"
    },
    {
      "id": 119,
      "name": "Burett"
    },
    {
      "id": 120,
      "name": "ButaTex"
    },
    {
      "id": 121,
      "name": "BWOO"
    },
    {
      "id": 122,
      "name": "BYRON"
    },
    {
      "id": 123,
      "name": "Caffitaly"
    },
    {
      "id": 124,
      "name": "Cancan"
    },
    {
      "id": 125,
      "name": "CANDY"
    },
    {
      "id": 126,
      "name": "Canon"
    },
    {
      "id": 127,
      "name": "CANYON"
    },
    {
      "id": 128,
      "name": "Carpets Z"
    },
    {
      "id": 129,
      "name": "Case"
    },
    {
      "id": 130,
      "name": "CASIM"
    },
    {
      "id": 131,
      "name": "CASIO"
    },
    {
      "id": 132,
      "name": "CASSA"
    },
    {
      "id": 133,
      "name": "Catel"
    },
    {
      "id": 134,
      "name": "CeCi"
    },
    {
      "id": 135,
      "name": "CELEBRAT"
    },
    {
      "id": 136,
      "name": "CELIKKASA"
    },
    {
      "id": 137,
      "name": "Cerve"
    },
    {
      "id": 138,
      "name": "CHAMP"
    },
    {
      "id": 139,
      "name": "CHESTNUT"
    },
    {
      "id": 140,
      "name": "CHICCO"
    },
    {
      "id": 141,
      "name": "CHIGO"
    },
    {
      "id": 142,
      "name": "Christy"
    },
    {
      "id": 143,
      "name": "CITY COCO"
    },
    {
      "id": 144,
      "name": "Colar"
    },
    {
      "id": 145,
      "name": "ColorWay"
    },
    {
      "id": 146,
      "name": "CombiGlassGlass"
    },
    {
      "id": 147,
      "name": "COMMA"
    },
    {
      "id": 148,
      "name": "CORELLI"
    },
    {
      "id": 149,
      "name": "CORSAIR"
    },
    {
      "id": 150,
      "name": "Cotton box"
    },
    {
      "id": 151,
      "name": "COX"
    },
    {
      "id": 152,
      "name": "CP"
    },
    {
      "id": 153,
      "name": "Cube"
    },
    {
      "id": 154,
      "name": "DAHUA"
    },
    {
      "id": 155,
      "name": "DAIKIN"
    },
    {
      "id": 156,
      "name": "DecArte"
    },
    {
      "id": 157,
      "name": "Deerma"
    },
    {
      "id": 158,
      "name": "Defender"
    },
    {
      "id": 159,
      "name": "DELL"
    },
    {
      "id": 160,
      "name": "DELONGHI"
    },
    {
      "id": 161,
      "name": "Demirdokum"
    },
    {
      "id": 162,
      "name": "DENN"
    },
    {
      "id": 163,
      "name": "DESNA"
    },
    {
      "id": 164,
      "name": "DEVIA"
    },
    {
      "id": 166,
      "name": "D-Factory"
    },
    {
      "id": 167,
      "name": "DICALLO"
    },
    {
      "id": 168,
      "name": "DIPLOMAT"
    },
    {
      "id": 169,
      "name": "DivanEv"
    },
    {
      "id": 170,
      "name": "DOMETIC"
    },
    {
      "id": 171,
      "name": "DR.WEB"
    },
    {
      "id": 172,
      "name": "DRM"
    },
    {
      "id": 173,
      "name": "DT"
    },
    {
      "id": 174,
      "name": "DURACELL"
    },
    {
      "id": 175,
      "name": "Duralex"
    },
    {
      "id": 176,
      "name": "E.C.A."
    },
    {
      "id": 177,
      "name": "EAGLE SAFES"
    },
    {
      "id": 178,
      "name": "Earldom"
    },
    {
      "id": 179,
      "name": "EAST"
    },
    {
      "id": 180,
      "name": "Egelbach"
    },
    {
      "id": 181,
      "name": "Egelsbach"
    },
    {
      "id": 182,
      "name": "EKO"
    },
    {
      "id": 183,
      "name": "Electrolux"
    },
    {
      "id": 184,
      "name": "ELEYUS"
    },
    {
      "id": 185,
      "name": "Emile Henry"
    },
    {
      "id": 186,
      "name": "EMY"
    },
    {
      "id": 187,
      "name": "ENERGIZER"
    },
    {
      "id": 188,
      "name": "EPIPHONE"
    },
    {
      "id": 189,
      "name": "Epson"
    },
    {
      "id": 190,
      "name": "ETERNA"
    },
    {
      "id": 191,
      "name": "EUFY"
    },
    {
      "id": 192,
      "name": "Eurogold"
    },
    {
      "id": 193,
      "name": "EVEREST"
    },
    {
      "id": 194,
      "name": "Faber"
    },
    {
      "id": 195,
      "name": "Fairy"
    },
    {
      "id": 196,
      "name": "FAKIR"
    },
    {
      "id": 197,
      "name": "Falez"
    },
    {
      "id": 198,
      "name": "Fangor"
    },
    {
      "id": 199,
      "name": "FAVORIT"
    },
    {
      "id": 200,
      "name": "FENDER"
    },
    {
      "id": 201,
      "name": "Finish"
    },
    {
      "id": 202,
      "name": "FINLUX"
    },
    {
      "id": 203,
      "name": "FIVER"
    },
    {
      "id": 204,
      "name": "FLAMINGO"
    },
    {
      "id": 205,
      "name": "FLIGHT"
    },
    {
      "id": 206,
      "name": "Fly Mobile"
    },
    {
      "id": 207,
      "name": "FOCUSRITE"
    },
    {
      "id": 208,
      "name": "Forester"
    },
    {
      "id": 209,
      "name": "Foriella"
    },
    {
      "id": 210,
      "name": "Franke"
    },
    {
      "id": 211,
      "name": "Funke"
    },
    {
      "id": 212,
      "name": "Fussenegger"
    },
    {
      "id": 213,
      "name": "GAGGENAU"
    },
    {
      "id": 214,
      "name": "GASTRO"
    },
    {
      "id": 215,
      "name": "Genius"
    },
    {
      "id": 216,
      "name": "Ghidini"
    },
    {
      "id": 217,
      "name": "Gigabyte"
    },
    {
      "id": 218,
      "name": "Gigaset"
    },
    {
      "id": 219,
      "name": "Gigilli"
    },
    {
      "id": 220,
      "name": "GILLETTE"
    },
    {
      "id": 221,
      "name": "Glendimplex"
    },
    {
      "id": 222,
      "name": "GNAT"
    },
    {
      "id": 223,
      "name": "Goldbach"
    },
    {
      "id": 224,
      "name": "Golden House"
    },
    {
      "id": 225,
      "name": "GOLDMASTER"
    },
    {
      "id": 226,
      "name": "Goodwill"
    },
    {
      "id": 227,
      "name": "GOOGLE"
    },
    {
      "id": 228,
      "name": "GoPro"
    },
    {
      "id": 229,
      "name": "Gorenje"
    },
    {
      "id": 230,
      "name": "GP"
    },
    {
      "id": 231,
      "name": "GREE"
    },
    {
      "id": 232,
      "name": "Green"
    },
    {
      "id": 233,
      "name": "GRETSCH"
    },
    {
      "id": 234,
      "name": "Guruss"
    },
    {
      "id": 235,
      "name": "HADID"
    },
    {
      "id": 236,
      "name": "HAIER"
    },
    {
      "id": 237,
      "name": "Hama"
    },
    {
      "id": 238,
      "name": "HANSA"
    },
    {
      "id": 239,
      "name": "HARMAN KARDON"
    },
    {
      "id": 240,
      "name": "HASANOGLU"
    },
    {
      "id": 241,
      "name": "HAVIT"
    },
    {
      "id": 242,
      "name": "Haylou"
    },
    {
      "id": 243,
      "name": "HEDGREN"
    },
    {
      "id": 244,
      "name": "Hesam"
    },
    {
      "id": 245,
      "name": "Hidrotherm"
    },
    {
      "id": 246,
      "name": "HIPER"
    },
    {
      "id": 247,
      "name": "HISENSE"
    },
    {
      "id": 248,
      "name": "Hitachi"
    },
    {
      "id": 249,
      "name": "Hnc"
    },
    {
      "id": 250,
      "name": "Hobbylife"
    },
    {
      "id": 251,
      "name": "HOCO"
    },
    {
      "id": 252,
      "name": "HOFFMANN"
    },
    {
      "id": 253,
      "name": "Honor"
    },
    {
      "id": 254,
      "name": "HOPERSTAR"
    },
    {
      "id": 255,
      "name": "Hotpoint"
    },
    {
      "id": 256,
      "name": "Hotpoint-Ariston"
    },
    {
      "id": 257,
      "name": "HP"
    },
    {
      "id": 258,
      "name": "Hrmycup"
    },
    {
      "id": 259,
      "name": "HT MOBİLYA"
    },
    {
      "id": 260,
      "name": "HTC"
    },
    {
      "id": 261,
      "name": "HUAWIND"
    },
    {
      "id": 262,
      "name": "HYPERX"
    },
    {
      "id": 263,
      "name": "Hytech"
    },
    {
      "id": 264,
      "name": "IBANEZ"
    },
    {
      "id": 265,
      "name": "iCarer"
    },
    {
      "id": 266,
      "name": "ILLY"
    },
    {
      "id": 267,
      "name": "İmmergas"
    },
    {
      "id": 268,
      "name": "IMOU"
    },
    {
      "id": 269,
      "name": "Indesit"
    },
    {
      "id": 270,
      "name": "Ines Tekstil"
    },
    {
      "id": 271,
      "name": "INFINIX"
    },
    {
      "id": 272,
      "name": "Inkax"
    },
    {
      "id": 273,
      "name": "INMOTION"
    },
    {
      "id": 274,
      "name": "INOI"
    },
    {
      "id": 275,
      "name": "INTEX"
    },
    {
      "id": 276,
      "name": "iRobot"
    },
    {
      "id": 277,
      "name": "İsma"
    },
    {
      "id": 278,
      "name": "İyiGecələr"
    },
    {
      "id": 279,
      "name": "JABRA"
    },
    {
      "id": 280,
      "name": "JACKSON"
    },
    {
      "id": 281,
      "name": "JAVEL"
    },
    {
      "id": 282,
      "name": "JBL"
    },
    {
      "id": 283,
      "name": "JOBY"
    },
    {
      "id": 284,
      "name": "Jooyroom"
    },
    {
      "id": 285,
      "name": "Kaku"
    },
    {
      "id": 286,
      "name": "KANUNI"
    },
    {
      "id": 287,
      "name": "KARCHER"
    },
    {
      "id": 288,
      "name": "KAS"
    },
    {
      "id": 289,
      "name": "Kaspersky"
    },
    {
      "id": 290,
      "name": "KATV"
    },
    {
      "id": 291,
      "name": "Keenetic"
    },
    {
      "id": 292,
      "name": "KENWOOD"
    },
    {
      "id": 293,
      "name": "Kilim"
    },
    {
      "id": 294,
      "name": "KIMBO"
    },
    {
      "id": 296,
      "name": "KINGSONG"
    },
    {
      "id": 297,
      "name": "KINGSTON"
    },
    {
      "id": 298,
      "name": "KINGXBAR"
    },
    {
      "id": 299,
      "name": "KitchenAid"
    },
    {
      "id": 300,
      "name": "KomPlus"
    },
    {
      "id": 301,
      "name": "Koopman"
    },
    {
      "id": 302,
      "name": "Korvet"
    },
    {
      "id": 303,
      "name": "KRAKKEN"
    },
    {
      "id": 304,
      "name": "Kramer"
    },
    {
      "id": 305,
      "name": "Krups"
    },
    {
      "id": 306,
      "name": "KUBA"
    },
    {
      "id": 307,
      "name": "Kutahya Porcelen"
    },
    {
      "id": 308,
      "name": "KYOCERA"
    },
    {
      "id": 309,
      "name": "Lagostina"
    },
    {
      "id": 310,
      "name": "LAVA"
    },
    {
      "id": 311,
      "name": "Lavazza"
    },
    {
      "id": 312,
      "name": "Ldnio"
    },
    {
      "id": 313,
      "name": "Legion"
    },
    {
      "id": 314,
      "name": "LEGO"
    },
    {
      "id": 315,
      "name": "LENOVO"
    },
    {
      "id": 316,
      "name": "LEXINGHTON"
    },
    {
      "id": 317,
      "name": "LG"
    },
    {
      "id": 318,
      "name": "LIEBHERR"
    },
    {
      "id": 319,
      "name": "Linea Decor"
    },
    {
      "id": 320,
      "name": "Linens"
    },
    {
      "id": 321,
      "name": "LOGITECH"
    },
    {
      "id": 322,
      "name": "LS2"
    },
    {
      "id": 323,
      "name": "Luckyboy"
    },
    {
      "id": 324,
      "name": "Lugga"
    },
    {
      "id": 325,
      "name": "LUIGI BORMIOLI"
    },
    {
      "id": 326,
      "name": "Luyuan"
    },
    {
      "id": 327,
      "name": "MACARONS"
    },
    {
      "id": 328,
      "name": "MACKIE"
    },
    {
      "id": 329,
      "name": "MAKUTE"
    },
    {
      "id": 330,
      "name": "MANFROTTO"
    },
    {
      "id": 331,
      "name": "Marantz"
    },
    {
      "id": 332,
      "name": "MARLEY"
    },
    {
      "id": 333,
      "name": "Marshall"
    },
    {
      "id": 334,
      "name": "MASTER WOOD"
    },
    {
      "id": 335,
      "name": "M-audio"
    },
    {
      "id": 336,
      "name": "Max"
    },
    {
      "id": 337,
      "name": "MAXWELL"
    },
    {
      "id": 338,
      "name": "Mebel Tasarım"
    },
    {
      "id": 339,
      "name": "MEDELI"
    },
    {
      "id": 340,
      "name": "MEDISANA"
    },
    {
      "id": 341,
      "name": "MEGOGO"
    },
    {
      "id": 342,
      "name": "MEIZU"
    },
    {
      "id": 343,
      "name": "MELİTTA"
    },
    {
      "id": 344,
      "name": "Mercury"
    },
    {
      "id": 345,
      "name": "Metaltex"
    },
    {
      "id": 346,
      "name": "MICHAEL KELLY"
    },
    {
      "id": 347,
      "name": "MICRO"
    },
    {
      "id": 348,
      "name": "Micromax"
    },
    {
      "id": 349,
      "name": "Microsoft"
    },
    {
      "id": 350,
      "name": "Midea"
    },
    {
      "id": 351,
      "name": "Miham MMC"
    },
    {
      "id": 352,
      "name": "MILI"
    },
    {
      "id": 353,
      "name": "MIRADO"
    },
    {
      "id": 354,
      "name": "Mitsubishi"
    },
    {
      "id": 355,
      "name": "MOBAKS"
    },
    {
      "id": 356,
      "name": "Modafabrik"
    },
    {
      "id": 357,
      "name": "Mokabs"
    },
    {
      "id": 358,
      "name": "Mollis"
    },
    {
      "id": 359,
      "name": "Moneta"
    },
    {
      "id": 360,
      "name": "MONIN"
    },
    {
      "id": 362,
      "name": "Moon"
    },
    {
      "id": 363,
      "name": "MOTOROLA"
    },
    {
      "id": 364,
      "name": "Moulinex"
    },
    {
      "id": 365,
      "name": "MOXOM"
    },
    {
      "id": 366,
      "name": "MSI"
    },
    {
      "id": 367,
      "name": "MT HELMETS"
    },
    {
      "id": 368,
      "name": "Nachtmann"
    },
    {
      "id": 369,
      "name": "Nadoba"
    },
    {
      "id": 370,
      "name": "NAXHUS"
    },
    {
      "id": 371,
      "name": "NB"
    },
    {
      "id": 372,
      "name": "Nehir"
    },
    {
      "id": 373,
      "name": "NEOS"
    },
    {
      "id": 374,
      "name": "NIKAI"
    },
    {
      "id": 375,
      "name": "Nikon"
    },
    {
      "id": 376,
      "name": "Nimex"
    },
    {
      "id": 377,
      "name": "Nintendo"
    },
    {
      "id": 378,
      "name": "NIU"
    },
    {
      "id": 379,
      "name": "NMP"
    },
    {
      "id": 380,
      "name": "Nokia"
    },
    {
      "id": 381,
      "name": "NOMI"
    },
    {
      "id": 382,
      "name": "NOVA"
    },
    {
      "id": 383,
      "name": "NOVATION"
    },
    {
      "id": 384,
      "name": "NUMARK"
    },
    {
      "id": 385,
      "name": "NuovaCer"
    },
    {
      "id": 386,
      "name": "OCULUS"
    },
    {
      "id": 387,
      "name": "OLMIO"
    },
    {
      "id": 388,
      "name": "Olympus"
    },
    {
      "id": 389,
      "name": "Omada"
    },
    {
      "id": 390,
      "name": "Oral B"
    },
    {
      "id": 391,
      "name": "OSRAM"
    },
    {
      "id": 392,
      "name": "Oucase"
    },
    {
      "id": 393,
      "name": "Panasonic"
    },
    {
      "id": 394,
      "name": "PARTNER"
    },
    {
      "id": 395,
      "name": "PASABAHÇE"
    },
    {
      "id": 397,
      "name": "Perilla"
    },
    {
      "id": 398,
      "name": "Persil"
    },
    {
      "id": 399,
      "name": "Pettinaroli"
    },
    {
      "id": 400,
      "name": "Philips"
    },
    {
      "id": 401,
      "name": "Pierre Cardin"
    },
    {
      "id": 402,
      "name": "PINTI"
    },
    {
      "id": 403,
      "name": "Pintinox"
    },
    {
      "id": 404,
      "name": "Pioneer"
    },
    {
      "id": 405,
      "name": "PITTI CAFFE"
    },
    {
      "id": 407,
      "name": "PIXI"
    },
    {
      "id": 408,
      "name": "PIXMA"
    },
    {
      "id": 409,
      "name": "PLAN"
    },
    {
      "id": 410,
      "name": "Plantronics"
    },
    {
      "id": 411,
      "name": "Playstation"
    },
    {
      "id": 412,
      "name": "PocketBook"
    },
    {
      "id": 413,
      "name": "Polaris"
    },
    {
      "id": 414,
      "name": "POLESIE"
    },
    {
      "id": 415,
      "name": "POLO"
    },
    {
      "id": 416,
      "name": "PORODO"
    },
    {
      "id": 417,
      "name": "PORT"
    },
    {
      "id": 418,
      "name": "POWEROLOGY"
    },
    {
      "id": 419,
      "name": "PRAKTIK"
    },
    {
      "id": 420,
      "name": "PRODA"
    },
    {
      "id": 421,
      "name": "PROHEL"
    },
    {
      "id": 422,
      "name": "Promate"
    },
    {
      "id": 423,
      "name": "PUKA"
    },
    {
      "id": 424,
      "name": "PYRAMİDA"
    },
    {
      "id": 425,
      "name": "Pyrex"
    },
    {
      "id": 426,
      "name": "QCY"
    },
    {
      "id": 427,
      "name": "Rampage"
    },
    {
      "id": 428,
      "name": "RAVPOWER"
    },
    {
      "id": 429,
      "name": "Razenni"
    },
    {
      "id": 430,
      "name": "RAZOR"
    },
    {
      "id": 431,
      "name": "RCR"
    },
    {
      "id": 432,
      "name": "RCR (local)"
    },
    {
      "id": 433,
      "name": "REALME"
    },
    {
      "id": 434,
      "name": "REDLINE"
    },
    {
      "id": 435,
      "name": "REMAX"
    },
    {
      "id": 436,
      "name": "Remington"
    },
    {
      "id": 437,
      "name": "RERE"
    },
    {
      "id": 438,
      "name": "RIGID"
    },
    {
      "id": 439,
      "name": "RIVACASE"
    },
    {
      "id": 440,
      "name": "Robert Welch"
    },
    {
      "id": 441,
      "name": "ROCK"
    },
    {
      "id": 442,
      "name": "Rokos"
    },
    {
      "id": 443,
      "name": "ROMOSS"
    },
    {
      "id": 444,
      "name": "ROOF"
    },
    {
      "id": 445,
      "name": "Rosa Maria"
    },
    {
      "id": 446,
      "name": "Rose Maria"
    },
    {
      "id": 447,
      "name": "Rowenta"
    },
    {
      "id": 448,
      "name": "RSK"
    },
    {
      "id": 449,
      "name": "SAFEWELL"
    },
    {
      "id": 450,
      "name": "Saflon"
    },
    {
      "id": 451,
      "name": "Saft"
    },
    {
      "id": 452,
      "name": "SALCANO"
    },
    {
      "id": 453,
      "name": "Samsung"
    },
    {
      "id": 454,
      "name": "Sandisc"
    },
    {
      "id": 455,
      "name": "SANDISK"
    },
    {
      "id": 456,
      "name": "Sanica"
    },
    {
      "id": 457,
      "name": "SANYOO"
    },
    {
      "id": 458,
      "name": "SCARLETT"
    },
    {
      "id": 459,
      "name": "Schaub Lorenz"
    },
    {
      "id": 460,
      "name": "School Bag"
    },
    {
      "id": 461,
      "name": "SCOOTER"
    },
    {
      "id": 462,
      "name": "SCULLCANDY"
    },
    {
      "id": 463,
      "name": "Seagate"
    },
    {
      "id": 464,
      "name": "SEGWAY"
    },
    {
      "id": 465,
      "name": "Selcuk tekstil"
    },
    {
      "id": 466,
      "name": "Serenade"
    },
    {
      "id": 467,
      "name": "SEVENTY DEGREES"
    },
    {
      "id": 468,
      "name": "Seyfi"
    },
    {
      "id": 469,
      "name": "SG"
    },
    {
      "id": 470,
      "name": "SHARK"
    },
    {
      "id": 471,
      "name": "Shgat"
    },
    {
      "id": 472,
      "name": "SHURE"
    },
    {
      "id": 473,
      "name": "Siemens"
    },
    {
      "id": 474,
      "name": "Silicon Power"
    },
    {
      "id": 475,
      "name": "SIMFER"
    },
    {
      "id": 476,
      "name": "Singer"
    },
    {
      "id": 477,
      "name": "Siretessile"
    },
    {
      "id": 478,
      "name": "Skullcandy"
    },
    {
      "id": 479,
      "name": "Sleepwell"
    },
    {
      "id": 480,
      "name": "S-link"
    },
    {
      "id": 481,
      "name": "SMARTOOOLS"
    },
    {
      "id": 482,
      "name": "Smeg"
    },
    {
      "id": 483,
      "name": "SMILE LIGHT"
    },
    {
      "id": 484,
      "name": "Snopy"
    },
    {
      "id": 485,
      "name": "SOEHNLE"
    },
    {
      "id": 486,
      "name": "SOLART"
    },
    {
      "id": 487,
      "name": "SOMAT"
    },
    {
      "id": 488,
      "name": "SONGRUI"
    },
    {
      "id": 489,
      "name": "Sonorous"
    },
    {
      "id": 490,
      "name": "Sony"
    },
    {
      "id": 491,
      "name": "Soundsation"
    },
    {
      "id": 492,
      "name": "Soyuzqril"
    },
    {
      "id": 493,
      "name": "STABA"
    },
    {
      "id": 494,
      "name": "STARK"
    },
    {
      "id": 495,
      "name": "STEEL LION"
    },
    {
      "id": 496,
      "name": "STEINBERG"
    },
    {
      "id": 497,
      "name": "Stenova"
    },
    {
      "id": 498,
      "name": "SUNPIN"
    },
    {
      "id": 499,
      "name": "SUNWOOD"
    },
    {
      "id": 500,
      "name": "Super Soco"
    },
    {
      "id": 501,
      "name": "Svad Dondi"
    },
    {
      "id": 502,
      "name": "SVEN"
    },
    {
      "id": 503,
      "name": "SVOLTA"
    },
    {
      "id": 504,
      "name": "Swc"
    },
    {
      "id": 505,
      "name": "SXP"
    },
    {
      "id": 506,
      "name": "SYM"
    },
    {
      "id": 507,
      "name": "Systemair"
    },
    {
      "id": 508,
      "name": "TAMA"
    },
    {
      "id": 509,
      "name": "TANNOY"
    },
    {
      "id": 510,
      "name": "TARA"
    },
    {
      "id": 511,
      "name": "TARGUS"
    },
    {
      "id": 512,
      "name": "TCL"
    },
    {
      "id": 513,
      "name": "TECNO"
    },
    {
      "id": 514,
      "name": "TECNOGAS"
    },
    {
      "id": 515,
      "name": "Tefal"
    },
    {
      "id": 516,
      "name": "Teka"
    },
    {
      "id": 517,
      "name": "Tekstil Z"
    },
    {
      "id": 518,
      "name": "TENDA"
    },
    {
      "id": 519,
      "name": "TESTA"
    },
    {
      "id": 520,
      "name": "Thermaltake"
    },
    {
      "id": 521,
      "name": "Thomas"
    },
    {
      "id": 522,
      "name": "Thun"
    },
    {
      "id": 523,
      "name": "Timberk"
    },
    {
      "id": 524,
      "name": "TOBA"
    },
    {
      "id": 525,
      "name": "Tognana"
    },
    {
      "id": 526,
      "name": "TOSHIBA"
    },
    {
      "id": 527,
      "name": "TOTU"
    },
    {
      "id": 528,
      "name": "TOYOTA"
    },
    {
      "id": 529,
      "name": "TP-LINK"
    },
    {
      "id": 530,
      "name": "Transcend"
    },
    {
      "id": 531,
      "name": "TREK"
    },
    {
      "id": 532,
      "name": "TTEC"
    },
    {
      "id": 533,
      "name": "TUCANO"
    },
    {
      "id": 534,
      "name": "TYMA"
    },
    {
      "id": 535,
      "name": "ULEFONE"
    },
    {
      "id": 536,
      "name": "Ultimate"
    },
    {
      "id": 537,
      "name": "USAMS"
    },
    {
      "id": 538,
      "name": "USB"
    },
    {
      "id": 539,
      "name": "VALBERG"
    },
    {
      "id": 540,
      "name": "Valera"
    },
    {
      "id": 541,
      "name": "Vanguard"
    },
    {
      "id": 542,
      "name": "VARTA"
    },
    {
      "id": 543,
      "name": "VCOM"
    },
    {
      "id": 544,
      "name": "Versa"
    },
    {
      "id": 545,
      "name": "VESPA"
    },
    {
      "id": 546,
      "name": "VEXO"
    },
    {
      "id": 547,
      "name": "Videoregistrator.az"
    },
    {
      "id": 548,
      "name": "VIDVIE"
    },
    {
      "id": 549,
      "name": "VIKO"
    },
    {
      "id": 550,
      "name": "VİLLA D'ESTE"
    },
    {
      "id": 551,
      "name": "VISTA"
    },
    {
      "id": 552,
      "name": "VITEK"
    },
    {
      "id": 553,
      "name": "VMF"
    },
    {
      "id": 554,
      "name": "VOX"
    },
    {
      "id": 555,
      "name": "V-STAR"
    },
    {
      "id": 556,
      "name": "WACACO"
    },
    {
      "id": 557,
      "name": "WAHL"
    },
    {
      "id": 558,
      "name": "WD"
    },
    {
      "id": 559,
      "name": "Whirlpool"
    },
    {
      "id": 560,
      "name": "WILMAX"
    },
    {
      "id": 561,
      "name": "Winzz"
    },
    {
      "id": 562,
      "name": "WK"
    },
    {
      "id": 563,
      "name": "WMF"
    },
    {
      "id": 564,
      "name": "Wonlex"
    },
    {
      "id": 565,
      "name": "Wopow"
    },
    {
      "id": 566,
      "name": "Xbox"
    },
    {
      "id": 567,
      "name": "XIAOMI"
    },
    {
      "id": 568,
      "name": "XKIN"
    },
    {
      "id": 569,
      "name": "YADEA"
    },
    {
      "id": 570,
      "name": "Yakut"
    },
    {
      "id": 571,
      "name": "YAMAHA"
    },
    {
      "id": 572,
      "name": "YANDEX"
    },
    {
      "id": 573,
      "name": "Yarny"
    },
    {
      "id": 574,
      "name": "Yatas"
    },
    {
      "id": 575,
      "name": "Yiltashali"
    },
    {
      "id": 576,
      "name": "YK"
    },
    {
      "id": 577,
      "name": "YOOKIE"
    },
    {
      "id": 578,
      "name": "YOSHIRO"
    },
    {
      "id": 579,
      "name": "ZANUSSI"
    },
    {
      "id": 580,
      "name": "Zebra Casa"
    },
    {
      "id": 581,
      "name": "ZİMMER"
    },
    {
      "id": 582,
      "name": "ZONTES"
    },
    {
      "id": 583,
      "name": "ZTE"
    },
    {
      "id": 584,
      "name": "Zwilling"
    }
  ],
  "category": [
    {
      "id": 1,
      "name": "Telefonlar, planşetlər və qadcetlər"
    },
    {
      "id": 15,
      "name": "Smartfonlar, mobil telefonlar"
    },
    {
      "id": 16,
      "name": "Planşetlər"
    },
    {
      "id": 4,
      "name": "Kondisionerlər və digər iqlim texnikası"
    },
    {
      "id": 5,
      "name": "Böyük məişət texnikası"
    },
    {
      "id": 10,
      "name": "Qab-qacaq, tava-qazan"
    },
    {
      "id": 2,
      "name": "Televizorlar, audio-video və foto"
    }
  ]
}