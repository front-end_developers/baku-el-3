/* eslint-disable @next/next/no-img-element */
import React, { Fragment } from "react";
import classes from "./index.module.scss";
import Layout from "../../src/hoc/Layout/Layout/Layout";
import Checkbox from "../../src/components/Checkbox/Checkbox";
import Pagination from "../../src/components/Pagination/Pagination";
import { Container, Row, Col } from "react-bootstrap";
import SearchBar from "../../src/components/ProductsSidebar/SearchBar/SearchBar";
import MobileBottomNavbar from "../../src/components/MobileBottomNavbar/MobileBottomNavbar";
import { useState, useEffect } from "react";
import { MdKeyboardArrowDown, MdKeyboardArrowUp } from "react-icons/md";
import { Button, Drawer } from "@mui/material";
import { connect, useSelector } from "react-redux";
// import Link from "next/link"
import {
  ArrowLeft,
  FilterSecondary,
  MenuStyleGrid,
  MenuStyleList,
} from "../../src/components/Icons";
import ActiveLink from "../../src/components/Link/Link";
import Link from "next/link";
import ProductsSidebar from "../../src/components/ProductsSidebar/ProductsSidebar";
import { useRouter } from "next/router";
// import axios from "axios";

const Index = (props) => {
  const { brands, loop } = props;
  let totalBrendPages = Math.ceil(brands.total / 15);
  // let size = s
  // console.log("loop", loop);
  let currPage = brands.page;

  let theme = useSelector((state) => state.layout.theme);
  const [showDetails, setShowDetails] = useState();
  const [page, setPage] = useState(currPage);

  const router = useRouter();

  const handleChange = (event, value) => {
    // console.log("pagination", page);
    setPage(value);
  };

  const productSidebarDrawerStyles = {
    "& .MuiPaper-root": {
      "& .ProductsSidebar": {
        background: theme === "light" ? "#f5f5f5" : "#191919",
        color: theme === "light" ? "#333" : "#fff",
        height: "unset",
        // back button styles
        "&TopBackToHome": {
          background: theme === "light" ? "#f5f5f5" : "#333",
        },
        // checkbox styles
        "& .Container": {
          color: theme === "light" ? "#333" : "#fff",
        },
        // search styles
        "& .SearchBar": {
          color: theme === "light" ? "#333" : "#fff",
          background: theme === "light" ? "#fff" : "#333",

          "&Input": {
            color: theme === "light" ? "#333" : "#fff",
          },
          "&ButtonIcon": {
            color: theme === "light" ? "#333" : "#fff",
          },
        },
        // range slider styles
        "& .PriceRangeInputs": {
          "& input": {
            color: theme === "light" ? "#333" : "#fff",
            background: theme === "light" ? "#f5f5f5" : "#333",
          },
        },
      },
    },
  };

  const [state, setState] = useState({
    left: false,
  });

  const toggleDrawer = (anchor, open) => (event) => {
    if (
      event.type === "keydown" &&
      (event.key === "Tab" || event.key === "Shift")
    ) {
      return;
    }

    setState({ ...state, [anchor]: open });
  };

  const list = (anchor) => <ProductsSidebar />;

  useEffect(() => {
    router.push({
      pathname: "/brend-list",
      query: { page: page },
    });
  }, [page]);

  return (
    <Layout>
      <div className={`${classes.BrandList} BrandList`}>
        <div className={classes.BrandListContainer}>
          <div className={`${classes.BrandListContainerSidebar} flex-shrink-0`}>
            <div className={classes.BrandListContainerSidebarColLeft}>
              <h3>Brendlər</h3>
              <div className={classes.ProductsSidebarBrends}>
                <div style={{ marginBottom: "15px" }}>
                  <SearchBar />
                </div>
                <div className={classes.ProductsSidebarBrendsFirstGroup}>
                  <div className={classes.ProductsSidebarBrendsCheckbox}>
                    <Checkbox />
                  </div>
                </div>
                <div
                  className={`${
                    showDetails
                      ? classes.ProductsSidebarBrendsSecondGroupShow
                      : classes.ProductsSidebarBrendsSecondGroup
                  }`}
                >
                  <div className={classes.ProductsSidebarBrendsCheckbox}>
                    <Checkbox />
                  </div>
                  <div className={classes.ProductsSidebarBrendsCheckbox}>
                    <Checkbox />
                  </div>
                  <div className={classes.ProductsSidebarBrendsCheckbox}>
                    <Checkbox />
                  </div>
                </div>
                <button
                  style={{ marginBottom: "32px" }}
                  type="button"
                  onClick={() => setShowDetails(!showDetails)}
                >
                  {showDetails ? (
                    <div>
                      Daha az
                      <MdKeyboardArrowUp style={{ fontSize: "20px" }} />
                    </div>
                  ) : (
                    <div>
                      Daha çox
                      <MdKeyboardArrowDown style={{ fontSize: "20px" }} />
                    </div>
                  )}
                </button>
              </div>

              <div className={classes.ProductsSidebarBrends}>
                <div style={{ marginBottom: "32px" }}>
                  <SearchBar />
                </div>
                <div className={classes.ProductsSidebarBrendsFirstGroup}>
                  <div className={classes.ProductsSidebarBrendsCheckbox}>
                    <Checkbox />
                  </div>
                  <div className={classes.ProductsSidebarBrendsCheckbox}>
                    <Checkbox />
                  </div>
                  <div className={classes.ProductsSidebarBrendsCheckbox}>
                    <Checkbox />
                  </div>
                  <div className={classes.ProductsSidebarBrendsCheckbox}>
                    <Checkbox />
                  </div>
                  <div className={classes.ProductsSidebarBrendsCheckbox}>
                    <Checkbox />
                  </div>
                  <div className={classes.ProductsSidebarBrendsCheckbox}>
                    <Checkbox />
                  </div>
                </div>
                <div
                  className={`${
                    showDetails
                      ? classes.ProductsSidebarBrendsSecondGroupShow
                      : classes.ProductsSidebarBrendsSecondGroup
                  }`}
                >
                  <div className={classes.ProductsSidebarBrendsCheckbox}>
                    <Checkbox />
                  </div>
                  <div className={classes.ProductsSidebarBrendsCheckbox}>
                    <Checkbox />
                  </div>
                  <div className={classes.ProductsSidebarBrendsCheckbox}>
                    <Checkbox />
                  </div>
                </div>
                <button
                  style={{ marginBottom: "32px" }}
                  type="button"
                  onClick={() => setShowDetails(!showDetails)}
                >
                  {showDetails ? (
                    <div>
                      Daha az
                      <MdKeyboardArrowUp style={{ fontSize: "20px" }} />
                    </div>
                  ) : (
                    <div>
                      Daha çox{" "}
                      <MdKeyboardArrowDown style={{ fontSize: "20px" }} />
                    </div>
                  )}
                </button>
              </div>
            </div>
          </div>
          <div className={classes.BrandListContainerColRight}>
            <h1>Brendlər</h1>

            <div className={classes.FilterMobile}>
              <Fragment key={"left"}>
                <Button
                  className={classes.FilterButton}
                  onClick={toggleDrawer("left", true)}
                >
                  <span>
                    <FilterSecondary />
                  </span>
                  <span>Filter</span>
                </Button>
                <Drawer
                  anchor={"left"}
                  open={state["left"]}
                  onClose={toggleDrawer("left", false)}
                  sx={productSidebarDrawerStyles}
                >
                  {list("left")}
                </Drawer>
              </Fragment>
            </div>

            <div className={classes.BrandListContainerColRightContent}>
              {brands?.items?.map((item) => {
                return (
                  <Link href={`/brands/${item.slug}`} key={item.name}>
                    <a className={classes.BrandListContainerColRightContentBox}>
                      {item.icon ? <img src={item.icon} alt="" /> : null}
                      {/* {item.name} */}
                      {/* <img
                        src={item.icon ? item.icon : "/img/samsungl.png"}
                        alt=""
                      /> */}
                    </a>
                  </Link>
                );
              })}
            </div>

            <Pagination
              page={page}
              totalBrendPages={totalBrendPages}
              setPage={setPage}
              handleChange={handleChange}
            />
          </div>
        </div>
      </div>
      <MobileBottomNavbar />
    </Layout>
  );
};
export default Index;

export async function getServerSideProps(context) {
  // console.log("context", context);
  const { locale, query } = context;

  const { page } = query;

  let baseUrl = process.env.base_url;
  let langStrQuery =
    locale === "AZ"
      ? "Az"
      : locale === "EN"
      ? "En"
      : locale === "RU"
      ? "Ru"
      : "Az";


  let brands = "";
  if (page) {
    try {
      const res = await fetch(
        `${baseUrl}${langStrQuery}/brands?size=15&page=${page}`
      );
      brands = await res.json();
    } catch (err) {
      console.log("err", err);
    }
  } else {
    try {
      const res = await fetch(
        `${process.env.base_url}${langStrQuery}/brands?size=15`
      );
      brands = await res.json();
    } catch (err) {
      console.log("err", err);
    }
  }

  let categories = "";  
  try {
    const res = await fetch(
      `${baseUrl}${langStrQuery}/brands-category`
    );
    categories = await res.json();
  } catch (err) {
    console.log("err", err);
  }

  return {
    props: {
      brands,
      categories,
    },
    // revalidate: 6000,
  };
}
