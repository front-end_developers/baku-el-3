// module.exports = {
//   reactStrictMode: true,
// };
const nextTranslate = require("next-translate");
// import nextTranslate  from "next-translate"

module.exports = {
  ...nextTranslate(),
  reactStrictMode: true,
  env: {
    base_url: "https://api.be.onneks.com/",
  },
};
