import * as types from "../types";
import { persistReducer } from "redux-persist";
import storage from "redux-persist/lib/storage";

const initialState = {
  userData: {
    email: "",
    user: {
      email: "",
      firstName: "",
      lastName: "",
      isActive: "",
      languageCode: "",
      metadata: [],
    },
    token: "",
    refreshToken: "",
    csrfToken: "",
    errors: [],
    second: 0,
  },
  verifyData: {},
};

const auth = persistReducer(
  {
    storage,
    key: "auth storage",
    whitelist: ["userData", "verifyData"],
  },
  (state = initialState, action) => {
    switch (action.type) {
      case types.SET_USER:
        return {
          ...state,
          userData: action.payload,
        };

      case types.SET_REGISTER_EMAIL:
        return {
          ...state,
          verifyData: { email: action.payload },
        };

      case types.LOGOUT:
        return initialState;

      default:
        return state;
    }
  }
);
export default auth;

// export default function auth(state = initialState, action) {
//   switch (action.type) {
//     case types.SET_USER:
//       return {
//         ...state,
//         userData: action.payload,
//       };

//     case types.SET_REGISTER_EMAIL:
//       return {
//         ...state,
//         verifyData: { email: action.payload },
//       };

//     case types.LOGOUT:
//       return initialState;

//     default:
//       return state;
//   }
// }
