import * as types from "../types";
import { persistReducer } from "redux-persist";
import storage from "redux-persist/lib/storage";
import { takeLatest, put, call } from "redux-saga/effects";

const initialState = {
  reduxAvailLangs: "",
  reduxLang: "",
  isDefault: true,
};

// export default function lang(state = initialState, action) {
//   switch (action.type) {
//     case types.SET_LANG:
//       return {
//         lang: action.payload,
//       };
//     default:
//       return state;
//   }
// }

const lang = persistReducer(
  {
    storage,
    key: "lang storage",
    whitelist: ["reduxLang", "reduxAvailLangs", "isDefault"],
  },

  (state = initialState, action) => {
    switch (action.type) {
      case types.SET_LANG:
        return {
          ...state,
          reduxLang: action.payload,
        };
      case types.SET_AVAILABLE_LANGS:
        return {
          reduxAvailLangs: action.payload,
          reduxLang: action.payload[0],
          isDefault: false,
        };
      default:
        return state;
    }
  }
);

export default lang;

// function* getLangsAsync() {
//   yield put({ type: types.GET_AVAILABLE_LANGS_ASYNC, value: 1 });
// }

// export function* watchGetAvailableLangs() {
//   yield takeLatest(types.GET_AVAILABLE_LANGS, getLangsAsync);
// }
