// import { combineReducers } from "redux";

// const rootReducer = combineReducers({
//   layout: layout,
// });

// export default rootReducer;

import layout from "./layout";
import auth from "./auth";
import lang from "./lang";
import { all } from "redux-saga/effects";
import { combineReducers } from "redux";

// import * as auth from "../../pages/auth/_redux/authRedux";

export const rootReducer = combineReducers({
  layout: layout,
  auth: auth,
  lang: lang,
});

// export function* rootSaga() {
//   yield all([auth.saga()]);
// }
