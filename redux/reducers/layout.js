import * as types from "../types";
import { persistReducer } from "redux-persist";
import storage from "redux-persist/lib/storage";

const initialState = {
  theme: "light",
  modal: {
    show: true,
    type: "error",
    title: null,
    body: null,
  },
  sidebar: {
    sidebarIsOpen: true,
    sidebarToggleBtns: {
      header: false,
      first: true,
      second: false,
      third: false,
    },
  },
};

// export default function layout(state = initialState, action) {
//   switch (action.type) {
//     case types.SWITCH_THEME:
//       return {
//         ...state,
//         theme: state.theme === "light" ? "dark" : "light",
//       };
//     case types.CLOSE_MODAL:
//       let newState = { ...state };
//       newState.modal.show = false;
//       return newState;
//     case types.TOGGLE_SIDEBAR:
//       if (state.sidebar.sidebarIsOpen) {
//         return {
//           ...state,
//           sidebar: {
//             sidebarIsOpen: false,
//             sidebarToggleBtns: {
//               header: true,
//               first: false,
//               second: false,
//               third: false,
//             },
//           },
//         };
//       } else {
//         return {
//           ...state,
//           sidebar: {
//             sidebarIsOpen: true,
//             sidebarToggleBtns: {
//               header: false,
//               first: true,
//               second: false,
//               third: false,
//             },
//           },
//         };
//       }
//     case types.TOGGLE_SIDEBAR_BTNS:
//       if (action.payload === "first") {
//         return {
//           ...state,
//           sidebar: {
//             sidebarIsOpen: true,
//             sidebarToggleBtns: {
//               header: false,
//               first: !state.sidebar.sidebarToggleBtns.first,
//               second: !state.sidebar.sidebarToggleBtns.second,
//               third: false,
//             },
//           },
//         };
//       } else if (action.payload === "second") {
//         return {
//           ...state,
//           sidebar: {
//             sidebarIsOpen: true,
//             sidebarToggleBtns: {
//               header: false,
//               first: false,
//               second: !state.sidebar.sidebarToggleBtns.second,
//               third: !state.sidebar.sidebarToggleBtns.third,
//             },
//           },
//         };
//       } else return state;

//     default:
//       return state;
//   }
// }

const layout = persistReducer(
  {
    storage,
    key: "layout storage",
    whitelist: ["modal", "theme"],
  },
  (state = initialState, action) => {
    switch (action.type) {
      case types.SWITCH_THEME:
        return {
          ...state,
          theme: state.theme === "light" ? "dark" : "light",
        };
      case types.CLOSE_MODAL:
        let newState = { ...state };
        newState.modal.show = false;
        return newState;
      case types.TOGGLE_SIDEBAR:
        if (state.sidebar.sidebarIsOpen) {
          return {
            ...state,
            sidebar: {
              sidebarIsOpen: false,
              sidebarToggleBtns: {
                header: true,
                first: false,
                second: false,
                third: false,
              },
            },
          };
        } else {
          return {
            ...state,
            sidebar: {
              sidebarIsOpen: true,
              sidebarToggleBtns: {
                header: false,
                first: true,
                second: false,
                third: false,
              },
            },
          };
        }
      case types.TOGGLE_SIDEBAR_BTNS:
        if (action.payload === "first") {
          return {
            ...state,
            sidebar: {
              sidebarIsOpen: true,
              sidebarToggleBtns: {
                header: false,
                first: !state.sidebar.sidebarToggleBtns.first,
                second: !state.sidebar.sidebarToggleBtns.second,
                third: false,
              },
            },
          };
        } else if (action.payload === "second") {
          return {
            ...state,
            sidebar: {
              sidebarIsOpen: true,
              sidebarToggleBtns: {
                header: false,
                first: false,
                second: !state.sidebar.sidebarToggleBtns.second,
                third: !state.sidebar.sidebarToggleBtns.third,
              },
            },
          };
        } else return state;

      default:
        return state;
    }
  }
);

export default layout;
