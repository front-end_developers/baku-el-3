// import { applyMiddleware, createStore } from "redux";
// import ReduxThunk from "redux-thunk";
// import { persistStore, persistReducer } from "redux-persist";
// import storage from "redux-persist/lib/storage";
// import { rootReducer } from "./reducers/rootReducer";

// const persistConfig = {
//   storage,
//   key: "persist-key",
//   whitelist: ["theme"]
// };

// const persistedReducer = persistReducer(persistConfig, rootReducer);
// const store = createStore(persistedReducer, applyMiddleware(ReduxThunk));
// const persistor = persistStore(store);

// export { persistor };
// export default store;

import { configureStore, getDefaultMiddleware } from "@reduxjs/toolkit";
import createSagaMiddleware from "redux-saga";
import { reduxBatch } from "@manaflair/redux-batch";
import { persistStore } from "redux-persist";
import { rootReducer } from "./reducers/rootReducer";

const sagaMiddleware = createSagaMiddleware();
const middleware = [
  ...getDefaultMiddleware({
    immutableCheck: false,
    serializableCheck: false,
    thunk: true,
  }),
  sagaMiddleware,
];

const store = configureStore({
  reducer: rootReducer,
  middleware,
  enhancers: [reduxBatch],
});

export const persistor = persistStore(store);
// sagaMiddleware.run(rootSaga);
export default store;
