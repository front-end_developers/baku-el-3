import { bindActionCreators } from "redux";
import * as types from "../types";
// import axios from "axios";
// import { request } from "../../util/request";

export const toggleTheme = () => (dispatch) => {
  dispatch({
    type: types.SWITCH_THEME,
  });
};
export const closeModal = () => (dispatch) => {
  dispatch({
    type: types.CLOSE_MODAL,
  });
};
export const toggleSidebar = () => (dispatch) => {
  dispatch({
    type: types.TOGGLE_SIDEBAR,
  });
};
export const toggleSidebarBtns = (payload) => (dispatch) => {
  dispatch({
    type: types.TOGGLE_SIDEBAR_BTNS,
    payload: payload,
  });
};
export const setUser = (payload) => (dispatch) => {
  dispatch({
    type: types.SET_USER,
    payload: payload,
  });
};
export const setRegisterEmail = (payload) => (dispatch) => {
  dispatch({
    type: types.SET_REGISTER_EMAIL,
    payload: payload,
  });
};
export const logout = () => (dispatch) => {
  dispatch({
    type: types.LOGOUT,
  });
};

export const reduxSetLang = (payload) => (dispatch) => {
  dispatch({
    type: types.SET_LANG,
    payload: payload,
  });
};

export const reduxSetAvailLangs = (payload) => (dispatch) => {
  // console.log("available lang dispatched", payload);
  dispatch({
    type: types.SET_AVAILABLE_LANGS,
    payload: payload,
  });
};

export const axiosLogout = () => ({ type: types.LOGOUT });
// export const axiossetRefreshToken = (token, refreshToken, expiresAt) => ({
//   type: actionTypes.RefreshToken,
//   payload: { token, refreshToken, expiresAt },
// });
