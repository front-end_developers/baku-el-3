// import * as auth from "../src/modules/Auth/_redux/authRedux";
import Axios from "axios";

const { base_url } = process.env;
export const REFRESH_TOKEN_URL = base_url + "account/refresh-token";
// export const TASKER_REFRESH_TOKEN_URL = base_url + "taskers/refresh-token";

export const getOwnerRefreshToken = (refreshToken) => {
  return Axios.post(REFRESH_TOKEN_URL, refreshToken);
};

export default function setupAxios(axios, store) {
  axios.defaults.baseURL = base_url;

  const {
    auth: {
      userData: { refreshToken },
    },
  } = store.getState();
  console.log("axios refreshToken", refreshToken);

  const { auth } = store.getState();
  console.log("axios auth state", auth);

  // intercepting axios requsets
  axios.interceptors.request.use(
    (config) => {
      // get the auth token from redux store
      const {
        auth: {
          userData: { token },
        },
      } = store.getState();

      // check if token is valid as a type
      if (token) {
        // if there is a token automatically add it to the header of the request
        config.headers.Authorization = `Bearer ${token}`;
      }
      config.headers.common["Accept-Language"] = language;

      // return to original request
      return config;
    },
    (err) => Promise.reject(err)
  );
  axios.interceptors.response.use(
  // intercepting axios response
  (response) => {
    return response;
  },
  async (error) => {
    const { dispatch, getState } = store;

  const {
    actions: { setRefreshToken, logout },
  } = auth;

  const response = error.response;
  const originalRequest = error.config;

  const {
    auth: { refreshToken },
  } = getState();
  if (response) {
    if (
      response.status === 401 &&
      response.data.message === "Token is expired"
    ) {
      dispatch(logout());
    } else if (
      response.status === 401 &&
      response.data.message === "Invalid token"
    ) {
      dispatch(logout());
    } else if (response.status === 401 && refreshToken) {
      const refreshData = {
        refreshToken,
      };

      let refreshResponse;

      if (refreshResponse.status === 200) {
        const {
          data: { token, refreshToken, expiresAt },
        } = refreshResponse;
        dispatch(setRefreshToken(token, refreshToken, expiresAt));
        originalRequest.headers.Authorization = `Bearer ${token}`;
        const responseData = await axios(originalRequest);
        return responseData;
      } else {
        dispatch(logout());
      }
    }
  }
  return Promise.reject(error);
  }
  );
}
