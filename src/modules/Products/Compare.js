import React, { useState } from "react";
import classes from "./Compare.module.scss";
import { Search, CheckArrow, CollapseArrow } from "../../components/Icons";
import { Collapse, Button } from "@mui/material";
import ProductCard from "../../components/ProductCard/ProductCard";
import MobileBottomNavbar from "../../components/MobileBottomNavbar/MobileBottomNavbar";
import Accordion from "@mui/material/Accordion";
import AccordionDetails from "@mui/material/AccordionDetails";
import AccordionSummary from "@mui/material/AccordionSummary";
import Typography from "@mui/material/Typography";
import ExpandMoreIcon from "@mui/icons-material/ExpandMore";
import ProductCardMobile from "../../components/ProductCardMobile/ProductCardMobile";

export default function Compare() {
  const [filterToggle, setFilterToggle] = useState(false);
  const [expanded, setExpanded] = React.useState(false);

  const toggleHandler = () => {
    setFilterToggle(!filterToggle);
  };

  const handleChange = (panel) => (event, isExpanded) => {
    setExpanded(isExpanded ? panel : false);
  };

  return (
    <div className={classes.Compare}>
      <div>
        <div className={classes.CompareWrapper}>
          <div className={classes.CompareWrapperLeft}>
            <div className={classes.Filter}>
              <div className={classes.FilterWrapper}>
                <div className={classes.FilterWrapperTitle}>
                  <h2>Kateqoriyalar</h2>
                </div>
                <div className={classes.FilterWrapperSearchInput}>
                  <input type="text" placeholder="Kateqoriya axtar..." />
                  <Search />
                </div>
                <div className={classes.FilterWrapperCheckbox}>
                  <label className={classes.CheckboxItem}>
                    <div className={classes.CheckboxItemInput}>
                      <input type="checkbox" />
                      <span className={classes.Marker}>
                        <CheckArrow />
                      </span>
                    </div>
                    <label className={classes.CheckboxItemLabel}>
                      Smartfonlar
                    </label>
                  </label>
                  <label className={classes.CheckboxItem}>
                    <div className={classes.CheckboxItemInput}>
                      <input type="checkbox" />
                      <span className={classes.Marker}>
                        <CheckArrow />
                      </span>
                    </div>
                    <label className={classes.CheckboxItemLabel}>
                      Smart saatlar
                    </label>
                  </label>
                  <label className={classes.CheckboxItem}>
                    <div className={classes.CheckboxItemInput}>
                      <input type="checkbox" />
                      <span className={classes.Marker}>
                        <CheckArrow />
                      </span>
                    </div>
                    <label className={classes.CheckboxItemLabel}>
                      Yumşaq mebellər
                    </label>
                  </label>
                  <label className={classes.CheckboxItem}>
                    <div className={classes.CheckboxItemInput}>
                      <input type="checkbox" />
                      <span className={classes.Marker}>
                        <CheckArrow />
                      </span>
                    </div>
                    <label className={classes.CheckboxItemLabel}>
                      Böyük məişət əşyaları
                    </label>
                  </label>
                  <label className={classes.CheckboxItem}>
                    <div className={classes.CheckboxItemInput}>
                      <input type="checkbox" />
                      <span className={classes.Marker}>
                        <CheckArrow />
                      </span>
                    </div>
                    <label className={classes.CheckboxItemLabel}>
                      Kiçik məişət əşyaları
                    </label>
                  </label>
                  <label className={classes.CheckboxItem}>
                    <div className={classes.CheckboxItemInput}>
                      <input type="checkbox" />
                      <span className={classes.Marker}>
                        <CheckArrow />
                      </span>
                    </div>
                    <label className={classes.CheckboxItemLabel}>
                      Televizorlar
                    </label>
                  </label>
                  <label className={classes.CheckboxItem}>
                    <div className={classes.CheckboxItemInput}>
                      <input type="checkbox" />
                      <span className={classes.Marker}>
                        <CheckArrow />
                      </span>
                    </div>
                    <label className={classes.CheckboxItemLabel}>
                      Smart saatlar
                    </label>
                  </label>
                  <label className={classes.CheckboxItem}>
                    <div className={classes.CheckboxItemInput}>
                      <input type="checkbox" />
                      <span className={classes.Marker}>
                        <CheckArrow />
                      </span>
                    </div>
                    <label className={classes.CheckboxItemLabel}>
                      Kiçik məişət əşyaları
                    </label>
                  </label>
                  <label className={classes.CheckboxItem}>
                    <div className={classes.CheckboxItemInput}>
                      <input type="checkbox" />
                      <span className={classes.Marker}>
                        <CheckArrow />
                      </span>
                    </div>
                    <label className={classes.CheckboxItemLabel}>
                      Yumşaq mebellər
                    </label>
                  </label>
                  <label className={classes.CheckboxItem}>
                    <div className={classes.CheckboxItemInput}>
                      <input type="checkbox" />
                      <span className={classes.Marker}>
                        <CheckArrow />
                      </span>
                    </div>
                    <label className={classes.CheckboxItemLabel}>
                      Kiçik məişət əşyaları
                    </label>
                  </label>
                  <Collapse in={filterToggle}>
                    <label className={classes.CheckboxItem}>
                      <div className={classes.CheckboxItemInput}>
                        <input type="checkbox" />
                        <span className={classes.Marker}>
                          <CheckArrow />
                        </span>
                      </div>
                      <label className={classes.CheckboxItemLabel}>
                        Kiçik məişət əşyaları
                      </label>
                    </label>
                    <label className={classes.CheckboxItem}>
                      <div className={classes.CheckboxItemInput}>
                        <input type="checkbox" />
                        <span className={classes.Marker}>
                          <CheckArrow />
                        </span>
                      </div>
                      <label className={classes.CheckboxItemLabel}>
                        Yumşaq mebellər
                      </label>
                    </label>
                    <label className={classes.CheckboxItem}>
                      <div className={classes.CheckboxItemInput}>
                        <input type="checkbox" />
                        <span className={classes.Marker}>
                          <CheckArrow />
                        </span>
                      </div>
                      <label className={classes.CheckboxItemLabel}>
                        Kiçik məişət əşyaları
                      </label>
                    </label>
                  </Collapse>
                  <div className={classes.FilterWrapperToggle}>
                    <button onClick={toggleHandler}>
                      {filterToggle ? (
                        <span>
                          Daha az
                          <span className={classes.FilterToggleArrowUp}>
                            <CollapseArrow />
                          </span>
                        </span>
                      ) : (
                        <span>
                          Daha çox
                          <span className={classes.FilterToggleArrowDown}>
                            <CollapseArrow />
                          </span>
                        </span>
                      )}
                    </button>
                  </div>
                </div>
              </div>
            </div>
            <div className={classes.Description}>
              <div className={classes.DescriptionTitle}>
                <Button className={`${classes.SettingsBtn}  ${classes.Active}`}>
                  Parametrlər
                </Button>
                <Button className={`${classes.ShowDifferenceBtn}`}>
                  Fərqləri göstər
                </Button>
              </div>
              <div className={classes.DescriptionItem}>
                <span>Qiymət:</span>
              </div>
              <div className={classes.DescriptionItem}>
                <span>Məhsul kodu:</span>
              </div>
              <div className={classes.DescriptionItem}>
                <span>Brend:</span>
              </div>
              <div className={classes.DescriptionItem}>
                <span>Qablaşdırmaya daxildir:</span>
              </div>
              <div className={classes.DescriptionItem}>
                <span>Sudan və tozdan qorunma:</span>
              </div>
              <div className={classes.DescriptionItem}>
                <span>Rəng:</span>
              </div>
              <div className={classes.DescriptionItem}>
                <span>Nüvələrin sayı:</span>
              </div>
              <div className={classes.DescriptionItem}>
                <span>Ölçülər (mm), Hündürlüyü:</span>
              </div>
              <div className={classes.DescriptionItem}>
                <span>SİM-kart sayı:</span>
              </div>
              <div className={classes.DescriptionItem}>
                <span>SİM-kart ölçüsü:</span>
              </div>
              <div className={classes.DescriptionItem}>
                <span>Şəbəkə:</span>
              </div>
              <div className={classes.DescriptionItem}>
                <span>Şarj bağlayıcısı:</span>
              </div>
              <div className={classes.DescriptionItem}>
                <span>Quraşdırılmış yaddaş:</span>
              </div>
            </div>
          </div>
          <div className={classes.CompareWrapperRight}>
            <div className={classes.ComparedProducts}>
              <div className={classes.ComparedProductsTitle}>
                <h2>
                  <span>Müqayisə et</span>
                </h2>
                <span className={classes.ComparedProductsTitleSmall}>
                  3 Məhsul
                </span>
              </div>
              <div className={classes.ComparedProductsCategory}>
                <Accordion
                  expanded={expanded === "panel2"}
                  onChange={handleChange("panel2")}
                  sx={{
                    color: "text.secondary",
                    backgroundColor: "var(--product-page-inner)",
                    borderRadius: "16px",
                    boxShadow: "none",
                  }}
                >
                  <AccordionSummary
                    expandIcon={<ExpandMoreIcon />}
                    aria-controls="panel2bh-content"
                    id="panel2bh-header"
                    sx={{
                      color: "text.secondary",
                      backgroundColor: "var(--f7-to-00)",
                      borderRadius: "16px",
                      "& svg": {
                        color: "var(--text-primary)",
                      },
                    }}
                  >
                    <Typography
                      sx={{
                        width: "33%",
                        flexShrink: 0,
                        fontSize: "16px",
                        fontWeight: "700",
                        fontFamily: "Poppins",
                        color: "var(--text-primary)",
                        lineHeight: "106.2%",
                      }}
                    >
                      Smartfonlar
                      <p
                        style={{
                          fontSize: "12px",
                          fontWeight: "400",
                          fontFamily: "Poppins",
                        }}
                      >
                        Kateqoriya
                      </p>
                    </Typography>
                  </AccordionSummary>
                  <AccordionDetails>
                    <Typography
                      sx={{
                        fontSize: "12px",
                        fontWeight: "400",
                        fontFamily: "Poppins",
                        color: "var(--text-primary)",

                      }}
                    >
                      Nulla facilisi. Phasellus sollicitudin nulla et quam
                      mattis feugiat.
                    </Typography>
                  </AccordionDetails>
                </Accordion>
              </div>
              <div className={classes.txt}>
                <p>Məhsullar</p>
              </div>
              <div className={classes.ComparedProductsBody}>
                {/* ================================================ */}
                <div className={classes.ComparedProduct}>
                  <div className={classes.ComparedProductTop}>
                    <ProductCard />
                  </div>
                  <div className={classes.ComparedProductTopMobile}>
                    <ProductCardMobile />
                  </div>
                  <div className={classes.ComparedProductBottom}>
                    <div className={classes.ComparedProductBottomText}>
                      <span>349.00</span>
                    </div>
                    <div className={classes.ComparedProductBottomText}>
                      <span>173366</span>
                    </div>
                    <div className={classes.ComparedProductBottomText}>
                      <span>REALME</span>
                    </div>
                    <div className={classes.ComparedProductBottomText}>
                      <span>Realme C21Y,USB Cable,Charge A</span>
                    </div>
                    <div className={classes.ComparedProductBottomText}>
                      <span>Yoxdur</span>
                    </div>
                    <div className={classes.ComparedProductBottomText}>
                      <span>Qara</span>
                    </div>
                    <div className={classes.ComparedProductBottomText}>
                      <span>8</span>
                    </div>
                    <div className={classes.ComparedProductBottomText}>
                      <span>164.5 x 76 x 9.1 mm</span>
                    </div>
                    <div className={classes.ComparedProductBottomText}>
                      <span>2</span>
                    </div>
                    <div className={classes.ComparedProductBottomText}>
                      <span>Nano</span>
                    </div>
                    <div className={classes.ComparedProductBottomText}>
                      <span>GSM / HSPA / LTE</span>
                    </div>
                    <div className={classes.ComparedProductBottomText}>
                      <span>5V6A Charge Adapter</span>
                    </div>
                    <div className={classes.ComparedProductBottomText}>
                      <span>64 GB</span>
                    </div>
                    {/* /////-->> */}
                    <div className={classes.ComparedProductBottomDescription}>
                      <div
                        className={classes.DescriptionItem}
                        style={{ paddingTop: "10px" }}
                      >
                        <span>Qiymət:</span>
                      </div>
                      <div className={classes.DescriptionItem}>
                        <span>Məhsul kodu:</span>
                      </div>
                      <div className={classes.DescriptionItem}>
                        <span>Brend:</span>
                      </div>
                      <div className={classes.DescriptionItem}>
                        <span>Qablaşdırmaya daxildir:</span>
                      </div>
                      <div className={classes.DescriptionItem}>
                        <span>Sudan və tozdan qorunma:</span>
                      </div>
                      <div className={classes.DescriptionItem}>
                        <span>Rəng:</span>
                      </div>
                      <div className={classes.DescriptionItem}>
                        <span>Nüvələrin sayı:</span>
                      </div>
                      <div className={classes.DescriptionItem}>
                        <span>Ölçülər (mm), Hündürlüyü:</span>
                      </div>
                      <div className={classes.DescriptionItem}>
                        <span>SİM-kart sayı:</span>
                      </div>
                      <div className={classes.DescriptionItem}>
                        <span>SİM-kart ölçüsü:</span>
                      </div>
                      <div className={classes.DescriptionItem}>
                        <span>Şəbəkə:</span>
                      </div>
                      <div className={classes.DescriptionItem}>
                        <span>Şarj bağlayıcısı:</span>
                      </div>
                      <div className={classes.DescriptionItem}>
                        <span>Quraşdırılmış yaddaş:</span>
                      </div>
                    </div>
                    {/* /////-->> */}
                  </div>
                </div>
                <div className={classes.ComparedProduct}>
                  <div className={classes.ComparedProductTop}>
                    <ProductCard />
                  </div>
                  <div className={classes.ComparedProductTopMobile}>
                    <ProductCardMobile />
                  </div>
                  <div className={classes.ComparedProductBottom}>
                    <div className={classes.ComparedProductBottomText}>
                      <span>349.00</span>
                    </div>
                    <div className={classes.ComparedProductBottomText}>
                      <span>173366</span>
                    </div>
                    <div className={classes.ComparedProductBottomText}>
                      <span>REALME</span>
                    </div>
                    <div className={classes.ComparedProductBottomText}>
                      <span>Realme C21Y,USB Cable,Charge A</span>
                    </div>
                    <div className={classes.ComparedProductBottomText}>
                      <span>Yoxdur</span>
                    </div>
                    <div className={classes.ComparedProductBottomText}>
                      <span>Qara</span>
                    </div>
                    <div className={classes.ComparedProductBottomText}>
                      <span>8</span>
                    </div>
                    <div className={classes.ComparedProductBottomText}>
                      <span>164.5 x 76 x 9.1 mm</span>
                    </div>
                    <div className={classes.ComparedProductBottomText}>
                      <span>2</span>
                    </div>
                    <div className={classes.ComparedProductBottomText}>
                      <span>Nano</span>
                    </div>
                    <div className={classes.ComparedProductBottomText}>
                      <span>GSM / HSPA / LTE</span>
                    </div>
                    <div className={classes.ComparedProductBottomText}>
                      <span>5V6A Charge Adapter</span>
                    </div>
                    <div className={classes.ComparedProductBottomText}>
                      <span>64 GB</span>
                    </div>
                  </div>
                </div>
                <div className={classes.ComparedProduct}>
                  <div className={classes.ComparedProductTop}>
                    <ProductCard />
                  </div>
                  <div className={classes.ComparedProductTopMobile}>
                    <ProductCardMobile />
                  </div>
                  <div className={classes.ComparedProductBottom}>
                    <div className={classes.ComparedProductBottomText}>
                      <span>349.00</span>
                    </div>
                    <div className={classes.ComparedProductBottomText}>
                      <span>173366</span>
                    </div>
                    <div className={classes.ComparedProductBottomText}>
                      <span>REALME</span>
                    </div>
                    <div className={classes.ComparedProductBottomText}>
                      <span>Realme C21Y,USB Cable,Charge A</span>
                    </div>
                    <div className={classes.ComparedProductBottomText}>
                      <span>Yoxdur</span>
                    </div>
                    <div className={classes.ComparedProductBottomText}>
                      <span>Qara</span>
                    </div>
                    <div className={classes.ComparedProductBottomText}>
                      <span>8</span>
                    </div>
                    <div className={classes.ComparedProductBottomText}>
                      <span>164.5 x 76 x 9.1 mm</span>
                    </div>
                    <div className={classes.ComparedProductBottomText}>
                      <span>2</span>
                    </div>
                    <div className={classes.ComparedProductBottomText}>
                      <span>Nano</span>
                    </div>
                    <div className={classes.ComparedProductBottomText}>
                      <span>GSM / HSPA / LTE</span>
                    </div>
                    <div className={classes.ComparedProductBottomText}>
                      <span>5V6A Charge Adapter</span>
                    </div>
                    <div className={classes.ComparedProductBottomText}>
                      <span>64 GB</span>
                    </div>
                  </div>
                </div>
                <div className={classes.ComparedProduct}>
                  <div className={classes.ComparedProductTop}>
                    <ProductCard />
                  </div>
                  <div className={classes.ComparedProductTopMobile}>
                    <ProductCardMobile />
                  </div>
                  <div className={classes.ComparedProductBottom}>
                    <div className={classes.ComparedProductBottomText}>
                      <span>349.00</span>
                    </div>
                    <div className={classes.ComparedProductBottomText}>
                      <span>173366</span>
                    </div>
                    <div className={classes.ComparedProductBottomText}>
                      <span>REALME</span>
                    </div>
                    <div className={classes.ComparedProductBottomText}>
                      <span>Realme C21Y,USB Cable,Charge A</span>
                    </div>
                    <div className={classes.ComparedProductBottomText}>
                      <span>Yoxdur</span>
                    </div>
                    <div className={classes.ComparedProductBottomText}>
                      <span>Qara</span>
                    </div>
                    <div className={classes.ComparedProductBottomText}>
                      <span>8</span>
                    </div>
                    <div className={classes.ComparedProductBottomText}>
                      <span>164.5 x 76 x 9.1 mm</span>
                    </div>
                    <div className={classes.ComparedProductBottomText}>
                      <span>2</span>
                    </div>
                    <div className={classes.ComparedProductBottomText}>
                      <span>Nano</span>
                    </div>
                    <div className={classes.ComparedProductBottomText}>
                      <span>GSM / HSPA / LTE</span>
                    </div>
                    <div className={classes.ComparedProductBottomText}>
                      <span>5V6A Charge Adapter</span>
                    </div>
                    <div className={classes.ComparedProductBottomText}>
                      <span>64 GB</span>
                    </div>
                  </div>
                </div>
                <div className={classes.ComparedProduct}>
                  <div className={classes.ComparedProductTop}>
                    <ProductCard />
                  </div>
                  <div className={classes.ComparedProductTopMobile}>
                    <ProductCardMobile />
                  </div>
                  <div className={classes.ComparedProductBottom}>
                    <div className={classes.ComparedProductBottomText}>
                      <span>349.00</span>
                    </div>
                    <div className={classes.ComparedProductBottomText}>
                      <span>173366</span>
                    </div>
                    <div className={classes.ComparedProductBottomText}>
                      <span>REALME</span>
                    </div>
                    <div className={classes.ComparedProductBottomText}>
                      <span>Realme C21Y,USB Cable,Charge A</span>
                    </div>
                    <div className={classes.ComparedProductBottomText}>
                      <span>Yoxdur</span>
                    </div>
                    <div className={classes.ComparedProductBottomText}>
                      <span>Qara</span>
                    </div>
                    <div className={classes.ComparedProductBottomText}>
                      <span>8</span>
                    </div>
                    <div className={classes.ComparedProductBottomText}>
                      <span>164.5 x 76 x 9.1 mm</span>
                    </div>
                    <div className={classes.ComparedProductBottomText}>
                      <span>2</span>
                    </div>
                    <div className={classes.ComparedProductBottomText}>
                      <span>Nano</span>
                    </div>
                    <div className={classes.ComparedProductBottomText}>
                      <span>GSM / HSPA / LTE</span>
                    </div>
                    <div className={classes.ComparedProductBottomText}>
                      <span>5V6A Charge Adapter</span>
                    </div>
                    <div className={classes.ComparedProductBottomText}>
                      <span>64 GB</span>
                    </div>
                  </div>
                </div>

                {/* ================================================ */}
              </div>
            </div>
          </div>
        </div>
      </div>
      <MobileBottomNavbar />
    </div>
  );
}
