import React, { useState } from "react";
import classes from "./Products.module.scss";
import { Button, Drawer } from "@mui/material";
import { connect } from "react-redux";
import {
  MenuStyleList,
  MenuStyleGrid,
  FilterSecondary,
  ArrowLeft,
} from "../../components/Icons";
import ProductPageCard from "../../components/ProductsPageCard/ProductsPageCard";
import ListStyleCard from "../../components/ProductsPageCard/ListStyleCard";
import ProductCard from "../../components/ProductCard/ProductCard";
import Pagination from "../../components/Pagination/Pagination";
import { ArrowRight } from "../../components/Icons";
import InfoCardList from "../../components/InfoCardList/InfoCardList";
import Link from "next/link";
import ActiveLink from "../../components/Link/Link";
import MobileBottomNavbar from "../../components/MobileBottomNavbar/MobileBottomNavbar";
import ProductsSidebar from "../../components/ProductsSidebar/ProductsSidebar";
import ProductEventSidebar from "../../components/ProductEventSidebar/ProductEventSidebar";

function Products(props) {
  const { theme } = props;
  const [listStyle, setListStyle] = useState("grid");
  const [state, setState] = useState({
    left: false,
  });

  const productSidebarDrawerStyles = {
    "& .MuiPaper-root": {
      "& .ProductsSidebar": {
        background: theme === "light" ? "#f5f5f5" : "#191919",
        color: theme === "light" ? "#333" : "#fff",
        height: "unset",
        // back button styles
        "&TopBackToHome": {
          background: theme === "light" ? "#f5f5f5" : "#333",
        },
        // checkbox styles
        "& .Container": {
          color: theme === "light" ? "#333" : "#fff",
        },
        // search styles
        "& .SearchBar": {
          color: theme === "light" ? "#333" : "#fff",
          background: theme === "light" ? "#fff" : "#333",

          "&Input": {
            color: theme === "light" ? "#333" : "#fff",
          },
          "&ButtonIcon": {
            color: theme === "light" ? "#333" : "#fff",
          },
        },
        // range slider styles
        "& .PriceRangeInputs": {
          "& input": {
            color: theme === "light" ? "#333" : "#fff",
            background: theme === "light" ? "#f5f5f5" : "#333",
          },
        },
      },
    },
  };

  const toggleDrawer = (anchor, open) => (event) => {
    if (
      event.type === "keydown" &&
      (event.key === "Tab" || event.key === "Shift")
    ) {
      return;
    }

    setState({ ...state, [anchor]: open });
  };

  const list = (anchor) => <ProductsSidebar />;

  return (
    <div className={classes.ProductsPage}>
      <div className={classes.ProductsInner}>
        <div className={classes.ProductsInnerHeader}>
          <div className={classes.ProductsInnerHeaderTitle}>
            <h2 className={classes.ProductsInnerHeaderTitleDesktop}>
              Smartfonlar
            </h2>
            <div className={classes.ProductsInnerHeaderTitleMobile}>
              <div className={classes.BackButton}>
                <Link href="/">
                  <a>
                    <ArrowLeft />
                  </a>
                </Link>
              </div>
              <div className={classes.Text}>
                <p>Telefonlar və qadcetlər</p>
                <p>1200 Məhsul</p>
              </div>
            </div>
          </div>
          <div className={classes.ProductsInnerHeaderMobileFilter}>
            <ActiveLink
              activeClassName={classes.ActiveFilterBtn}
              href="/products"
            >
              <a>Hamısı</a>
            </ActiveLink>
            <ActiveLink
              activeClassName={classes.ActiveFilterBtn}
              href="/products/smartphones"
            >
              <a>Smartfonlar</a>
            </ActiveLink>
            <ActiveLink
              activeClassName={classes.ActiveFilterBtn}
              href="/products/smartwatches"
            >
              <a>Smart saatlar</a>
            </ActiveLink>
            <ActiveLink
              activeClassName={classes.ActiveFilterBtn}
              href="/products/ebooks"
            >
              <a>Elektron kitablar</a>
            </ActiveLink>
          </div>
          <div className={classes.ProductsInnerHeaderFilter}>
            <div className={classes.FilterLeft}>
              <ul className={classes.FilterLeftDesktop}>
                <li>
                  <button className={classes.ActiveLink}>Popular</button>
                </li>
                <li>
                  <button>Endirim</button>
                </li>

                <li>
                  <button>Ucuzdan bahaya</button>
                </li>
                <li>
                  <button>Bahadan ucuza</button>
                </li>
              </ul>
              <div className={classes.FilterLeftMobile}>
                <Button
                  className={classes.FilterButton}
                  onClick={toggleDrawer("left", true)}
                >
                  <span>
                    <FilterSecondary />
                  </span>
                  <span>Filter</span>
                </Button>
                <Drawer
                  anchor={"left"}
                  open={state["left"]}
                  onClose={toggleDrawer("left", false)}
                  sx={productSidebarDrawerStyles}
                >
                  {list("left")}
                </Drawer>
              </div>
            </div>
            <div className={classes.FilterRight}>
              <div className={classes.FilterRightButtons}>
                <Button
                  className={listStyle === "grid" && classes.ActiveButton}
                  onClick={() => setListStyle("grid")}
                >
                  <MenuStyleGrid />
                </Button>
                <Button
                  className={listStyle === "list" && classes.ActiveButton}
                  onClick={() => setListStyle("list")}
                >
                  <MenuStyleList />
                </Button>
              </div>
            </div>
          </div>
        </div>
        <div className={classes.ProductsInnerBody}>
          <div
            className={
              listStyle === "grid"
                ? `${classes.ProductsInnerBodyGrid} ${classes.GridActive}`
                : classes.ProductsInnerBodyGrid
            }
          >
            <ProductPageCard />
            <ProductPageCard />
            <ProductPageCard />
            <ProductPageCard />
            <ProductPageCard />
            <ProductPageCard />
          </div>
          <div
            className={
              listStyle === "list"
                ? `${classes.ProductsInnerBodyList} ${classes.ListActive}`
                : classes.ProductsInnerBodyList
            }
          >
            <ListStyleCard />
            <ListStyleCard />
            <ListStyleCard />
            <ListStyleCard />
            <ListStyleCard />
            <ListStyleCard />
            <ListStyleCard />
            <ListStyleCard />
          </div>
        </div>
        <div className={classes.ProductsInnerMobileBody}>
          <div className={classes.ProductsInnerMobileBodyItem}>
            <ProductCard />
          </div>
          <div className={classes.ProductsInnerMobileBodyItem}>
            <ProductCard />
          </div>
          <div className={classes.ProductsInnerMobileBodyItem}>
            <ProductCard />
          </div>
          <div className={classes.ProductsInnerMobileBodyItem}>
            <ProductCard />
          </div>
          <div className={classes.ProductsInnerMobileBodyItem}>
            <ProductCard />
          </div>
        </div>
        <div
          className={`pagination products-pagination ${classes.ProductsInnerPagination}`}
        >
          <Pagination />
        </div>
      </div>

      <ProductEventSidebar
        filtration={true}
        title={{ small: "Trend", big: "Ən çox baxılan məhsullar" }}
        cards={["", "", "", ""]}
        background="grey"
      />

      <ProductEventSidebar
        campaignLink={true}
        title={{ small: "Yenidən dəyərləndir", big: "Ən son baxdıqlarınız" }}
        cards={["", "", "", ""]}
        background="white"
      />

      <div className={classes.InfoCardContainer}>
        <InfoCardList />
      </div>

      <div className={classes.LastSection}>
        <div className={classes.LastSectionContent}>
          <h2>Niyə Baku Electronicsdən?</h2>
          <p>
            Elektron kitab səyahət edərkən oxumağı sevənlər üçün əla seçimdir.
            Artıq kitabınızın yolda əzilməsini və rəfinizdə əlavə yer
            tutub-tutmaması barədə narahat olmağınız lazım deyil. “Elektron
            mürəkkəb” texnologiyası, oxuma müddətiniz uzun olduğunda da,
            maksimum rahatlığı təmin edir. Modeldən asılı olaraq, bəzi cihazlar
            Wi-Fi simsiz şəbəkələri vasitəsilə İnternetə qoşula bilər. Baku
            Electronics sizə lazım olan modeli seçməyə kömək edəcəkdir.
          </p>
        </div>
      </div>
      <MobileBottomNavbar />
    </div>
  );
}

const mapStateToProps = (state) => {
  const {
    layout: { theme },
  } = state;
  return { theme: theme };
};

export default connect(mapStateToProps, null)(Products);
