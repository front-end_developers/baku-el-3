import { Tab, Tabs } from "@mui/material";
import Accordion from "@mui/material/Accordion";
import AccordionSummary from "@mui/material/AccordionSummary";
import AccordionDetails from "@mui/material/AccordionDetails";
import Typography from "@mui/material/Typography";
import ExpandMoreIcon from "@mui/icons-material/ExpandMore";
import Link from "next/link";
import classes from "./Categories.module.scss";
import { styled } from "@mui/material/styles";
import { Pan, PhoneDark, PhoneLight } from "../../components/Icons";
import TabPanel from "@mui/lab/TabPanel";
import TabContext from "@mui/lab/TabContext";
import { useState } from "react";
import { connect } from "react-redux";
function Categories(props) {
  const { theme, categories } = props;

  console.log("categories", categories);

  var size3 = 3;
  var categories3 = [];
  for (var i = 0; i < categories.length; i += size3) {
    categories3.push(categories.slice(i, i + size3));
  }
  console.log(categories3);
  var size4 = 4;
  var categories4 = [];
  for (var i = 0; i < categories.length; i += size4) {
    categories4.push(categories.slice(i, i + size4));
  }
  // console.log(categories4);

  const StyledTabs = styled(Tabs)({
    marginBottom: "8px",
    "& .MuiTabs-scroller": {
      "& .MuiTabs-flexContainer": {
        justifyContent: "space-between",
        "& .MuiButtonBase-root": {
          fontFamily: "Poppins",
          fontWeight: "500",
          fontSize: "11px",
          background: "var(--navbar-bg-main)",
          height: "130px",
          width: "130px",
          borderRadius: "24px",
          color: "var(--text-primary)",
          padding: "20px",
          justifyContent: "space-between",
          "& svg": { color: "var(--sidebar-icon-color)", fontSize: "45px" },
          "&.Mui-selected": {
            "& svg": { color: "red" },
          },
        },
      },
      "& .MuiTabs-indicator": {
        display: "none",
      },
    },
    "@media(max-width: 768px)": {
      "& .MuiTabs-scroller": {
        "& .MuiTabs-flexContainer": {
          "& .MuiButtonBase-root": {
            fontSize: "8px",
            height: "100px",
            width: "100px",
            borderRadius: "16px",
            padding: "15px 10px",
            "&.Mui-selected": {
              "& svg": { color: "red" },
            },
          },
        },
      },
    },
    "@media(max-width: 350px)": {
      "& .MuiTabs-scroller": {
        "& .MuiTabs-flexContainer": {
          "& .MuiButtonBase-root": {
            height: "85px",
            width: "85px",
            borderRadius: "12px",
            padding: "10px",
            minHeight: "unset",
            "& svg": { fontSize: "30px" },
            "&.Mui-selected": {
              "& svg": { color: "red" },
            },
          },
        },
      },
    },
  });
  const StyledTabPanel = styled(TabPanel)({
    marginTop: "16px",
    padding: "0",
  });
  const StyledAccordion = styled(Accordion)({
    borderRadius: "24px",
    overflow: "hidden",
    padding: "0",
    "&.MuiPaper-root ": {
      background: "inherit",
      "& .MuiButtonBase-root": {
        background: "var(--navbar-bg-main)",
        borderRadius: "16px",
        minHeight: "unset",
        padding: "16px",
        "& .MuiAccordionSummary-content": {
          margin: "0",
          "& .MuiTypography-root": {
            color: "var(--text-primary)",
          },
        },
        "& .MuiAccordionSummary-expandIconWrapper": {
          "& svg": {
            color: "var(--text-primary)",
          },
        },
      },
      "& .MuiCollapse-root ": {
        "& .MuiCollapse-wrapper": {
          "& .MuiCollapse-wrapperInner": {
            "& .MuiAccordion-region": {
              "& .MuiAccordionDetails-root": { paddingInline: "0" },
            },
          },
        },
      },
    },
  });

  const [currentTab, setCurrentTab] = useState("0");
  const handleChange2 = (val) => setCurrentTab(val === currentTab ? "0" : val);

  return (
    // page container
    <div className={classes.Categories}>
      {/* categories container */}

      <div className={classes.CategoriesTablet}>
        <TabContext value={currentTab}>
          <StyledTabs value={currentTab} aria-label="lab API tabs example">
            <Tab
              value="0"
              sx={{
                display: "none",
              }}
            />
            <Tab
              icon={theme === "dark" ? <PhoneDark /> : <PhoneLight />}
              label="Telefonlar və qadcetlər"
              value="1"
              onClick={() => handleChange2("1")}
            />
            <Tab
              icon={theme === "dark" ? <PhoneDark /> : <PhoneLight />}
              label="Telefonlar və qadcetlər"
              value="2"
              onClick={() => handleChange2("2")}
            />
            <Tab
              icon={theme === "dark" ? <PhoneDark /> : <PhoneLight />}
              label="Telefonlar və qadcetlər"
              value="3"
              onClick={() => handleChange2("3")}
            />
            <Tab
              icon={theme === "dark" ? <PhoneDark /> : <PhoneLight />}
              label="Telefonlar və qadcetlər"
              value="4"
              onClick={() => handleChange2("4")}
            />
          </StyledTabs>
          <StyledTabPanel value="1">
            <div className={classes.CategoriesLink}>
              <Link href="">Bütün məhsullar</Link>
            </div>
            <div className={classes.CategoriesLink}>
              <Link href="">Bütün məhsullar</Link>
            </div>
            <div className={classes.CategoriesLink}>
              <Link href="">Bütün məhsullar</Link>
            </div>
          </StyledTabPanel>
          <StyledTabPanel value="2">
            <StyledAccordion elevation={0}>
              <AccordionSummary
                expandIcon={<ExpandMoreIcon />}
                aria-controls="panel1a-content"
                id="panel1a-header"
              >
                <Typography>Accordion 1</Typography>
              </AccordionSummary>
              <AccordionDetails>
                <div className={classes.CategoriesLink}>
                  <Link href="">Bütün məhsullar</Link>
                </div>
              </AccordionDetails>
              <AccordionDetails>
                <div className={classes.CategoriesLink}>
                  <Link href="">Bütün məhsullar</Link>
                </div>
              </AccordionDetails>
              <AccordionDetails>
                <div className={classes.CategoriesLink}>
                  <Link href="">Bütün məhsullar</Link>
                </div>
              </AccordionDetails>
            </StyledAccordion>
          </StyledTabPanel>
          <StyledTabPanel value="3">Item Three</StyledTabPanel>
          <StyledTabPanel value="4">Item Four</StyledTabPanel>
        </TabContext>
      </div>
      <div className={classes.CategoriesMobile}>
        <TabContext value={currentTab}>
          <StyledTabs value={currentTab} aria-label="lab API tabs example">
            <Tab
              value="0"
              sx={{
                display: "none",
              }}
            />
            <Tab
              icon={theme === "dark" ? <PhoneDark /> : <PhoneLight />}
              label="Telefonlar və qadcetlər"
              value="1"
              onClick={() => handleChange2("1")}
            />
            <Tab
              icon={theme === "dark" ? <PhoneDark /> : <PhoneLight />}
              label="Telefonlar və qadcetlər"
              value="2"
              onClick={() => handleChange2("2")}
            />
            <Tab
              icon={theme === "dark" ? <PhoneDark /> : <PhoneLight />}
              label="Telefonlar və qadcetlər"
              value="3"
              onClick={() => handleChange2("3")}
            />
          </StyledTabs>
          <StyledTabPanel value="1">
            <div className={classes.CategoriesLink}>
              <Link href="">Bütün məhsullar</Link>
            </div>
            <div className={classes.CategoriesLink}>
              <Link href="">Bütün məhsullar</Link>
            </div>
            <div className={classes.CategoriesLink}>
              <Link href="">Bütün məhsullar</Link>
            </div>
          </StyledTabPanel>
          <StyledTabPanel value="2">
            <StyledAccordion elevation={0}>
              <AccordionSummary
                expandIcon={<ExpandMoreIcon />}
                aria-controls="panel1a-content"
                id="panel1a-header"
              >
                <Typography>Accordion 1</Typography>
              </AccordionSummary>
              <AccordionDetails>
                <div className={classes.CategoriesLink}>
                  <Link href="">Bütün məhsullar</Link>
                </div>
              </AccordionDetails>
              <AccordionDetails>
                <div className={classes.CategoriesLink}>
                  <Link href="">Bütün məhsullar</Link>
                </div>
              </AccordionDetails>
              <AccordionDetails>
                <div className={classes.CategoriesLink}>
                  <Link href="">Bütün məhsullar</Link>
                </div>
              </AccordionDetails>
            </StyledAccordion>
          </StyledTabPanel>
          <StyledTabPanel value="3">Item Three</StyledTabPanel>
        </TabContext>
      </div>
    </div>
  );
}

// export default Categories;

const mapStateToProps = (state) => {
  return {
    theme: state.layout.theme,
  };
};

export default connect(mapStateToProps, null)(Categories);
