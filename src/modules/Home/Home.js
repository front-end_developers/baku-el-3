/* eslint-disable jsx-a11y/alt-text */
/* eslint-disable @next/next/no-img-element */
import React, { useState } from "react";
import classes from "./Home.module.scss";
import { Row, Col } from "react-bootstrap";
import BrandList from "../../components/BrandList/BrandList";
import { TopRatedBadge, Manat, ArrowRight } from "../../components/Icons";
import Slider from "react-slick";
import ProductCard from "../../components/ProductCard/ProductCard";
import Modal from "../../components/Modal/Modal";
import InfoCardList from "../../components/InfoCardList/InfoCardList";
import HomeNewsSlider from "../../components/HomeNewsSlider/HomeNewsSlider";
import Link from "next/link";
import { connect } from "react-redux";
import ActiveLink from "../../components/Link/Link";
import MobileBottomNavbar from "../../components/MobileBottomNavbar/MobileBottomNavbar";
import VideoPlayer from "../../components/VideoPlayer/VideoPlayer";
import SectionFive from "../../components/SectionFive/SectionFive";
import ProductEventSidebar from "../../components/ProductEventSidebar/ProductEventSidebar";

function Home(props) {
  const campaignsSettings = {
    dots: true,
    infinite: false,
    speed: 500,
    slidesToShow: 1,
    slidesToScroll: 1,
  };
  const newsSettings = {
    dots: true,
    infinite: false,
    speed: 500,
    slidesToShow: 1,
    slidesToScroll: 1,
  };
  const productOfWeekSettings = {
    dots: true,
    infinite: false,
    speed: 500,
    slidesToShow: 1,
    slidesToScroll: 1,
  };

  return (
    <div>
      <div className={classes.CampaignsNews_TopProducts_Wrapper}>
        <div className={classes.CampaignsNews_TopProducts}>
          <div className={classes.CampaignsNews}>
            <div className={classes.Campaigns}>
              <div className={`${classes.CampaignsSlider} CampaignsSlider`}>
                <Slider {...campaignsSettings}>
                  <div className={classes.CampaignsSlideItem}>
                    <img
                      src="img/carousel-1.png"
                      className={classes.CampaignsImg}
                    />
                    <div className={classes.CampaignsCaptions}>
                      <div className={classes.CampaignsCaptionsContent}>
                        <div className={classes.CampaignsCaptionsContentTitle}>
                          <span>Iphone 13</span>
                        </div>
                        <div className={classes.CampaignsCaptionsContentBody}>
                          <span>EVVELCEDEN SIFARIS</span>
                        </div>
                      </div>
                      <div className={classes.CampaignsCaptionsButton}>
                        <button>Sifaris et</button>
                      </div>
                    </div>
                  </div>
                  <div className={classes.CampaignsSlideItem}>
                    <img
                      src="img/slick-slider-img.png"
                      className={classes.CampaignsImg}
                    />
                    <div className={classes.CampaignsCaptions}>
                      <div className={classes.CampaignsCaptionsContent}>
                        <div className={classes.CampaignsCaptionsContentTitle}>
                          <span>Iphone 13</span>
                        </div>
                        <div className={classes.CampaignsCaptionsContentBody}>
                          <span>EVVELCEDEN SIFARIS</span>
                        </div>
                      </div>
                      <div className={classes.CampaignsCaptionsButton}>
                        <button>Sifaris et</button>
                      </div>
                    </div>
                  </div>
                  <div className={classes.CampaignsSlideItem}>
                    <img
                      src="img/carousel-1.png"
                      className={classes.CampaignsImg}
                    />
                    <div className={classes.CampaignsCaptions}>
                      <div className={classes.CampaignsCaptionsContent}>
                        <div className={classes.CampaignsCaptionsContentTitle}>
                          <span>Iphone 13</span>
                        </div>
                        <div className={classes.CampaignsCaptionsContentBody}>
                          <span>EVVELCEDEN SIFARIS</span>
                        </div>
                      </div>
                      <div className={classes.CampaignsCaptionsButton}>
                        <button>Sifaris et</button>
                      </div>
                    </div>
                  </div>
                </Slider>
              </div>
            </div>
            <div className={classes.News}>
              <div className={classes.NewsSliderWrapper}>
                <div className={`${classes.NewsSlider} NewsSlider`}>
                  <Slider {...newsSettings}>
                    <div className={classes.NewsSliderItem}>
                      <div className={classes.NewsSliderItemImgWrapper}>
                        <img src="img/card-1.png" />
                      </div>

                      <div className={classes.NewsSliderItemContent}>
                        <p className={classes.NewsSliderItemContentTitle}>
                          Card Title
                        </p>
                        <p className={classes.NewsSliderItemContentBody}>
                          Card text
                        </p>
                      </div>

                      <div className={classes.NewsSliderItemExpand}>
                        <Link href="/product-details/1">
                          <a>Daha cox</a>
                        </Link>
                      </div>
                    </div>
                    <div className={classes.NewsSliderItem}>
                      <div className={classes.NewsSliderItemImgWrapper}>
                        <img src="img/card-1.png" />
                      </div>

                      <div className={classes.NewsSliderItemContent}>
                        <p className={classes.NewsSliderItemContentTitle}>
                          Card Title
                        </p>
                        <p className={classes.NewsSliderItemContentBody}>
                          Card text
                        </p>
                      </div>

                      <div className={classes.NewsSliderItemExpand}>
                        <Link href="/product-details/1">
                          <a>Daha cox</a>
                        </Link>
                      </div>
                    </div>
                  </Slider>
                </div>
              </div>
              <div className={classes.NewsSliderWrapper}>
                <div className={`${classes.NewsSlider} NewsSlider`}>
                  <Slider {...newsSettings}>
                    <div className={classes.NewsSliderItem}>
                      <div className={classes.NewsSliderItemImgWrapper}>
                        <img src="img/card-1.png" />
                      </div>

                      <div className={classes.NewsSliderItemContent}>
                        <p className={classes.NewsSliderItemContentTitle}>
                          Card Title
                        </p>
                        <p className={classes.NewsSliderItemContentBody}>
                          Card text
                        </p>
                      </div>

                      <div className={classes.NewsSliderItemExpand}>
                        <Link href="/product-details/1">
                          <a>Daha cox</a>
                        </Link>
                      </div>
                    </div>
                    <div className={classes.NewsSliderItem}>
                      <div className={classes.NewsSliderItemImgWrapper}>
                        <img src="img/card-1.png" />
                      </div>

                      <div className={classes.NewsSliderItemContent}>
                        <p className={classes.NewsSliderItemContentTitle}>
                          Card Title
                        </p>
                        <p className={classes.NewsSliderItemContentBody}>
                          Card text
                        </p>
                      </div>

                      <div className={classes.NewsSliderItemExpand}>
                        <Link href="/product-details/1">
                          <a>Daha cox</a>
                        </Link>
                      </div>
                    </div>
                  </Slider>
                </div>
              </div>
              <div className={classes.NewsSliderWrapper}>
                <div className={`${classes.NewsSlider} NewsSlider`}>
                  <Slider {...newsSettings}>
                    <div className={classes.NewsSliderItem}>
                      <div className={classes.NewsSliderItemImgWrapper}>
                        <img src="img/card-1.png" />
                      </div>

                      <div className={classes.NewsSliderItemContent}>
                        <p className={classes.NewsSliderItemContentTitle}>
                          Card Title
                        </p>
                        <p className={classes.NewsSliderItemContentBody}>
                          Card text
                        </p>
                      </div>

                      <div className={classes.NewsSliderItemExpand}>
                        <Link href="/product-details/1">
                          <a>Daha cox</a>
                        </Link>
                      </div>
                    </div>
                    <div className={classes.NewsSliderItem}>
                      <div className={classes.NewsSliderItemImgWrapper}>
                        <img src="img/card-1.png" />
                      </div>

                      <div className={classes.NewsSliderItemContent}>
                        <p className={classes.NewsSliderItemContentTitle}>
                          Card Title
                        </p>
                        <p className={classes.NewsSliderItemContentBody}>
                          Card text
                        </p>
                      </div>

                      <div className={classes.NewsSliderItemExpand}>
                        <Link href="/product-details/1">
                          <a>Daha cox</a>
                        </Link>
                      </div>
                    </div>
                  </Slider>
                </div>
              </div>
            </div>
          </div>
          <div className={classes.TopProducts}>
            <div className={classes.Top5Products}>
              {/* red topping */}
              <div className={classes.Top5ProductsCardTitle}>
                <TopRatedBadge />
                <span>Top 5 mehsullar</span>
              </div>

              {/* content */}
              <div className={classes.Top5ProductsCardBody}>
                <Link href="/products">
                  <a className={classes.Top5ProductsCardBodyItem}>
                    <div className={classes.Top5ProductsCardBodyItemImgWrapper}>
                      <img src="img/card-img-1.png" />
                    </div>
                    <div className={classes.Top5ProductsCardBodyItemContent}>
                      <p>Iphone 13 Pro MAX</p>
                      <span>1500 AZN-dən başlayaraq</span>
                    </div>
                  </a>
                </Link>
                <Link href="/products">
                  <a className={classes.Top5ProductsCardBodyItem}>
                    <div className={classes.Top5ProductsCardBodyItemImgWrapper}>
                      <img src="img/card-img-1.png" />
                    </div>
                    <div className={classes.Top5ProductsCardBodyItemContent}>
                      <p>Iphone 13 Pro MAX</p>
                      <span>1500 AZN-dən başlayaraq</span>
                    </div>
                  </a>
                </Link>
                <Link href="/products">
                  <a className={classes.Top5ProductsCardBodyItem}>
                    <div className={classes.Top5ProductsCardBodyItemImgWrapper}>
                      <img src="img/card-img-1.png" />
                    </div>
                    <div className={classes.Top5ProductsCardBodyItemContent}>
                      <p>Iphone 13 Pro MAX</p>
                      <span>1500 AZN-dən başlayaraq</span>
                    </div>
                  </a>
                </Link>
                <Link href="/products">
                  <a className={classes.Top5ProductsCardBodyItem}>
                    <div className={classes.Top5ProductsCardBodyItemImgWrapper}>
                      <img src="img/card-img-1.png" />
                    </div>
                    <div className={classes.Top5ProductsCardBodyItemContent}>
                      <p>Iphone 13 Pro MAX</p>
                      <span>1500 AZN-dən başlayaraq</span>
                    </div>
                  </a>
                </Link>
                <Link href="/products">
                  <a className={classes.Top5ProductsCardBodyItem}>
                    <div className={classes.Top5ProductsCardBodyItemImgWrapper}>
                      <img src="img/card-img-1.png" />
                    </div>
                    <div className={classes.Top5ProductsCardBodyItemContent}>
                      <p>Iphone 13 Pro MAX</p>
                      <span>1500 AZN-dən başlayaraq</span>
                    </div>
                  </a>
                </Link>
              </div>
            </div>
            <div className={classes.ProductsOfWeek}>
              {/* red topping */}
              {/* <div className={classes.ProductsOfWeekTop}>
                <div>
                  <TopRatedBadge />
                  <span>Həhtənin məhsulu</span>
                </div>
              </div> */}
              <div className={classes.Top5ProductsCardTitle}>
                <TopRatedBadge />
                <span>Həftənin məhsulu</span>
              </div>
              {/* content */}
              <div
                className={`${classes.ProductsOfWeekSlider} ProductsOfWeekSlider`}
              >
                <Slider {...productOfWeekSettings}>
                  <div className={classes.ProductsOfWeekSliderItem}>
                    <div className={classes.ProductsOfWeekSliderItemImg}>
                      <img src="img/card-1.png" />
                      <span>1 gün 9:32:27</span>
                    </div>

                    <div className={classes.ProductsOfWeekSliderItemTitle}>
                      Iphone 13 Pro max, 128GB, Blue
                    </div>

                    <div className={classes.ProductPrice}>
                      <div className={classes.ProductPriceLeft}>
                        <p>
                          699
                          <Manat />
                        </p>
                        <p>
                          499
                          <Manat />
                        </p>
                      </div>
                      <div className={classes.ProductPriceRight}>
                        <p>
                          290
                          <Manat />
                          <span>/ 15 ay</span>
                        </p>
                      </div>
                    </div>
                  </div>
                  <div className={classes.ProductsOfWeekSliderItem}>
                    <div className={classes.ProductsOfWeekSliderItemImg}>
                      <img src="img/card-1.png" />
                      <span>1 gün 9:32:27</span>
                    </div>

                    <div className={classes.ProductsOfWeekSliderItemTitle}>
                      Iphone 13 Pro max, 128GB, Blue
                    </div>

                    <div className={classes.ProductPrice}>
                      <div className={classes.ProductPriceLeft}>
                        <p>
                          699
                          <Manat />
                        </p>
                        <p>
                          499
                          <Manat />
                        </p>
                      </div>
                      <div className={classes.ProductPriceRight}>
                        <p>
                          290
                          <Manat />
                          <span>/ 15 ay</span>
                        </p>
                      </div>
                    </div>
                  </div>
                </Slider>
              </div>
            </div>
          </div>
        </div>
        {/* </div> */}
      </div>

      <ProductEventSidebar filtration={true} />

      <VideoPlayer />

      <div className={classes.SectionFour}>
        <InfoCardList />
      </div>

      <SectionFive
        blogData={[
          {
            img: "/img/gold-apple.png",
            title: "İphone 13 Pro Max",
            body: "Əvvəlcədən Sİfarİş",
            btn: "Sifariş et",
            color: "white",
          },
          {
            img: "/img/refrigerator.png",
            title: "Bosch Soyuducuları",
            body: "Nəyə görə məhz Bosch?",
            btn: "Ətraflı",
            color: "gray",
          },
        ]}
      />

      <div className={classes.HomeNews}>
        <div className={classes.HomeNewsTop}>
          <div className={classes.HomeNewsTopTitle}>Xəbərlər</div>
          <div className={classes.HomeNewsTopHeader}>
            <h2>Ən son yeniliklər</h2>
            <Link href="/">
              <a>
                Daha çox <ArrowRight />
              </a>
            </Link>
          </div>
        </div>
        <HomeNewsSlider />
      </div>
      <BrandList />
      <MobileBottomNavbar />

      <div>
        <Modal />
      </div>
    </div>
  );
}

const mapStateToProps = (state) => {
  return {
    sidebarIsOpen: state.layout.sidebarIsOpen,
  };
};

export default connect(mapStateToProps, null)(Home);
