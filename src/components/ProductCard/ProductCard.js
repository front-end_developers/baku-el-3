import React, { useState } from "react";
import classes from "./ProductCard.module.scss";
import {
  CloseIcon,
  Heart,
  AddToShoppingCar,
  Scales,
  Star,
  Chat,
  DetailsIcon,
  Gift,
  DetailsQuality,
  Manat,
} from "../../components/Icons";
import { IconButton } from "@mui/material";
import { Collapse } from "@mui/material";

export default function ProductCard() {
  const [cardToggle, setCardToggle] = useState(false);
  const handleToggle = (e) => {
    // e.stopPropagation();
    e.preventDefault();
    setCardToggle(!cardToggle);
  };

  return (
    <div className={classes.Product}>
      <div className={classes.ProductImg}>
        <img src="/img/iphone-13.png" alt="product" />
        <Collapse className={classes.MoreOption} in={cardToggle}>
          <div className={classes.CardMenu}>
            <a href="#" className={classes.DetailsQuality}>
              <DetailsQuality />
            </a>
            <a href="#" className={classes.Birkart}>
              <img src="/img/bircart.png" />
            </a>
            <a href="#" className={classes.Gift}>
              <Gift />
            </a>
          </div>
        </Collapse>
        <div className={classes.Discount}>
          <span>-30%</span>
        </div>
        <div className={classes.OptionButton}>
          <IconButton onClick={handleToggle}>
            {cardToggle ? (
              <CloseIcon className={classes.Close} />
            ) : (
              <DetailsIcon />
            )}
          </IconButton>
        </div>

        <div className={classes.Buttons}>
          <div className={classes.Comments_Favoutite}>
            <div className={classes.Comments}>
              <button>
                <Chat /> <span>6 rəy</span>
              </button>
            </div>
            <div className={classes.FavouriteButton}>
              <button>
                <Star /> <span>4.6</span>
              </button>
            </div>
          </div>
          <div className={classes.ComparisorButton}>
            <button>
              <Scales />
            </button>
          </div>
        </div>
      </div>
      <div className={classes.ProductTitle}>
        <p>Iphone 13 Pro max, 128GB, Blu...</p>
        {/* <p>Iphone 13 Pro max, 128GB</p> */}
      </div>
      <div className={classes.ProductPrice}>
        <div className={classes.ProductPriceLeft}>
          <p>
            3699
            <Manat />
          </p>
          <p>
            3499
            <Manat />
          </p>
        </div>
        <div className={classes.ProductPriceRight}>
          <p>
            290
            <Manat /> <span>/ 15 ay</span>
          </p>
        </div>
      </div>
      <div className={classes.ProductButtons}>
        <button
          className={classes.ProductButtonsAddToCartBtn}
          onClick={(e) => e.preventDefault()}
        >
          <AddToShoppingCar />
        </button>
        <button
          className={classes.ProductButtonsAddToFavsBtn}
          onClick={(e) => e.preventDefault()}
        >
          <Heart />
        </button>
      </div>
    </div>
  );
}
