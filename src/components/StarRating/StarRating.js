import * as React from 'react';
import Rating from '@mui/material/Rating';
import Star from "../Icons/Star";


export default function BasicRating() {
    const [value, setValue] = React.useState(5);

    return (
        <div className="star-rating">
            <Rating icon={<Star/>} name="read-only" value={value} readOnly/>
            <p>({value})</p>
        </div>
    );
}
