import { useState, useEffect } from "react";
import { connect } from "react-redux";
import axios from "axios";
import Box from "@mui/material/Box";
import InputLabel from "@mui/material/InputLabel";
import MenuItem from "@mui/material/MenuItem";
import FormControl from "@mui/material/FormControl";
import Select from "@mui/material/Select";
import { reduxSetLang, reduxSetAvailLangs } from "../../../redux/actions/main";
import Link from "next/link";
import { useRouter } from "next/router";
import { createTheme, ThemeProvider, styled } from "@mui/material/styles";

const muiTheme = createTheme({ shadows: ["none"] });

function LangSelect(props) {
  const {
    reduxLang,
    reduxAvailLangs,
    isDefault,
    reduxSetLang,
    reduxSetAvailLangs,
    theme,
  } = props;

  const handleChange = (event) => {
    reduxSetLang(event.target.value);
    console.log(event.target.value);
  };

  useEffect(() => {
    console.log("reduxLang", reduxLang);
  });

  useEffect(() => {
    if (!reduxAvailLangs) {
      console.log("get langs api");
      axios({
        method: "get",
        url: `${process.env.base_url}dict/languages`,
      })
        .then((res) => {
          reduxSetAvailLangs(res.data);
        })
        .catch((err) => {
          console.log(err.response);
        });
    }
  }, []);

  const langSelectStyles = {
    "& .MuiOutlinedInput-root ": {
      "& .MuiSelect-select": {
        padding: "0",
        paddingRight: "0!important",
        borderRadius: "12px",
        color: "var(--text-primary)",
        backgroundColor: "var(--navbar-bg-main)",
        fontFamily: "Poppins",
        fontWeight: "700",
        fontSize: "14px",
        height: "38px",
        width: "38px",
        display: "flex",
        alignItems: "center",
        justifyContent: "center",
        transition: "0.2s",

        "&:hover": {
          backgroundColor: "var(--red-main)",
          color: "white",
        },
      },
      "& .MuiSelect-nativeInput ": {},
      "& svg": {
        display: "none",
      },
      "& .MuiOutlinedInput-notchedOutline": {
        top: "0",
        border: "0",
        "& legend": {
          display: "none",
        },
      },
    },
    "& .MuiList-root": {
      backgroundColor: "red",
    },
  };

  let { locale, locales, asPath } = useRouter();

  return (
    <ThemeProvider theme={muiTheme}>
      <FormControl fullWidth sx={langSelectStyles} elevation={0}>
        <Select
          labelId={`lang-select-${theme === "dark" ? "dark" : "light"}`}
          value={reduxAvailLangs}
          onChange={handleChange}
          renderValue={() => reduxLang.code}
          elevation={0}
        >
          {locale !== locales[0] ? (
            <MenuItem value={reduxAvailLangs?.[0]}>
              <Link href={asPath} locale={locales[0]}>
                {reduxAvailLangs?.[0]?.code}
              </Link>
            </MenuItem>
          ) : null}
          {locale !== locales[1] ? (
            <MenuItem value={reduxAvailLangs?.[1]}>
              <Link href={asPath} locale={locales[1]}>
                {reduxAvailLangs?.[1]?.code}
              </Link>
            </MenuItem>
          ) : null}
          {locale !== locales[2] ? (
            <MenuItem value={reduxAvailLangs?.[2]}>
              <Link href={asPath} locale={locales[2]}>
                {reduxAvailLangs?.[2]?.code}
              </Link>
            </MenuItem>
          ) : null}
          {/* <MenuItem value={reduxAvailLangs?.[1]}>
          <Link href={asPath} locale={locales[1]}>
            {reduxAvailLangs?.[1]?.code}
          </Link>
        </MenuItem>
        <MenuItem value={reduxAvailLangs?.[2]}>
          <Link href={asPath} locale={locales[2]}>
            {reduxAvailLangs?.[2]?.code}
          </Link>
        </MenuItem> */}
        </Select>
      </FormControl>
    </ThemeProvider>
  );
}

const mapStateToProps = (state) => {
  return {
    reduxLang: state.lang.reduxLang,
    reduxAvailLangs: state.lang.reduxAvailLangs,
    isDefault: state.lang.isDefault,
    theme: state.layout.theme,
  };
};

const mapDispatchToProps = {
  reduxSetLang,
  reduxSetAvailLangs,
};

export default connect(mapStateToProps, mapDispatchToProps)(LangSelect);
