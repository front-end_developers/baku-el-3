import classes from "./CampaignsNews.module.scss";

const CampaignsNews = () => {
  return (
    <div className={classes.CampaignsNews}>
      <div className={classes.CampaignsNewsItems}>
        <div className={classes.CampaignsNewsItemsItem}>
          <div className={classes.CampaignsNewsItemsItemImgWrapper}>
            <img src="/img/black-friday.png" />

            <div className={classes.CampaignsNewsItemsItemImgWrapperButton}>
              <button>Hədiyyə</button>
            </div>
          </div>

          <div className={classes.CampaignsNewsItemsItemContent}>
            <div className={classes.CampaignsNewsItemsItemContentTitle}>
              BLACK FRİDAY-də UNİKAL TƏKLİFLƏR!
            </div>
            <div className={classes.CampaignsNewsItemsItemContentBody}>
              <div className={classes.CampaignsNewsItemsItemContentBodyDay}>
                <span>04</span>
                <span>Gün</span>
              </div>
              <div className={classes.CampaignsNewsItemsItemContentBodyHour}>
                <span>22</span>
                <span>Saat</span>
              </div>
              <div className={classes.CampaignsNewsItemsItemContentBodyMinutes}>
                <span>18</span>
                <span>Dəqiqə</span>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default CampaignsNews;
