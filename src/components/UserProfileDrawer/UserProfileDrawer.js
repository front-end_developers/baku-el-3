import Drawer from "@mui/material/Drawer";
import {
  ArrowRight,
  CloseIcon,
  User,
  Bag,
  UserLatest,
  Location,
  UserCarts,
  UserBonuses,
  Login,
  ArrowLeft,
} from "../Icons";
import { makeStyles } from "@mui/styles";
import { connect } from "react-redux";
import Link from "next/link";
import UserProfileAside from "../UserProfileAside/UserProfileAside";
import { useEffect } from "react";
import ActiveLink from "../Link/Link";
import { logout } from "../../../redux/actions/main";
import classes from "./UserProfileDrawer.module.scss";
import { styled } from "@mui/system";

function UserProfileDrawer(props) {
  const { open, setOpen, theme, logout } = props;

  const StyledDrawer = styled((props) => <Drawer {...props} />)({
    position: "relative",
    "& .MuiPaper-root": {
      overflow: "visible",
    },
    "& .SectionAside": {
      background: theme === "light" ? "#ffffff" : "#191919",
      "&Top": {
        color: theme === "light" ? "#333333" : "#fff",
        "&Icon": {
          background: theme === "light" ? "#f5f5f5" : "#333",
        },
      },
      "&Separator": {
        "&:before": {
          border: `1px solid ${theme === "light" ? "#333333" : "#fff"}`,
        },
      },
      "&Body": {
        "&Item": {
          "& a": {
            color: theme === "light" ? "#333333" : "#fff",
            "&:hover, &.activeLink": {
              background: theme === "light" ? "#f5f5f5" : "#000",
              fontWeight: "500",
              "& svg": {
                color: "#ea2427",
              },
            },
          },
          "&:last-child": {
            "& button": {
              color: theme === "light" ? "#333333" : "#fff",
            },
          },
        },
      },
    },
    "& .closeBtn": {
      position: "absolute",
      height: "38px",
      width: "38px",
      display: "flex",
      alignItems: "center",
      justifyContent: "center",
      borderRadius: "12px",
      left: "-60px",
      top: "3rem",
      border: "unset",
      background: theme === "light" ? "#ffffff" : "#191919",
      color: theme === "light" ? "#333333" : "#fff",

      "& svg": {
        fontSize: "12px",
      },

      "@media(max-width: 768px)": {
        left: "-43px",
      },
    },
  });

  const onClose = () => {
    setOpen(false);
  };

  return (
    <StyledDrawer anchor="right" open={open} onClose={onClose}>
      <button
        onClick={() => setOpen(false)}
        className={`${classes.closeBtn} closeBtn`}
      >
        <CloseIcon />
      </button>

      <aside className={`${classes.SectionAside} SectionAside`}>
        <div className={`${classes.SectionAsideTop} SectionAsideTop`}>
          <div className={`${classes.SectionAsideTopIcon} SectionAsideTopIcon`}>
            <User />
          </div>
          <div className={classes.SectionAsideTopBody}>
            <h2>Ulviyya Imanova</h2>
            <span>ulviyaaimanova@gmail.com</span>
          </div>
        </div>
        <div
          className={`${classes.SectionAsideSeparator} SectionAsideSeparator`}
        ></div>
        <div className={`${classes.SectionAsideBody} SectionAsideBody`}>
          <div
            className={`${classes.SectionAsideBodyItem} SectionAsideBodyItem`}
          >
            <ActiveLink
              href="/user-profile/orders"
              activeClassName="activeLink"
            >
              <a>
                <span>
                  <Bag />
                </span>
                <span>Sifarislerim</span>
              </a>
            </ActiveLink>
          </div>
          <div
            className={`${classes.SectionAsideBodyItem} SectionAsideBodyItem`}
          >
            <ActiveLink
              href="/user-profile/latest"
              activeClassName={classes.activeLink}
            >
              <a>
                <span>
                  <UserLatest />
                </span>
                <span>Ən son baxdıqlarım</span>
              </a>
            </ActiveLink>
          </div>
          <div
            className={`${classes.SectionAsideBodyItem} SectionAsideBodyItem`}
          >
            <ActiveLink
              href="/user-profile/location"
              activeClassName={classes.activeLink}
            >
              <a>
                <span>
                  <Location />
                </span>
                <span>Ünvanlarım</span>
              </a>
            </ActiveLink>
          </div>
          <div
            className={`${classes.SectionAsideBodyItem} SectionAsideBodyItem`}
          >
            <ActiveLink
              href="/user-profile/carts"
              activeClassName={classes.activeLink}
            >
              <a>
                <span>
                  <UserCarts />
                </span>
                <span>Kartlarım</span>
              </a>
            </ActiveLink>
          </div>
          <div
            className={`${classes.SectionAsideBodyItem} SectionAsideBodyItem`}
          >
            <ActiveLink
              href="/user-profile/bonuses"
              activeClassName={classes.activeLink}
            >
              <a>
                <span>
                  <UserBonuses />
                </span>
                <span>Bonuslarım</span>
              </a>
            </ActiveLink>
          </div>
          <div
            className={`${classes.SectionAsideBodyItem} SectionAsideBodyItem`}
          >
            <ActiveLink
              href="/user-profile/personal-info"
              activeClassName={classes.activeLink}
            >
              <a>
                <span>
                  <User />
                </span>
                <span>Şəxsi məlumatlarım</span>
              </a>
            </ActiveLink>
          </div>
          <div
            className={`${classes.SectionAsideBodyItem} SectionAsideBodyItem`}
          >
            <ActiveLink
              href="/user-profile/apply"
              activeClassName={classes.activeLink}
            >
              <a>
                <span>
                  <User />
                </span>
                <span>Müraciət et</span>
              </a>
            </ActiveLink>
          </div>
          <div
            className={`${classes.SectionAsideBodyItem} SectionAsideBodyItem`}
          >
            <button onClick={logout}>
              <span>
                <Login />
              </span>
              <span>Çıxış</span>
            </button>
          </div>
        </div>
      </aside>
    </StyledDrawer>
  );
}

const mapStateToProps = (state) => {
  const {
    layout: { theme },
  } = state;
  return { theme };
};

const mapDispatchToProps = {
  logout,
};

export default connect(mapStateToProps, mapDispatchToProps)(UserProfileDrawer);
