import * as React from "react";
import PropTypes from "prop-types";
import { Global } from "@emotion/react";
import { styled } from "@mui/material/styles";
import CssBaseline from "@mui/material/CssBaseline";
import { grey } from "@mui/material/colors";
import Button from "@mui/material/Button";
import Box from "@mui/material/Box";
import Skeleton from "@mui/material/Skeleton";
import Typography from "@mui/material/Typography";
import SwipeableDrawer from "@mui/material/SwipeableDrawer";
import classes from "./SwipeableDrawer.module.scss";
import {
  AddToShoppingCar,
  Gift,
  Heart,
  OneClick,
  PaymentDiscount1,
  Scales,
  Profile,
  StartCall,
  PhoneHandle,
  Manat,
} from "../Icons";
import ProductCreditTab from "../ProductCreditTab/ProductTab";
import { Collapse } from "@mui/material";
import { useSelector } from "react-redux";

const drawerBleeding = 225;

// const Root = styled('div')(({theme}) => ({
//     height: '100%',
//     backgroundColor:
//         theme.palette.mode === 'light' ? grey[100] : theme.palette.background.default,
// }));

const StyledBox = styled(Box)(({ theme }) => ({
  backgroundColor: theme.palette.mode === "light" ? "#fff" : grey[800],
}));

const Puller = styled(Box)(({ theme }) => ({
  width: 30,
  height: 6,
  backgroundColor: theme.palette.mode === "light" ? grey[300] : grey[900],
  borderRadius: 3,
  position: "absolute",
  top: 8,
  left: "calc(50% - 15px)",
}));

function SwipeableEdgeDrawer(props) {
  const { window } = props;
  const [open, setOpen] = React.useState(false);
  const [oneClickToggle, setOneClickToggle] = React.useState(false);
  const [creditToggle, setCreditToggle] = React.useState(false);

  const toggleDrawer = (newOpen) => () => {
    setOpen(newOpen);
    setOneClickToggle(newOpen);
    setCreditToggle(newOpen);
  };

  const toggleOneClick = () => {
    setOneClickToggle(!oneClickToggle);
    setCreditToggle(false);
    if (oneClickToggle === true) {
      setOpen(false);
    } else if (oneClickToggle === false) {
      setOpen(true);
    }
  };

  const toggleCredit = () => {
    setCreditToggle(!creditToggle);
    setOneClickToggle(false);
    if (creditToggle === true) {
      setOpen(false);
    } else if (creditToggle === false) {
      setOpen(true);
    }
  };

  const { theme } = useSelector((state) => state.layout);
  //   console.log("theme", theme);

  // This is used only for the example
  const container =
    window !== undefined ? () => window().document.body : undefined;

  return (
    <div className={classes.Drawer}>
      {/* <Root> */}
      <CssBaseline />
      <Global
        styles={{
          ".MuiDrawer-root > .MuiPaper-root": {
            height: `calc(${
              creditToggle === true ? "95%" : "80%"
            } - ${drawerBleeding}px)`,
            overflow: "visible",
          },
        }}
      />
      <SwipeableDrawer
        labelId={`swipeableDrawer-${theme}`}
        className={classes.Drawer}
        container={container}
        anchor="bottom"
        open={open}
        onClose={toggleDrawer(false)}
        onOpen={toggleDrawer(true)}
        swipeAreaWidth={drawerBleeding}
        disableSwipeToOpen={false}
        ModalProps={{
          keepMounted: true,
        }}
      >
        <StyledBox
          sx={{
            position: "absolute",
            top: -drawerBleeding,
            borderTopLeftRadius: 24,
            borderTopRightRadius: 24,
            boxShadow: "0px -2px 30px rgba(37, 55, 70, 0.1)",
            visibility: "visible",
            right: 0,
            left: 0,
          }}
        >
          {/*<Puller />*/}
          <Typography sx={{ p: 2, color: "text.secondary" }}>
            <div className={classes.DrawerTop}>
              <div className={classes.PriceSection}>
                <div className={classes.PriceSectionLeft}>
                  <div className={classes.OldPrice}>
                    <span>
                      1699 <Manat />
                    </span>
                  </div>
                  <div className={`${classes.CurrentPrice} CurrentPrice`}>
                    <span>
                      1499 <Manat />
                    </span>
                    <span>
                      90 <Manat /> / 15 ay
                    </span>
                  </div>
                </div>
                <div className={classes.PriceSectionRight}>
                  <span>
                    <Gift /> 259 <Manat />
                  </span>
                </div>
              </div>
              <div className={`${classes.ButtonGroup} ButtonGroup`}>
                <Button className={classes.ButtonGroupAddToCart}>
                  <AddToShoppingCar /> <span>Səbətə əlavə et</span>
                </Button>
                <div className={classes.ButtonGroupRight}>
                  <Button
                    className={`${classes.ButtonGroupCompare} ButtonGroupCompare`}
                  >
                    <Scales />
                  </Button>
                  <Button
                    className={`${classes.ButtonGroupFavourite} ButtonGroupFavourite`}
                  >
                    <Heart />
                  </Button>
                </div>
              </div>
              <div className={`${classes.BottomButtons} BottomButtons`}>
                <div className={`${classes.OneClick} OneClick`}>
                  <Button onClick={toggleOneClick}>
                    <div
                      className={
                        !oneClickToggle
                          ? `${classes.OneClickIcon} OneClickIcon`
                          : `${classes.OneClickIcon} ${classes.Active} OneClickIcon Active`
                      }
                    >
                      <OneClick />
                    </div>
                    <div className={classes.OneClickText}>
                      <span>Bir kliklə al</span>
                      <span>Sürətli sifariş</span>
                    </div>
                  </Button>
                </div>
                <div className={`${classes.Credit} Credit`}>
                  <Button onClick={toggleCredit}>
                    <div
                      className={
                        !creditToggle
                          ? `${classes.CreditIcon} CreditIcon`
                          : `${classes.CreditIcon} ${classes.Active} CreditIcon Active`
                      }
                    >
                      <PaymentDiscount1 />
                    </div>
                    <div className={classes.CreditText}>
                      <span>Hissə-hissə al</span>
                      <span>18 ay 269 ₼</span>
                    </div>
                  </Button>
                </div>
              </div>
            </div>
          </Typography>
        </StyledBox>
        <StyledBox
          sx={{
            px: 2,
            pb: 2,
            height: "100%",
            overflow: "auto",
          }}
        >
          <Collapse in={oneClickToggle}>
            <div className={`${classes.OneClickToggle} OneClickToggle`}>
              <div className={classes.OneClickToggleTitle}>
                <span>“Bir kliklə al”-a xoş gəlmisən!</span>
              </div>
              <form className={classes.OneClickToggleForm}>
                <div className={classes.UserInputWrapper}>
                  <div className={classes.UserInputWrapperTitle}>
                    <span>Sənə necə müraciət edək?</span>
                  </div>
                  <div className={classes.UserInput}>
                    <input placeholder="Ad, Soyad" type="text" />
                    <Profile />
                  </div>
                </div>
                <div className={classes.NumberInputWrapper}>
                  <div className={classes.NumberInput}>
                    <input placeholder="Telefon" type="text" />
                    <PhoneHandle />
                  </div>
                </div>
                <div className={classes.SubmitButton}>
                  <Button>Göndər</Button>
                </div>
              </form>
            </div>
          </Collapse>
          <Collapse in={creditToggle}>
            <div
              className={`${classes.CreditToggle} CreditToggle credit-toggle-mobile`}
            >
              <ProductCreditTab />
            </div>
          </Collapse>
        </StyledBox>
      </SwipeableDrawer>
      {/* </Root> */}
    </div>
  );
}

SwipeableEdgeDrawer.propTypes = {
  window: PropTypes.func,
};

export default SwipeableEdgeDrawer;
