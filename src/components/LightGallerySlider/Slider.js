import React from "react";
import "react-responsive-carousel/lib/styles/carousel.min.css"; // requires a loader
import { Carousel } from "react-responsive-carousel";
import "lightgallery/css/lightgallery.css";
import "lightgallery/css/lg-zoom.css";
import "lightgallery/css/lg-thumbnail.css";

const CustomThumbs = () => {
  const imgArr = [
    // <img src="/img/360-thumb.png" alt="" key={1} />,
    <img src="/img/thumb-1.png" alt="" key={2} />,
    <img src="/img/iphone-13.png" alt="" key={3} />,
  ];

  return imgArr;
};

export default function Slider() {
  return (
    <div className="product-inner-slider">
      <Carousel
        renderThumbs={CustomThumbs}
        showIndicators={false}
        showArrows={false}
        showStatus={false}
        autoPlay={false}
        swipeable={true}
        emulateTouch={true}
      >
        {/* <div>
          <iframe
            height="300px"
            width="100%"
            allowFullScreen="true"
            src=" https://momento360.com/e/u/a9b53aa8f8b0403ba7a4e18243aabc66"
          ></iframe>
        </div> */}
        <div>
          <img src="/img/iphone13promax.png" alt="" />
        </div>
        <div>
          <img src="/img/iphone13promax.png" alt="" />
        </div>
      </Carousel>
    </div>
  );
}
