import { Manat } from "../../components/Icons";

import {
  styled,
  Table,
  TableBody,
  TableCell,
  TableHead,
  TableRow,
  IconButton,
  TableContainer,
} from "@mui/material";

function createData(
  productImg,
  product,
  amount,
  price,
  purchOption,
  payementType,
  shop,
  orderDate,
  bonus
) {
  return {
    productImg,
    product,
    amount,
    price,
    purchOption,
    payementType,
    shop,
    orderDate,
    bonus,
  };
}

const rows = [
  createData(
    "/img/user-bonuses-table1.png",
    "İphone 12 Pro Max",
    "1 ədəd",
    "3499",
    "Kredit",
    "Kart",
    "28 May filialı",
    "29 Avq 2021",
    "+150"
  ),
  createData(
    "/img/user-bonuses-table1.png",
    "İphone 12 Pro Max",
    "1 ədəd",
    "3499",
    "Kredit",
    "Kart",
    "28 May filialı",
    "29 Avq 2021",
    "+150"
  ),
  createData(
    "/img/user-bonuses-table1.png",
    "İphone 12 Pro Max",
    "1 ədəd",
    "3499",
    "Kredit",
    "Kart",
    "28 May filialı",
    "29 Avq 2021",
    "+150"
  ),
];

const UserTable = styled((props) => <Table {...props} />)(({ theme }) => ({
  "& .MuiTableHead-root": {
    borderBottom: "1px solid rgba(224, 224, 224, 1)",
    "& .MuiTableCell-head": {
      color: "var(--text-primary)",
      fontFamily: "Poppins",
      opacity: "0.5",
      fontSize: "10px",
      padding: "10px",
      textAlign: "left",
    },
  },
  "& .MuiTableBody-root": {
    "& .MuiTableRow-root": {
      "&:nth-of-type(odd)": {
        backgroundColor: theme.palette.action.hover,
      },
      "& .MuiTableCell-body": {
        color: "var(--text-primary)",
        fontFamily: "Poppins",
        borderBottom: "0",
        fontSize: "12px",
        paddingBlock: "10px",
        paddingInline: "5px",

        "& svg": {
          width: "9px",
          paddingBottom: "2px",
        },

        "&:first-child": {
          fontWeight: "600",
          fontSize: "12px",

          "& img": {
            height: "40px",
            width: "40px",
            marginRight: "12px",
          },
        },
        "&:last-child": {
          color: "#00AC07",
          fontWeight: "700",
        },
      },
      "&:last-child": {
        borderBottom: "1px solid rgba(224, 224, 224, 1)",
      },
    },
  },
}));

const UserBonusesTable = () => {
  return (
    <div>
      <TableContainer
        sx={{
          width: "100%",
        }}
      >
        <UserTable sx={{ minWidth: "max-content" }} aria-label="bonuses table">
          <TableHead>
            <TableRow>
              <TableCell>Məhsul</TableCell>
              <TableCell align="right">Miqdar</TableCell>
              <TableCell align="right">Qiymət</TableCell>
              <TableCell align="right">Satınalma stausu</TableCell>
              <TableCell align="right">Ödəniş növü</TableCell>
              <TableCell align="right">Mağaza</TableCell>
              <TableCell align="right">Sifariş tarixi</TableCell>
              <TableCell align="right">Bonus</TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {rows.map((row) => (
              <TableRow
                key={row.name}
                sx={{ "&:last-child td, &:last-child th": { border: 0 } }}
              >
                <TableCell component="th" scope="row">
                  <img src={row.productImg} />
                  {row.product}
                </TableCell>
                <TableCell component="th" scope="row">
                  {row.amount}
                </TableCell>
                <TableCell component="th" scope="row">
                  {row.price} <Manat />
                </TableCell>
                <TableCell component="th" scope="row">
                  {row.purchOption}
                </TableCell>
                <TableCell component="th" scope="row">
                  {row.payementType}
                </TableCell>
                <TableCell component="th" scope="row">
                  {row.shop}
                </TableCell>
                <TableCell component="th" scope="row">
                  {row.orderDate}
                </TableCell>
                <TableCell component="th" scope="row">
                  {row.bonus} <Manat />
                </TableCell>
              </TableRow>
            ))}
          </TableBody>
        </UserTable>
      </TableContainer>
    </div>
  );
};

export default UserBonusesTable;
