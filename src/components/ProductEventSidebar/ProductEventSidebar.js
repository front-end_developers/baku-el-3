import classes from "./ProductEventSidebar.module.scss";
import ActiveLink from "../Link/Link";
import ProductCard from "../ProductCard/ProductCard";
import Link from "next/link";
import { ArrowRight } from "../../components/Icons";

const ProductEventSidebar = (props) => {
  const { filtration, campaignLink, cards, title, background } = props;
  return (
    <div
      className={`${classes.SectionTwo} SectionTwo`}
      style={
        !background
          ? null
          : background === "grey"
          ? { background: "var(--section-two)" }
          : { background: "var(--bg-main)" }
      }
    >
      <div className={classes.SectionTwoTitle}>
        <div className={classes.TitleLeft}>
          <h2>{title ? title.small : "Özəl təkliflər"}</h2>
          <p>{title ? title.big : "Payız gəldi, şərtlər daha da sadələşdi!"}</p>
        </div>
        <div className={classes.TitleRight}>
          {filtration ? (
            <ul className={classes.TitleRightFilter}>
              <li className={classes.ActiveLink}>
                <a>Hamısı</a>
              </li>
              <li>
                <a>Elektronika</a>
              </li>
              <li>
                <a>Məişət əşyaları</a>
              </li>
              <li>
                <a>Mebellər</a>
              </li>
            </ul>
          ) : campaignLink ? (
            <div className={classes.TitleRightLink}>
              <Link href="">
                <a>
                  Hamısına bax <ArrowRight />
                </a>
              </Link>
            </div>
          ) : null}
        </div>
      </div>
      <div className={classes.SectionTwoProducts}>
        <div className={classes.SectionTwoProductsContainer}>
          {cards ? (
            cards.map((el, idx) => {
              return (
                <div
                  key={idx}
                  className={classes.SectionTwoProductsContainerItem}
                >
                  <ActiveLink href="/product-details/1">
                    <a>
                      <ProductCard />
                    </a>
                  </ActiveLink>
                </div>
              );
            })
          ) : (
            <>
              <div className={classes.SectionTwoProductsContainerItem}>
                <ActiveLink href="/product-details/1">
                  <a>
                    <ProductCard />
                  </a>
                </ActiveLink>
              </div>
              <div className={classes.SectionTwoProductsContainerItem}>
                <ActiveLink href="/product-details/1">
                  <a>
                    <ProductCard />
                  </a>
                </ActiveLink>
              </div>
              <div className={classes.SectionTwoProductsContainerItem}>
                <ActiveLink href="/product-details/1">
                  <a>
                    <ProductCard />
                  </a>
                </ActiveLink>
              </div>
              <div className={classes.SectionTwoProductsContainerItem}>
                <ActiveLink href="/product-details/1">
                  <a>
                    <ProductCard />
                  </a>
                </ActiveLink>
              </div>
              <div className={classes.SectionTwoProductsContainerItem}>
                <ActiveLink href="/product-details/1">
                  <a>
                    <ProductCard />
                  </a>
                </ActiveLink>
              </div>
              <div className={classes.SectionTwoProductsContainerItem}>
                <ActiveLink href="/product-details/1">
                  <a>
                    <ProductCard />
                  </a>
                </ActiveLink>
              </div>
              <div className={classes.SectionTwoProductsContainerItem}>
                <ActiveLink href="/product-details/1">
                  <a>
                    <ProductCard />
                  </a>
                </ActiveLink>
              </div>
              <div className={classes.SectionTwoProductsContainerItem}>
                <ActiveLink href="/product-details/1">
                  <a>
                    <ProductCard />
                  </a>
                </ActiveLink>
              </div>
              <div className={classes.SectionTwoProductsContainerItem}>
                <ActiveLink href="/product-details/1">
                  <a>
                    <ProductCard />
                  </a>
                </ActiveLink>
              </div>
              <div className={classes.SectionTwoProductsContainerItem}>
                <ActiveLink href="/product-details/1">
                  <a>
                    <ProductCard />
                  </a>
                </ActiveLink>
              </div>
              <div className={classes.SectionTwoProductsContainerItem}>
                <ActiveLink href="/product-details/1">
                  <a>
                    <ProductCard />
                  </a>
                </ActiveLink>
              </div>
              <div className={classes.SectionTwoProductsContainerItem}>
                <ActiveLink href="/product-details/1">
                  <a>
                    <ProductCard />
                  </a>
                </ActiveLink>
              </div>
            </>
          )}
        </div>
      </div>
    </div>
  );
};

export default ProductEventSidebar;
