/* eslint-disable @next/next/no-img-element */
import ThemeSwitcher from "../ThemeSwitcher/ThemeSwitcer";
import classes from "./FullHeader.module.scss";
import {
  DoubleForwardVector,
  ShoppingCar,
  Heart,
  Login,
  Scales,
  MenuIcon,
  User,
} from "../Icons";
import SearchBar from "../Search Bar/SearchBar";
import ActiveLink from "../Link/Link";
// import ReactSelect from "../ReactSelect/Select";
import LangSelect from "../LangSelect/LangSelect";
import Drawer from "../Drawer/Drawer";
import { useState } from "react";
import Link from "next/link";
import { connect } from "react-redux";

const FullHeader = (props) => {
  const { isLogged } = props;

  const [drawer, setDrawer] = useState(false);

  const UserIcon = () => (
    <div>
      <ActiveLink href="/user-profile" activeClassName="active">
        <a>
          <User />
        </a>
      </ActiveLink>
    </div>
  );
  const LoginIcon = () => (
    <div>
      <ActiveLink href="/auth/login" activeClassName="active">
        <a>
          <Login />
        </a>
      </ActiveLink>
    </div>
  );
  return (
    <div className={classes.Header}>
      <Drawer open={drawer} setOpen={setDrawer} />
      <nav className={classes.HeaderNav}>
        <div className={classes.Theme_Search_Campaigns_Warapper}>
          <div className={classes.Theme_Search_Warapper}>
            <Link href="/">
              <a>
                <img
                  src="/img/BakuElLogo.png"
                  alt=""
                  className={classes.HeaderNavLogo}
                />
              </a>
            </Link>
            <div className={classes.HeaderNavThemeSwitcher}>
              <ThemeSwitcher />
            </div>

            <div className={classes.HeaderNavSearchBar}>
              <SearchBar />
            </div>
          </div>

          <div className={classes.CampaignsOthers}>
            <div className={classes.Campaigns}>
              <ActiveLink href="/campaigns" activeClassName="">
                <a>Kampanyalar</a>
              </ActiveLink>
            </div>
            <div className={classes.Services}>
              <ActiveLink href="/" activeClassName="">
                <a>Xidmetler</a>
              </ActiveLink>
            </div>
            <div className={classes.Others}>
              <button
                onClick={() => {
                  setDrawer(true);
                }}
              >
                {"Diger >"}
              </button>
            </div>
          </div>
        </div>

        <div className={classes.HeaderNavBtns}>
          <div>
            <LangSelect />
          </div>

          <div>
            <ActiveLink
              href="/products/compare"
              activeClassName={classes.active}
            >
              <a>
                <Scales />
              </a>
            </ActiveLink>
          </div>
          <div className={classes.CartIcon}>
            <ActiveLink href="/basket" activeClassName={classes.active}>
              <a>
                <ShoppingCar />
              </a>
            </ActiveLink>
          </div>
          <div className={classes.HeartIcon}>
            <ActiveLink href="/favorites" activeClassName={classes.active}>
              <a>
                <Heart />
              </a>
            </ActiveLink>

            <span>9+</span>
          </div>
          {typeof isLogged !== "undefined" ? (
            isLogged ? (
              <UserIcon />
            ) : (
              <LoginIcon />
            )
          ) : null}
          {/* <div className="d-none d-md-block">
            <ActiveLink href="/user-profile" activeClassName={classes.active}>
              <a>
                <User />
              </a>
            </ActiveLink>
          </div>
          <div>
            <ActiveLink href="/auth/login" activeClassName={classes.active}>
              <a>
                <Login />
              </a>
            </ActiveLink>
          </div> */}
          <div
            className={classes.MenuIcon}
            onClick={() => {
              setDrawer(true);
            }}
          >
            <MenuIcon />
          </div>
        </div>
      </nav>
    </div>
  );
};

// export default FullHeader;
const mapStateToProps = (state) => {
  return {
    isLogged: state.auth.userData.token ? true : false,
  };
};

// const mapDispatchToProps = {
//   setUser,
// };

export default connect(mapStateToProps, null)(FullHeader);
