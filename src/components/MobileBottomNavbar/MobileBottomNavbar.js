import React, { useState } from "react";
import classes from "./MobileBottomNavbar.module.scss";
import ActiveLink from "../Link/Link";
import Home from "../Icons/Home";
import { Bag, Heart, ShoppingCar, Profile } from "../Icons";
import UserProfileDrawer from "../UserProfileDrawer/UserProfileDrawer";
import { connect } from "react-redux";

function MobileBottomNavbar(props) {
  // const { isLogged } = props;
  const [isOpenUserProfile, setIsOpenUserProfile] = useState(false);
  return (
    <div className={classes.Navbar}>
      <ul>
        <li>
          <ActiveLink activeClassName={classes.ActiveHome} href="/">
            <a>
              <Home />
            </a>
          </ActiveLink>
        </li>
        <li>
          <ActiveLink
            activeClassName={classes.ActiveProducts}
            href="/categories"
          >
            <a>
              <Bag />
            </a>
          </ActiveLink>
        </li>
        <li>
          <ActiveLink
            activeClassName={classes.ActiveFavourites}
            href="/favorites"
          >
            <a>
              <Heart />
            </a>
          </ActiveLink>
        </li>
        <li>
          <ActiveLink
            activeClassName={classes.ActiveCart}
            href="/basket/mobile"
          >
            <a>
              <ShoppingCar />
            </a>
          </ActiveLink>
        </li>
        {/* {typeof isLogged !== "undefined" ? (
          isLogged ? (
            <li>
              <button onClick={() => setIsOpenUserProfile(!isOpenUserProfile)}>
                <Profile />
              </button>
              <UserProfileDrawer
                open={isOpenUserProfile}
                setOpen={setIsOpenUserProfile}
              />
            </li>
          ) : null
        ) : null} */}
                    <li>
              <button onClick={() => setIsOpenUserProfile(!isOpenUserProfile)}>
                <Profile />
              </button>
              <UserProfileDrawer
                open={isOpenUserProfile}
                setOpen={setIsOpenUserProfile}
              />
            </li>
      </ul>
    </div>
  );
}
const mapStateToProps = (state) => {
  return {
    isLogged: state.auth.userData.token ? true : false,
  };
};

// const mapDispatchToProps = {
//   setUser,
// };

export default connect(mapStateToProps, null)(MobileBottomNavbar);
