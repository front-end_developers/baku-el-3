import { useState } from "react";
import { Modal, Button } from "react-bootstrap";
import { connect } from "react-redux";
import { closeModal } from "../../../redux/actions/main";

import classes from "./Modal.module.scss";

function ModalComponent(props) {
  const { state, closeModal } = props;
  // console.log(state.layout.modal.type);

  let modalStyles;
  switch (state.layout.modal.type) {
    case "success":
      modalStyles = {
        header: { color: "green" },
        button: { backgroundColor: "green", color: "white" },
      };
      break;
    case "error":
      modalStyles = {
        header: { color: "red" },
        button: { backgroundColor: "red", color: "white" },
      };
      break;
    default:
      modalStyles = {
        header: { color: "black" },
        button: { backgroundColor: "black", color: "white" },
      };
  }
  // function handleHideModal() {
  //   closeModal();
  // }
  return (
    <Modal
      size="md"
      onHide={closeModal}
      show={state.layout.modal.show}
      aria-labelledby="edit"
      className="modal-center pr-0"
      centered
    >
      <Modal.Header closeButton>
        <h1 style={modalStyles.header} className={classes.ModalHeader}>
          Təbriklər
        </h1>
      </Modal.Header>
      <Modal.Body>
        <p className={classes.ModalBody}>
          Dui phasellus fringilla eleifend sit in massa. Vitae commodo ac dui
          netus blandit. Metus, nisl dignissim.
        </p>
      </Modal.Body>
      <Modal.Footer>
        <div className={classes.ModalFooter}>
          <button style={modalStyles.button}>Ok</button>
          <button>Ləğv et</button>
        </div>
      </Modal.Footer>
    </Modal>
  );
}

// export default ModalComponent;

const mapStateToProps = (state) => {
  return { state: state };
};

const mapDispatchToProps = {
  // toggleTheme,
  closeModal,
};

export default connect(mapStateToProps, mapDispatchToProps)(ModalComponent);
