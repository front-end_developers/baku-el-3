import { useState } from "react";
import classes from "./FullVideoPlayer.module.scss";
import ReactPlayer from "react-player";
import { PlayIcon, TopRatedBadge } from "../Icons";
import { BsPauseCircle } from "react-icons/bs";

const FullVideoPlayer = () => {
  const [play, setPlay] = useState(false);
  const playHandler = () => {
    setPlay(!play);
  };

  return (
    <>
      <div className={classes.SectionThree}>
        <div className={classes.SectionThreeVideo}>
          <ReactPlayer
            playing={play}
            onPause={() => {
              setPlay(false);
            }}
            onEnded={() => {
              setPlay(false);
            }}
            controls={false}
            // controls={false}
            // onClick={playHandler}
            width={"100%"}
            height={"100%"}
            style={{ width: "100%" }}
            url="/Video/Music.mp4"
            // currentSeek={false}
          />
          {!play && (
            <div
              className={
                play
                  ? classes.VideoControls
                  : `${classes.VideoControls} ${classes.OnPause}`
              }
            >
              <div className={classes.PlayIcon}>
                <button onClick={playHandler}>
                  {play ? <BsPauseCircle /> : <PlayIcon />}
                </button>
              </div>
            </div>
          )}
          {!play && (
            <div className={classes.Text}>
              <div className={classes.TextTitle}>
                <h2>İnnovasiya</h2>
              </div>
              <div className={classes.TextMain}>
                <p>Yeni JBL UA Sport, Musiqi ilə yaşayanlar üçün.</p>
              </div>
            </div>
          )}
          {play && (
            <div
              onClick={() => setPlay(!play)}
              className={classes.ClickToPause}
            ></div>
          )}
        </div>
      </div>
    </>
  );
};

export default FullVideoPlayer;
