import Drawer from "@mui/material/Drawer";
import { ArrowRight, CloseIcon } from "../Icons";
import { makeStyles } from "@mui/styles";
import { connect } from "react-redux";
import ProductsSidebar from "../ProductsSidebar/ProductsSidebar";
import Link from "next/link";

function DrawerComponent(props) {
  const { open, setOpen, theme } = props;

  let drawerStyles;

  if (theme === "light") {
    drawerStyles = {
      backgroundColor: "white",
      color: "#333333",
      title: { color: "black" },
      liHover: { backgroundColor: "#f5f5f5" },
      closeButton: {
        backgroundColor: "white",
        color: "#333333",
      },
    };
  } else if (theme === "dark") {
    drawerStyles = {
      backgroundColor: "#333333",
      color: "white",
      title: { color: "white" },
      liHover: { backgroundColor: "black" },
      closeButton: {
        backgroundColor: "#333333",
        color: "white",
      },
    };
  }

  const useStyles = makeStyles({
    root: {
      "& .MuiPaper-root": {
        minWidth: "300px",
        backgroundColor: drawerStyles.backgroundColor,
        height: "100%!important",
        overflow: "visible",
        "& .drawer-section-container": {
          padding: "48px 32px",
          height: "100%",
          overflowY: "scroll",
          "& .drawer-section": {
            "& .drawer-section__title": {
              fontWeight: "400",
              fontSize: "9px",
              lineHeight: "14px",
              color: drawerStyles.title.color,
              opacity: "0.5",
              marginLeft: "5px",
              marginBottom: "8px",
            },
            "& .drawer-section__item": {
              "& ul": {
                "& li": {
                  "& a": {
                    padding: "16px",
                    display: "flex",
                    justifyContent: "space-between",
                    fontWeight: "400",
                    fontSize: "14px",
                    lineHeight: "21px",
                    color: drawerStyles.color,

                    "& span:last-child": {
                      "& svg": {
                        color: "#EA2427",
                        opacity: "0.2",
                      },
                    },
                  },

                  "&:hover": {
                    backgroundColor: drawerStyles.liHover.backgroundColor,
                    borderRadius: "16px",
                    "& span:first-child": {
                      fontWeight: "600",
                    },
                    "& span:last-child": {
                      "& svg": {
                        opacity: "unset",
                      },
                    },
                  },
                },
              },
            },
          },
        },
      },
    },
    closeButton: {
      position: "absolute",
      height: "38px",
      width: "38px",
      display: "flex",
      alignItems: "center",
      justifyContent: "center",
      borderRadius: "12px",
      left: "-60px",
      top: "3rem",
      border: "unset",
      backgroundColor: drawerStyles.closeButton.backgroundColor,
      "& svg": {
        fontSize: "12px",
        color: drawerStyles.closeButton.color,
      },

      "@media(max-width: 768px)": {
        left: "-43px",
      },

      "@media(max-width: 425px)": {
        left: "250px",
        top: "5px",
      },
    },
  });

  const classes = useStyles();

  const onClose = () => {
    setOpen(false);
  };

  return (
    <Drawer
      anchor="right"
      open={open}
      onClose={onClose}
      className={`${classes.root} drawer`}
    >
      <button
        onClick={() => setOpen(false)}
        className={`${classes.closeButton} close-button`}
      >
        <CloseIcon />
      </button>
      <div className="drawer-section-container">
        <div className="drawer-section">
          <div className="drawer-section__title">
            <span>Məlumat</span>
          </div>
          <div className="drawer-section__item">
            <ul>
              <li>
                <Link href="/bonus-user-card">
                  <a>
                    <span>Bonus - Müştəri kartı</span>
                    <span>
                      <ArrowRight />
                    </span>
                  </a>
                </Link>
              </li>
              <li>
                <Link href="/brend-list">
                  <a>
                    <span>Brendlər</span>
                    <span>
                      <ArrowRight />
                    </span>
                  </a>
                </Link>
              </li>
              <li>
                <Link href="/news">
                  <a>
                    <span>Bloq</span>
                    <span>
                      <ArrowRight />
                    </span>
                  </a>
                </Link>
              </li>
            </ul>
          </div>
        </div>
        <div className="drawer-section">
          <div className="drawer-section__title">
            <span>Şirkət</span>
          </div>
          <div className="drawer-section__item">
            <ul>
              <li>
                <Link href="/shops">
                  <a>
                    <span>Mağazalar</span>
                    <span>
                      <ArrowRight />
                    </span>
                  </a>
                </Link>
              </li>
            </ul>
          </div>
        </div>
      </div>
    </Drawer>
  );
}

const mapStateToProps = (state) => {
  const {
    layout: { theme },
  } = state;
  return { theme };
};

const mapDispatchToProps = {};

export default connect(mapStateToProps, mapDispatchToProps)(DrawerComponent);
