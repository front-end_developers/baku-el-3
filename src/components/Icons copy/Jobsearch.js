import * as React from "react";

const SvgJobsearch = (props) => (
  <svg
    width="1em"
    height="1em"
    viewBox="0 0 38 38"
    fill="none"
    xmlns="http://www.w3.org/2000/svg"
    {...props}
  >
    <g clipPath="url(#jobsearch_svg__a)">
      <path
        opacity={0.4}
        d="M22.98 13.846a3.679 3.679 0 0 1-3.674-3.675 3.678 3.678 0 0 1 3.674-3.674 3.678 3.678 0 0 1 3.674 3.674 3.679 3.679 0 0 1-3.674 3.675Zm0-4.38a.706.706 0 1 0 .002 1.412.706.706 0 0 0-.002-1.412Zm-4.658 6.359a1.485 1.485 0 0 1 1.444-1.142h6.428c.688 0 1.286.473 1.444 1.142l1.397 5.891a1.485 1.485 0 0 1-1.445 1.827h-9.22a1.485 1.485 0 0 1-1.445-1.827l1.397-5.891Zm7.391 4.75-.693-2.923h-4.08l-.693 2.922h5.466ZM.435 31.476l5.64-5.64c.58-.58 1.52-.58 2.1 0l.944.945 2.248-2.248A14.95 14.95 0 0 1 7.96 15.02C7.96 6.738 14.698 0 22.98 0S38 6.738 38 15.02s-6.738 15.02-15.02 15.02a14.95 14.95 0 0 1-9.514-3.406l-2.247 2.247.944.945c.58.58.58 1.519 0 2.099l-5.64 5.64c-.58.58-1.52.58-2.1 0L.435 33.577c-.58-.58-.58-1.52 0-2.1ZM35.03 15.02c0-6.645-5.406-12.051-12.05-12.051-6.646 0-12.052 5.406-12.052 12.05 0 6.646 5.406 12.052 12.051 12.052 6.645 0 12.051-5.406 12.051-12.051ZM5.473 34.416l3.541-3.541-1.889-1.89-3.541 3.542 1.889 1.89Z"
        fill="currentColor"
      />
    </g>
    <defs>
      <clipPath id="jobsearch_svg__a">
        <path
          fill="currentColor"
          transform="matrix(-1 0 0 1 38 0)"
          d="M0 0h38v38H0z"
        />
      </clipPath>
    </defs>
  </svg>
);

export default SvgJobsearch;
