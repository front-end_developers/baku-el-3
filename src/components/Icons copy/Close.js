import * as React from "react";

const SvgClose = (props) => (
  <svg
    width="1em"
    height="1em"
    viewBox="0 0 8 8"
    fill="none"
    xmlns="http://www.w3.org/2000/svg"
    {...props}
  >
    <path
      d="M6.692.25 4 2.942 1.308.25.25 1.308 2.942 4 .25 6.692 1.308 7.75 4 5.058 6.692 7.75 7.75 6.692 5.058 4 7.75 1.308 6.692.25Z"
      fill="currentColor"
    />
  </svg>
);

export default SvgClose;
