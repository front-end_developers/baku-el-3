import * as React from "react";

const SvgArrowRight2 = (props) => (
  <svg
    width="1em"
    height="1em"
    viewBox="0 0 14 12"
    fill="none"
    xmlns="http://www.w3.org/2000/svg"
    {...props}
  >
    <path
      d="M12.962 5.789H1.423M8.308 1.155l4.654 4.634-4.654 4.634"
      stroke="currentColor"
      strokeWidth={1.5}
      strokeLinecap="round"
      strokeLinejoin="round"
    />
  </svg>
);

export default SvgArrowRight2;
