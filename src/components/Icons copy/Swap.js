import * as React from "react";

const SvgSwap = (props) => (
  <svg
    width="1em"
    height="1em"
    viewBox="0 0 16 14"
    fill="none"
    xmlns="http://www.w3.org/2000/svg"
    {...props}
  >
    <path
      d="M11.63 13.123V2.91M14.687 10.051l-3.058 3.073L8.57 10.05M4.182.875v10.213M1.125 3.947 4.183.874l3.059 3.073"
      stroke="currentColor"
      strokeWidth={1.5}
      strokeLinecap="round"
      strokeLinejoin="round"
    />
  </svg>
);

export default SvgSwap;
