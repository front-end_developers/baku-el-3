import * as React from "react";

const SvgHeart = (props) => (
  <svg
    width="1em"
    height="1em"
    viewBox="0 0 16 16"
    fill="none"
    xmlns="http://www.w3.org/2000/svg"
    {...props}
  >
    <path
      clipRule="evenodd"
      d="M1.279 7.449c-.805-2.513.136-5.385 2.773-6.234a4.505 4.505 0 0 1 4.073.684c1.091-.844 2.679-1.129 4.065-.684 2.638.85 3.584 3.721 2.78 6.234-1.252 3.982-6.845 7.05-6.845 7.05s-5.552-3.021-6.846-7.05Z"
      stroke="currentColor"
      strokeWidth={1.5}
      strokeLinecap="round"
      strokeLinejoin="round"
    />
    <path
      d="M11.125 3.775c.803.26 1.37.976 1.438 1.817"
      stroke="currentColor"
      strokeWidth={1.5}
      strokeLinecap="round"
      strokeLinejoin="round"
    />
  </svg>
);

export default SvgHeart;
