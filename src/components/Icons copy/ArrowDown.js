import * as React from "react";

const SvgArrowDown = (props) => (
  <svg
    width="1em"
    height="1em"
    viewBox="0 0 16 16"
    fill="none"
    xmlns="http://www.w3.org/2000/svg"
    {...props}
  >
    <path
      d="m8 10.475 4.007-4.006-.942-.944L8 8.592 4.936 5.525l-.943.943L8 10.475Z"
      fill="currentColor"
    />
  </svg>
);

export default SvgArrowDown;
