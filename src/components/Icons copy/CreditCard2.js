import * as React from "react";

const SvgCreditCard2 = (props) => (
  <svg
    width="1em"
    height="1em"
    viewBox="0 0 16 16"
    fill="none"
    xmlns="http://www.w3.org/2000/svg"
    {...props}
  >
    <path
      d="M16 5.094V4.25a1.75 1.75 0 0 0-1.75-1.75H1.75A1.75 1.75 0 0 0 0 4.25v.844c0 .086.07.156.156.156h15.688c.086 0 .156-.07.156-.156ZM0 6.406v5.344c0 .966.783 1.75 1.75 1.75h12.5A1.75 1.75 0 0 0 16 11.75V6.406a.156.156 0 0 0-.156-.156H.156A.156.156 0 0 0 0 6.406ZM4 10.5a.5.5 0 0 1-.5.5H3a.5.5 0 0 1-.5-.5V10a.5.5 0 0 1 .5-.5h.5a.5.5 0 0 1 .5.5v.5Z"
      fill="currentColor"
    />
  </svg>
);

export default SvgCreditCard2;
