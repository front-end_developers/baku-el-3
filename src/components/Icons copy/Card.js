import * as React from "react";

const SvgCard = (props) => (
  <svg
    width="1em"
    height="1em"
    viewBox="0 0 20 14"
    fill="none"
    xmlns="http://www.w3.org/2000/svg"
    {...props}
  >
    <path
      d="M16.87.875H2.948A1.951 1.951 0 0 0 1 2.824v8.352c0 1.075.874 1.949 1.949 1.949h13.92a1.951 1.951 0 0 0 1.95-1.949V2.824a1.951 1.951 0 0 0-1.95-1.949ZM2.948 1.989h13.92c.46 0 .835.374.835.835v1.114H2.114V2.824c0-.46.374-.835.835-.835Zm13.92 10.022H2.95a.836.836 0 0 1-.835-.835V5.051h15.59v6.125c0 .46-.374.835-.835.835Z"
      fill="currentColor"
      stroke="currentColor"
      strokeWidth={0.5}
    />
    <path
      d="M4.897 10.34H4.34a.557.557 0 0 1-.557-.556v-.557c0-.307.25-.557.557-.557h.557c.307 0 .557.25.557.557v.557c0 .307-.25.557-.557.557Z"
      fill="currentColor"
      stroke="currentColor"
      strokeWidth={0.5}
    />
  </svg>
);

export default SvgCard;
