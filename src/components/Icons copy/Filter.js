import * as React from "react";

const SvgFilter = (props) => (
  <svg
    width="1em"
    height="1em"
    viewBox="0 0 14 12"
    fill="none"
    xmlns="http://www.w3.org/2000/svg"
    {...props}
  >
    <path
      d="M5.887 9.062h-4.2M7.76 2.6h4.2"
      stroke="currentColor"
      strokeWidth={1.5}
      strokeLinecap="round"
      strokeLinejoin="round"
    />
    <path
      clipRule="evenodd"
      d="M4.818 2.564A1.57 1.57 0 0 0 3.242 1c-.87 0-1.575.7-1.575 1.564a1.57 1.57 0 0 0 1.575 1.564c.87 0 1.576-.7 1.576-1.564ZM12.334 9.036a1.57 1.57 0 0 0-1.575-1.564c-.87 0-1.576.7-1.576 1.564a1.57 1.57 0 0 0 1.576 1.564c.87 0 1.575-.7 1.575-1.564Z"
      stroke="currentColor"
      strokeWidth={1.5}
      strokeLinecap="round"
      strokeLinejoin="round"
    />
  </svg>
);

export default SvgFilter;
