import * as React from "react";

const SvgProfile = (props) => (
  <svg
    width="1em"
    height="1em"
    viewBox="0 0 14 16"
    fill="none"
    xmlns="http://www.w3.org/2000/svg"
    {...props}
  >
    <path
      d="M6.997 10.47c-3.354 0-6.22.528-6.22 2.644s2.848 2.663 6.22 2.663c3.355 0 6.22-.528 6.22-2.644s-2.847-2.664-6.22-2.664Z"
      fill="currentColor"
    />
    <path
      opacity={0.4}
      d="M6.997 8.454a4.101 4.101 0 0 0 4.116-4.116A4.101 4.101 0 0 0 6.997.222 4.102 4.102 0 0 0 2.88 4.338a4.102 4.102 0 0 0 4.116 4.116Z"
      fill="currentColor"
    />
  </svg>
);

export default SvgProfile;
