import * as React from "react";

const SvgUpload = (props) => (
  <svg
    width="1em"
    height="1em"
    viewBox="0 0 16 17"
    fill="none"
    xmlns="http://www.w3.org/2000/svg"
    {...props}
  >
    <path
      d="M4.542 6.738h-.7A2.764 2.764 0 0 0 1.08 9.502v3.656a2.764 2.764 0 0 0 2.763 2.763h8.348a2.764 2.764 0 0 0 2.764-2.763V9.494a2.756 2.756 0 0 0-2.756-2.756h-.707M8.016 1.643v9.03M5.83 3.839l2.186-2.196 2.187 2.196"
      stroke="currentColor"
      strokeWidth={1.5}
      strokeLinecap="round"
      strokeLinejoin="round"
    />
  </svg>
);

export default SvgUpload;
