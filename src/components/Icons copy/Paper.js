import * as React from "react";

const SvgPaper = (props) => (
  <svg
    width="1em"
    height="1em"
    viewBox="0 0 18 20"
    fill="none"
    xmlns="http://www.w3.org/2000/svg"
    {...props}
  >
    <path
      clipRule="evenodd"
      d="M11.738.762H5.085A3.82 3.82 0 0 0 1.25 4.49v10.713a3.828 3.828 0 0 0 3.742 3.91c.03 0 .06.002.092 0h7.989a3.887 3.887 0 0 0 3.729-3.91V6.038L11.738.762Z"
      stroke="currentColor"
      strokeWidth={1.5}
      strokeLinecap="round"
      strokeLinejoin="round"
    />
    <path
      d="M11.475.75v2.909a2.574 2.574 0 0 0 2.568 2.575h2.755M11.288 13.358h-5.4M9.243 9.606H5.887"
      stroke="currentColor"
      strokeWidth={1.5}
      strokeLinecap="round"
      strokeLinejoin="round"
    />
  </svg>
);

export default SvgPaper;
