import * as React from "react";

const SvgDocument = (props) => (
  <svg
    width="1em"
    height="1em"
    viewBox="0 0 15 16"
    fill="none"
    xmlns="http://www.w3.org/2000/svg"
    {...props}
  >
    <path
      d="M9.786 11.168H4.371M9.786 8.028H4.371M6.437 4.895H4.371"
      stroke="currentColor"
      strokeWidth={1.5}
      strokeLinecap="round"
      strokeLinejoin="round"
    />
    <path
      clipRule="evenodd"
      d="m9.931 1.062-5.767.003C2.094 1.078.813 2.44.813 4.518v6.897c0 2.088 1.291 3.455 3.379 3.455l5.767-.002c2.07-.013 3.352-1.376 3.352-3.453V4.518c0-2.088-1.292-3.456-3.38-3.456Z"
      stroke="currentColor"
      strokeWidth={1.5}
      strokeLinecap="round"
      strokeLinejoin="round"
    />
  </svg>
);

export default SvgDocument;
