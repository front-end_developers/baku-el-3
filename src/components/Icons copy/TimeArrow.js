import * as React from "react";

const SvgTimeArrow = (props) => (
  <svg
    width="1em"
    height="1em"
    viewBox="0 0 18 16"
    fill="none"
    xmlns="http://www.w3.org/2000/svg"
    {...props}
  >
    <path
      d="M9.428 4.571v4.286l3.668 2.177.617-1.041-3-1.779V4.571H9.428Z"
      fill="currentColor"
    />
    <path
      d="M10.281.286A7.708 7.708 0 0 0 2.571 8H0l3.339 3.338.06.125L6.857 8H4.286a6.001 6.001 0 0 1 12 0c0 3.313-2.687 6-6 6a5.976 5.976 0 0 1-4.239-1.761L4.834 13.45a7.665 7.665 0 0 0 5.447 2.263A7.716 7.716 0 0 0 18 8 7.716 7.716 0 0 0 10.281.286Z"
      fill="currentColor"
    />
  </svg>
);

export default SvgTimeArrow;
