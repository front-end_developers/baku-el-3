import * as React from "react";

const SvgArrowRight = (props) => (
  <svg
    width="1em"
    height="1em"
    viewBox="0 0 6 9"
    fill="none"
    xmlns="http://www.w3.org/2000/svg"
    {...props}
  >
    <path
      fillRule="evenodd"
      clipRule="evenodd"
      d="M.935 8.349a1 1 0 0 1-.03-1.414L3.242 4.5.906 2.065A1 1 0 1 1 2.349.68l3 3.127a1 1 0 0 1 0 1.385l-3 3.127a1 1 0 0 1-1.414.03Z"
      fill="currentColor"
    />
  </svg>
);

export default SvgArrowRight;
