import * as React from "react";

const SvgVector = (props) => (
  <svg
    width="1em"
    height="1em"
    viewBox="0 0 10 6"
    fill="none"
    xmlns="http://www.w3.org/2000/svg"
    {...props}
  >
    <path
      d="M5 5.475 9.007 1.47 8.065.525 5 3.592 1.936.525l-.943.943L5 5.475Z"
      fill="currentColor"
    />
  </svg>
);

export default SvgVector;
