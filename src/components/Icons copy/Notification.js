import * as React from "react";

const SvgNotification = (props) => (
  <svg
    width="1em"
    height="1em"
    viewBox="0 0 16 18"
    fill="none"
    xmlns="http://www.w3.org/2000/svg"
    {...props}
  >
    <path
      fillRule="evenodd"
      clipRule="evenodd"
      d="M8 13.386c4.23 0 6.186-.543 6.375-2.72 0-2.177-1.364-2.037-1.364-4.707C13.01 3.873 11.034 1.5 8 1.5S2.99 3.873 2.99 5.959c0 2.67-1.365 2.53-1.365 4.706.19 2.186 2.146 2.72 6.375 2.72Z"
      fill="currentColor"
      stroke="currentColor"
      strokeWidth={1.5}
      strokeLinecap="round"
      strokeLinejoin="round"
    />
    <path
      d="M9.79 15.643c-1.022 1.136-2.618 1.15-3.651 0"
      stroke="currentColor"
      strokeWidth={1.5}
      strokeLinecap="round"
      strokeLinejoin="round"
    />
  </svg>
);

export default SvgNotification;
