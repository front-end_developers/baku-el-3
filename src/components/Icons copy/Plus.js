import * as React from "react";

const SvgPlus = (props) => (
  <svg
    width="1em"
    height="1em"
    viewBox="0 0 13 12"
    fill="none"
    xmlns="http://www.w3.org/2000/svg"
    {...props}
  >
    <path
      d="M5.503 12h1.994V6.893H13V5.107H7.497V0H5.503v5.107H0v1.786h5.503V12Z"
      fill="currentColor"
    />
  </svg>
);

export default SvgPlus;
