import * as React from "react";

const SvgDublArrowRight = (props) => (
  <svg
    width="1em"
    height="1em"
    viewBox="0 0 10 10"
    fill="none"
    xmlns="http://www.w3.org/2000/svg"
    {...props}
  >
    <path
      d="m5.276 9.007-.943-.942 3.066-3.067-3.066-3.067.943-.938L9.283 5 5.277 9.007h-.001Zm-3.617 0-.942-.942 3.066-3.067L.717 1.936l.942-.943L5.667 5 1.66 9.007Z"
      fill="currentColor"
    />
  </svg>
);

export default SvgDublArrowRight;
