import * as React from "react";

const SvgPaperUpload = (props) => (
  <svg
    width="1em"
    height="1em"
    viewBox="0 0 18 20"
    fill="none"
    xmlns="http://www.w3.org/2000/svg"
    {...props}
  >
    <path
      clipRule="evenodd"
      d="M11.736.762H5.084A3.82 3.82 0 0 0 1.25 4.49v10.737a3.807 3.807 0 0 0 3.724 3.887h8.098a3.867 3.867 0 0 0 3.73-3.887v-9.19L11.736.762Z"
      stroke="currentColor"
      strokeWidth={1.5}
      strokeLinecap="round"
      strokeLinejoin="round"
    />
    <path
      d="M11.475.75v2.909a2.575 2.575 0 0 0 2.569 2.575h2.754M8.64 7.909v6.04M10.986 10.264 8.641 7.91l-2.345 2.355"
      stroke="currentColor"
      strokeWidth={1.5}
      strokeLinecap="round"
      strokeLinejoin="round"
    />
  </svg>
);

export default SvgPaperUpload;
