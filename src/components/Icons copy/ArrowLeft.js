import * as React from "react";

const SvgArrowLeft = (props) => (
  <svg
    width="1em"
    height="1em"
    viewBox="0 0 5 9"
    fill="none"
    xmlns="http://www.w3.org/2000/svg"
    {...props}
  >
    <path
      fillRule="evenodd"
      clipRule="evenodd"
      d="M4.692.651a1 1 0 0 1 .03 1.414L2.386 4.5l2.336 2.435A1 1 0 0 1 3.278 8.32l-3-3.127a1 1 0 0 1 0-1.385l3-3.127A1 1 0 0 1 4.692.65Z"
      fill="currentColor"
    />
  </svg>
);

export default SvgArrowLeft;
