import * as React from "react";

const SvgFolder = (props) => (
  <svg
    width="1em"
    height="1em"
    viewBox="0 0 17 17"
    fill="none"
    xmlns="http://www.w3.org/2000/svg"
    {...props}
  >
    <path
      clipRule="evenodd"
      d="M16.064 11.8c0 2.683-1.582 4.264-4.265 4.264H5.963c-2.69 0-4.276-1.581-4.276-4.265V5.95c0-2.68.986-4.261 3.67-4.261h1.5a1.71 1.71 0 0 1 1.368.684l.685.91c.324.431.831.685 1.37.686h2.122c2.69 0 3.683 1.37 3.683 4.107l-.02 3.724Z"
      stroke="currentColor"
      strokeWidth={1.5}
      strokeLinecap="round"
      strokeLinejoin="round"
    />
    <path
      d="M5.611 10.847h6.552"
      stroke="currentColor"
      strokeWidth={1.5}
      strokeLinecap="round"
      strokeLinejoin="round"
    />
  </svg>
);

export default SvgFolder;
