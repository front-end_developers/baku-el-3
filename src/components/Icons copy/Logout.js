import * as React from "react";

const SvgLogout = (props) => (
  <svg
    width="1em"
    height="1em"
    viewBox="0 0 17 16"
    fill="none"
    xmlns="http://www.w3.org/2000/svg"
    {...props}
  >
    <path
      d="M10.261 4.542v-.7A2.764 2.764 0 0 0 7.497 1.08H3.841a2.764 2.764 0 0 0-2.763 2.763v8.348a2.764 2.764 0 0 0 2.763 2.764h3.664a2.756 2.756 0 0 0 2.756-2.756v-.707M15.357 8.016h-9.03M13.16 5.83l2.196 2.186-2.196 2.187"
      stroke="currentColor"
      strokeWidth={1.5}
      strokeLinecap="round"
      strokeLinejoin="round"
    />
  </svg>
);

export default SvgLogout;
