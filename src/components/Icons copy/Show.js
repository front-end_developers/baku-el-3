import * as React from "react";

const SvgShow = (props) => (
  <svg
    width="1em"
    height="1em"
    viewBox="0 0 14 12"
    fill="none"
    xmlns="http://www.w3.org/2000/svg"
    {...props}
  >
    <path
      clipRule="evenodd"
      d="M9.108 6.035a2.108 2.108 0 1 1-4.216 0 2.108 2.108 0 0 1 4.216 0Z"
      stroke="currentColor"
      strokeWidth={1.5}
      strokeLinecap="round"
      strokeLinejoin="round"
    />
    <path
      clipRule="evenodd"
      d="M6.999 10.903c2.538 0 4.86-1.825 6.168-4.868-1.307-3.042-3.63-4.868-6.168-4.868h.003c-2.54 0-4.861 1.826-6.169 4.868 1.308 3.043 3.63 4.868 6.168 4.868H7Z"
      stroke="currentColor"
      strokeWidth={1.5}
      strokeLinecap="round"
      strokeLinejoin="round"
    />
  </svg>
);

export default SvgShow;
