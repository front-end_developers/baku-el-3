import * as React from "react";

const SvgHome = (props) => (
  <svg
    width="1em"
    height="1em"
    viewBox="0 0 16 18"
    fill="none"
    xmlns="http://www.w3.org/2000/svg"
    {...props}
  >
    <path
      d="M5.868 15.579v-2.3c0-.585.477-1.06 1.068-1.065H9.1a1.07 1.07 0 0 1 1.075 1.064v2.308c0 .496.4.902.902.914h1.443c1.439 0 2.605-1.155 2.605-2.579V7.378a1.83 1.83 0 0 0-.722-1.428L9.468 2.014a2.385 2.385 0 0 0-2.958 0L1.597 5.957c-.45.34-.716.868-.722 1.429v6.535c0 1.425 1.166 2.579 2.605 2.579h1.443c.514 0 .93-.413.93-.921"
      stroke="currentColor"
      strokeWidth={1.5}
      strokeLinecap="round"
      strokeLinejoin="round"
    />
  </svg>
);

export default SvgHome;
