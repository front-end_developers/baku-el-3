import * as React from "react";

const SvgStroke1 = (props) => (
  <svg
    width="1em"
    height="1em"
    viewBox="0 0 16 16"
    fill="none"
    xmlns="http://www.w3.org/2000/svg"
    {...props}
  >
    <path
      clipRule="evenodd"
      d="M8.312 8.315C5.653 10.974 5.05 7.898 3.356 9.59c-1.632 1.632-2.57 1.959-.502 4.027.26.208 1.905 2.713 7.69-3.07 5.785-5.784 3.282-7.432 3.073-7.691-2.073-2.073-2.394-1.13-4.027.502-1.693 1.693 1.382 2.298-1.278 4.957Z"
      stroke="currentColor"
      strokeWidth={1.5}
      strokeLinecap="round"
      strokeLinejoin="round"
    />
  </svg>
);

export default SvgStroke1;
