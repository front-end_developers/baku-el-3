import classes from "./UserOrdersItems.module.scss";

const UserOrdersItems = () => {
  return (
    <div className={classes.UserOrdersItems}>
      <div className={classes.UserOrdersItemsItem}>
        <div className={classes.UserOrdersItemsItemLeft}>
          <img src="/img/airpod.png" alt="" />
        </div>
        <div className={classes.UserOrdersItemsItemRight}>
          <div className={classes.UserOrdersItemsItemRightTop}>
            <div className={classes.UserOrdersItemsItemRightTopTitle}>
              <div className={classes.UserOrdersItemsItemRightTopTitleName}>
                Apple Airpods Pro
              </div>
              <div className={classes.UserOrdersItemsItemRightTopTitleCount}>
                1 ədəd
              </div>
            </div>
            <div className={classes.UserOrdersItemsItemRightTopDate}>
              29 Avq 2021
            </div>
          </div>
          <div className={classes.UserOrdersItemsItemRightBottom}>
            <div className={classes.UserOrdersItemsItemRightBottomPrice}>
              499 ₼
            </div>
            <div className={classes.UserOrdersItemsItemRightBottomOperations}>
              <li>Təsdiqlənib</li>
            </div>
          </div>
        </div>
      </div>

      <div className={classes.UserOrdersItemsItem}>
        <div className={classes.UserOrdersItemsItemLeft}>
          <img src="/img/airpod.png" alt="" />
        </div>
        <div className={classes.UserOrdersItemsItemRight}>
          <div className={classes.UserOrdersItemsItemRightTop}>
            <div className={classes.UserOrdersItemsItemRightTopTitle}>
              <div className={classes.UserOrdersItemsItemRightTopTitleName}>
                Apple Airpods Pro
              </div>
              <div className={classes.UserOrdersItemsItemRightTopTitleCount}>
                1 ədəd
              </div>
            </div>
            <div className={classes.UserOrdersItemsItemRightTopDate}>
              29 Avq 2021
            </div>
          </div>
          <div className={classes.UserOrdersItemsItemRightBottom}>
            <div className={classes.UserOrdersItemsItemRightBottomPrice}>
              499 ₼
            </div>
            <div className={classes.UserOrdersItemsItemRightBottomOperations}>
              <li>Təsdiqlənib</li>
            </div>
          </div>
        </div>
      </div>

      <div className={classes.UserOrdersItemsItem}>
        <div className={classes.UserOrdersItemsItemLeft}>
          <img src="/img/airpod.png" alt="" />
        </div>
        <div className={classes.UserOrdersItemsItemRight}>
          <div className={classes.UserOrdersItemsItemRightTop}>
            <div className={classes.UserOrdersItemsItemRightTopTitle}>
              <div className={classes.UserOrdersItemsItemRightTopTitleName}>
                Apple Airpods Pro
              </div>
              <div className={classes.UserOrdersItemsItemRightTopTitleCount}>
                1 ədəd
              </div>
            </div>
            <div className={classes.UserOrdersItemsItemRightTopDate}>
              29 Avq 2021
            </div>
          </div>
          <div className={classes.UserOrdersItemsItemRightBottom}>
            <div className={classes.UserOrdersItemsItemRightBottomPrice}>
              499 ₼
            </div>
            <div className={classes.UserOrdersItemsItemRightBottomOperations}>
              <li>Təsdiqlənib</li>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default UserOrdersItems;
