import * as React from "react";
import MenuItem from "@mui/material/MenuItem";
import Select from "@mui/material/Select";
import { AngleDown } from "../Icons";

export default function BasicSelect() {
  const [age, setAge] = React.useState("10");

  const handleChange = (event) => {
    setAge(event.target.value);
  };

  return (
    <Select
      value={age}
      onChange={handleChange}
      IconComponent={() => <AngleDown />}
      label={false}
      labelId="demo-simple-select-label-ASLAN"
      id="demo-simple-select"
      style={{ paddingInline: "10px", fontSize: "12px", fontFamily: "Poppins" }}
    >
      <MenuItem value={10}>Ten</MenuItem>
      <MenuItem value={20}>Twenty</MenuItem>
      <MenuItem value={30}>Thirty</MenuItem>
    </Select>
  );
}
