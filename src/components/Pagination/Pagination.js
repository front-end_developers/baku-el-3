import * as React from "react";
import Pagination from "@mui/material/Pagination";
import PaginationItem from "@mui/material/PaginationItem";
import Stack from "@mui/material/Stack";
import ArrowBackIcon from "@mui/icons-material/ArrowBack";
import ArrowForwardIcon from "@mui/icons-material/ArrowForward";
import ArrowLeft from "../Icons/AngleLeft";
import ArrowRight from "../Icons/AngleRight";
import { makeStyles } from "@mui/styles";

const useStyles = makeStyles({
  Pagination: {
    // position: "absolute",
    // bottom: 0,
    // "& .MuiPagination-root": {
    //   "& .MuiPagination-ul": {
    //     "& li": {
    //       "& .MuiButtonBase-root": {
    //         background: "rgba(234, 36, 39, 0.1) !important",
    //         margin: "unset",
    //         padding: "unset",
    //         fontWeight: "700",
    //         fontSize: "12px",
    //         lineHeight: "13px",
    //         borderRadius: "unset",
    //         "& svg": {
    //           width: "6px",
    //           height: "9px",
    //           color: "#EA2427",
    //         },
    //         "&:hover": {
    //           background: "rgba(234, 36, 39)",
    //           color: "#fff",
    //           "& svg": {
    //             color: "#fff",
    //           },
    //         },
    //       },
    //       "& .Mui-selected": {
    //         color: "#EA2427",
    //         "&:hover": {
    //           color: "#fff",
    //         },
    //       },
    //       "&:first-child": {
    //         "& button": {
    //           margin: "unset",
    //           marginRight: "10px",
    //           padding: "unset",
    //           borderRadius: "12px",
    //         },
    //       },
    //       "&:last-child": {
    //         "& button": {
    //           margin: "unset",
    //           marginLeft: "10px",
    //           padding: "unset",
    //           borderRadius: "12px",
    //         },
    //       },
    //       "&:nth-child(2)": {
    //         "& button": {
    //           borderRadius: "12px 0 0 12px",
    //         },
    //       },
    //       "&:nth-last-child(2)": {
    //         "& button": {
    //           borderRadius: "0 12px 12px 0",
    //         },
    //       },
    //     },
    //   },
    // },
  },
});

export default function CustomPagination(props) {
  const { page,totalBrendPages, setPage, handleChange } = props;
  // console.log("page",page)
  // console.log("setPage",setPage)
  // console.log("handleChange",onChange)
  // const classes = useStyles();
  return (
    <Stack spacing={2} 
    sx={{
      position: "absolute",
      bottom: "0"
    }}>
      <Pagination
        count={totalBrendPages}
        page={page}
        onChange={handleChange}
        renderItem={(item) => (
          <PaginationItem
            components={{ previous: ArrowLeft, next: ArrowRight }}
            {...item}
          />
        )}
        // className={classes.Pagination}
      />
    </Stack>
  );
}
