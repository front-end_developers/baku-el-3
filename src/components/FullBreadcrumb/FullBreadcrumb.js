import Link from "next/link";
import classes from "./FullBreadcrumb.module.scss";
import { DoubleArrow } from "../Icons";

const FullBreadcrumb = ({ steps }) => {
  console.log("steps", steps)
  return (
    <div className={classes.Breadcrumb}>
      <div className={classes.BreadcrumbTitle}>
        {steps.map((step, idx) => (
          <span key={idx}>
            <span>
              <Link href={`${step.slug}`}>{step.title}</Link>
            </span>
            {idx !== steps.length - 1 ? (
              <span>
                <DoubleArrow className={classes.doubleArrow} />
              </span>
            ) : null}
          </span>
        ))}
      </div>
    </div>
  );
};

export default FullBreadcrumb;
