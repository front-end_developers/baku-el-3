import {
  styled,
  Table,
  TableBody,
  TableCell,
  TableHead,
  TableRow,
  IconButton,
  TableContainer,
} from "@mui/material";

import {
  ChatGray,
  Call,
  Delete,
  DetailsIcon,
  Manat,
} from "../../../src/components/Icons";

function createData(
  imgPath,
  product,
  quantity,
  price,
  orderDate,
  status,
  operations
) {
  return { imgPath, product, quantity, price, orderDate, status, operations };
}
const rows = [
  createData(
    "/img/user-bonuses-table1.png",
    "İphone 12 Pro Max",
    "1 ədəd",
    "3499",
    "29 Avq 2021",
    "İmtina",
    "axirinci sira"
  ),
  createData(
    "/img/user-bonuses-table1.png",
    "İphone 12 Pro Max",
    "1 ədəd",
    "3499",
    "29 Avq 2021",
    "İmtina",
    "axirinci sira"
  ),
  createData(
    "/img/user-bonuses-table1.png",
    "İphone 12 Pro Max",
    "1 ədəd",
    "3499",
    "29 Avq 2021",
    "İmtina",
    "axirinci sira"
  ),
  createData(
    "/img/user-bonuses-table1.png",
    "İphone 12 Pro Max",
    "1 ədəd",
    "3499",
    "29 Avq 2021",
    "İmtina",
    "axirinci sira"
  ),
];

const StyledTableHead = styled(TableHead)(({ theme }) => ({
  backgroundColor: "transparent",
  display: "table-header-group",
  fontSize: "10px",
  paddingBottom: "5px",
  // borderBottom: "2px solid var(--user-tab-btn-bg)",
  borderBottom: "1px solid rgba(224, 224, 224, 1)",
  tetxAlign: "left",
  opacity: "0.5",
}));

const StyledTableRow = styled(TableRow)(({ theme }) => ({
  "&:nth-of-type(even)": {
    backgroundColor: "var(--user-table-row-bg)",
  },
  "&:nth-of-type(odd)": {
    backgroundColor: "transparent",
  },
  "&:last-child": {
    borderBottom: "1px solid rgba(224, 224, 224, 1)",
  },
}));

const StyledTableCell = styled(TableCell)(({ theme }) => ({
  color: "var(--text-primary)",
  border: "none",
  padding: "12px",
  textTransform: "inherit",
  fontFamily: "Poppins",
  "&.MuiTableCell-head": {
    padding: "5px 12px",
  },
  "&:first-child:not(.MuiTableCell-head)": {
    fontWeight: "600",
    fontSize: "14px",
    "& img": {
      height: "40px",
      width: "40px",
      marginRight: "16px",
    },
  },
  "&.price": {
    "& svg": {
      width: "12px",
      paddingBottom: "2px",
    },
  },
  "& .status-indicator": {
    height: "5px",
    width: "5px",
    borderRadius: "50%",
    backgroundColor: "red",
    display: "inline-block",
    verticalAlign: "middle",
    marginRight: "5px",
  },
  "&.operations": {
    "& svg": {
      height: "16px",
      width: "16px",
      color: "var(--user-table-operations-btn)",
    },
    "& button:hover": {
      "& svg": {
        color: "var(--red-main)",
      },
    },
  },
}));

export default function UserOrdersTable() {
  return (
    <TableContainer
      sx={{
        width: "100%",
        // position: "absolute",
        // left: 0,
        paddingTop: "10px",

        ["@media (max-width:1200px)"]: {
          marginBottom: "60px",
        },
      }}
    >
      <Table
        aria-label="customized table"
        sx={{
          minWidth: "max-content",
        }}
      >
        <StyledTableHead>
          <TableRow>
            <StyledTableCell>Məhsul</StyledTableCell>
            <StyledTableCell>Miqdar</StyledTableCell>
            <StyledTableCell>Qiymet</StyledTableCell>
            <StyledTableCell>Sifariş tarixi</StyledTableCell>
            <StyledTableCell>Statusu</StyledTableCell>
            <StyledTableCell>Əməliyyatlar</StyledTableCell>
          </TableRow>
        </StyledTableHead>
        <TableBody>
          {rows.map((row, idx) => (
            <StyledTableRow key={row.idx}>
              <StyledTableCell component="th" scope="row">
                <img src={row.imgPath} />
                {row.product}
              </StyledTableCell>
              <StyledTableCell>{row.quantity}</StyledTableCell>
              <StyledTableCell className="price">
                {row.price} <Manat />
              </StyledTableCell>
              <StyledTableCell>{row.orderDate}</StyledTableCell>
              <StyledTableCell>
                <div className="status-indicator"></div>
                {row.status}
              </StyledTableCell>
              <StyledTableCell className="operations">
                <IconButton>
                  <ChatGray />
                </IconButton>
                <IconButton>
                  <Call />
                </IconButton>
                <IconButton>
                  <Delete />
                </IconButton>
                <IconButton>
                  <DetailsIcon />
                </IconButton>
              </StyledTableCell>
            </StyledTableRow>
          ))}
        </TableBody>
      </Table>
    </TableContainer>
  );
}
