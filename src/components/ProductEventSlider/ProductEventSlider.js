import { useRef } from "react";
import classes from "./ProductEventSlider.module.scss";
import ProductCard from "../ProductCard/ProductCard";
import { styled } from "@mui/material/styles";
import Slider from "react-slick";
import { MdWest, MdEast } from "react-icons/md";

const PrevBtn = styled("button")({
  display: "flex",
  justifyContent: "center",
  alignItems: "center",
  width: "50px",
  height: "50px",
  background: "#EDEDED",
  border: "none",
  borderRadius: "16px",

  "@media(max-width: 768px)": {
    width: "30px",
    height: "30px",
    borderRadius: "10px",
  },
});

const NextBtn = styled("button")({
  display: "flex",
  justifyContent: "center",
  alignItems: "center",
  width: "50px",
  height: "50px",
  background: "#333333",
  color: "white",
  border: "none",
  borderRadius: "16px",

  "@media(max-width: 768px)": {
    width: "30px",
    height: "30px",
    borderRadius: "10px",
  },
});

const ProductEventSlider = () => {
  const customeSlider1 = useRef();

  const gotoPrev1 = () => {
    customeSlider1.current.slickPrev();
  };

  const gotoNext1 = () => {
    customeSlider1.current.slickNext();
  };

  const customeSlider2 = useRef();

  const gotoPrev2 = () => {
    customeSlider2.current.slickPrev();
  };

  const gotoNext2 = () => {
    customeSlider2.current.slickNext();
  };

  const settings = {
    dots: false,
    infinite: false,
    speed: 500,
    slidesToShow: 5,
    slidesToScroll: 5,
    responsive: [
      {
        breakpoint: 1200,
        settings: {
          slidesToShow: 4,
          slidesToScroll: 4,
          infinite: true,
          dots: false,
        },
      },
      {
        breakpoint: 1000,
        settings: {
          slidesToShow: 3,
          slidesToScroll: 3,
          infinite: true,
          dots: false,
        },
      },
      {
        breakpoint: 768,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 2,
          infinite: true,
          dots: false,
        },
      },
      {
        breakpoint: 475,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 2,
          infinite: true,
          dots: false,
        },
      },
    ],
  };

  return (
    <div className={classes.SectionTwo}>
      <div className={classes.SectionTwoTitle}>
        <div className={classes.SectionTwoTitleText}>
          <div className={classes.SectionTwoTitleTextTop}>Özəl təkliflər</div>
          <div className={classes.SectionTwoTitleTextBottom}>
            Black Friday Endirimindən yararlan
          </div>
        </div>

        <div className={classes.SectionTwoTitleActions}>
          <PrevBtn onClick={gotoPrev1}>
            <MdWest />
          </PrevBtn>
          <NextBtn onClick={gotoNext1}>
            <MdEast />
          </NextBtn>
        </div>
      </div>

      <div className={classes.SectionTwoProducts}>
        <Slider
          {...settings}
          ref={customeSlider1}
          className={classes.SectionTwoProductsFirstSlider}
        >
          <div className={classes.SectionTwoProductsFirstSliderItem}>
            <ProductCard />
          </div>
          <div className={classes.SectionTwoProductsFirstSliderItem}>
            <ProductCard />
          </div>
          <div className={classes.SectionTwoProductsFirstSliderItem}>
            <ProductCard />
          </div>
          <div className={classes.SectionTwoProductsFirstSliderItem}>
            <ProductCard />
          </div>
          <div className={classes.SectionTwoProductsFirstSliderItem}>
            <ProductCard />
          </div>
          <div className={classes.SectionTwoProductsFirstSliderItem}>
            <ProductCard />
          </div>
          <div className={classes.SectionTwoProductsFirstSliderItem}>
            <ProductCard />
          </div>
          <div className={classes.SectionTwoProductsFirstSliderItem}>
            <ProductCard />
          </div>
        </Slider>
      </div>
    </div>
  );
};

export default ProductEventSlider;
