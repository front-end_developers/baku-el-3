/* eslint-disable @next/next/no-img-element */
import React from "react";
import classes from "./Footer.module.scss";
import {
  FacebookLogo,
  TelegramLogo,
  LinkedinLogo,
  LogoInstagram,
  WhatssappLogo,
} from "../Icons";
import Link from "next/link";

const Footer = () => {
  return (
    <div className={classes.Footer}>
      <div className={classes.FooterTop}>
        {/* content */}

        <div className={classes.FooterTopInfo}>
          {/* email form & social media links & payment methods*/}
          <div className={classes.FooterEmailGroup}>
            {/* email form */}
            <span className={classes.FooterEmailGroupTitle}>
              Heç bir yeniliyi qaçırma!
            </span>
            <form className={classes.FooterEmailGroupForm}>
              <input
                placeholder="E-poçt"
                type="email"
                className={classes.FooterEmailGroupFormInput}
              />
              <input
                type="submit"
                className={classes.FooterEmailGroupFormSubmit}
                value="Abunə ol"
              />
            </form>
          </div>
          <div className={classes.FooterTopInfoOthers}>
            {/* social media links & payement methods */}
            <div className={classes.FooterSocialMedia}>
              {/* social media links */}
              <span className={classes.FooterSocialMediaTitle}>
                Bizi sosial mediada izləyin:
              </span>
              <div className={classes.FooterSocialMediaBody}>
                <a className={classes.FooterSocialMediaLink} href="">
                  <FacebookLogo />
                </a>
                <a className={classes.FooterSocialMediaLink} href="">
                  <TelegramLogo />
                </a>
                <a className={classes.FooterSocialMediaLink} href="">
                  <LinkedinLogo />
                </a>
                <a className={classes.FooterSocialMediaLink} href="">
                  <LogoInstagram />
                </a>
                <a className={classes.FooterSocialMediaLink} href="">
                  <WhatssappLogo />
                </a>
              </div>
            </div>
            <div className={classes.FooterPayementMethods}>
              {/* payement methods */}

              <span className={classes.FooterPayementMethodsTitle}>
                Ödəmə üsulları:
              </span>

              <div className={classes.FooterPayementMethodsBody}>
                <img
                  className={classes.FooterPayementImage}
                  src="../../img/mastercard.png"
                  alt="mastercard"
                  title="mastercard"
                />
                <img
                  className={classes.FooterPayementImage}
                  src="../../img/visa.png"
                  alt="visa"
                  title="visa"
                />
              </div>
            </div>
          </div>
        </div>

        <div className={classes.FooterTopMenu}>
          {/* links */}
          <div className={classes.FooterTopMenuGroup}>
            <div className={classes.FooterTopMenuGroupTitle}>
              <span>Alıcılara</span>
            </div>
            <div className={classes.FooterTopMenuGroupLinks}>
              <a>Onlayn Ödəmə Sistemi</a>
              <a>Çatdırılma və ödəniş</a>
              <a>Servis mərkəzləri</a>
              <a>Nisyə alış</a>
              <a>Hədiyyə kartları</a>
            </div>
          </div>
          <div className={classes.FooterTopMenuGroup}>
            <div className={classes.FooterTopMenuGroupTitle}>
              <span>Məlumat</span>
            </div>
            <div className={classes.FooterTopMenuGroupLinks}>
              <a>Bonus-müştəri kart</a>
              <a>Brendlər</a>
              <a>Bloq</a>
            </div>
          </div>
          <div className={classes.FooterTopMenuGroup}>
            <div className={classes.FooterTopMenuGroupTitle}>
              <span>Şirkət</span>
            </div>
            <div className={classes.FooterTopMenuGroupLinks}>
              <a>Korporativ sayt</a>
              <a>Mağazalar</a>
            </div>
          </div>
        </div>
      </div>

      <div className={classes.FooterBottom}>
        <hr />
        <div>
          <div>
            Copyright © 2022 <b>Baku Electronics.</b> Bütün hüquqlar qorunur.
          </div>
          <div className={classes.FooterBottomCompany}>
            Designed & Developed by{" "}
            <b>
              <Link href="https://crocusoft.com/">Crocusoft LLC</Link>
            </b>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Footer;
