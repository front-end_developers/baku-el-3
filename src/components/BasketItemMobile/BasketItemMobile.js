import classes from "./BasketItemMobile.module.scss";
import { Manat } from "../Icons";

const BasketItemMobile = () => {
  return (
    <div className={classes.BasketItemMobile}>
      <div className={classes.BasketItemLeft}>
        <img src="/img/airpod.png" />
      </div>
      <div className={classes.BasketItemRight}>
        <div className={classes.BasketItemRightTitle}>
          Apple Iphone 13 Pro Max, 256GB, Blue
        </div>
        <div className={classes.BasketItemRightBody}>
          <div className={classes.BasketItemRightBodyPrices}>
            <span className={classes.Discount}>
              699 <Manat />
            </span>
            <span className={classes.PriceWrapper}>
              <span className={classes.Price}>
                499 <Manat />
              </span>
              <span className={classes.Separator}></span>
              <span className={classes.Monthly}>
                90 <Manat /> / 15 ay
              </span>
            </span>
          </div>
          <div className={classes.BasketItemRightBodyButtons}>
            <button className={classes.Button}>-</button>
            <input value="99" />
            <button className={classes.Button}>+</button>
          </div>
        </div>
      </div>
    </div>
  );
};

export default BasketItemMobile;
