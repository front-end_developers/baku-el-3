import * as React from "react";

function SvgCalendar(props) {
  return (
    <svg
      width="1em"
      height="1em"
      viewBox="0 0 18 18"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
      {...props}
    >
      <g
        stroke="currentColor"
        strokeWidth={1.5}
        strokeLinecap="round"
        strokeLinejoin="round"
      >
        <path d="M2.32 7.054h13.367M12.331 9.982h.007M9.003 9.982h.007M5.67 9.982h.006M12.331 12.898h.007M9.003 12.898h.007M5.67 12.898h.006M12.032 1.5v2.468M5.973 1.5v2.468" />
        <path
          clipRule="evenodd"
          d="M12.179 2.684h-6.35c-2.203 0-3.579 1.227-3.579 3.482v6.787c0 2.29 1.376 3.546 3.578 3.546h6.344c2.21 0 3.578-1.234 3.578-3.49V6.167c.007-2.255-1.362-3.482-3.571-3.482z"
        />
      </g>
    </svg>
  );
}

export default SvgCalendar;
