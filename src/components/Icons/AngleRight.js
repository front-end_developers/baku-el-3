import * as React from "react";

function SvgAngleRight(props) {
  return (
    <svg
      width="1em"
      height="1em"
      viewBox="0 0 6 9"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
      {...props}
    >
      <path
        fillRule="evenodd"
        clipRule="evenodd"
        d="M.935 8.349a1 1 0 01-.03-1.414L3.241 4.5.905 2.065A1 1 0 112.35.68l3 3.127a1 1 0 010 1.385l-3 3.127a1 1 0 01-1.414.03z"
        fill="currentColor"
      />
    </svg>
  );
}

export default SvgAngleRight;
