import * as React from "react";

function SvgPaper(props) {
  return (
    <svg
      width="1em"
      height="1em"
      viewBox="0 0 12 15"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
      {...props}
    >
      <path
        clipRule="evenodd"
        d="M7.824 1.342H3.389A2.547 2.547 0 00.833 3.828v7.142a2.552 2.552 0 002.494 2.607h5.388A2.591 2.591 0 0011.2 10.97V4.86L7.824 1.341z"
        stroke="currentColor"
        strokeWidth={1.5}
        strokeLinecap="round"
        strokeLinejoin="round"
      />
      <path
        d="M7.648 1.334v1.94c0 .946.766 1.713 1.712 1.716h1.837"
        stroke="#EA2427npm"
        strokeWidth={1.5}
        strokeLinecap="round"
        strokeLinejoin="round"
      />
      <path
        d="M7.526 9.74h-3.6M6.16 7.238H3.921"
        stroke="currentColor"
        strokeWidth={1.5}
        strokeLinecap="round"
        strokeLinejoin="round"
      />
    </svg>
  );
}

export default SvgPaper;
