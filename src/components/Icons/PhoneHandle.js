import * as React from "react";

function SvgPhoneHandle(props) {
  return (
    <svg
      width="1em"
      height="1em"
      viewBox="0 0 16 16"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
      {...props}
    >
      <path
        clipRule="evenodd"
        d="M8.351 8.354c-2.992 2.991-3.67-.469-5.575 1.435-1.837 1.836-2.892 2.203-.565 4.53.291.234 2.143 3.052 8.65-3.454 6.51-6.507 3.693-8.36 3.458-8.652-2.332-2.333-2.693-1.271-4.53.565-1.905 1.904 1.554 2.585-1.438 5.576z"
        stroke="currentColor"
        strokeWidth={1.5}
        strokeLinecap="round"
        strokeLinejoin="round"
      />
    </svg>
  );
}

export default SvgPhoneHandle;
