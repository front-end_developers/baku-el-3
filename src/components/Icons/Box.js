import * as React from "react";

function SvgBox(props) {
  return (
    <svg
      width="1em"
      height="1em"
      viewBox="0 0 24 24"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
      {...props}
    >
      <path
        d="M12.383 3.033l7.982 3.621-3.39 1.234a.366.366 0 00-.072-.043L9.34 4.413l3.044-1.38zm-3.96 1.795l.043.019.038.017.042.02c1.303.59 6.05 2.746 7.423 3.368l-3.604 1.311L4.38 6.66l4.044-1.832zm12.588 2.398v10.361l-8.265 3.936V10.23l8.265-3.005zm-17.27.008l8.258 3.005v11.284L3.74 17.59V7.234z"
        fill="currentColor"
      />
    </svg>
  );
}

export default SvgBox;
