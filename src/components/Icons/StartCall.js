import * as React from "react";

function SvgStartCall(props) {
  return (
    <svg
      width="1em"
      height="1em"
      viewBox="0 0 24 23"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
      {...props}
    >
      <path
        d="M14.617 1.112a8.855 8.855 0 017.822 7.814M14.617 5.048a4.918 4.918 0 013.89 3.889"
        stroke="currentColor"
        strokeWidth={1.5}
        strokeLinecap="round"
        strokeLinejoin="round"
      />
      <path
        clipRule="evenodd"
        d="M10.929 12.193c4.432 4.43 5.437-.695 8.26 2.125 2.72 2.72 4.285 3.264.837 6.71-.432.348-3.175 4.522-12.816-5.116-9.642-9.64-5.47-12.386-5.123-12.818C5.542-.36 6.079 1.212 8.8 3.932c2.82 2.82-2.304 3.83 2.129 8.26z"
        stroke="currentColor"
        strokeWidth={1.5}
        strokeLinecap="round"
        strokeLinejoin="round"
      />
    </svg>
  );
}

export default SvgStartCall;
