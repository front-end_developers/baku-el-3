import * as React from "react";

function SvgArrowLeft(props) {
  return (
    <svg
      width="1em"
      height="1em"
      viewBox="0 0 17 14"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
      {...props}
    >
      <path
        d="M.973 6.884H15.73M6.925 12.811L.973 6.884 6.925.956"
        stroke="currentColor"
        strokeWidth={1.5}
        strokeLinecap="round"
        strokeLinejoin="round"
      />
    </svg>
  );
}

export default SvgArrowLeft;
