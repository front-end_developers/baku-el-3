import * as React from "react";

function SvgBeauty(props) {
  return (
    <svg
      width="1em"
      height="1em"
      viewBox="0 0 20 17"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
      {...props}
    >
      <path
        d="M12.647 5.199l3.062-.58-1.423 2.78 1.489 2.747-3.078-.496-2.134 2.266-.497-3.077-2.813-1.34 2.78-1.423.414-3.078 2.2 2.2zM10.066 8.855L.833 15.871M7.535 4.52L5.73 3.46M13.409 2.535L13.92.5M17.065 7.465h2.102M13.458 12.266l.596 2.002"
        stroke="currentColor"
        strokeMiterlimit={10}
        strokeLinecap="round"
        strokeLinejoin="round"
      />
    </svg>
  );
}

export default SvgBeauty;
