import * as React from "react";

function SvgMenuIcon(props) {
  return (
    <svg
      width="1em"
      height="1em"
      viewBox="0 0 16 7"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
      {...props}
    >
      <path
        stroke="currentColor"
        strokeWidth={2}
        strokeLinecap="round"
        d="M1 1h14M1 6h14"
      />
    </svg>
  );
}

export default SvgMenuIcon;
