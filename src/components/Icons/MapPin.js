import * as React from "react";

function SvgMapPin(props) {
  return (
    <svg
      width="1em"
      height="1em"
      viewBox="0 0 36 40"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
      {...props}
    >
      <path
        d="M16 19.799v10.2a2 2 0 004 0V19.8a10 10 0 10-4 0zm2-15.8a6 6 0 110 11.999 6 6 0 010-12zm8.42 20.84a2.046 2.046 0 00-2.429 2.378 2.044 2.044 0 001.589 1.622c4.54.9 6.42 2.52 6.42 3.16 0 1.16-4.9 4-14 4S4 33.16 4 32c0-.64 1.88-2.26 6.42-3.24a2.042 2.042 0 001.267-3.153 2.044 2.044 0 00-2.107-.847C3.5 26.16 0 28.78 0 32 0 37.26 9.06 40 18 40s18-2.74 18-8c0-3.22-3.5-5.84-9.58-7.16z"
        fill="red"
      />
    </svg>
  );
}

export default SvgMapPin;
