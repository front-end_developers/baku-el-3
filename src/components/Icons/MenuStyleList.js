import * as React from "react";

function SvgMenuStyleList(props) {
  return (
    <svg
      width="1em"
      height="1em"
      viewBox="0 0 16 16"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
      {...props}
    >
      <g
        stroke="currentColor"
        strokeWidth={1.5}
        strokeLinecap="round"
        strokeLinejoin="round"
      >
        <path d="M10.477 10.816H5.664M10.477 8.024H5.664M7.5 5.24H5.665" />
        <path
          clipRule="evenodd"
          d="M10.605 1.833l-5.126.003C3.64 1.847 2.5 3.058 2.5 4.904v6.131c0 1.856 1.148 3.071 3.004 3.071l5.126-.002c1.84-.011 2.98-1.222 2.98-3.069v-6.13c0-1.857-1.149-3.072-3.005-3.072z"
        />
      </g>
    </svg>
  );
}

export default SvgMenuStyleList;
