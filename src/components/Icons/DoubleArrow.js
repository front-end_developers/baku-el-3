import * as React from "react";

function SvgDoubleArrow(props) {
  return (
    <svg
      width="1em"
      height="1em"
      viewBox="0 0 10 10"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
      {...props}
    >
      <path
        d="M5.274 9.006l-.943-.942 3.067-3.067L4.33 1.93l.943-.938L9.281 5 5.275 9.006zm-3.616 0l-.943-.942 3.067-3.067L.715 1.935l.943-.943L5.665 5 1.658 9.006z"
        fill="currentColor"
      />
    </svg>
  );
}

export default SvgDoubleArrow;
