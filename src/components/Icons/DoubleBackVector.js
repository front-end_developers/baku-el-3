import * as React from "react";

function SvgDoubleBackVector(props) {
  return (
    <svg
      width="1em"
      height="1em"
      viewBox="0 0 14 14"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
      {...props}
    >
      <path
        d="M6.585 13.01L.575 7 6.585.99 8 2.404l-4.6 4.6 4.6 4.6-1.414 1.406h-.001zm5.425 0L5.999 7 12.01.99l1.414 1.414-4.6 4.6 4.6 4.6-1.413 1.406h-.001z"
        fill="currentColor"
      />
    </svg>
  );
}

export default SvgDoubleBackVector;
