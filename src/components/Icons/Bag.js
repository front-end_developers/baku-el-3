import * as React from "react";

function SvgBag(props) {
  return (
    <svg
      width="1em"
      height="1em"
      viewBox="0 0 21 22"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
      {...props}
    >
      <path
        clipRule="evenodd"
        d="M14.514 20.5H6.166c-3.066 0-5.419-1.107-4.75-5.565l.777-6.041C2.605 6.67 4.024 5.818 5.27 5.818h10.178c1.264 0 2.6.916 3.076 3.076l.778 6.041c.567 3.954-1.72 5.565-4.787 5.565z"
        stroke="currentColor"
        strokeWidth={1.5}
        strokeLinecap="round"
        strokeLinejoin="round"
      />
      <path
        d="M14.651 5.598a4.32 4.32 0 00-4.32-4.32v0a4.32 4.32 0 00-4.339 4.32h0M13.296 10.1h-.045M7.466 10.1H7.42"
        stroke="currentColor"
        strokeWidth={1.5}
        strokeLinecap="round"
        strokeLinejoin="round"
      />
    </svg>
  );
}

export default SvgBag;
