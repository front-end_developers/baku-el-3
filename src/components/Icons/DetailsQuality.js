import * as React from "react";

function SvgDetailsQuality(props) {
  return (
    <svg
      width="1em"
      height="1em"
      viewBox="0 0 24 24"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
      {...props}
    >
      <path
        fillRule="evenodd"
        clipRule="evenodd"
        d="M19.016 5.203a8.437 8.437 0 11-14.03 9.375 8.437 8.437 0 0114.03-9.375zm-2.797 7.599l.703-4.922h-.014a.704.704 0 00-1.013-.703l-2.257 1.125-1.061-1.59a.703.703 0 00-1.168 0l-1.061 1.59L8.09 7.177a.703.703 0 00-1.013.703l.703 4.922a.703.703 0 00.704.604h7.03a.704.704 0 00.704-.604zM1.657 17.864l2.306-2.306a9.886 9.886 0 005.225 3.755l-3.017 3.03a.703.703 0 01-.499.204.703.703 0 01-.703-.52l-.654-2.32-2.341-.64a.704.704 0 01-.5-.5.703.703 0 01.183-.703zm18.38-2.306l2.306 2.306v-.014a.703.703 0 01-.316 1.181l-2.37.675-.64 2.342a.703.703 0 01-.499.5.81.81 0 01-.183 0 .701.701 0 01-.499-.205l-3.023-3.03a9.886 9.886 0 005.224-3.755zM12 8.344l.823 1.23a.703.703 0 00.9.24l1.61-.802L14.904 12H9.096l-.429-2.988 1.61.802a.703.703 0 00.9-.24L12 8.344z"
        fill="url(#detailsQuality_svg__paint0_linear_1983_44152)"
      />
      <defs>
        <linearGradient
          id="detailsQuality_svg__paint0_linear_1983_44152"
          x1={2}
          y1={22}
          x2={22}
          y2={1.375}
          gradientUnits="userSpaceOnUse"
        >
          <stop stopColor="#F4DD4B" />
          <stop offset={0.234} stopColor="#A38F0A" />
          <stop offset={0.773} stopColor="#E3D577" />
          <stop offset={1} stopColor="#F4DD4B" />
        </linearGradient>
      </defs>
    </svg>
  );
}

export default SvgDetailsQuality;
