import * as React from "react";

function SvgChat(props) {
  return (
    <svg
      width="1em"
      height="1em"
      viewBox="0 0 16 16"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
      {...props}
    >
      <path
        fillRule="evenodd"
        clipRule="evenodd"
        d="M12.714 12.713a6.67 6.67 0 01-7.523 1.336c-.365-.147-.663-.265-.947-.265-.792.004-1.777.772-2.289.26-.511-.512.256-1.497.256-2.293 0-.284-.113-.578-.26-.943a6.67 6.67 0 0110.763-7.524 6.668 6.668 0 010 9.429z"
        fill="#ED96A4"
        stroke="#ED96A4"
        strokeWidth={1.5}
        strokeLinecap="round"
        strokeLinejoin="round"
      />
      <path
        d="M10.626 8.275h.006M7.954 8.275h.006M5.28 8.275h.007"
        stroke="currentColor"
        strokeWidth={2}
        strokeLinecap="round"
        strokeLinejoin="round"
      />
    </svg>
  );
}

export default SvgChat;
