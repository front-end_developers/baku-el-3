import * as React from "react";

function SvgClipboardIcon(props) {
  return (
    <svg
      width="1em"
      height="1em"
      viewBox="0 0 14 14"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
      {...props}
    >
      <path
        d="M2.258.332a1.938 1.938 0 00-1.924 1.93v9.478c0 1.056.868 1.93 1.924 1.93h9.484a1.938 1.938 0 001.924-1.93V2.262c0-1.056-.868-1.93-1.925-1.93H2.258zm0 1.335H5v4.669a.667.667 0 001.137.467l.867-.86.86.86a.667.667 0 001.138-.467v-4.67h2.74c.336 0 .592.26.592.596v9.478a.584.584 0 01-.591.596H2.258a.583.583 0 01-.59-.596V2.262c0-.336.255-.595.59-.595zm4.075 0h1.334v3.06l-.196-.197a.667.667 0 00-.94 0l-.198.197v-3.06z"
        fill="currentColor"
      />
    </svg>
  );
}

export default SvgClipboardIcon;
