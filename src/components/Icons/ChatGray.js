import * as React from "react";

function SvgChatGray(props) {
  return (
    <svg
      width="1em"
      height="1em"
      viewBox="0 0 20 20"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
      {...props}
    >
      <g stroke="currentColor" strokeLinecap="round" strokeLinejoin="round">
        <path
          clipRule="evenodd"
          d="M15.893 15.89a8.338 8.338 0 01-9.404 1.671c-.456-.183-.83-.332-1.184-.332-.99.006-2.22.965-2.86.326-.64-.64.32-1.872.32-2.867 0-.355-.143-.722-.326-1.178a8.338 8.338 0 011.67-9.405c3.25-3.252 8.533-3.252 11.784 0a8.335 8.335 0 010 11.786z"
          strokeWidth={1.5}
        />
        <path
          d="M13.283 10.344h.008M9.942 10.344h.007M6.601 10.344h.008"
          strokeWidth={2}
        />
      </g>
    </svg>
  );
}

export default SvgChatGray;
