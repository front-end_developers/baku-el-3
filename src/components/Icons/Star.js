import * as React from "react";

function SvgStar(props) {
  return (
    <svg
      width="1em"
      height="1em"
      viewBox="0 0 14 14"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
      {...props}
    >
      <path
        fillRule="evenodd"
        clipRule="evenodd"
        d="M7.736 1.784l1.218 2.434c.12.24.35.405.618.443l2.725.392c.673.098.941.914.454 1.381l-1.97 1.894a.802.802 0 00-.237.717l.465 2.673c.115.662-.59 1.166-1.191.853l-2.436-1.263a.832.832 0 00-.764 0l-2.436 1.263c-.602.313-1.306-.191-1.19-.853l.464-2.673a.802.802 0 00-.236-.717L1.25 6.434c-.488-.467-.22-1.283.453-1.38l2.725-.393a.82.82 0 00.618-.443l1.218-2.434a.826.826 0 011.472 0z"
        fill="#F4DD4B"
        stroke="#F4DD4B"
        strokeWidth={1.5}
        strokeLinecap="round"
        strokeLinejoin="round"
      />
    </svg>
  );
}

export default SvgStar;
