import * as React from "react";

function SvgBicycle(props) {
  return (
    <svg
      width="1em"
      height="1em"
      viewBox="0 0 20 12"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
      {...props}
    >
      <path
        d="M5.948 5.898c.418.494.675 1.148.675 1.855a2.896 2.896 0 01-2.895 2.895A2.896 2.896 0 01.833 7.753a2.896 2.896 0 012.895-2.895M16.272 4.858a2.896 2.896 0 012.895 2.895 2.896 2.896 0 01-2.895 2.895 2.896 2.896 0 01-2.895-2.895c0-.493.118-.954.343-1.361"
        stroke="currentColor"
        strokeMiterlimit={10}
        strokeLinecap="round"
        strokeLinejoin="round"
      />
      <path
        d="M3.728 7.754L6.623 1h.965"
        stroke="currentColor"
        strokeMiterlimit={10}
        strokeLinecap="round"
        strokeLinejoin="round"
      />
      <path
        d="M9.517 7.753h6.755L13.377 2.93h-7.58l3.72 4.824zM13.377 2.929L10.504 6.52M12.412 1.695h1.962"
        stroke="currentColor"
        strokeMiterlimit={10}
        strokeLinecap="round"
        strokeLinejoin="round"
      />
    </svg>
  );
}

export default SvgBicycle;
