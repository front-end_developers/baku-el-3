import * as React from "react";

function SvgMoneyCaseLogo(props) {
  return (
    <svg
      width="1em"
      height="1em"
      viewBox="0 0 27 26"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
      {...props}
    >
      <path
        d="M22.664 7.583h-3.25V5.417a2.07 2.07 0 00-1.95-2.167h-6.933a2.07 2.07 0 00-1.95 2.167v2.166H5.33A2.167 2.167 0 003.164 9.75v10.833a2.167 2.167 0 002.167 2.167h17.333a2.167 2.167 0 002.167-2.167V9.75a2.166 2.166 0 00-2.167-2.167zM10.747 5.417h6.5v2.166h-6.5V5.417zm11.917 15.166H5.331V9.75h17.333v10.833z"
        fill="currentColor"
      />
      <path
        d="M6.414 13a1.083 1.083 0 001.083 1.083h13a1.083 1.083 0 000-2.167h-13a1.084 1.084 0 00-1.083 1.083zm14.083 3.25h-13a1.084 1.084 0 000 2.166h13a1.083 1.083 0 100-2.167z"
        fill="currentColor"
      />
    </svg>
  );
}

export default SvgMoneyCaseLogo;
