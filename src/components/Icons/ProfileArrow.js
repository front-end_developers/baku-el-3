import * as React from "react";

function SvgProfileArrow(props) {
  return (
    <svg
      width="1em"
      height="1em"
      viewBox="0 0 20 18"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
      {...props}
    >
      <path
        clipRule="evenodd"
        d="M7.23 11.672c-3.203 0-5.939.484-5.939 2.424s2.72 2.442 5.94 2.442c3.203 0 5.938-.485 5.938-2.425 0-1.939-2.718-2.441-5.939-2.441zM7.23 8.904a3.807 3.807 0 10-3.806-3.806 3.793 3.793 0 003.78 3.806h.026z"
        stroke="currentColor"
        strokeWidth={1.5}
        strokeLinecap="round"
        strokeLinejoin="round"
      />
      <path
        d="M18.166 8.166h-5M16.13 5.666l2.44 2.43-2.44 2.43"
        stroke="currentColor"
        strokeWidth={1.5}
        strokeLinecap="round"
        strokeLinejoin="round"
      />
    </svg>
  );
}

export default SvgProfileArrow;
