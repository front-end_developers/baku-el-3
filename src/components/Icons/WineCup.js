import * as React from "react";

function SvgWineCup(props) {
  return (
    <svg
      width="1em"
      height="1em"
      viewBox="0 0 12 20"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
      {...props}
    >
      <path
        d="M9.664 1.642s4.478 10.262-3.61 10.262h-.209c-8.087.016-3.608-10.262-3.608-10.262"
        stroke="currentColor"
        strokeMiterlimit={10}
        strokeLinecap="round"
        strokeLinejoin="round"
      />
      <path
        d="M5.958 2.446c2.055 0 3.721-.36 3.721-.806 0-.444-1.666-.805-3.72-.805-2.056 0-3.722.36-3.722.805 0 .445 1.666.806 3.721.806zM5.958 19.168c2.438 0 4.414-.476 4.414-1.064 0-.587-1.976-1.063-4.414-1.063-2.438 0-4.414.476-4.414 1.063 0 .588 1.976 1.064 4.414 1.064zM5.958 17.75V11.92M9.148 6.071c.193 1.611-.08 3.4-1.965 3.931"
        stroke="currentColor"
        strokeMiterlimit={10}
        strokeLinecap="round"
        strokeLinejoin="round"
      />
    </svg>
  );
}

export default SvgWineCup;
