import * as React from "react";

function SvgFacebookLogo(props) {
  return (
    <svg
      width="1em"
      height="1em"
      viewBox="0 0 9 16"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
      {...props}
    >
      <path
        d="M6.665 2.657h1.46V.113A18.861 18.861 0 005.997 0C3.891 0 2.45 1.325 2.45 3.76V6H.125v2.844h2.324V16h2.85V8.845h2.23L7.881 6H5.297V4.04c.001-.822.222-1.384 1.368-1.384z"
        fill="currentColor"
      />
    </svg>
  );
}

export default SvgFacebookLogo;
