import * as React from "react";

function SvgStartVideo(props) {
  return (
    <svg
      width="1em"
      height="1em"
      viewBox="0 0 24 18"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
      {...props}
    >
      <path
        clipRule="evenodd"
        d="M16.776 12.933c.09 2.036-1.553 3.757-3.67 3.843-.155.007-7.754-.008-7.754-.008-2.106.16-3.95-1.354-4.115-3.382-.013-.15-.01-8.304-.01-8.304-.093-2.038 1.548-3.764 3.665-3.854.158-.007 7.747.007 7.747.007 2.116-.158 3.965 1.366 4.129 3.404.011.147.008 8.294.008 8.294z"
        stroke="currentColor"
        strokeWidth={1.5}
        strokeLinecap="round"
        strokeLinejoin="round"
      />
      <path
        d="M16.78 6.755l3.658-2.994c.907-.742 2.267-.096 2.266 1.074l-.014 8.166c0 1.17-1.362 1.81-2.266 1.069l-3.645-2.995"
        stroke="currentColor"
        strokeWidth={1.5}
        strokeLinecap="round"
        strokeLinejoin="round"
      />
    </svg>
  );
}

export default SvgStartVideo;
