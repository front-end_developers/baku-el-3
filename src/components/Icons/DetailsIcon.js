import * as React from "react";

function SvgDetailsIcon(props) {
  return (
    <svg
      width="1em"
      height="1em"
      viewBox="0 0 16 16"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
      {...props}
    >
      <path
        d="M8 3.556A1.778 1.778 0 108 0a1.778 1.778 0 000 3.556zM8 9.778a1.778 1.778 0 100-3.555 1.778 1.778 0 000 3.555zM8 16a1.778 1.778 0 100-3.556A1.778 1.778 0 008 16z"
        fill="currentColor"
      />
    </svg>
  );
}

export default SvgDetailsIcon;
