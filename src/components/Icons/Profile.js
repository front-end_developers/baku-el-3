import * as React from "react";

function SvgProfile(props) {
  return (
    <svg
      width="1em"
      height="1em"
      viewBox="0 0 16 20"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
      {...props}
    >
      <path
        clipRule="evenodd"
        d="M7.985 13.346c-3.868 0-7.17.585-7.17 2.926 0 2.342 3.281 2.948 7.17 2.948 3.867 0 7.17-.586 7.17-2.927 0-2.34-3.282-2.947-7.17-2.947z"
        stroke="currentColor"
        strokeWidth={1.5}
        strokeLinecap="round"
        strokeLinejoin="round"
      />
      <path
        clipRule="evenodd"
        d="M7.985 10.006A4.596 4.596 0 103.389 5.41a4.58 4.58 0 004.563 4.596h.033z"
        stroke="currentColor"
        strokeWidth={1.429}
        strokeLinecap="round"
        strokeLinejoin="round"
      />
    </svg>
  );
}

export default SvgProfile;
