import * as React from "react";

function SvgTableLamp(props) {
  return (
    <svg
      width="1em"
      height="1em"
      viewBox="0 0 16 20"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
      {...props}
    >
      <path
        d="M14.07.832H2.394L1.333 9.973h13.798L14.069.832zM4.226 4.563L3.747 8.62M10.594 16.735H5.716c-.89 0-1.61.72-1.61 1.61v.821h8.08v-.822c0-.89-.719-1.609-1.592-1.609zM8.3 16.735V9.974M12.152 12.935V9.974"
        stroke="currentColor"
        strokeMiterlimit={10}
        strokeLinecap="round"
        strokeLinejoin="round"
      />
    </svg>
  );
}

export default SvgTableLamp;
