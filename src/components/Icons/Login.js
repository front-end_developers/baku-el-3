import * as React from "react";

function SvgLogin(props) {
  return (
    <svg
      width="1em"
      height="1em"
      viewBox="0 0 17 18"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
      {...props}
    >
      <path
        d="M11.177 9.017H1.143M8.737 6.588l2.44 2.43-2.44 2.43"
        stroke="currentColor"
      />
      <path
        d="M5.087 5.157V4.38a3.07 3.07 0 013.07-3.07h4.07a3.062 3.062 0 013.063 3.062v9.283a3.071 3.071 0 01-3.07 3.071H8.148a3.062 3.062 0 01-3.062-3.062v-.785"
        stroke="currentColor"
      />
    </svg>
  );
}

export default SvgLogin;
