import * as React from "react";

function SvgLinfo(props) {
  return (
    <svg
      width="1em"
      height="1em"
      viewBox="0 0 15 14"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
      {...props}
    >
      <path
        d="M6.674 10.153h1.333v-4H6.674v4zm.666-10A6.665 6.665 0 00.674 6.82a6.665 6.665 0 006.666 6.667 6.665 6.665 0 006.667-6.667A6.665 6.665 0 007.34.153zm0 12A5.34 5.34 0 012.007 6.82 5.34 5.34 0 017.34 1.487a5.34 5.34 0 015.334 5.333 5.34 5.34 0 01-5.334 5.333zM6.674 4.82h1.333V3.487H6.674V4.82z"
        fill="currentColor"
      />
    </svg>
  );
}

export default SvgLinfo;
