import * as React from "react";

function SvgSmartphone(props) {
  return (
    <svg
      width="1em"
      height="1em"
      viewBox="0 0 26 26"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
      {...props}
    >
      <path
        d="M17.274 2.373H8.856A2.356 2.356 0 006.5 4.729v16.543a2.356 2.356 0 002.356 2.356h8.418a2.356 2.356 0 002.356-2.356V4.729a2.356 2.356 0 00-2.356-2.356zm.601 18.899a.6.6 0 01-.601.601H8.856a.601.601 0 01-.601-.601V4.729a.601.601 0 01.601-.601h8.418a.601.601 0 01.601.601v16.543z"
        fill="currentColor"
      />
      <path
        d="M13.553 19.436h-1.625a.878.878 0 000 1.755h1.625a.877.877 0 100-1.755z"
        fill="currentColor"
      />
    </svg>
  );
}

export default SvgSmartphone;
