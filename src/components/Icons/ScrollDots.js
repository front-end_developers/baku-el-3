import * as React from "react";

function SvgScrollDots(props) {
  return (
    <svg
      width="1em"
      height="1em"
      viewBox="0 0 53 8"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
      {...props}
    >
      <rect width={17} height={8} rx={3} fill="currentColor" />
      <rect
        opacity={0.5}
        x={21}
        width={8}
        height={8}
        rx={3}
        fill="currentColor"
      />
      <rect
        opacity={0.5}
        x={33}
        width={8}
        height={8}
        rx={3}
        fill="currentColor"
      />
      <rect
        opacity={0.5}
        x={45}
        width={8}
        height={8}
        rx={3}
        fill="currentColor"
      />
    </svg>
  );
}

export default SvgScrollDots;
