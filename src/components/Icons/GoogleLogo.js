import * as React from "react";

function SvgGoogleLogo(props) {
  return (
    <svg
      width="1em"
      height="1em"
      viewBox="0 0 19 19"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
      {...props}
    >
      <g clipPath="url(#google-logo_svg__clip0_721_6588)">
        <path
          d="M4.49 11.378l-.627 2.339-2.29.048A8.96 8.96 0 01.5 9.5c0-1.492.363-2.9 1.006-4.139l2.04.374.893 2.027a5.35 5.35 0 00-.29 1.738c.001.661.12 1.294.34 1.878z"
          fill="#FBBB00"
        />
        <path
          d="M18.344 7.818a9.021 9.021 0 01-.04 3.56 8.999 8.999 0 01-3.168 5.14l-2.569-.131-.363-2.27a5.364 5.364 0 002.308-2.738H9.699v-3.56h8.645z"
          fill="#518EF8"
        />
        <path
          d="M15.136 16.518A8.962 8.962 0 019.502 18.5a8.999 8.999 0 01-7.928-4.735l2.917-2.387a5.351 5.351 0 007.713 2.74l2.931 2.4z"
          fill="#28B446"
        />
        <path
          d="M15.246 2.572l-2.915 2.387A5.353 5.353 0 004.44 7.76l-2.933-2.4A8.998 8.998 0 019.502.5c2.183 0 4.185.778 5.744 2.072z"
          fill="#F14336"
        />
      </g>
      <defs>
        <clipPath id="google-logo_svg__clip0_721_6588">
          <path
            fill="currentColor"
            transform="translate(.5 .5)"
            d="M0 0h18v18H0z"
          />
        </clipPath>
      </defs>
    </svg>
  );
}

export default SvgGoogleLogo;
