import * as React from "react";

function SvgBell(props) {
  return (
    <svg
      width="1em"
      height="1em"
      viewBox="0 0 20 21"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
      {...props}
    >
      <g
        stroke="currentColor"
        strokeWidth={1.5}
        strokeLinecap="round"
        strokeLinejoin="round"
      >
        <path
          clipRule="evenodd"
          d="M10.001 15.373c4.7 0 6.874-.602 7.084-3.022 0-2.418-1.516-2.263-1.516-5.23 0-2.317-2.197-4.954-5.568-4.954-3.37 0-5.567 2.637-5.567 4.954 0 2.967-1.516 2.812-1.516 5.23.21 2.429 2.385 3.022 7.083 3.022z"
        />
        <path d="M11.991 17.88c-1.136 1.263-2.91 1.278-4.057 0" />
      </g>
    </svg>
  );
}

export default SvgBell;
