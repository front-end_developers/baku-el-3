import * as React from "react";

function SvgEye(props) {
  return (
    <svg
      width="1em"
      height="1em"
      viewBox="0 0 16 14"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
      {...props}
    >
      <path
        clipRule="evenodd"
        d="M10.371 7.04a2.371 2.371 0 11-4.742 0 2.371 2.371 0 014.742 0z"
        stroke="currentColor"
        strokeWidth={1.5}
        strokeLinecap="round"
        strokeLinejoin="round"
      />
      <path
        clipRule="evenodd"
        d="M7.998 12.517c2.856 0 5.469-2.054 6.94-5.477-1.471-3.423-4.084-5.477-6.94-5.477h.003c-2.855 0-5.468 2.054-6.938 5.477 1.47 3.423 4.083 5.477 6.939 5.477h-.004z"
        stroke="currentColor"
        strokeWidth={1.5}
        strokeLinecap="round"
        strokeLinejoin="round"
      />
    </svg>
  );
}

export default SvgEye;
