import * as React from "react";

function SvgLaptop(props) {
  return (
    <svg
      width="1em"
      height="1em"
      viewBox="0 0 20 18"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
      {...props}
    >
      <path
        d="M12.44 2.68L6.894 8.24M13.107 4.193l-2.224 2.224M8.575 4.193L6.352 6.417"
        stroke="currentColor"
        strokeMiterlimit={10}
        strokeLinecap="round"
      />
      <path
        d="M17.499 10.286V1.738a1.07 1.07 0 00-1.07-1.07H3.558a1.07 1.07 0 00-1.07 1.07v8.548L.833 15.54h18.334l-1.668-5.254z"
        stroke="currentColor"
        strokeMiterlimit={10}
        strokeLinecap="round"
        strokeLinejoin="round"
      />
      <path
        d="M2.487 10.284h14.47"
        stroke="currentColor"
        strokeMiterlimit={10}
        strokeLinecap="round"
      />
      <path
        d="M11.41 12.232H8.13l-.166 1.64h3.614l-.167-1.64zM17.499 17.206H2.5c-.917 0-1.668-.75-1.668-1.668h18.334c0 .917-.75 1.668-1.668 1.668z"
        stroke="currentColor"
        strokeMiterlimit={10}
        strokeLinecap="round"
        strokeLinejoin="round"
      />
    </svg>
  );
}

export default SvgLaptop;
