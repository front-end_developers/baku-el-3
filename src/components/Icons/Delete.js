import * as React from "react";

function SvgDelete(props) {
  return (
    <svg
      width="1em"
      height="1em"
      viewBox="0 0 14 14"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
      {...props}
    >
      <path
        d="M11.883 5.313s-.361 4.49-.571 6.38c-.1.904-.659 1.434-1.572 1.45-1.74.031-3.481.034-5.22-.003-.88-.018-1.428-.554-1.526-1.442-.211-1.908-.571-6.386-.571-6.386M12.805 3.16H1.5M10.627 3.16c-.523 0-.974-.37-1.076-.883l-.162-.81a.853.853 0 00-.825-.633H5.742a.853.853 0 00-.825.633l-.162.81a1.099 1.099 0 01-1.076.883"
        stroke="currentColor"
        strokeWidth={1.5}
        strokeLinecap="round"
        strokeLinejoin="round"
      />
    </svg>
  );
}

export default SvgDelete;
