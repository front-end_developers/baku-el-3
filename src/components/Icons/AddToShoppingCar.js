import * as React from "react";

function SvgAddToShoppingCar(props) {
  return (
    <svg
      width="1em"
      height="1em"
      viewBox="0 0 18 20"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
      {...props}
    >
      <path
        d="M1.292 3.708l1.733.3.803 9.56a1.5 1.5 0 001.5 1.378h9.09a1.5 1.5 0 001.488-1.288l.79-5.465a1.118 1.118 0 00-.945-1.266c-.053-.007-12.448-.011-12.448-.011M9.46 4.333V1M8.166 3.033l1.294 1.3 1.294-1.3"
        stroke="currentColor"
        strokeWidth={1.5}
        strokeLinecap="round"
        strokeLinejoin="round"
      />
      <path
        fillRule="evenodd"
        clipRule="evenodd"
        d="M4.962 17.835a.453.453 0 110 .906.453.453 0 010-.906zM14.362 17.835a.454.454 0 110 .908.454.454 0 010-.908z"
        fill="currentColor"
        stroke="currentColor"
        strokeWidth={1.5}
        strokeLinecap="round"
        strokeLinejoin="round"
      />
    </svg>
  );
}

export default SvgAddToShoppingCar;
