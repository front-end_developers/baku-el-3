import * as React from "react";

function SvgFreezer(props) {
  return (
    <svg
      width="1em"
      height="1em"
      viewBox="0 0 13 20"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
      {...props}
    >
      <path
        d="M11.579.832H1v18.333h10.579V.832zM1.367 7.337h9.845M3.01 9.307v4.087M3.01 2.611v3.11"
        stroke="currentColor"
        strokeMiterlimit={10}
        strokeLinecap="round"
        strokeLinejoin="round"
      />
    </svg>
  );
}

export default SvgFreezer;
