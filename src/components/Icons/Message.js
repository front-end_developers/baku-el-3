import * as React from "react";

function SvgMessage(props) {
  return (
    <svg
      width="1em"
      height="1em"
      viewBox="0 0 18 16"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
      {...props}
    >
      <path
        d="M13.428 5.639l-3.333 2.71c-.63.499-1.515.499-2.145 0l-3.36-2.71"
        stroke="currentColor"
        strokeWidth={1.5}
        strokeLinecap="round"
        strokeLinejoin="round"
      />
      <path
        clipRule="evenodd"
        d="M12.682 14.75c2.28.006 3.818-1.868 3.818-4.171V5.428c0-2.304-1.537-4.178-3.818-4.178H5.318C3.038 1.25 1.5 3.124 1.5 5.428v5.15c0 2.304 1.537 4.178 3.818 4.172h7.364z"
        stroke="currentColor"
        strokeWidth={1.5}
        strokeLinecap="round"
        strokeLinejoin="round"
      />
    </svg>
  );
}

export default SvgMessage;
