import * as React from "react";

function SvgUserCarts(props) {
  return (
    <svg
      width="1em"
      height="1em"
      viewBox="0 0 18 18"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
      {...props}
    >
      <path
        clipRule="evenodd"
        d="M12.612 1.291H5.388c-2.518 0-4.096 1.783-4.096 4.305v6.807c0 2.522 1.57 4.305 4.096 4.305h7.223c2.526 0 4.097-1.783 4.097-4.305V5.596c0-2.522-1.571-4.305-4.096-4.305z"
        stroke="currentColor"
        strokeWidth={1.5}
        strokeLinecap="round"
        strokeLinejoin="round"
      />
      <path
        clipRule="evenodd"
        d="M7.908 8.998a1.543 1.543 0 11-1.544-1.543h.003c.85 0 1.54.692 1.54 1.543z"
        stroke="currentColor"
        strokeWidth={1.5}
        strokeLinecap="round"
        strokeLinejoin="round"
      />
      <path
        d="M7.91 9h5.265v1.543M10.818 10.543V9"
        stroke="currentColor"
        strokeWidth={1.5}
        strokeLinecap="round"
        strokeLinejoin="round"
      />
    </svg>
  );
}

export default SvgUserCarts;
