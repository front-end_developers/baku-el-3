import * as React from "react";

function SvgTelegramLogo(props) {
  return (
    <svg
      width="1em"
      height="1em"
      viewBox="0 0 16 14"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
      {...props}
    >
      <path
        d="M6.278 9.12l-.265 3.723c.38 0 .543-.162.74-.358l1.775-1.696 3.679 2.694c.675.375 1.15.178 1.332-.621l2.415-11.315C16.166.55 15.594.16 14.935.404L.743 5.838c-.969.376-.954.916-.165 1.16l3.629 1.13 8.428-5.275c.397-.262.758-.117.461.146L6.278 9.12z"
        fill="currentColor"
      />
    </svg>
  );
}

export default SvgTelegramLogo;
