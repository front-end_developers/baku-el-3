import * as React from "react";

function SvgClimateControl(props) {
  return (
    <svg
      width="1em"
      height="1em"
      viewBox="0 0 20 13"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
      {...props}
    >
      <path
        d="M19.167 7.64V1H.833v6.64c0 .84.69 1.528 1.53 1.528h15.275c.854 0 1.529-.675 1.529-1.529zM2.693 6.883h11.625M16.233 6.882h1.088M3.588 10.986l-1.529.923M7.858 10.985l-.523.91M12.142 10.985l.455.91M16.412 10.985l1.46.896"
        stroke="currentColor"
        strokeMiterlimit={10}
        strokeLinecap="round"
        strokeLinejoin="round"
      />
    </svg>
  );
}

export default SvgClimateControl;
