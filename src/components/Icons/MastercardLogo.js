import * as React from "react";

function SvgMastercardLogo(props) {
  return (
    <svg
      width="1em"
      height="1em"
      viewBox="0 0 25 25"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
      {...props}
    >
      <g
        clipPath="url(#mastercardLogo_svg__clip0_2666_288)"
        fill="currentColor"
      >
        <path d="M9.5 12.389a7.48 7.48 0 013-5.99A7.456 7.456 0 008 4.889a7.5 7.5 0 000 15 7.46 7.46 0 004.5-1.51 7.482 7.482 0 01-3-5.99z" />
        <path
          opacity={0.5}
          d="M17 4.889a7.456 7.456 0 00-4.5 1.51 7.48 7.48 0 013 5.99 7.482 7.482 0 01-3 5.99 7.456 7.456 0 004.5 1.51 7.5 7.5 0 000-15z"
        />
        <path d="M12.5 18.378c1.657 0 3-2.681 3-5.99 0-3.308-1.343-5.99-3-5.99s-3 2.682-3 5.99c0 3.309 1.343 5.99 3 5.99z" />
      </g>
      <defs>
        <clipPath id="mastercardLogo_svg__clip0_2666_288">
          <path
            fill="currentColor"
            transform="translate(.5 .389)"
            d="M0 0h24v24H0z"
          />
        </clipPath>
      </defs>
    </svg>
  );
}

export default SvgMastercardLogo;
