import * as React from "react";

function SvgMenuStyleGrid(props) {
  return (
    <svg
      width="1em"
      height="1em"
      viewBox="0 0 14 14"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
      {...props}
    >
      <path
        clipRule="evenodd"
        d="M1 3.333C1 1.583 1.019 1 3.333 1c2.315 0 2.334.583 2.334 2.333 0 1.75.007 2.334-2.334 2.334C.993 5.667 1 5.083 1 3.333zM8.334 3.333c0-1.75.019-2.333 2.333-2.333 2.315 0 2.334.583 2.334 2.333 0 1.75.007 2.334-2.334 2.334-2.34 0-2.333-.584-2.333-2.334zM1 10.666c0-1.75.019-2.333 2.333-2.333 2.315 0 2.334.583 2.334 2.333 0 1.75.007 2.334-2.334 2.334C.993 13 1 12.416 1 10.666zM8.334 10.666c0-1.75.019-2.333 2.333-2.333 2.315 0 2.334.583 2.334 2.333 0 1.75.007 2.334-2.334 2.334-2.34 0-2.333-.584-2.333-2.334z"
        stroke="currentColor"
        strokeWidth={1.5}
        strokeLinecap="round"
        strokeLinejoin="round"
      />
    </svg>
  );
}

export default SvgMenuStyleGrid;
