import * as React from "react";

function SvgAngleLeft(props) {
  return (
    <svg
      width="1em"
      height="1em"
      viewBox="0 0 5 9"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
      {...props}
    >
      <path
        fillRule="evenodd"
        clipRule="evenodd"
        d="M4.692.651a1 1 0 01.03 1.414L2.386 4.5l2.336 2.435A1 1 0 013.278 8.32l-3-3.127a1 1 0 010-1.385l3-3.127A1 1 0 014.692.65z"
        fill="currentColor"
      />
    </svg>
  );
}

export default SvgAngleLeft;
