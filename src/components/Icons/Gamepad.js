import * as React from "react";

function SvgGamepad(props) {
  return (
    <svg
      width="1em"
      height="1em"
      viewBox="0 0 20 15"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
      {...props}
    >
      <path
        d="M8.423 3.396c-.299-.081-.801-.38-1.018-.502-.72-.38-1.562-.462-2.39-.231-1.372.367-2.608 1.48-3.382 3.083-1.372 2.77-.937 5.948.991 7.252.53.353 1.14.53 1.78.53.298 0 .583-.041.882-.123 1.317-.353 2.526-1.398 3.314-2.892a4.013 4.013 0 012.92-.041c.787 1.467 1.955 2.485 3.245 2.825.938.23 1.861.095 2.622-.421 1.901-1.29 2.35-4.414 1.005-7.117-.788-1.561-1.997-2.675-3.341-3.028-.802-.204-1.59-.15-2.296.19-.244.122-.733.53-2.593.53V1.332"
        stroke="currentColor"
        strokeMiterlimit={10}
        strokeLinecap="round"
        strokeLinejoin="round"
      />
      <path
        d="M12.755 6.602c-.136.272-.217.57-.217.896 0 1.087.883 1.983 1.955 1.983 1.073 0 1.956-.896 1.956-1.983 0-1.086-.883-1.982-1.956-1.982M5.286 5.842v3.517M3.52 7.607h3.531"
        stroke="currentColor"
        strokeMiterlimit={10}
        strokeLinecap="round"
        strokeLinejoin="round"
      />
    </svg>
  );
}

export default SvgGamepad;
