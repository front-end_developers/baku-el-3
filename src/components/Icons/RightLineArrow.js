import * as React from "react";

function SvgRightLineArrow(props) {
  return (
    <svg
      width="1em"
      height="1em"
      viewBox="0 0 14 6"
      fill="red"
      xmlns="http://www.w3.org/2000/svg"
      {...props}
    >
      <path
        d="M10.999.332v2H.332v1.333h10.667v2L13.665 3 11 .332z"
        fill="currentColor"
      />
    </svg>
  );
}

export default SvgRightLineArrow;
