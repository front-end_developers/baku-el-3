import * as React from "react";

function SvgHeart(props) {
  return (
    <svg
      width="1em"
      height="1em"
      viewBox="0 0 18 18"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
      {...props}
    >
      <path
        clipRule="evenodd"
        d="M1.393 8.665C.5 5.874 1.544 2.683 4.475 1.74A5.006 5.006 0 019 2.499c1.212-.938 2.976-1.255 4.517-.76 2.93.944 3.982 4.135 3.089 6.926C15.214 13.09 9 16.5 9 16.5S2.83 13.142 1.393 8.665z"
        stroke="currentColor"
        strokeWidth={1.5}
        strokeLinecap="round"
        strokeLinejoin="round"
      />
      <path
        d="M12.333 4.583a2.318 2.318 0 011.598 2.018"
        stroke="currentColor"
        strokeWidth={1.5}
        strokeLinecap="round"
        strokeLinejoin="round"
      />
    </svg>
  );
}

export default SvgHeart;
