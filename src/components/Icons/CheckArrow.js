import * as React from "react";

function SvgCheckArrow(props) {
  return (
    <svg
      width="1em"
      height="1em"
      viewBox="0 0 8 6"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
      {...props}
    >
      <path
        d="M1.625 3l1.583 1.582 3.164-3.164"
        stroke="currentColor"
        strokeWidth={1.5}
        strokeLinecap="round"
        strokeLinejoin="round"
      />
    </svg>
  );
}

export default SvgCheckArrow;
