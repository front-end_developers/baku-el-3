import * as React from "react";

function SvgPhone(props) {
  return (
    <svg
      width="1em"
      height="1em"
      viewBox="0 0 13 20"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
      {...props}
    >
      <path
        d="M10.234 19.165H2.847C1.816 19.165 1 18.38 1 17.42V2.58C1 1.617 1.831.831 2.847.831h7.387c1.03 0 1.847.786 1.847 1.746V17.42c0 .96-.832 1.746-1.847 1.746zM2.816 17.492h9.265M1 2.49h9.265M9.172 7.63L4.14 12.375M9.757 8.927l-2 1.906M5.663 8.927l-2 1.906"
        stroke="currentColor"
        strokeMiterlimit={10}
        strokeLinecap="round"
      />
    </svg>
  );
}

export default SvgPhone;
