import * as React from "react";

function SvgFbLogo(props) {
  return (
    <svg
      width="1em"
      height="1em"
      viewBox="0 0 19 19"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
      {...props}
    >
      <g clipPath="url(#fb-logo_svg__clip0_721_6603)">
        <path
          d="M18.5 9.5a9 9 0 10-10.406 8.89v-6.288H5.809V9.5h2.285V7.517c0-2.255 1.343-3.501 3.4-3.501.984 0 2.014.175 2.014.175v2.215h-1.135c-1.118 0-1.467.694-1.467 1.406V9.5h2.496l-.399 2.602h-2.097v6.289C15.21 17.716 18.5 13.992 18.5 9.5z"
          fill="#1877F2"
        />
        <path
          d="M13.003 12.102l.4-2.602h-2.497V7.812c0-.712.349-1.406 1.467-1.406h1.135V4.191s-1.03-.175-2.015-.175c-2.056 0-3.4 1.246-3.4 3.501V9.5H5.81v2.602h2.285v6.289a9.09 9.09 0 002.812 0V12.1h2.097z"
          fill="currentColor"
        />
      </g>
      <defs>
        <clipPath id="fb-logo_svg__clip0_721_6603">
          <path
            fill="currentColor"
            transform="translate(.5 .5)"
            d="M0 0h18v18H0z"
          />
        </clipPath>
      </defs>
    </svg>
  );
}

export default SvgFbLogo;
