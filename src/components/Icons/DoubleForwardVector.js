import * as React from "react";

function SvgDoubleForwardVector(props) {
  return (
    <svg
      width="1em"
      height="1em"
      viewBox="0 0 14 14"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
      {...props}
    >
      <path
        d="M7.415 13.01L13.425 7 7.415.99 6 2.404l4.6 4.6-4.6 4.6 1.414 1.406h.001zm-5.425 0L8.001 7 1.99.99.576 2.404l4.6 4.6-4.6 4.6 1.413 1.406h.001z"
        fill="currentColor"
      />
    </svg>
  );
}

export default SvgDoubleForwardVector;
