import * as React from "react";

function SvgPlayIconNew(props) {
  return (
    <svg
      width="1em"
      height="1em"
      viewBox="0 0 27 42"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
      {...props}
    >
      <path
        d="M0 38.64V3.36C0 .77 2.052-.843 3.686.461l22.098 17.64c1.621 1.295 1.621 4.501 0 5.796L3.686 41.538C2.052 42.843 0 41.23 0 38.64z"
        fill="currentColor"
      />
    </svg>
  );
}

export default SvgPlayIconNew;
