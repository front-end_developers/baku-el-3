import * as React from "react";

function SvgHistory(props) {
  return (
    <svg
      width="1em"
      height="1em"
      viewBox="0 0 18 18"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
      {...props}
    >
      <g opacity={0.4} clipPath="url(#history_svg__clip0_6758_49589)">
        <path d="M18 0H0v18h18V0z" fill="currentColor" fillOpacity={0.01} />
        <path
          d="M2.182 2.522V5.25h2.727M1.5 9a7.5 7.5 0 101.004-3.75"
          stroke="currentColor"
          strokeWidth={1.5}
          strokeLinecap="round"
          strokeLinejoin="round"
        />
        <path
          d="M9.002 4.5v4.503l3.18 3.18"
          stroke="currentColor"
          strokeWidth={1.5}
          strokeLinecap="round"
          strokeLinejoin="round"
        />
      </g>
      <defs>
        <clipPath id="history_svg__clip0_6758_49589">
          <path fill="currentColor" d="M0 0h18v18H0z" />
        </clipPath>
      </defs>
    </svg>
  );
}

export default SvgHistory;
