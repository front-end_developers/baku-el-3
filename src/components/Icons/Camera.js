import * as React from "react";

function SvgCamera(props) {
  return (
    <svg
      width="1em"
      height="1em"
      viewBox="0 0 20 15"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
      {...props}
    >
      <path
        d="M6.629 13.38H.833V3.149h11.228l1.279-1.816h4.548l1.279 1.816v10.233h-1.674"
        stroke="currentColor"
        strokeMiterlimit={10}
        strokeLinecap="round"
        strokeLinejoin="round"
      />
      <path
        d="M12.06 14.407a4.721 4.721 0 100-9.443 4.721 4.721 0 000 9.443zM8.918 9.687a3.14 3.14 0 013.143-3.142M3.676 4.712v7.106M14.382 3.291h2.795"
        stroke="currentColor"
        strokeMiterlimit={10}
        strokeLinecap="round"
        strokeLinejoin="round"
      />
    </svg>
  );
}

export default SvgCamera;
