import * as React from "react";

function SvgDoubleDownVector(props) {
  return (
    <svg
      width="1em"
      height="1em"
      viewBox="0 0 14 14"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
      {...props}
    >
      <path
        d="M7 13.424L.99 7.414 2.404 6l4.6 4.6 4.6-4.6 1.406 1.415-6.009 6.01H7zM7 8L.99 1.99 2.404.574l4.6 4.6 4.6-4.6L13.01 1.99 7.001 8 7 8z"
        fill="currentColor"
      />
    </svg>
  );
}

export default SvgDoubleDownVector;
