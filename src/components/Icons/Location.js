import * as React from "react";

function SvgLocation(props) {
  return (
    <svg
      width="1em"
      height="1em"
      viewBox="0 0 14 18"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
      {...props}
    >
      <path
        clipRule="evenodd"
        d="M9.083 7.75a2.083 2.083 0 10-4.166-.001 2.083 2.083 0 004.166 0z"
        stroke="currentColor"
        strokeWidth={1.5}
        strokeLinecap="round"
        strokeLinejoin="round"
      />
      <path
        clipRule="evenodd"
        d="M7 16.5C6 16.5.75 12.249.75 7.803.75 4.323 3.548 1.5 7 1.5s6.25 2.822 6.25 6.303c0 4.446-5.252 8.697-6.25 8.697z"
        stroke="currentColor"
        strokeWidth={1.5}
        strokeLinecap="round"
        strokeLinejoin="round"
      />
    </svg>
  );
}

export default SvgLocation;
