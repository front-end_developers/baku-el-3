import * as React from "react";

function SvgIronLight(props) {
  return (
    <svg
      width="1em"
      height="1em"
      viewBox="0 0 20 11"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
      {...props}
    >
      <path
        d="M1.665 8.766L3.2 5.619A8.014 8.014 0 0110.454 1h7.088l1.625 5.501"
        stroke="currentColor"
        strokeMiterlimit={10}
        strokeLinecap="round"
        strokeLinejoin="round"
      />
      <path
        d="M1.665 8.765l-.832 1.701h15.558l-.205-1.471H3.52M4.582 6.5h.358M6.322 6.5h.358M8.062 6.5h.358M10.39 2.473h3.3l.666 2.712H5.541l.013-.026c.78-1.637 3.02-2.686 4.836-2.686z"
        stroke="currentColor"
        strokeMiterlimit={10}
        strokeLinecap="round"
        strokeLinejoin="round"
      />
      <path
        d="M16.186 8.996h1.471l-1.663-5.975"
        stroke="currentColor"
        strokeMiterlimit={10}
        strokeLinecap="round"
        strokeLinejoin="round"
      />
    </svg>
  );
}

export default SvgIronLight;
