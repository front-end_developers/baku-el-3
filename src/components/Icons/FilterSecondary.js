import * as React from "react";

function SvgFilterSecondary(props) {
  return (
    <svg
      width="1em"
      height="1em"
      viewBox="0 0 14 15"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
      {...props}
    >
      <path
        clipRule="evenodd"
        d="M2.293 1.5c-.576 0-1.043.475-1.043 1.06v.624c0 .434.165.85.46 1.164l3.23 3.435.001-.002c.624.638.975 1.501.975 2.401v3.048c0 .204.213.334.39.238l1.84-1.003a.873.873 0 00.45-.766v-1.525c0-.895.347-1.754.965-2.391l3.23-3.435c.294-.314.459-.73.459-1.164V2.56c0-.585-.467-1.06-1.043-1.06H2.293z"
        stroke="currentColor"
        strokeWidth={1.5}
        strokeLinecap="round"
        strokeLinejoin="round"
      />
    </svg>
  );
}

export default SvgFilterSecondary;
