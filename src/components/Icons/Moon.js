import * as React from "react";

function SvgMoon(props) {
  return (
    <svg
      width="1em"
      height="1em"
      viewBox="0 0 14 14"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
      {...props}
    >
      <path
        d="M7.14 13.79A6.904 6.904 0 01.245 6.893 6.875 6.875 0 013.972.772a.55.55 0 01.796.595c-.072.366-.109.739-.11 1.112a5.8 5.8 0 005.794 5.794c.933 0 1.828-.219 2.66-.652a.552.552 0 01.796.596 6.905 6.905 0 01-6.767 5.573zM3.557 2.346a5.765 5.765 0 00-2.208 4.547 5.8 5.8 0 005.793 5.793 5.803 5.803 0 005.371-3.626 6.904 6.904 0 01-8.956-6.714z"
        fill="currentColor"
      />
    </svg>
  );
}

export default SvgMoon;
