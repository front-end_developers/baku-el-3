import * as React from "react";

function SvgPlayIcon(props) {
  return (
    <svg
      width="1em"
      height="1em"
      viewBox="0 0 111 111"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
      {...props}
    >
      <circle
        opacity={0.7}
        cx={55.5}
        cy={55.5}
        r={54}
        stroke="currentColor"
        strokeWidth={3}
      />
      <path
        d="M66.96 53.523h0c1.124.77 1.56 1.667 1.56 2.475v0c0 .805-.44 1.704-1.568 2.477 0 0 0 0 0 0l-22.228 15.21h-.001c-.88.603-1.603.815-2.059.815-.38 0-.6-.13-.768-.35-.204-.27-.416-.803-.416-1.71V39.565c0-.908.212-1.443.417-1.713.168-.221.39-.352.773-.352.46 0 1.2.216 2.071.812h0L66.96 53.524z"
        stroke="currentColor"
        strokeWidth={3}
      />
    </svg>
  );
}

export default SvgPlayIcon;
