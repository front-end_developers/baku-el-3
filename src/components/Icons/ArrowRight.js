import * as React from "react";

function SvgArrowRight(props) {
  return (
    <svg
      width="1em"
      height="1em"
      viewBox="0 0 14 10"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
      {...props}
    >
      <path
        d="M10.475 5.833L7.492 8.825 8.667 10l5-5-5-5-1.175 1.175 2.983 2.992H.333v1.666h10.142z"
        fill="currentColor"
      />
    </svg>
  );
}

export default SvgArrowRight;
