import * as React from "react";

function SvgCreditCard(props) {
  return (
    <svg
      width="1em"
      height="1em"
      viewBox="0 0 24 24"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
      {...props}
    >
      <path
        d="M20.25 5.25H3.75a1.5 1.5 0 00-1.5 1.5v10.5a1.5 1.5 0 001.5 1.5h16.5a1.5 1.5 0 001.5-1.5V6.75a1.5 1.5 0 00-1.5-1.5zm-12 11.25h-3a.75.75 0 110-1.5h3a.75.75 0 110 1.5zM9 11.25a.75.75 0 01-.75.75h-3a.75.75 0 01-.75-.75v-3a.75.75 0 01.75-.75h3a.75.75 0 01.75.75v3zm9.75 5.25h-6a.75.75 0 110-1.5h6a.75.75 0 110 1.5z"
        fill="currentColor"
      />
    </svg>
  );
}

export default SvgCreditCard;
