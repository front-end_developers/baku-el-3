import * as React from "react";

function SvgTv(props) {
  return (
    <svg
      width="1em"
      height="1em"
      viewBox="0 0 20 16"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
      {...props}
    >
      <path
        d="M19.167 1.332H.833v10.313h18.334V1.332zM3.125 15.097l6.875-.65 6.862.637M10 14.449v-1.146M15.309 3.306l-5.92 5.933M16.022 4.924l-2.38 2.38M11.184 4.924l-2.38 2.38"
        stroke="currentColor"
        strokeMiterlimit={10}
        strokeLinecap="round"
      />
    </svg>
  );
}

export default SvgTv;
