import * as React from "react";

function SvgPan(props) {
  return (
    <svg
      width="1em"
      height="1em"
      viewBox="0 0 20 19"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
      {...props}
    >
      <path
        d="M12.436 11.25c-.115 2.588-2.324 4.599-4.911 4.483-2.588-.115-4.599-2.324-4.483-4.911.115-2.588 2.324-4.599 4.911-4.483"
        stroke="currentColor"
        strokeMiterlimit={10}
        strokeLinecap="round"
        strokeLinejoin="round"
      />
      <path
        d="M7.723 17.91a6.89 6.89 0 100-13.78 6.89 6.89 0 000 13.78z"
        stroke="currentColor"
        strokeMiterlimit={10}
        strokeLinecap="round"
        strokeLinejoin="round"
      />
      <path
        d="M12.272 5.76l4.5-4.433c.708-.643 1.45-.972 2.092-.263.643.708.165 1.318-.544 1.96l-5.01 3.89M7.722 12.206a1.187 1.187 0 100-2.373 1.187 1.187 0 000 2.373z"
        stroke="currentColor"
        strokeMiterlimit={10}
        strokeLinecap="round"
        strokeLinejoin="round"
      />
    </svg>
  );
}

export default SvgPan;
