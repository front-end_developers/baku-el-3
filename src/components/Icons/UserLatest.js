import * as React from "react";

function SvgUserLatest(props) {
  return (
    <svg
      width="1em"
      height="1em"
      viewBox="0 0 19 18"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
      {...props}
    >
      <path
        d="M5.037 11.317l2.495-3.242 2.845 2.235 2.44-3.15"
        stroke="currentColor"
        strokeWidth={1.5}
        strokeLinecap="round"
        strokeLinejoin="round"
      />
      <circle
        cx={15.663}
        cy={2.5}
        stroke="currentColor"
        strokeWidth={1.5}
        strokeLinecap="round"
        strokeLinejoin="round"
        r={1.602}
      />
      <path
        d="M11.437 1.6H5.381c-2.51 0-4.066 1.777-4.066 4.286v6.736c0 2.51 1.526 4.279 4.066 4.279h7.17c2.51 0 4.065-1.77 4.065-4.28V6.757"
        stroke="currentColor"
        strokeWidth={1.5}
        strokeLinecap="round"
        strokeLinejoin="round"
      />
    </svg>
  );
}

export default SvgUserLatest;
