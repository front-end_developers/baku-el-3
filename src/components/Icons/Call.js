import * as React from "react";

function SvgCall(props) {
  return (
    <svg
      width="1em"
      height="1em"
      viewBox="0 0 18 19"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
      {...props}
    >
      <path
        clipRule="evenodd"
        d="M8.649 9.854c2.992 2.991 3.67-.469 5.575 1.435 1.837 1.836 2.892 2.203.565 4.53-.291.234-2.143 3.052-8.65-3.454-6.51-6.507-3.693-8.36-3.459-8.652 2.333-2.333 2.694-1.271 4.53.565C9.117 6.182 5.658 6.863 8.65 9.854z"
        stroke="currentColor"
        strokeWidth={1.5}
        strokeLinecap="round"
        strokeLinejoin="round"
      />
    </svg>
  );
}

export default SvgCall;
