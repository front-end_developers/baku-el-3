import * as React from "react";

function SvgLocation2(props) {
  return (
    <svg
      width="1em"
      height="1em"
      viewBox="0 0 12 15"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
      {...props}
    >
      <path
        clipRule="evenodd"
        d="M7.665 6.501a1.667 1.667 0 10-3.333 0 1.667 1.667 0 003.333 0z"
        stroke="red"
        strokeWidth={1.5}
        strokeLinecap="round"
        strokeLinejoin="round"
      />
      <path
        clipRule="evenodd"
        d="M6 13.5c-.8 0-5-3.401-5-6.958C1 3.758 3.238 1.5 6 1.5c2.761 0 5 2.258 5 5.042C11 10.1 6.799 13.5 6 13.5z"
        stroke="red"
        strokeWidth={1.5}
        strokeLinecap="round"
        strokeLinejoin="round"
      />
    </svg>
  );
}

export default SvgLocation2;
