import * as React from "react";

function SvgUserBonuses(props) {
  return (
    <svg
      width="1em"
      height="1em"
      viewBox="0 0 18 18"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
      {...props}
    >
      <path
        d="M2.996 4.88c0-1.041.843-1.885 1.884-1.886h.857c.498 0 .975-.197 1.328-.547l.6-.6a1.885 1.885 0 012.665-.008v.001l.008.007.6.6c.354.35.83.547 1.328.547h.856c1.041 0 1.885.844 1.885 1.885v.856c0 .498.197.975.548 1.329l.6.6c.738.734.742 1.927.008 2.665v.001l-.008.008-.6.6a1.88 1.88 0 00-.548 1.326v.858c0 1.04-.843 1.884-1.884 1.884h0-.859c-.497 0-.975.198-1.327.549l-.6.599a1.884 1.884 0 01-2.665.01l-.002-.003-.008-.007-.599-.6a1.886 1.886 0 00-1.327-.548H4.88a1.884 1.884 0 01-1.884-1.884v-.86a1.88 1.88 0 00-.549-1.326l-.599-.6a1.884 1.884 0 01-.007-2.667l.007-.007.6-.6c.35-.353.548-.83.548-1.328v-.855M6.86 11.143l4.283-4.284"
        stroke="currentColor"
        strokeWidth={1.5}
        strokeLinecap="round"
        strokeLinejoin="round"
      />
      <path
        d="M11.08 11.083h.007M6.913 6.917h.007"
        stroke="currentColor"
        strokeWidth={2}
        strokeLinecap="round"
        strokeLinejoin="round"
      />
    </svg>
  );
}

export default SvgUserBonuses;
