import * as React from "react";

function SvgLinkedinLogo(props) {
  return (
    <svg
      width="1em"
      height="1em"
      viewBox="0 0 16 16"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
      {...props}
    >
      <g
        clipPath="url(#linkedinLogo_svg__clip0_1189_17395)"
        fill="currentColor"
      >
        <path d="M16.337 15.95h.004v-5.868c0-2.87-.618-5.082-3.974-5.082-1.614 0-2.696.885-3.138 1.725h-.047V5.268H6V15.95h3.313v-5.29c0-1.392.264-2.739 1.989-2.739 1.7 0 1.725 1.59 1.725 2.829v5.2h3.31zM.264 5.318H3.58V16H.264V5.318zM1.921 0C.861 0 0 .86 0 1.921s.86 1.94 1.921 1.94 1.922-.879 1.922-1.94C3.842.861 2.98 0 1.92 0z" />
      </g>
      <defs>
        <clipPath id="linkedinLogo_svg__clip0_1189_17395">
          <path fill="currentColor" d="M0 0h16v16H0z" />
        </clipPath>
      </defs>
    </svg>
  );
}

export default SvgLinkedinLogo;
