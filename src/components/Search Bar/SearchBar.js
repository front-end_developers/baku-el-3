import classes from "./SearchBar.module.scss";
import { Search } from "../Icons";

export default function SearchBar() {
  return (
    <div className={classes.SearchBar}>
      <button className={classes.SearchBarButton}>
        <Search className={classes.SearchBarButtonIcon} />
      </button>
      <input placeholder="Axtar..." className={classes.SearchBarInput} />
    </div>
  );
}
