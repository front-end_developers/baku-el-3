import React from "react";
import classes from "./ColorInput.module.scss"

export default function ColorInput(){
    return(
        <span className={classes.ColorInput}>
            <input type="checkbox"/>
            <span style={{background: "#02379D"}}></span>
        </span>
    )
}