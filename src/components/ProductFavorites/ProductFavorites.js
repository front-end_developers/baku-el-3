import React from "react";
import classes from "./ProductFavorites.module.scss";
import ProductCard from "../ProductCard/ProductCard";

const ProductFavorites = () => {
  return (
    <div className={classes.FavoritesContainer}>
      <div className={classes.FavoritesContainerTitle}>
        <div className={classes.FavoritesContainerTitleTop}>Sevimlilərim</div>
        <div className={classes.FavoritesContainerTitleProductCount}>
          3 Məhsul
        </div>
      </div>
      <div className={classes.FavoritesContainerBody}>
        <ProductCard />
        <ProductCard />
        <ProductCard />
        <ProductCard />
        <ProductCard />
      </div>
    </div>
  );
};

export default ProductFavorites;
