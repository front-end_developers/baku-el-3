import React from "react";
import classes from "./NewsCards.module.scss"
import { RightLineArrow } from "../Icons";
import { Container, Row, Col } from "react-bootstrap";

const NewsCards = () => {
  return (
    <div className={classes.NewsCardsContainer}>
      <div className={classes.NewsCardsContainerTitle}>
        <p className={classes.NewsCardsContainerTitleTop}>Xəbərlər</p>
        <h1 className={classes.NewsCardsContainerTitleBottom}>
          Ən son yeniliklər
        </h1>
      </div>
      <div className={classes.NewsCardsContainerItems}>
        <div className={classes.NewsCard}>
          <div className={classes.NewsCardImg}>
            <img src="/img/newscards.png" alt="" />
            <div className={classes.NewsCardText}>İnnovasiya</div>
          </div>

          <div className={classes.NewsCardBody}>
            <h4>Artıq Apple Iphone 13 Modellərini təqdim etdi</h4>
            <p className={classes.NewsCardContent}>
              Orci, pellentesque quis nunc sollicitin vitae proin eget aliquam.
              Aliquam ornare pretium diam amet.
            </p>
          </div>
          <div className={classes.NewsCardEnd}>
            <p className={classes.DateText}>29 Avq 2021</p>
            <div>
              Bax
              <RightLineArrow className={classes.IconCard} />
            </div>
          </div>
        </div>

        <div className={classes.NewsCard}>
          <div className={classes.NewsCardImg}>
            <img src="/img/newscards.png" alt="" />
            <div className={classes.NewsCardText}>İnnovasiya</div>
          </div>

          <div className={classes.NewsCardBody}>
            <h4>Artıq Apple Iphone 13 Modellərini təqdim etdi</h4>
            <p className={classes.NewsCardContent}>
              Orci, pellentesque quis nunc sollicitin vitae proin eget aliquam.
              Aliquam ornare pretium diam amet.
            </p>
          </div>
          <div className={classes.NewsCardEnd}>
            <p className={classes.DateText}>29 Avq 2021</p>
            <div>
              Bax
              <RightLineArrow className={classes.IconCard} />
            </div>
          </div>
        </div>

        <div className={classes.NewsCard}>
          <div className={classes.NewsCardImg}>
            <img src="/img/newscards.png" alt="" />
            <div className={classes.NewsCardText}>İnnovasiya</div>
          </div>

          <div className={classes.NewsCardBody}>
            <h4>Artıq Apple Iphone 13 Modellərini təqdim etdi</h4>
            <p className={classes.NewsCardContent}>
              Orci, pellentesque quis nunc sollicitin vitae proin eget aliquam.
              Aliquam ornare pretium diam amet.
            </p>
          </div>
          <div className={classes.NewsCardEnd}>
            <p className={classes.DateText}>29 Avq 2021</p>
            <div>
              Bax
              <RightLineArrow className={classes.IconCard} />
            </div>
          </div>
        </div>

        <div className={classes.NewsCard}>
          <div className={classes.NewsCardImg}>
            <img src="/img/newscards.png" alt="" />
            <div className={classes.NewsCardText}>İnnovasiya</div>
          </div>

          <div className={classes.NewsCardBody}>
            <h4>Artıq Apple Iphone 13 Modellərini təqdim etdi</h4>
            <p className={classes.NewsCardContent}>
              Orci, pellentesque quis nunc sollicitin vitae proin eget aliquam.
              Aliquam ornare pretium diam amet.
            </p>
          </div>
          <div className={classes.NewsCardEnd}>
            <p className={classes.DateText}>29 Avq 2021</p>
            <div>
              Bax
              <RightLineArrow className={classes.IconCard} />
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default NewsCards;
