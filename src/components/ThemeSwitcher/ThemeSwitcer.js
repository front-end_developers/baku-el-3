import { toggleTheme } from "../../../redux/actions/main";
import { connect } from "react-redux";

import classes from "./ThemeSwitcher.module.scss";
import { Moon, Sun } from "../Icons";

function ThemeSwitcher(props) {
  const { toggleTheme, theme } = props;
  return (
    <div className={classes.ThemeSwitcher}>
      <input
        type="checkbox"
        className={classes.checkbox}
        id="checkbox"
        onChange={toggleTheme}
        checked={theme === "light" ? false : true}
      />
      <label htmlFor="checkbox" className={classes.label}>
        <Moon className={classes.MoonIcon} />
        <Sun className={classes.SunIcon} />
        <div className={classes.ball}></div>
      </label>
    </div>
  );
}

const mapStateToProps = (state) => {
  return { theme: state.layout.theme };
};

const mapDispatchToProps = {
  toggleTheme,
};

export default connect(mapStateToProps, mapDispatchToProps)(ThemeSwitcher);
