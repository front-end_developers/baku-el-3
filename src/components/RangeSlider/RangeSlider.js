import React, { useEffect, useState } from "react";
import { createTheme, ThemeProvider } from "@mui/material/styles";
import { Slider } from "@mui/material";
import classes from "./RangeSlider.module.scss";

const theme = createTheme({
  palette: {
    primary: {
      main: "#EA2427",
    },
  },
});

export default function RangeSlider() {
  const [value, setValue] = useState([300, 3000]);
  
  const handleChange = (event, newValue) => {
    setValue(newValue);
  };

  return (
    <div className={classes.PriceRange}>
      <div className={`${classes.PriceRangeInputs} PriceRangeInputs`}>
        <input
          value={value[0]}
          placeholder="Min."
          type="text"
          onChange={(event) => {
            let newState = [...value];
            newState[0] = +event.target.value;
            handleChange(event, newState);
          }}
        />
        <input
          value={value[1]}
          placeholder="Max."
          type="text"
          onChange={(event) => {
            let newState = [...value];
            newState[1] = +event.target.value;
            handleChange(event, newState);
          }}
        />
      </div>
      <div className="products-range-slider">
        <ThemeProvider theme={theme}>
          <Slider
            getAriaLabel={() => "Temperature range"}
            value={value}
            onChange={(event, newValue) => handleChange(event, newValue)}
            valueLabelDisplay="auto"
            min={3}
            max={4669}
            color="primary"
          />
        </ThemeProvider>
      </div>
    </div>
  );
}
