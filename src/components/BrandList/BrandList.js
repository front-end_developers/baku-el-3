/* eslint-disable @next/next/no-img-element */
import classes from "./BrandList.module.scss";

export default function BrandList() {
  return (
    <div className={classes.BrandList}>
      <div className={classes.BrandListContent}>
        <div className={classes.BrandListItem}>
          <img
            className={classes.BrandImage}
            src="img/samsung.png"
            alt="samsung"
            title="samsung"
          />
        </div>
        <div className={classes.BrandListItem}>
          <img
            className={classes.BrandImage}
            src="img/bosch.png"
            alt="bosch"
            title="bosch"
          />
        </div>
        <div className={classes.BrandListItem}>
          <img
            className={classes.BrandImage}
            src="img/tefal.png"
            alt="tefal"
            title="tefal"
          />
        </div>
        <div className={classes.BrandListItem}>
          <img
            className={classes.BrandImage}
            src="img/moulinex.png"
            alt="moulinex"
            title="moulinex"
          />
        </div>
        <div className={classes.BrandListItem}>
          <img
            className={classes.BrandImage}
            src="img/wmf.png"
            alt="wmf"
            title="wmf"
          />
        </div>
      </div>
    </div>
  );
}
