import React, { useState } from "react";
import classes from "./ProductCardMobile.module.scss";
import {
  CloseIcon,
  Heart,
  AddToShoppingCar,
  Scales,
  Star,
  Chat,
  DetailsIcon,
  Gift,
  DetailsQuality,
} from "../../components/Icons";
import { IconButton } from "@mui/material";
import { Collapse } from "@mui/material";

export default function ProductCardMobile() {
  return (
    // <div className={classes.ProductCardMobile}>
    //   <div className={classes.ProductImg}>
    //     <img src="/img/iphone-13.png" alt="product" />

    //     <div className={classes.Heart}>
    //       <button>
    //         <Heart />
    //       </button>
    //     </div>
    // <div className={classes.Comments}>
    //   <button>
    //     <Chat /> <span>6 rəy</span>
    //   </button>
    // </div>
    // <div className={classes.FavouriteButton}>
    //   <button>
    //     <Star /> <span>4.6</span>
    //   </button>
    // </div>
    //   </div>
    //   <div className={classes.ProductTitle}>
    //     <p>Iphone 13 Pro max, 128GB, Blu...</p>
    //   </div>
    //   <div className={classes.ProductPrice}>
    //     <div className={classes.ProductPriceLeft}>
    //       <p>3699 ₼</p>
    //       <p>3499 ₼</p>
    //     </div>
    //     <div className={classes.ProductPriceRight}>
    //       <p>
    //         290 ₼ <span>/ 15 ay</span>
    //       </p>
    //     </div>
    //   </div>
    // </div>
    <div className={classes.Product}>
      <div className={classes.Heart}>
        <button>
          <Heart />
        </button>
      </div>
      <div className={classes.ProductImg}>
        <img src="/img/image 4.png" alt="product" />
      </div>
      <div className={classes.ProductIcons}>
        <div className={classes.Comments}>
          <button>
            <Chat style={{ width: "10px" }} /> <span>6 rəy</span>
          </button>
        </div>
        <div className={classes.FavouriteButton}>
          <button>
            <Star style={{ width: "10px" }} /> <span>4.6</span>
          </button>
        </div>
      </div>
      <div className={classes.ProductTitle}>
        <p>Iphone 13 Pro max, 128GB, Blu...</p>
      </div>
    </div>
  );
}
