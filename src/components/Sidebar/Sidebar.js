/* eslint-disable @next/next/no-img-element */
import ActiveLink from "../Link/Link";
import { useEffect, useState, useRef } from "react";
import classes from "./Sidebar.module.scss";
import { toggleSidebar, toggleSidebarBtns } from "../../../redux/actions/main";
import SVG from "react-inlinesvg";
import {
  DoubleForwardVector,
  DoubleBackVector,
  Phone,
  Tv,
  Laptop,
  Freezer,
  // Iron,
  ClimateControl,
  Beauty,
  Camera,
  Gamepad,
  Bicycle,
  TableLamp,
  WineCup,
  Pan,
  BakuElLogo,
  AngleRight,
} from "../Icons";
import { connect } from "react-redux";
import Link from "next/link";
import axios from "axios";
// import Blur from "../Blur/Blur";

function Sidebar(props) {
  const {
    sidebarIsOpen,
    toggleSidebar,
    sidebar,
    toggleSidebarBtns,
    categories,
    theme,
  } = props;

  console.log("categories", !!categories);

  return (
    <div
      // ref={sidebarRef}
      className={`${classes.Sidebar} ${!sidebarIsOpen && classes.Collapse}`}
    >
      {/* =========================== SIDEBAR ============================== */}
      {/* ========================= SIDEBAR TOP ============================ */}
      <div className={classes.SidebarTop}>
        <Link href="/" activeClassName="">
          <a>
            <img
              className={classes.SidebarTopLogo}
              src="/img/BakuElLogo.png"
              title="logo"
              alt="logo"
            />
          </a>
        </Link>

        {sidebar.sidebarToggleBtns.first && (
          <button className={classes.SidebarToggle} onClick={toggleSidebar}>
            <DoubleBackVector className={classes.SidebarToggleBtn} />
          </button>
        )}

        {!sidebarIsOpen && (
          <button className={classes.SidebarTopToggle} onClick={toggleSidebar}>
            <DoubleForwardVector className={classes.SidebarToggleBtn} />
          </button>
        )}
      </div>

      {/* ================================= SIDEBAR CONTENT ============================== */}
      {/* ================================= SIDEBAR CONTENT =============================== */}
      <div className={classes.SidebarContent}>
        <ul className={classes.SidebarContentList}>
          {categories
            ? categories.map((category, idx) => {
                return (
                  <li
                    key={idx}
                    onMouseEnter={() =>
                      sidebarIsOpen && !!category.children.length
                        ? toggleSidebarBtns("first")
                        : null
                    }
                    onMouseLeave={() =>
                      sidebarIsOpen && !!category.children.length
                        ? toggleSidebarBtns("first")
                        : null
                    }
                  >
                    <Link href={`/products/${category.slug}`}>
                      <a>
                        <object
                          className={classes.ListItemIcon}
                          data={`${
                            theme === "light"
                              ? category.icon_light
                              : category.icon_dark
                          }`}
                        />
                        {/* <Laptop className={classes.ListItemIcon} /> */}
                        <span className={classes.ListItemTitle}>
                          {category.name}
                        </span>
                      </a>
                    </Link>

                    {/* =============================== API SECOND SIDEBAR ========================== */}
                    {/* =============================== API SECOND SIDEBAR ========================== */}
                    {!!category.children.length && sidebarIsOpen && (
                      <div className={classes.SecondSidebar}>
                        <div
                          onClick={toggleSidebar}
                          className={classes.FirstSubcategoryBlur}
                        ></div>

                        <div className={classes.FirstSubcategory}>
                          <div className={classes.FirstSubcategoryTop}>
                            {sidebar.sidebarToggleBtns.second && (
                              <button
                                className={classes.SubFirstSidebarToggle}
                                onClick={toggleSidebar}
                              >
                                <DoubleBackVector
                                  className={classes.SubFirstSidebarToggleBtn}
                                />
                              </button>
                            )}
                          </div>
                          <div className={classes.FirstSubcategoryContent}>
                            <ul className={classes.FirstSubcategoryContentList}>
                              {category.children.map((childCategory, idx2) => {
                                return (
                                  <li
                                    key={idx2}
                                    onMouseEnter={() =>
                                      !!childCategory.children.length
                                        ? toggleSidebarBtns("second")
                                        : null
                                    }
                                    onMouseLeave={() =>
                                      !!childCategory.children.length
                                        ? toggleSidebarBtns("second")
                                        : null
                                    }
                                  >
                                    <Link
                                      href={`/products/${category.slug}/${childCategory.slug}`}
                                      activeClassName=""
                                    >
                                      <a>
                                        <span
                                          className={
                                            classes.SecondListItemTitle
                                          }
                                        >
                                          {childCategory.name}
                                          <span
                                            className={
                                              classes.SecondListItemExpandIcon
                                            }
                                          >
                                            {!!childCategory.children
                                              .length && <AngleRight />}
                                            {/* <AngleRight /> */}
                                          </span>
                                        </span>
                                      </a>
                                    </Link>

                                    {/* ====================== API THIRD SIDEBAR ====================== */}
                                    {/* ====================== API THIRD SIDEBAR ====================== */}
                                    {!!childCategory.children.length &&
                                      sidebarIsOpen && (
                                        <div className={classes.ThirdSidebar}>
                                          <div
                                            className={
                                              classes.SecondSubcategory
                                            }
                                          >
                                            <div
                                              className={
                                                classes.SecondSubcategoryTop
                                              }
                                            >
                                              {sidebar.sidebarToggleBtns
                                                .third && (
                                                <button
                                                  className={
                                                    classes.SubSecondSidebarToggle
                                                  }
                                                  onClick={toggleSidebar}
                                                >
                                                  <DoubleBackVector
                                                    className={
                                                      classes.SubSecondSidebarToggleBtn
                                                    }
                                                  />
                                                </button>
                                              )}
                                            </div>
                                            <div
                                              className={
                                                classes.SecondSubcategoryContent
                                              }
                                            >
                                              <ul
                                                className={
                                                  classes.SecondSubcategoryContentList
                                                }
                                              >
                                                {childCategory.children.map(
                                                  (
                                                    secondChildCategory,
                                                    idx3
                                                  ) => {
                                                    return (
                                                      <li key={idx3}>
                                                        <Link
                                                          href={`/products/${category.slug}/${childCategory.slug}/${secondChildCategory.slug}`}
                                                          activeClassName=""
                                                        >
                                                          <a>
                                                            <span
                                                              className={
                                                                classes.ThirdListItemTitle
                                                              }
                                                            >
                                                              {
                                                                secondChildCategory.name
                                                              }
                                                            </span>
                                                          </a>
                                                        </Link>
                                                      </li>
                                                    );
                                                  }
                                                )}
                                              </ul>
                                            </div>
                                          </div>
                                        </div>
                                      )}
                                  </li>
                                );
                              })}
                            </ul>
                          </div>
                        </div>
                      </div>
                    )}
                  </li>
                );
              })
            : null}
        </ul>
      </div>
    </div>
  );
}

const mapStateToProps = (state) => {
  return {
    sidebarIsOpen: state.layout.sidebar.sidebarIsOpen,
    sidebar: state.layout.sidebar,
    theme: state.layout.theme,
  };
};

const mapDispatchToProps = {
  toggleSidebar,
  toggleSidebarBtns,
};

export default connect(mapStateToProps, mapDispatchToProps)(Sidebar);
