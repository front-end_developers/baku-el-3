import classes from "./ProductEvent.module.scss";
import ActiveLink from "../Link/Link";
import ProductCard from "../ProductCard/ProductCard";

const ProductEvent = () => {
  return (
    <div className={classes.SectionTwo}>
      {/* <div className="container p-0"> */}
      <div className={classes.SectionTwoTitle}>
        <h2>Özəl təkliflər</h2>
        <span>Alışverişinizi tamamlayın</span>
      </div>
      <div className={classes.SectionTwoInner}>
        <ProductCard />
        <ProductCard />
        <ProductCard />
        <ProductCard />
        <ProductCard />
      </div>
    </div>
  );
};

export default ProductEvent;
