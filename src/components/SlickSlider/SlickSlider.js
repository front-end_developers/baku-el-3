/* eslint-disable @next/next/no-img-element */
/* eslint-disable jsx-a11y/alt-text */

import React, { Component, useRef } from "react";
import Slider from "react-slick";
import { Container, Row, Col } from "react-bootstrap";
import classes from "./slick.module.scss";
import Link from "next/link";
import { styled } from "@mui/material";
import { MdWest, MdEast } from "react-icons/md";

export default function SimpleSlider() {
  const customeSlider = useRef();

  const gotoPrev = () => {
    customeSlider.current.slickPrev();
  };

  const gotoNext = () => {
    customeSlider.current.slickNext();
  };

  const PrevBtn = styled("button")({
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    width: "50px",
    height: "50px",
    background: "#EDEDED",
    border: "none",
    borderRadius: "16px",

    "@media(max-width: 768px)": {
      width: "30px",
      height: "30px",
      borderRadius: "10px",
    },
  });

  const NextBtn = styled("button")({
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    width: "50px",
    height: "50px",
    background: "#333333",
    color: "white",
    border: "none",
    borderRadius: "16px",

    "@media(max-width: 768px)": {
      width: "30px",
      height: "30px",
      borderRadius: "10px",
    },
  });

  const settings = {
    dots: true,
    infinite: false,
    speed: 500,
    slidesToShow: 1,
    slidesToScroll: 1,
    arrows: false,
    responsive: [
      {
        breakpoint: 1200,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1,
          infinite: true,
          dots: false,
        },
      },
      {
        breakpoint: 1000,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1,
          infinite: true,
          dots: false,
        },
      },
      {
        breakpoint: 768,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1,
          infinite: true,
          dots: false,
        },
      },
      {
        breakpoint: 475,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1,
          infinite: true,
          dots: false,
        },
      },
    ],
  };
  return (
    <div className={`${classes.SlickSlider} SlickSlider`}>
      <Slider {...settings} ref={customeSlider}>
        <div>
          <div className="slick-slide-content">
            <div className="slick-slide-content-img">
              <img src="/img/NewsMain.png" />
            </div>
            <div className="slick-slide-content-text">
              <div className="slick-slide-content-text-date">29 Avq 2021</div>
              <h2 className="slick-slide-content-text-title">
                Mebel, qab-qacaq, tekstil bir sözlə eviniz üçün hər şey...
              </h2>
              <p className="slick-slide-content-text-body">
                “Baku Electronics” bəzi mağazalarında müştərilərin rahatlığını
                nəzərə alaraq “home konsepti” yaradıb.
              </p>
              <div className="slick-slide-content-text-btn">
                <Link href="">Ətraflı bax</Link>
              </div>
            </div>
          </div>
        </div>
        <div>
          <div className="slick-slide-content">
            <div className="slick-slide-content-img">
              <img src="/img/NewsMain.png" />
            </div>
            <div className="slick-slide-content-text">
              <div className="slick-slide-content-text-date">29 Avq 2021</div>
              <h2 className="slick-slide-content-text-title">
                Mebel, qab-qacaq, tekstil bir sözlə eviniz üçün hər şey...
              </h2>
              <p className="slick-slide-content-text-body">
                “Baku Electronics” bəzi mağazalarında müştərilərin rahatlığını
                nəzərə alaraq “home konsepti” yaradıb.
              </p>
              <div className="slick-slide-content-text-btn">
                <Link href="">Ətraflı bax</Link>
              </div>
            </div>
          </div>
        </div>
      </Slider>

      <div className={classes.SlickSliderArrows}>
        <PrevBtn onClick={gotoPrev}>
          <MdWest />
        </PrevBtn>
        <NextBtn onClick={gotoNext}>
          <MdEast />
        </NextBtn>
      </div>
    </div>
  );
}
