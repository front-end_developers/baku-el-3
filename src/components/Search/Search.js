/* eslint-disable @next/next/no-img-element */
import Link from "next/link";
import { useState, useEffect } from "react";
import { Carousel } from "react-bootstrap";
import { BakuElLogo, Search, DoubleDownVector } from "../Icons";
import ActiveLink from "../Link/Link";
import { Hint } from "react-autocomplete-hint";
import { History } from "../Icons";

import classes from "./Search.module.scss";

const SearchPage = () => {
  const [isFocused, setIsFocused] = useState(false);
  const [text, setText] = useState("");

  const options = [
    "Apple Iphone 13 pro, 128GB, Gold",
    "Apple Iphone 12 pro max, 128GB, Gold",
    "Apple Iphone 12, 128GB, Silver",
    // "Apple Iphone 12 mini",
  ];
  return (
    <div className={classes.search}>
      <Carousel
        controls={false}
        indicators={false}
        className={classes.carousel}
      >
        <Carousel.Item interval={4000} className={classes.carousel_item}>
          <img
            className={`${classes.carousel_img} d-block`}
            src="/img/searchSlides/slide_1.png"
            alt="Second slide"
          />
        </Carousel.Item>
        {/* <Carousel.Item interval={4000} className={classes.carousel_item}>
          <img
            className={`${classes.carousel_img} d-block h-90`}
            src="/img/searchSlides/slide_2.png"
            alt="Second slide"
          />
        </Carousel.Item>
        <Carousel.Item interval={4000} className={classes.carousel_item}>
          <img
            className={`${classes.carousel_img} d-block h-90`}
            src="/img/searchSlides/slide_3.png"
            alt="Second slide"
          />
        </Carousel.Item> */}
      </Carousel>

      <div className={classes.search_body}>
        <div className={classes.body_top}>
          <div>
            <ActiveLink href="/">
              <a>
                <img
                  src="/img/BakuElLogo.png"
                  alt=""
                  className={classes.logo}
                />
              </a>
            </ActiveLink>
          </div>
          <div>
            <div className={classes.brand}>
              <span className={classes.brand_name}>Xioami</span>
              <span className={classes.brand_detail}>
                Daha çox Xioami məhsulları
              </span>
            </div>
          </div>
        </div>
        <div className={classes.body_middle}>
          <div
            className={`${classes.searchbar} ${
              isFocused ? classes.focused : ""
            }`}
          >
            <Hint
              options={options}
              allowTabFill
              // className={classes.inputHint}
            >
              <input
                className={classes.input}
                placeholder="Axtar..."
                value={text}
                onChange={(e) => setText(e.target.value)}
                onFocus={() => setIsFocused(true)}
                onBlur={() => setIsFocused(false)}
              />
            </Hint>
            <div className={classes.searchIcon}>
              <Search />
            </div>
            {isFocused ? (
              <div className={classes.search_dropdown}>
                {!text ? (
                  <div className={classes.search_dropdown_history}>
                    <Link href="">
                      <a>
                        <History /> Apple Iphone 13 pro, 128GB, Gold
                      </a>
                    </Link>
                    <Link href="">
                      <a>
                        <History /> Apple Iphone 12 pro max, 128GB, Gold
                      </a>
                    </Link>
                    <Link href="">
                      <a>
                        <History /> Apple Iphone 12, 128GB, Silver
                      </a>
                    </Link>
                    <Link href="">
                      <a>
                        <History /> Apple Iphone 12 mini
                      </a>
                    </Link>
                  </div>
                ) : null}

                {text ? (
                  <div className={classes.search_dropdown_autocomplete}>
                    <Link href="">
                      <a>Apple Iphone 13 pro, 128GB, Gold</a>
                    </Link>
                    <Link href="">
                      <a>Apple Iphone 12 pro max, 128GB, Gold</a>
                    </Link>
                    <Link href="">
                      <a>Apple Iphone 12, 128GB, Silver</a>
                    </Link>
                    <Link href="">
                      <a>Apple Iphone 12 mini</a>
                    </Link>
                  </div>
                ) : null}
              </div>
            ) : null}
          </div>
          {!isFocused ? (
            <div className={classes.search_links}>
              <ActiveLink href="/">
                <a>Canon 4410</a>
              </ActiveLink>
              <ActiveLink href="/">
                <a>Canon 2520 Driver</a>
              </ActiveLink>
              <ActiveLink href="/">
                <a>Nikon M45</a>
              </ActiveLink>
            </div>
          ) : null}
        </div>
        <div className={classes.body_bottom}>
          <div className={classes.down_arrow}>
            <DoubleDownVector />
          </div>
        </div>
        {/* <Hint options={options} allowTabFill>
          <input value={text} onChange={(e) => setText(e.target.value)} />
        </Hint> */}
      </div>
    </div>
  );
};
export default SearchPage;
