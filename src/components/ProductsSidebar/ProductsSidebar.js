/* eslint-disable @next/next/no-img-element */
import ActiveLink from "../Link/Link";
import classes from "./ProductsSidebar.module.scss";
import { MdKeyboardArrowDown, MdKeyboardArrowUp } from "react-icons/md";
import { ArrowLeft } from "../Icons";
import Checkbox from "../Checkbox/Checkbox";
import RangeSlider from "../RangeSlider/RangeSlider";
import ColorInputs from "../ColorInputs/ColorInput";
import SearchBar from "./SearchBar/SearchBar";
import { useState } from "react";

export default function ProductsSidebar() {
  const [showDetails, setShowDetails] = useState();

  return (
    <div className={`${classes.ProductsSidebar} ProductsSidebar`}>
      <div className={classes.ProductsSidebarTop}>
        <ActiveLink href="/">
          <a className={`${classes.ProductsSidebarTopBackToHome} ProductsSidebarTopBackToHome`}>
            <ArrowLeft />
          </a>
        </ActiveLink>
        <img
          className={classes.ProductsSidebarTopLogo}
          src="/img/BakuElLogo.png"
          alt=""
        />
      </div>
      <form
        className={classes.ProductsSidebarForm}
        onSubmit={(e) => e.preventDefault()}
      >
        <div className={classes.ProductsSidebarSubcategories}>
          <h2>Alt Kateqoriyalar</h2>
          <div>
            <div className={classes.ProductsSidebarSubcategoriesCheckbox}>
              <Checkbox />
            </div>
            <div className={classes.ProductsSidebarSubcategoriesCheckbox}>
              <Checkbox />
            </div>
            <div className={classes.ProductsSidebarSubcategoriesCheckbox}>
              <Checkbox />
            </div>
          </div>
        </div>
        <div className={classes.ProductsSidebarBrends}>
          <h2>Brend</h2>
          <div style={{ marginBottom: "24px" }}>
            <SearchBar />
          </div>
          <div className={classes.ProductsSidebarBrendsFirstGroup}>
            <div className={classes.ProductsSidebarBrendsCheckbox}>
              <Checkbox />
              <span>60 mehsul</span>
            </div>
            <div className={classes.ProductsSidebarBrendsCheckbox}>
              <Checkbox />
              <span>60 mehsul</span>
            </div>
            <div className={classes.ProductsSidebarBrendsCheckbox}>
              <Checkbox />
              <span>60 mehsul</span>
            </div>
          </div>
          <div
            className={`${
              showDetails
                ? classes.ProductsSidebarBrendsSecondGroupShow
                : classes.ProductsSidebarBrendsSecondGroup
            }`}
          >
            <div className={classes.ProductsSidebarBrendsCheckbox}>
              <Checkbox />
              <span>7 mehsul</span>
            </div>
            <div className={classes.ProductsSidebarBrendsCheckbox}>
              <Checkbox />
              <span>6 mehsul</span>
            </div>
            <div className={classes.ProductsSidebarBrendsCheckbox}>
              <Checkbox />
              <span>12 mehsul</span>
            </div>
          </div>

          <button
            style={{ marginBottom: "32px" }}
            type="button"
            onClick={() => setShowDetails(!showDetails)}
          >
            {showDetails ? (
              <div>
                Daha az
                <MdKeyboardArrowUp style={{ fontSize: "20px" }} />
              </div>
            ) : (
              <div>
                Daha çox <MdKeyboardArrowDown style={{ fontSize: "20px" }} />
              </div>
            )}
          </button>
        </div>
        <div className={classes.ProductsSidebarSubcategories}>
          <h2>Alt Kateqoriyalar</h2>
          <div>
            <div className={classes.ProductsSidebarSubcategoriesCheckbox}>
              <Checkbox />
            </div>
            <div className={classes.ProductsSidebarSubcategoriesCheckbox}>
              <Checkbox />
            </div>
            <div className={classes.ProductsSidebarSubcategoriesCheckbox}>
              <Checkbox />
            </div>
          </div>
        </div>
        <div className={classes.ProductsSidebarBrends}>
          <h2>Brend</h2>
          <div style={{ marginBottom: "15px" }}>
            <SearchBar />
          </div>
          <div className={classes.ProductsSidebarBrendsFirstGroup}>
            <div className={classes.ProductsSidebarBrendsCheckbox}>
              <Checkbox />
              <span>60 mehsul</span>
            </div>
            <div className={classes.ProductsSidebarBrendsCheckbox}>
              <Checkbox />
              <span>60 mehsul</span>
            </div>
            <div className={classes.ProductsSidebarBrendsCheckbox}>
              <Checkbox />
              <span>60 mehsul</span>
            </div>
          </div>
          <div
            className={`${
              showDetails
                ? classes.ProductsSidebarBrendsSecondGroupShow
                : classes.ProductsSidebarBrendsSecondGroup
            }`}
          >
            <div className={classes.ProductsSidebarBrendsCheckbox}>
              <Checkbox />
              <span>7 mehsul</span>
            </div>
            <div className={classes.ProductsSidebarBrendsCheckbox}>
              <Checkbox />
              <span>6 mehsul</span>
            </div>
            <div className={classes.ProductsSidebarBrendsCheckbox}>
              <Checkbox />
              <span>12 mehsul</span>
            </div>
          </div>

          <button
            style={{ marginBottom: "15px" }}
            type="button"
            onClick={() => setShowDetails(!showDetails)}
          >
            {showDetails ? (
              <div>
                Daha az
                <MdKeyboardArrowUp style={{ fontSize: "20px" }} />
              </div>
            ) : (
              <div>
                Daha çox <MdKeyboardArrowDown style={{ fontSize: "20px" }} />
              </div>
            )}
          </button>
        </div>
        <div>
          <RangeSlider />
        </div>

        <div style={{ display: "flex" }}>
          <ColorInputs />
          <ColorInputs />
          <ColorInputs />
          <ColorInputs />
          <ColorInputs />
          <ColorInputs />
        </div>
      </form>
    </div>
  );
}
