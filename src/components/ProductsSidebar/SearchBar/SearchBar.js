import classes from "./SearchBar.module.scss";
import { Search } from "../../Icons";

export default function SearchBar() {
  return (
    <div className={`${classes.SearchBar} SearchBar`}>
      <input placeholder="Axtar..." className={`${classes.SearchBarInput} SearchBarInput`} />
      <button className={classes.SearchBarButton}>
        <Search className={`${classes.SearchBarButtonIcon} SearchBarButtonIcon`} />
      </button>
    </div>
  );
}
