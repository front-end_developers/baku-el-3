import classes from "./SectionFive.module.scss";

const SectionFive = (props) => {
  const { blogData } = props;
  // console.log(blogData);
  return (
    <>
      {blogData ? (
        <div className={classes.SectionFive}>
          <div className={classes.SectionFiveContent}>
            {blogData.map((cardData, idx) => {
              return (
                <div
                  key={idx}
                  className={`${classes.SectionFiveItem} ${
                    cardData.color === "gray" ? classes.Gray : ""
                  }`}
                >
                  <div className={classes.SectionFiveItemTitle}>
                    <h2>{cardData.title}</h2>
                  </div>
                  <div className={classes.SectionFiveItemText}>
                    <p>
                      {/* Əvvəlcədən <b>Sİfarİş</b> */}
                      {cardData.body}
                    </p>
                  </div>
                  <div className={classes.SectionFiveItemButton}>
                    <a href="#">{cardData.btn}</a>
                  </div>
                  <div className={classes.SectionFiveItemImg}>
                    <img src={cardData.img} />
                  </div>
                </div>
              );
            })}
          </div>
        </div>
      ) : (
        <div className={classes.SectionFive}>
          <div className={classes.SectionFiveContent}>
            <div className={classes.SectionFiveItem}>
              <div className={classes.SectionFiveItemTitle}>
                <h2>İphone 13 Pro Max</h2>
              </div>
              <div className={classes.SectionFiveItemText}>
                <p>
                  Əvvəlcədən <b>Sİfarİş</b>
                </p>
              </div>
              <div className={classes.SectionFiveItemButton}>
                <a href="#">Sifariş et</a>
              </div>
              <div className={classes.SectionFiveItemImg}>
                <img src="/img/iphone.png" />
              </div>
            </div>

            <div className={classes.SectionFiveItem}>
              <div className={classes.SectionFiveItemTitle}>
                <h2>İphone 13 Pro Max</h2>
              </div>
              <div className={classes.SectionFiveItemText}>
                <p>
                  Əvvəlcədən <b>Sİfarİş</b>
                </p>
              </div>
              <div className={classes.SectionFiveItemButton}>
                <a href="#">Sifariş et</a>
              </div>
              <div className={classes.SectionFiveItemImg}>
                <img src="/img/iphone.png" />
              </div>
            </div>
          </div>
        </div>
      )}
    </>
  );
};

export default SectionFive;
