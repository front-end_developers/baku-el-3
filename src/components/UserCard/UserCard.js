/* eslint-disable @next/next/no-img-element */
import { Star, Chat, AddToShoppingCar, Heart, Manat } from "../Icons";
import classes from "./UserCard.module.scss";

export default function UserCard() {
  return (
    <div className={classes.UserCard}>
      <div className={classes.UserCardImg}>
        <img src="/img/user-card-1.png" />
      </div>
      <div className={classes.UserCardOthers}>
        {/* comment and ratings */}
        <div className={classes.UserCardReviews}>
          <span className={classes.UserCardReviewsStar}>
            <Star />
            <span className={classes.UserCardReviewsStarData}>4.6</span>
          </span>
          <span className={classes.UserCardReviewsComments}>
            <Chat />
            <span className={classes.UserCardReviewsCommentsData}>6 rey</span>
          </span>
        </div>
        {/* title */}
        <div className={classes.UserCardTitle}>
          <span>Iphone 13 Pro max</span>
        </div>
        {/* prices */}
        <div className={classes.UserCardPrices}>
          <span className={classes.UserCardPricesCash}>499 <Manat/></span>
          <span className={classes.UserCardPricesDivider}></span>
          <span className={classes.UserCardPricesMonthly}>
            <span>90 <Manat/></span>
            <span>/ 15 ay</span>
          </span>
        </div>
        {/* buttons */}
        <div className={classes.UserCardButtons}>
          <button>
            <AddToShoppingCar />
          </button>
          <button>
            <Heart />
          </button>
        </div>
      </div>
    </div>
  );
}
