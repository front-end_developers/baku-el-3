import { useState } from "react";
import classes from "./NewsVideoCard.module.scss"
import { RightLineArrow } from "../Icons";
import { Container, Row, Col } from "react-bootstrap";
import { PlayIconNew } from "../../../src/components/Icons";
import ReactPlayer from "react-player";
import { BsPauseCircle } from "react-icons/bs";
import { MdOutlineVisibility } from "react-icons/md";
import { Button } from "@mui/material";

const NewsVideoCard = () => {
  const [play, setPlay] = useState(false);

  const playHandler = () => {
    setPlay(!play);
  };

  return (
    <div className={classes.NewsVideoCardsContainer}>
      <div className={classes.NewsVideoCardsContainerTitle}>
        <p className={classes.NewsVideoCardsContainerTitleTop}>Xəbərlər</p>
        <h1 className={classes.NewsVideoCardsContainerTitleBottom}>
          Ən son yeniliklər
        </h1>
      </div>
      <div className={classes.NewsVideoCardsContainerItems}>
        <div className={classes.NewsCard}>
          <div className={classes.NewsCardImg}>
            <div className={classes.Video}>
              <ReactPlayer
                playing={play}
                onPause={() => {
                  setPlay(false);
                }}
                onEnded={() => {
                  setPlay(false);
                }}
                controls={play}
                width={"100%"}
                height={"100%"}
                style={{ width: "100%", color: "red" }}
                url="/Video/Music.mp4"
              />
              {!play && (
                <div
                  className={
                    play
                      ? classes.VideoControls
                      : `${classes.VideoControls} ${classes.OnPause}`
                  }
                >
                  <div className={classes.PlayIcon}>
                    <button onClick={playHandler}>
                      {play ? <BsPauseCircle /> : <PlayIconNew />}
                    </button>
                  </div>
                </div>
              )}
              {/* {!play && (
                    <div className={classes.Text}>
                      <div className={classes.TextTitle}>
                        <h2>İnnovasiya</h2>
                      </div>
                      <div className={classes.TextMain}>
                        <p>Apple yeni İphone 13 modellərini təqdim etdi</p>
                      </div>
                    </div>
                  )} */}
            </div>
            <div className={classes.NewsCardText}>İnnovasiya</div>
          </div>

          <div className={classes.NewsCardBody}>
            <h4>Artıq Apple Iphone 13 Modellərini təqdim etdi</h4>
          </div>
          <div className={classes.NewsCardEnd}>
            <p className={classes.DateText}>29 Avq 2021</p>
            <div>
              Bax
              <RightLineArrow className={classes.IconCard} />
            </div>
          </div>
        </div>
        <div className={classes.NewsCard}>
          <div className={classes.NewsCardImg}>
            <div className={classes.Video}>
              <ReactPlayer
                playing={play}
                onPause={() => {
                  setPlay(false);
                }}
                onEnded={() => {
                  setPlay(false);
                }}
                controls={play}
                width={"100%"}
                height={"100%"}
                style={{ width: "100%" }}
                url="/Video/Music.mp4"
              />
              {!play && (
                <div
                  className={
                    play
                      ? classes.VideoControls
                      : `${classes.VideoControls} ${classes.OnPause}`
                  }
                >
                  <div className={classes.PlayIcon}>
                    <button onClick={playHandler}>
                      {play ? <BsPauseCircle /> : <PlayIconNew />}
                    </button>
                  </div>
                </div>
              )}
              {/* {!play && (
                    <div className={classes.Text}>
                      <div className={classes.TextTitle}>
                        <h2>İnnovasiya</h2>
                      </div>
                      <div className={classes.TextMain}>
                        <p>Apple yeni İphone 13 modellərini təqdim etdi</p>
                      </div>
                    </div>
                  )} */}
            </div>
            <div className={classes.NewsCardText}>İnnovasiya</div>
          </div>

          <div className={classes.NewsCardBody}>
            <h4>Artıq Apple Iphone 13 Modellərini təqdim etdi</h4>
          </div>
          <div className={classes.NewsCardEnd}>
            <p className={classes.DateText}>29 Avq 2021</p>
            <div>
              Bax
              <RightLineArrow className={classes.IconCard} />
            </div>
          </div>
        </div>
        <div className={classes.NewsCard}>
          <div className={classes.NewsCardImg}>
            <div className={classes.Video}>
              <ReactPlayer
                playing={play}
                onPause={() => {
                  setPlay(false);
                }}
                onEnded={() => {
                  setPlay(false);
                }}
                controls={play}
                width={"100%"}
                height={"100%"}
                style={{ width: "100%" }}
                url="/Video/Music.mp4"
              />
              {!play && (
                <div
                  className={
                    play
                      ? classes.VideoControls
                      : `${classes.VideoControls} ${classes.OnPause}`
                  }
                >
                  <div className={classes.PlayIcon}>
                    <button onClick={playHandler}>
                      {play ? <BsPauseCircle /> : <PlayIconNew />}
                    </button>
                  </div>
                </div>
              )}
              {/* {!play && (
                    <div className={classes.Text}>
                      <div className={classes.TextTitle}>
                        <h2>İnnovasiya</h2>
                      </div>
                      <div className={classes.TextMain}>
                        <p>Apple yeni İphone 13 modellərini təqdim etdi</p>
                      </div>
                    </div>
                  )} */}
            </div>
            <div className={classes.NewsCardText}>İnnovasiya</div>
          </div>

          <div className={classes.NewsCardBody}>
            <h4>Artıq Apple Iphone 13 Modellərini təqdim etdi</h4>
          </div>
          <div className={classes.NewsCardEnd}>
            <p className={classes.DateText}>29 Avq 2021</p>
            <div>
              Bax
              <RightLineArrow className={classes.IconCard} />
            </div>
          </div>
        </div>
        <div className={classes.NewsCard}>
          <div className={classes.NewsCardImg}>
            <div className={classes.Video}>
              <ReactPlayer
                playing={play}
                onPause={() => {
                  setPlay(false);
                }}
                onEnded={() => {
                  setPlay(false);
                }}
                controls={play}
                width={"100%"}
                height={"100%"}
                style={{ width: "100%" }}
                url="/Video/Music.mp4"
              />
              {!play && (
                <div
                  className={
                    play
                      ? classes.VideoControls
                      : `${classes.VideoControls} ${classes.OnPause}`
                  }
                >
                  <div className={classes.PlayIcon}>
                    <button onClick={playHandler}>
                      {play ? <BsPauseCircle /> : <PlayIconNew />}
                    </button>
                  </div>
                </div>
              )}
              {/* {!play && (
                    <div className={classes.Text}>
                      <div className={classes.TextTitle}>
                        <h2>İnnovasiya</h2>
                      </div>
                      <div className={classes.TextMain}>
                        <p>Apple yeni İphone 13 modellərini təqdim etdi</p>
                      </div>
                    </div>
                  )} */}
            </div>
            <div className={classes.NewsCardText}>İnnovasiya</div>
          </div>

          <div className={classes.NewsCardBody}>
            <h4>Artıq Apple Iphone 13 Modellərini təqdim etdi</h4>
          </div>
          <div className={classes.NewsCardEnd}>
            <p className={classes.DateText}>29 Avq 2021</p>
            <div>
              Bax
              <RightLineArrow className={classes.IconCard} />
            </div>
          </div>
        </div>
        <div className={classes.NewsCard}>
          <div className={classes.NewsCardImg}>
            <div className={classes.Video}>
              <ReactPlayer
                playing={play}
                onPause={() => {
                  setPlay(false);
                }}
                onEnded={() => {
                  setPlay(false);
                }}
                controls={play}
                width={"100%"}
                height={"100%"}
                style={{ width: "100%" }}
                url="/Video/Music.mp4"
              />
              {!play && (
                <div
                  className={
                    play
                      ? classes.VideoControls
                      : `${classes.VideoControls} ${classes.OnPause}`
                  }
                >
                  <div className={classes.PlayIcon}>
                    <button onClick={playHandler}>
                      {play ? <BsPauseCircle /> : <PlayIconNew />}
                    </button>
                  </div>
                </div>
              )}
              {/* {!play && (
                    <div className={classes.Text}>
                      <div className={classes.TextTitle}>
                        <h2>İnnovasiya</h2>
                      </div>
                      <div className={classes.TextMain}>
                        <p>Apple yeni İphone 13 modellərini təqdim etdi</p>
                      </div>
                    </div>
                  )} */}
            </div>
            <div className={classes.NewsCardText}>İnnovasiya</div>
          </div>

          <div className={classes.NewsCardBody}>
            <h4>Artıq Apple Iphone 13 Modellərini təqdim etdi</h4>
          </div>
          <div className={classes.NewsCardEnd}>
            <p className={classes.DateText}>29 Avq 2021</p>
            <div>
              Bax
              <RightLineArrow className={classes.IconCard} />
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default NewsVideoCard;
