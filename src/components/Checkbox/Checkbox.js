import Link from "next/link";
import classes from "./Checkbox.module.scss";

export default function Checkbox() {
  return (
    <Link href="/">
      <label className={`${classes.Container} Container`}>
        <span>checkbox</span>
        <input type="checkbox" />
        <div className={classes.Checkmark}></div>
      </label>
    </Link>
  );
}
