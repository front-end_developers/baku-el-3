import React from "react";
import classes from "./UserProfileAside.module.scss";
import {
  User,
  Bag,
  UserLatest,
  Location,
  UserCarts,
  UserBonuses,
  Login,
  ArrowLeft,
} from "../Icons";
import ActiveLink from "../Link/Link";
import { logout } from "../../../redux/actions/main";
import { connect } from "react-redux";

const UserProfileAside = (props) => {
  const { logout } = props;
  return (
    <div className={classes.UserProfileSidebar}>
      <div
        className={classes.UserProfileSidebarForm}
        // onSubmit={(e) => e.preventDefault()}
      >
        <aside className={classes.SectionAside}>
          <div className={classes.SectionAsideTop}>
            {/* icon wrapper */}
            <div className={classes.SectionAsideTopIcon}>
              <User />
            </div>

            {/* user name & mail */}
            <div className={classes.SectionAsideTopBody}>
              <h2>Ulviyya Imanova</h2>
              <span>ulviyaaimanova@gmail.com</span>
            </div>
          </div>
          <div className={classes.SectionAsideSeparator}></div>
          <div className={classes.SectionAsideBody}>
            <div className={classes.SectionAsideBodyItem}>
              <ActiveLink
                href="/user-profile/orders"
                activeClassName={classes.activeLink}
              >
                <a>
                  <span>
                    <Bag />
                  </span>
                  <span>Sifarislerim</span>
                </a>
              </ActiveLink>
            </div>
            <div className={classes.SectionAsideBodyItem}>
              <ActiveLink
                href="/user-profile/latest"
                activeClassName={classes.activeLink}
              >
                <a>
                  <span>
                    <UserLatest />
                  </span>
                  <span>Ən son baxdıqlarım</span>
                </a>
              </ActiveLink>
            </div>
            <div className={classes.SectionAsideBodyItem}>
              <ActiveLink
                href="/user-profile/location"
                activeClassName={classes.activeLink}
              >
                <a>
                  <span>
                    <Location />
                  </span>
                  <span>Ünvanlarım</span>
                </a>
              </ActiveLink>
            </div>
            <div className={classes.SectionAsideBodyItem}>
              <ActiveLink
                href="/user-profile/carts"
                activeClassName={classes.activeLink}
              >
                <a>
                  <span>
                    <UserCarts />
                  </span>
                  <span>Kartlarım</span>
                </a>
              </ActiveLink>
            </div>
            <div className={classes.SectionAsideBodyItem}>
              <ActiveLink
                href="/user-profile/bonuses"
                activeClassName={classes.activeLink}
              >
                <a>
                  <span>
                    <UserBonuses />
                  </span>
                  <span>Bonuslarım</span>
                </a>
              </ActiveLink>
            </div>
            <div className={classes.SectionAsideBodyItem}>
              <ActiveLink
                href="/user-profile/personal-info"
                activeClassName={classes.activeLink}
              >
                <a>
                  <span>
                    <User />
                  </span>
                  <span>Şəxsi məlumatlarım</span>
                </a>
              </ActiveLink>
            </div>
            <div className={classes.SectionAsideBodyItem}>
              <ActiveLink
                href="/user-profile/apply"
                activeClassName={classes.activeLink}
              >
                <a>
                  <span>
                    <User />
                  </span>
                  <span>Müraciət et</span>
                </a>
              </ActiveLink>
            </div>
            <div className={classes.SectionAsideBodyItem}>
              <button onClick={logout}>
                <span>
                  <Login />
                </span>
                <span>Çıxış</span>
              </button>
              {/* <ActiveLink href="/user-profile/orders" activeClassName="">
                <a>
                  <span>
                    <Login />
                  </span>
                  <span>Çıxış</span>
                </a>
              </ActiveLink> */}
            </div>
          </div>
        </aside>
      </div>
    </div>
  );
};

// export default UserProfileAside

// export default Index;
// const mapStateToProps = (state) => {
//   return {
//     email: state.auth.verifyData.email,
//   };
// };

const mapDispatchToProps = {
  logout,
};

export default connect(null, mapDispatchToProps)(UserProfileAside);
