import * as React from 'react';
import Slider from '@mui/material/Slider';
import {createTheme, ThemeProvider} from "@mui/material/styles";
import {useState} from "react";

const theme = createTheme({
    palette: {
        primary: {
            main: "#EA2427",
        },
    },
});

export default function SliderSizes({defaultValue, min, max, value, handleChange}) {

    const [number, setNumber] = useState(1);

    const handleChangeNumber = (event, newValue) => {
        setNumber(newValue);
    };

    return (
        <ThemeProvider theme={theme}>
            <Slider
                defaultValue={defaultValue}
                value={value}
                onChange={handleChange}
                min={min}
                max={max}
                aria-label="Default"
                valueLabelDisplay="auto"
            />
        </ThemeProvider>
    )

}