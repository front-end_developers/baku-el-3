import * as React from "react";
import PropTypes from "prop-types";
import Tabs from "@mui/material/Tabs";
import Tab from "@mui/material/Tab";
import Typography from "@mui/material/Typography";
import Box from "@mui/material/Box";
import { Button } from "@mui/material";
import classes from "./ProductTab.module.scss";
import { PaymentDiscount1, Manat } from "../Icons";
import RangeSlider from "./RangeSlider";
import { useState } from "react";

function TabPanel(props) {
  const { children, value, index, ...other } = props;

  return (
    <div
      role="tabpanel"
      hidden={value !== index}
      id={`simple-tabpanel-${index}`}
      aria-labelledby={`simple-tab-${index}`}
      {...other}
    >
      {value === index && (
        <Box sx={{ p: 3 }}>
          <Typography>{children}</Typography>
        </Box>
      )}
    </div>
  );
}

TabPanel.propTypes = {
  children: PropTypes.node,
  index: PropTypes.number.isRequired,
  value: PropTypes.number.isRequired,
};

function a11yProps(index) {
  return {
    id: `simple-tab-${index}`,
    "aria-controls": `simple-tabpanel-${index}`,
  };
}

const Birkart = () => {
  return (
    <div className={classes.Tab}>
      <span>Birkart</span>{" "}
      <span>
        <img src="/img/birkart.png" alt="" />
      </span>
    </div>
  );
};

const Tamkart = () => {
  return (
    <div className={classes.Tab}>
      <span>TamKart</span>{" "}
      <span>
        <img src="/img/tamkart.png" alt="" />
      </span>
    </div>
  );
};

const Other = () => {
  return (
    <div className={classes.Tab}>
      <span>Nisyə al</span>{" "}
      <span>
        <PaymentDiscount1 />
      </span>
    </div>
  );
};

export default function BasicTabs() {
  const [value, setValue] = React.useState(0);
  const [number, setNumber] = useState(1);
  const [initialPay, setInitialPay] = useState(1000);

  const handleChange = (event, newValue) => {
    setValue(newValue);
  };

  const handleChangeNumber = (event, newValue) => {
    setNumber(newValue);
  };

  const handleChangePay = (event, newValue) => {
    setInitialPay(newValue);
  };

  return (
    <div>
      <Box sx={{ borderBottom: 1, borderColor: "divider" }}>
        <Tabs
          value={value}
          onChange={handleChange}
          aria-label="basic tabs example"
        >
          <Tab icon={<Birkart />} {...a11yProps(0)} />
          <Tab icon={<Tamkart />} {...a11yProps(1)} />
          <Tab icon={<Other />} {...a11yProps(2)} />
        </Tabs>
      </Box>
      <TabPanel value={value} index={0}>
        <div className="row">
          <div className="col-6">
            <div className={`${classes.LoanTerm} loan-term`}>
              <p className={`${classes.LoanTermTitle} LoanTermTitle`}>Müddət</p>
              <p className={`${classes.LoanTermText} LoanTermText`}>
                {number} ay
              </p>
              <RangeSlider
                defaultValue={1}
                min={1}
                max={18}
                value={number}
                handleChange={handleChangeNumber}
              />
            </div>
          </div>
          <div className="col-6">
            <div className={`${classes.InitialPayment} initial-payment`}>
              <p className={`${classes.InitialPaymentTitle} InitialPaymentTitle`}>İlkin ödəniş</p>
              <p className={`${classes.InitialPaymentText} InitialPaymentText`}>
                {initialPay} <Manat />
              </p>
              <RangeSlider
                defaultValue={1}
                min={1000}
                max={2000}
                value={initialPay}
                handleChange={handleChangePay}
              />
            </div>
          </div>
        </div>
        <div className="row">
          <div className="col-12 col-lg-8">
            <div className={`${classes.BottomSection} BottomSection`}>
              <div className={classes.BottomSectionTitle}>
                <p>Aylıq ödəniş:</p>
              </div>
              <div className={classes.BottomSectionText}>
                <div className={classes.BottomSectionTextLeft}>
                  <span>
                    2969 <Manat />
                  </span>
                </div>
                <div className={classes.BottomSectionTextRight}>
                  <span>Kommissiya: 0%</span>
                  <span>Kredit faizi: 0%</span>
                </div>
              </div>
            </div>
          </div>
          <div className="col-12 col-lg-4">
            <div className={classes.BottomBtn}>
              <Button>Hissə-hissə al</Button>
            </div>
          </div>
        </div>
      </TabPanel>
      <TabPanel value={value} index={1}>
        <div className="row">
          <div className="col-6">
            <div className={`${classes.LoanTerm} loan-term`}>
              <p className={`${classes.LoanTermTitle} LoanTermTitle`}>Müddət</p>
              <p className={`${classes.LoanTermText} LoanTermText`}>
                {number} ay
              </p>
              <RangeSlider
                defaultValue={1}
                min={1}
                max={18}
                value={number}
                handleChange={handleChangeNumber}
              />
            </div>
          </div>
          <div className="col-6">
            <div className={`${classes.InitialPayment} initial-payment`}>
              <p className={`${classes.InitialPaymentTitle} InitialPaymentTitle`}>İlkin ödəniş</p>
              <p className={`${classes.InitialPaymentText} InitialPaymentText`}>
                {initialPay} <Manat />
              </p>
              <RangeSlider
                defaultValue={1}
                min={1000}
                max={2000}
                value={initialPay}
                handleChange={handleChangePay}
              />
            </div>
          </div>
        </div>
        <div className="row">
          <div className="col-12 col-lg-8">
            <div className={`${classes.BottomSection} BottomSection`}>
              <div className={classes.BottomSectionTitle}>
                <p>Aylıq ödəniş:</p>
              </div>
              <div className={classes.BottomSectionText}>
                <div className={classes.BottomSectionTextLeft}>
                  <span>
                    2969 <Manat />
                  </span>
                </div>
                <div className={classes.BottomSectionTextRight}>
                  <span>Kommissiya: 0%</span>
                  <span>Kredit faizi: 0%</span>
                </div>
              </div>
            </div>
          </div>
          <div className="col-12 col-lg-4">
            <div className={classes.BottomBtn}>
              <Button>Hissə-hissə al</Button>
            </div>
          </div>
        </div>
      </TabPanel>
      <TabPanel value={value} index={2}>
        <div className="row">
          <div className="col-6">
            <div className={`${classes.LoanTerm} loan-term`}>
              <p className={`${classes.LoanTermTitle} LoanTermTitle`}>Müddət</p>
              <p className={`${classes.LoanTermText} LoanTermText`}>
                {number} ay
              </p>
              <RangeSlider
                defaultValue={1}
                min={1}
                max={18}
                value={number}
                handleChange={handleChangeNumber}
              />
            </div>
          </div>
          <div className="col-6">
            <div className={`${classes.InitialPayment} initial-payment`}>
              <p className={`${classes.InitialPaymentTitle} InitialPaymentTitle`}>İlkin ödəniş</p>
              <p className={`${classes.InitialPaymentText} InitialPaymentText`}>
                {initialPay} <Manat />
              </p>
              <RangeSlider
                defaultValue={1}
                min={1000}
                max={2000}
                value={initialPay}
                handleChange={handleChangePay}
              />
            </div>
          </div>
        </div>
        <div className="row">
          <div className="col-12 col-lg-8">
            <div className={`${classes.BottomSection} BottomSection`}>
              <div className={classes.BottomSectionTitle}>
                <p>Aylıq ödəniş:</p>
              </div>
              <div className={classes.BottomSectionText}>
                <div className={classes.BottomSectionTextLeft}>
                  <span>
                    2969 <Manat />
                  </span>
                </div>
                <div className={classes.BottomSectionTextRight}>
                  <span>Kommissiya: 0%</span>
                  <span>Kredit faizi: 0%</span>
                </div>
              </div>
            </div>
          </div>
          <div className="col-12 col-lg-4">
            <div className={classes.BottomBtn}>
              <Button>Hissə-hissə al</Button>
            </div>
          </div>
        </div>
      </TabPanel>

    </div>
  );
}
