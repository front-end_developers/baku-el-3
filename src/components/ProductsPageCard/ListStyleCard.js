import React from "react";
import classes from "./ProductPageCard.module.scss";
import { Star, Chat, AddToShoppingCar, Heart, Manat } from "../Icons";

export default function ListStyleCard() {
  return (
    <div className={classes.ListStyleCard}>
      <div className={classes.ListStyleCardBody}>
        <div className={classes.ListStyleCardBodyItem}>
          <div className={classes.ListStyleCardImg}>
            <img src="/img/iphone-13.png" />
          </div>
          <div>
            <div className={classes.ListStyleCardInfo}>
              <p className={classes.ListStyleCardInfoInner}>
                <span>
                  <Star />
                </span>
                <span>4.6</span>
              </p>
              <p className={classes.ListStyleCardInfoInner}>
                <span>
                  <Chat />
                </span>
                <span>4.6</span>
              </p>
            </div>
            <div className={classes.ListStyleCardTitle}>
              <h2>Iphone 13 Pro max, 128GB</h2>
            </div>
            <div className={classes.ListStyleCardPrice}>
              <div className={classes.ListStyleCardPriceLeft}>
                <p>3699</p>
                <p>
                  3499 <Manat />
                </p>
              </div>
              <div className={classes.ListStyleCardPriceRight}>
                <p>
                  290 <Manat />
                  <span>/ 15 ay</span>
                </p>
              </div>
            </div>
          </div>
        </div>
        <div className={classes.ListStyleCardButtons}>
          <button className={classes.ListStyleCardButtonsCartBtn}>
            <AddToShoppingCar />
          </button>
          <button className={classes.ListStyleCardButtonsFavBtn}>
            <Heart />
          </button>
        </div>
      </div>
    </div>
  );
}
