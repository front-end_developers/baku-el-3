import React from "react";
import classes from "./ProductPageCard.module.scss";
import { Star, Chat, AddToShoppingCar, Heart, Manat } from "../Icons";

export default function ProductsPageCard() {
  return (
    <div className={classes.Card}>
      <div className={classes.CardBody}>
        <div className={classes.CardImg}>
          <img src="/img/iphone-13.png" />
          <div className={classes.CardImgItem}>
            <p className={classes.DiscountInfo}>son 1 ədəd</p>
          </div>
        </div>
        <div className={classes.CardInfo}>
          <p className={classes.CardInfoInner}>
            <span>
              <Star />
            </span>
            <span>4.6</span>
          </p>
          <p className={classes.CardInfoInner}>
            <span>
              <Chat />
            </span>
            <span>4.6</span>
          </p>
        </div>
        <div className={classes.CardTitle}>
          <h2>Iphone 13 Pro max, 128GB</h2>
        </div>
        <div className={classes.CardPrice}>
          <div className={classes.CardPriceLeft}>
            <p>3699</p>
            <p>3499 <Manat/></p>
          </div>
          <div className={classes.CardPriceRight}>
            <p>
              290 <Manat/><span>/ 15 ay</span>
            </p>
          </div>
        </div>
        <div className={classes.CardButtons}>
          <button className={classes.CardButtonsCartBtn}>
            <AddToShoppingCar />
          </button>
          <button className={classes.CardButtonsFavBtn}>
            <Heart />
          </button>
        </div>
      </div>
    </div>
  );
}
