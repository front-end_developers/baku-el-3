import classes from "./BrandTopHeader.module.scss";
import {
  ClipboardIcon,
  ComputerLogo,
  MenuLogo,
  MoneyCaseLogo,
  ProfileArrow,
  ProfileHeart,
  Smartphone,
  SmartwatchLogo,
} from "../../components/Icons";
import Link from "next/link";
import { Button, ButtonBase } from "@mui/material";
import ActiveLink from "../Link/Link";

const BrandTopHeader = () => {
  return (
    <>
      <div className={classes.BrandTopHeader}>
        <div className={classes.BrandTopHeaderCard}>
          <div className={classes.BrandTopHeaderCardTop}>
            <div className={classes.BrandTopHeaderCardTopLeft}>
              <div className={classes.BrandTopHeaderCardTopLeftLogo}>
                <img src="/img/AppleImage.png" alt="" />
              </div>
              <div className={classes.BrandTopHeaderCardTopLeftText}>
                <div className={classes.BrandTopHeaderCardTopLeftTextTitle}>
                  <span>Apple</span>
                  <Link href="/">
                    <Button>
                      <ProfileArrow />
                    </Button>
                  </Link>
                </div>
                <div
                  className={classes.BrandTopHeaderCardTopLeftTextInformation}
                >
                  <div
                    className={
                      classes.BrandTopHeaderCardTopLeftTextInformationLeft
                    }
                  >
                    <span>
                      <ProfileHeart />
                    </span>
                    <span>8209 izləyici</span>
                  </div>
                  <div
                    className={
                      classes.BrandTopHeaderCardTopLeftTextInformationRight
                    }
                  >
                    <span>
                      <ClipboardIcon />
                    </span>
                    <span>203 Məhsul</span>
                  </div>
                </div>
                <div
                  className={classes.BrandTopHeaderCardTopLeftTextDescription}
                >
                  Ante amet, risus amet turpis sapien, commodo. Fusce pulvinar
                  amet posuere a, diam viverra in.
                </div>
              </div>
            </div>
            <div className={classes.BrandTopHeaderCardTopRight}>
              <div className={classes.BrandTopHeaderCardTopRightTitle}>
                BLACK FRİDAY-də UNİKAL TƏKLİFLƏR!
              </div>
              <div className={classes.BrandTopHeaderCardTopRightInformation}>
                <div
                  className={classes.BrandTopHeaderCardTopRightInformationDay}
                >
                  <span>04</span>
                  <span>Gün</span>
                </div>
                <div
                  className={classes.BrandTopHeaderCardTopRightInformationHour}
                >
                  <span>22</span>
                  <span>Saat</span>
                </div>
                <div
                  className={
                    classes.BrandTopHeaderCardTopRightInformationMinutes
                  }
                >
                  <span>18</span>
                  <span>Dəqiqə</span>
                </div>
              </div>
            </div>
          </div>
          <div className={classes.BrandTopHeaderCardBottom}>
            <div className={classes.BrandTopHeaderCardBottomNavLinks}>
              <div className={classes.BrandTopHeaderCardBottomNavLinksHome}>
                <ActiveLink href="/brands" activeClassName={classes.active}>
                  <a>Ana səhifə</a>
                </ActiveLink>
              </div>
              <div
                className={classes.BrandTopHeaderCardBottomNavLinksAllProducts}
              >
                <ActiveLink href="/brands/all-products">
                  <a>Bütün məhsullar</a>
                </ActiveLink>
              </div>
            </div>
          </div>
        </div>
        <div className={classes.BrandTopHeaderNavFilter}>
          <div className={classes.BrandTopHeaderNavFilterItem}>
            <div className={classes.BrandTopHeaderNavFilterItemLeft}>
              <span>
                <Smartphone className={classes.logo} />
              </span>
            </div>
            <div className={classes.BrandTopHeaderNavFilterItemRight}>
              <Link href="/">Smartfonlar</Link>
              <Link href="/">100 məhsul</Link>
            </div>
          </div>

          <div className={classes.BrandTopHeaderNavFilterItem}>
            <div className={classes.BrandTopHeaderNavFilterItemLeft}>
              <span>
                <ComputerLogo className={classes.logo} />
              </span>
            </div>
            <div className={classes.BrandTopHeaderNavFilterItemRight}>
              <Link href="/">Noutbuklar</Link>
              <Link href="/">100 məhsul</Link>
            </div>
          </div>

          <div className={classes.BrandTopHeaderNavFilterItem}>
            <div className={classes.BrandTopHeaderNavFilterItemLeft}>
              <span>
                <SmartwatchLogo className={classes.logo} />
              </span>
            </div>
            <div className={classes.BrandTopHeaderNavFilterItemRight}>
              <Link href="/">Smart Saatlar</Link>
              <Link href="/">100 məhsul</Link>
            </div>
          </div>

          <div className={classes.BrandTopHeaderNavFilterItem}>
            <div className={classes.BrandTopHeaderNavFilterItemLeft}>
              <span>
                <MoneyCaseLogo className={classes.logo} />
              </span>
            </div>
            <div className={classes.BrandTopHeaderNavFilterItemRight}>
              <Link href="/">Aksesuarlar</Link>
              <Link href="/">100 məhsul</Link>
            </div>
          </div>

          <div className={classes.BrandTopHeaderNavFilterItem}>
            <div className={classes.BrandTopHeaderNavFilterItemLeft}>
              <span>
                <MenuLogo className={classes.logo} />
              </span>
            </div>
            <div className={classes.BrandTopHeaderNavFilterItemRight}>
              <Link href="/">Digər</Link>
              <Link href="/">100 məhsul</Link>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};

export default BrandTopHeader;
