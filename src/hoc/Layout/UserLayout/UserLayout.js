/* eslint-disable @next/next/no-img-element */
import FullHeader from "../../../components/FullHeader/FullHeader";
import FullFooter from "../../../components/FullFooter/FullFooter";
import ActiveLink from "../../../components/Link/Link";
import MobileBottomNavbar from "../../../components/MobileBottomNavbar/MobileBottomNavbar";
import Router from "next/router";

import {
  User,
  Bag,
  UserLatest,
  Location,
  UserCarts,
  UserBonuses,
  Login,
} from "../../../components/Icons";
import classes from "./UserLayout.module.scss";
import { connect } from "react-redux";
import { logout } from "../../../../redux/actions/main";
import FullBreadcrumb from "../../../components/FullBreadcrumb/FullBreadcrumb";

function UserLayout(props) {
  const { logout } = props;
  return (
    <div
    // data-theme={theme}
    >
      <div className={classes.Layout}>
        <header className={classes.Header}>
          <FullHeader />
          <FullBreadcrumb
            steps={[
              { title: "Ana səhifə", slug: "/" },
              {
                title: "Şəxsi kabinetim",
                slug: "/user-profile",
              },
              {
                title: "Sifarişlərim",
                slug: "/user-profile/orders",
              },
            ]}
          />
        </header>

        <section className={classes.Section}>
          <div className={classes.SectionContent}>
            <aside className={classes.SectionContentAside}>
              <div className={classes.SectionContentAsideTop}>
                {/* icon wrapper */}
                <div className={classes.SectionContentAsideTopIcon}>
                  <User />
                </div>

                {/* user name & mail */}
                <div className={classes.SectionContentAsideTopBody}>
                  <h2>Ulviyya Imanova</h2>
                  <span>ulviyaaimanova@gmail.com</span>
                </div>
              </div>
              <div className={classes.SectionContentAsideSeparator}></div>
              <div className={classes.SectionContentAsideBody}>
                <div className={classes.SectionContentAsideBodyItem}>
                  <ActiveLink
                    href="/user-profile/orders"
                    activeClassName={classes.activeLink}
                  >
                    <a>
                      <span>
                        <Bag />
                      </span>
                      <span>Sifarislerim</span>
                    </a>
                  </ActiveLink>
                </div>
                <div className={classes.SectionContentAsideBodyItem}>
                  <ActiveLink
                    href="/user-profile/latest"
                    activeClassName={classes.activeLink}
                  >
                    <a>
                      <span>
                        <UserLatest />
                      </span>
                      <span>Ən son baxdıqlarım</span>
                    </a>
                  </ActiveLink>
                </div>
                <div className={classes.SectionContentAsideBodyItem}>
                  <ActiveLink
                    href="/user-profile/location"
                    activeClassName={classes.activeLink}
                  >
                    <a>
                      <span>
                        <Location />
                      </span>
                      <span>Ünvanlarım</span>
                    </a>
                  </ActiveLink>
                </div>
                <div className={classes.SectionContentAsideBodyItem}>
                  <ActiveLink
                    href="/user-profile/carts"
                    activeClassName={classes.activeLink}
                  >
                    <a>
                      <span>
                        <UserCarts />
                      </span>
                      <span>Kartlarım</span>
                    </a>
                  </ActiveLink>
                </div>
                <div className={classes.SectionContentAsideBodyItem}>
                  <ActiveLink
                    href="/user-profile/bonuses"
                    activeClassName={classes.activeLink}
                  >
                    <a>
                      <span>
                        <UserBonuses />
                      </span>
                      <span>Bonuslarım</span>
                    </a>
                  </ActiveLink>
                </div>
                <div className={classes.SectionContentAsideBodyItem}>
                  <ActiveLink
                    href="/user-profile/personal-info"
                    activeClassName={classes.activeLink}
                  >
                    <a>
                      <span>
                        <User />
                      </span>
                      <span>Şəxsi məlumatlarım</span>
                    </a>
                  </ActiveLink>
                </div>
                <div className={classes.SectionContentAsideBodyItem}>
                  <ActiveLink
                    href="/user-profile/apply"
                    activeClassName={classes.activeLink}
                  >
                    <a>
                      <span>
                        <User />
                      </span>
                      <span>Müraciət et</span>
                    </a>
                  </ActiveLink>
                </div>
                <div className={classes.SectionContentAsideBodyItem}>
                  <button
                    onClick={() => {
                      logout();

                      Router.push("/");
                    }}
                  >
                    <span>
                      <Login />
                    </span>
                    <span>Çıxış</span>
                  </button>
                </div>
              </div>
            </aside>
            <main className={classes.SectionContentMain}>{props.children}</main>
          </div>
        </section>

        <footer className={classes.Footer}>
          <FullFooter />
        </footer>
        <MobileBottomNavbar />
      </div>
    </div>
  );
}

const mapStateToProps = (state) => {
  return { theme: state.layout.theme };
};

const mapDispatchToProps = {
  logout,
};

export default connect(mapStateToProps, mapDispatchToProps)(UserLayout);
