import React, { useState, useEffect, useRef } from "react";
import Header from "../../../components/Header/Header";
import FullHeader from "../../../components/FullHeader/FullHeader";
import Footer from "../../../components/Footer/Footer";
import FullFooter from "../../../components/FullFooter/FullFooter";
import Sidebar from "../../../components/Sidebar/Sidebar";
import classes from "./HomeLayout.module.scss";
import { DoubleForwardVector } from "../../../components/Icons";
import { connect } from "react-redux";
import { toggleSidebar } from "../../../../redux/actions/main";

const HomeLayout = (props) => {
  const { theme, sidebarIsOpen, toggleSidebar, categories } = props;

  // let themeRef = useRef("");

  // useEffect(() => {
  //   themeRef.current.setAttribute("data-theme", theme);
  // }, [theme]);

  return (
    <div
    // ref={themeRef} data-theme={theme}
    >
      <div className={classes.HomeLayout}>
        <aside className={classes.HomeAside}>
          <Sidebar categories={categories} />
        </aside>

        <section className={classes.Container}>
          {/* {!sidebarIsOpen && (
            <button className={classes.SidebarToggle} onClick={toggleSidebar}>
              <DoubleForwardVector className={classes.SidebarToggleBtn} />
            </button>
          )} */}

          <header className={classes.Header}>
            <div className={classes.HeaderDesktop}>
              <Header />
            </div>
            <div className={classes.HeaderFull}>
              <FullHeader />
            </div>
          </header>

          <main>{props.children}</main>

          <footer className={classes.Footer}>
            <div className={classes.FooterDesktop}>
              <Footer />
            </div>
            <div className={classes.FooterFull}>
              <FullFooter />
            </div>
          </footer>
        </section>
      </div>
    </div>
  );
};

const mapStateToProps = (state) => {
  return {
    theme: state.layout.theme,
    sidebarIsOpen: state.layout.sidebar.sidebarIsOpen,
  };
};

const mapDispatchToProps = { toggleSidebar };

export default connect(mapStateToProps, mapDispatchToProps)(HomeLayout);
