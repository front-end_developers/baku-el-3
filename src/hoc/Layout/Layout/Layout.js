/* eslint-disable @next/next/no-img-element */
// import Header from "../../../components/Header/Header";
import FullHeader from "../../../components/FullHeader/FullHeader";
import FullFooter from "../../../components/FullFooter/FullFooter";
import ActiveLink from "../../../components/Link/Link";
import FullBreadcrumb from "../../../components/FullBreadcrumb/FullBreadcrumb";

import classes from "./Layout.module.scss";
import { connect } from "react-redux";
import { useRef, useEffect } from "react";

const Layout = (props) => {
  let themeRef = useRef("");

  // const { theme } = props;
  // useEffect(() => {
  //   "ref", themeRef.current.setAttribute("data-theme", theme);
  // }, [theme]);

  return (
    <div
    // ref={themeRef} data-theme={theme}
    >
      <div className={classes.Layout}>
        <header className={classes.Header}>
          <FullHeader />
          <FullBreadcrumb
            steps={[
              { title: "Ana səhifə", slug: "/" },
              {
                title: "İnteraktiv Link",
                slug: "/",
              },
              {
                title: "İnteraktiv Link 2",
                slug: "/",
              },
            ]}
          />
        </header>

        <main>{props.children}</main>

        <footer className={classes.Footer}>
          <FullFooter />
        </footer>
      </div>
    </div>
  );
};

const mapStateToProps = (state) => {
  return { theme: state.layout.theme };
};

// const mapDispatchToProps = {};

export default connect(mapStateToProps, null)(Layout);
