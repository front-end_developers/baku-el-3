/* eslint-disable @next/next/no-img-element */
// import Header from "../../../components/Header/Header";
import FullHeader from "../../../components/FullHeader/FullHeader";
import FullFooter from "../../../components/FullFooter/FullFooter";
import ActiveLink from "../../../components/Link/Link";

import classes from "./AuthLayout.module.scss";
import { connect } from "react-redux";
import MobileBottomNavbar from "../../../components/MobileBottomNavbar/MobileBottomNavbar";

const AuthLayout = (props) => {
  // const { theme } = props;
  return (
    <div
    // data-theme={theme}
    >
      <div className={classes.AuthLayout}>
        <header className={classes.Header}>
          <FullHeader />
        </header>

        <main>{props.children}</main>

        <footer className={classes.Footer}>
          <FullFooter />
        </footer>
      </div>
      <MobileBottomNavbar />
    </div>
  );
};

const mapStateToProps = (state) => {
  return { theme: state.layout.theme };
};

// const mapDispatchToProps = {};

export default connect(mapStateToProps, null)(AuthLayout);
