import Header from "../../../components/Header/Header";
import FullHeader from "../../../components/FullHeader/FullHeader";
import Breadcrumb from "../../../components/Breadcrumb/Breadcrumb";
import Footer from "../../../components/Footer/Footer";
import FullFooter from "../../../components/FullFooter/FullFooter";
import ProductsSidebar from "../../../components/ProductsSidebar/ProductsSidebar";
import classes from "./ProductLayout.module.scss";
import Router from "next/router";

export default function ProductLayout(props) {
  // const { asPath } = Router;
  // console.log("asPath", asPath);
  return (
    <div className={classes.ProductLayout}>
      <aside className={classes.ProductAside}>
        <ProductsSidebar />
      </aside>

      <section className={classes.Container}>
        {/* <Navbar */}

        <header className={classes.Header}>
          <div className={classes.HeaderDesktop}>
            <Header />
            <Breadcrumb
              steps={[
                { title: "Ana səhifə", slug: "/" },
                {
                  title: "Telefonlar və qadcetlər",
                  slug: "/products/telefonlar-qadcetlər",
                },
              ]}
            />
          </div>
          <div className={classes.HeaderFull}>
            <FullHeader />
          </div>
        </header>

        {/* <Breadcrumb steps={["Telefonlar və qadcetlər", "Smartfonlar"]} /> */}

        <main>{props.children}</main>

        <footer className={classes.Footer}>
          <div className={classes.FooterDesktop}>
            <Footer />
          </div>
          <div className={classes.FooterFull}>
            <FullFooter />
          </div>
        </footer>
      </section>
    </div>
  );
}
