import { useEffect, useRef } from "react";
import { useSelector } from "react-redux";

const Auxilary = (props) => {
  let theme = useSelector((state) => state.layout.theme);
  let themeRef = useRef("");
  useEffect(() => {
    themeRef.current.setAttribute("data-theme", theme);
  }, [theme]);
  return (
    <div ref={themeRef} data-theme={theme}>
      {props.children}
    </div>
    // <div data-theme={theme}>{props.children}</div>
  );
};

export default Auxilary;
